﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CallDispositionTool.Model
{
    [MetadataType(typeof(DataTypeMetadata))]
    [ScaffoldTable(false)]
    public partial class DataType
    {
        public class DataTypeMetadata
        {

        }
    }
}
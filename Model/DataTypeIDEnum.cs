﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DataTypeIDEnum
/// </summary>
/// 

namespace CallDispositionTool.Model
{
    public enum DataTypeIDEnum
    {
        String = 1,
        Boolean = 2,
        Numeric = 3,
        Date = 4,
        DropdownList = 5,
        Listbox = 6,
        Disposition = 7,
        DropdownTreeView = 8,
        Contact = 9,
        TimerAuto = 10,
        TimerManual = 11,
        SmartPad = 12,
        CustomerSatisfactionRadioButton = 13,
        UserCaptionUserControl = 14,
        AssignToUserControl = 15
    }
}

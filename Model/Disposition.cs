﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CallDispositionTool.Model
{
    [MetadataType(typeof(DispositionMetadata))]
    [ScaffoldTable(false)]
    public partial class Disposition
    {
        public class DispositionMetadata
        {

        }
    }
}
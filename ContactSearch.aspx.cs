﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using CallDispositionTool.UserControl;
using Telerik.Web.UI;
using System.Web.Security;
using System.Web.Configuration;
using Microsoft.AspNet.SignalR;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Web.Services;
using System.Text.RegularExpressions;


namespace CallDispositionTool
{
    public partial class ContactSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                grdContactSearch.Rebind();
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            HttpCookie ck = Request.Cookies["UserInfo"];
            string skinID = string.Empty;

            if (ck != null)
            {
                skinID = ck.Values["Skin"];
                this.skinManager1.Skin = skinID;
            }
            else
            {
                ck = new HttpCookie("UserInfo");
                ck.Values["Skin"] = this.skinManager1.Skin;
                Response.Cookies.Remove("UserInfo");
                Response.Cookies.Add(ck);
            }
        }


        bool isGrouping = false;

        public bool ShouldApplySortFilterOrGroup()
        {
            return grdContactSearch.MasterTableView.FilterExpression != "" ||
                (grdContactSearch.MasterTableView.GroupByExpressions.Count > 0 || isGrouping) ||
                grdContactSearch.MasterTableView.SortExpressions.Count > 0;
        }

        protected void grdContactSearch_PreRender(object sender, EventArgs e)
        {
            try
            {
                grdContactSearch.MasterTableView.Items[0].Focus();
            }
            catch (Exception)
            {
            }
        }

        private static Regex digitsOnly = new Regex(@"[^\d]");

        static string CleanPhone(string phone)
        {
            return digitsOnly.Replace(phone, "");
        }

        protected void grdContactSearch_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                string filterString = Request["SearchText"];
                int iContactCount;
                int startRowIndex;
                int maximumRows;

                startRowIndex = (ShouldApplySortFilterOrGroup()) ? 0 : grdContactSearch.CurrentPageIndex * grdContactSearch.PageSize;
                grdContactSearch.AllowCustomPaging = !ShouldApplySortFilterOrGroup();
                filterString = CleanPhone(filterString);

                if (radFilterOption.SelectedIndex == 0)
                {
                    iContactCount = (from c in db.Contacts
                                     let tempPhone = c.Phone.Replace(" ", string.Empty).Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Replace(".", string.Empty)
                                     where tempPhone.StartsWith(filterString)
                                     select c).Count();
                    maximumRows = (ShouldApplySortFilterOrGroup()) ? iContactCount : grdContactSearch.PageSize;

                    grdContactSearch.DataSource = (from c in db.Contacts
                                                   let tempPhone = c.Phone.Replace(" ", string.Empty).Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Replace(".", string.Empty)
                                                   where tempPhone.StartsWith(filterString)
                                                   select new
                                                   {
                                                       c.ContactID,
                                                       c.FirstName,
                                                       c.LastName,
                                                       c.Phone
                                                   }).OrderBy(contact => contact.ContactID).Skip(startRowIndex).Take(maximumRows).ToList();
                }
                else
                {
                    iContactCount = (from c in db.Contacts
                                     let tempPhone = c.Phone.Replace(" ", string.Empty).Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Replace(".", string.Empty)
                                     where tempPhone.Contains(filterString)
                                     select c).Count();
                    maximumRows = (ShouldApplySortFilterOrGroup()) ? iContactCount : grdContactSearch.PageSize;

                    grdContactSearch.DataSource = (from c in db.Contacts
                                                   let tempPhone = c.Phone.Replace(" ", string.Empty).Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Replace(".", string.Empty)
                                                   where tempPhone.Contains(filterString)
                                                   select new
                                                   {
                                                       c.ContactID,
                                                       c.FirstName,
                                                       c.LastName,
                                                       c.Phone
                                                   }).OrderBy(contact => contact.ContactID).Skip(startRowIndex).Take(maximumRows).ToList();
                }
            }
        }

        protected void radFilterOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdContactSearch.Rebind();
        }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.UI.WebControls;
using System.Linq;
using CallDispositionTool.Model;
using System.Web.Security;
using System.Web.UI;
using System.Collections.Generic;
using Telerik.Web.UI;
//using Microsoft.AspNet.SignalR;
//using Microsoft.AspNet.SignalR.Client.Hubs;
using System.Web.Configuration;

namespace CallDispositionTool
{
    public partial class CallBack : NotificationPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["Assigned"] != null)
            {
                TabStrip1.SelectedIndex = 1;
                MultiPage1.SelectedIndex = 1;
            }
        }

        protected void TabStrip1_TabClick(object sender, RadTabStripEventArgs e)
        {
            if (TabStrip1.SelectedIndex == 0)
            {
                CallBackManagerUserControl1.Rebind();
            }
            else
            {
                CallBackAssignedUserControl1.Rebind();
            }
        }
    }
}
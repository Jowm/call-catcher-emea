﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CallDispositionTool.Model;
using CallDispositionTool.Security;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Elmah;

namespace CallDispositionTool
{
    public class Helper
    {
        /// <summary>
        /// Log error to Elmah
        /// </summary>
        public static void LogError(Exception ex, string contextualMessage = null)
        {
            try
            {
                // log error to Elmah
                if (contextualMessage != null)
                {
                    // log exception with contextual information that's visible when 
                    // clicking on the error in the Elmah log
                    var annotatedException = new Exception(contextualMessage, ex);
                    ErrorSignal.FromCurrentContext().Raise(annotatedException, HttpContext.Current);
                }
                else
                {
                    ErrorSignal.FromCurrentContext().Raise(ex, HttpContext.Current);
                }

                // send errors to ErrorWS (my own legacy service)
                // using (ErrorWSSoapClient client = new ErrorWSSoapClient())
                // {
                //    client.LogErrors(...);
                // }
            }
            catch (Exception)
            {
                // uh oh! just keep going
            }
        }

        public static void ClearCache()
        {
            try
            {
                CacheFactory.GetCacheManager().Flush();
            }
            catch (Exception)
            {
            }
        }

        public static int GetOutstandingCallBack(int UserID)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                return (from p in db.CallBacks
                        where (p.CreatedBy == UserID || p.AssignedTo == UserID) && (p.CallBackStatu.CallBackStatus == "Outstanding" || !p.CallBackStatusID.HasValue)
                        select new { p.CallBackID }).Count();
            }
        }

        public static bool HasAudienceManagerFeature(int FormID)
        {
            bool retValue = false;

            if (CacheFactory.GetCacheManager().Contains("HasAudienceManagerFeature~" + FormID.ToString()))
            {
                retValue = (bool)CacheFactory.GetCacheManager().GetData("HasAudienceManagerFeature~" + FormID);
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    retValue = (from v in db.FormProperties
                                where v.FormID == FormID && v.PropertyName == "AudienceManager"
                                select v).Any();

                    CacheFactory.GetCacheManager().Add("HasAudienceManagerFeature~" + FormID, retValue);
                }
            }

            return retValue;
        }

        public static bool HasTransferCallFeature(int FormID)
        {
            bool retValue = false;

            if (CacheFactory.GetCacheManager().Contains("HasTransferCallFeature~" + FormID.ToString()))
            {
                retValue = (bool)CacheFactory.GetCacheManager().GetData("HasTransferCallFeature~" + FormID);
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    retValue = (from v in db.FormProperties
                                where v.FormID == FormID && v.PropertyName == "TransferCallFeature"
                                select v).Any();

                    CacheFactory.GetCacheManager().Add("HasTransferCallFeature~" + FormID, retValue);
                }
            }

            return retValue;
        }

        public static bool HasCallBackFeature(int FormID)
        {
            bool retValue = false;

            if (CacheFactory.GetCacheManager().Contains("HasCallBackFeature~" + FormID.ToString()))
            {
                retValue = (bool)CacheFactory.GetCacheManager().GetData("HasCallBackFeature~" + FormID);
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    retValue = (from v in db.FormProperties
                                where v.FormID == FormID && v.PropertyName == "CallBackFeature"
                                select v).Any();

                    CacheFactory.GetCacheManager().Add("HasCallBackFeature~" + FormID, retValue);
                }
            }

            return retValue;
        }

        public static bool HasContactFeature(int FormID)
        {
            bool hasValue = false;

            if (CacheFactory.GetCacheManager().Contains("HasContactFeature~" + FormID.ToString()))
            {
                hasValue = (bool)CacheFactory.GetCacheManager().GetData("HasContactFeature~" + FormID);
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    hasValue = (from v in db.FormProperties
                                where v.FormID == FormID && v.PropertyName == "ContactFeature"
                                select v).Any();

                    CacheFactory.GetCacheManager().Add("HasContactFeature~" + FormID, hasValue);
                }
            }

            return hasValue;
        }

        public static bool HasLinkToLiberator(int FormID)
        {
            bool hasValue = false;

            if (CacheFactory.GetCacheManager().Contains("HasLinkToLiberator~" + FormID.ToString()))
            {
                hasValue = (bool)CacheFactory.GetCacheManager().GetData("HasLinkToLiberator~" + FormID);
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    hasValue = (from v in db.FormProperties
                                where v.FormID == FormID && v.PropertyName == "LinkToLiberator"
                                select v).Any();

                    CacheFactory.GetCacheManager().Add("HasLinkToLiberator~" + FormID, hasValue);
                }
            }

            return hasValue;
        }


        public static bool CheckFormAccess(int FormID, int UserID)
        {
            bool retValue = false;

            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            if (_objCacheManager.Contains("CheckFormAccess~" + FormID.ToString() + "~" + UserID.ToString()))
            {
                retValue = (bool)_objCacheManager.GetData("CheckFormAccess~" + FormID.ToString() + "~" + UserID.ToString());
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    retValue = (from p in db.UserForms
                                where p.FormID == FormID && p.UserID == UserID
                                select p).Any();

                    if (retValue)
                    {
                        _objCacheManager.Add("CheckFormAccess~" + FormID.ToString() + "~" + UserID.ToString(), retValue);
                    }
                }
            }

            return retValue;
        }

        public static string GetAvayaLoginID(string EmployeeID)
        {
            string avayaLoginID = string.Empty;

            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            if (_objCacheManager.Contains("GetAvayaLoginID~" + EmployeeID))
            {
                avayaLoginID = _objCacheManager.GetData("GetAvayaLoginID~" + EmployeeID) as string;
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    var emp = (from e in db.Users
                               where e.EmployeeID == EmployeeID
                               select new { e.AvayaLogin }).FirstOrDefault();

                    if (emp != null)
                    {
                        avayaLoginID = emp.AvayaLogin;
                        _objCacheManager.Add("GetAvayaLoginID~" + EmployeeID, avayaLoginID);
                    }
                }
            }

            return avayaLoginID;
        }

        public static string GetEmployeeName(string EmployeeID)
        {
            string empName = string.Empty;

            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            if (_objCacheManager.Contains("GetEmployeeName~" + EmployeeID))
            {
                empName = _objCacheManager.GetData("GetEmployeeName~" + EmployeeID) as string;
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    var emp = (from e in db.Users
                               where e.EmployeeID == EmployeeID
                               select new { e.FirstName, e.LastName }).FirstOrDefault();

                    if (emp != null)
                    {
                        empName = emp.FirstName + " " + emp.LastName;
                        _objCacheManager.Add("GetEmployeeName~" + EmployeeID, empName);
                    }
                }
            }

            return empName;
        }

        public static string GetEmployeeIDByUserID(int UserID)
        {
            string EmployeeID = string.Empty;

            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            if (_objCacheManager.Contains("GetEmployeeIDByUserID~" + UserID.ToString()))
            {
                EmployeeID = (string)_objCacheManager.GetData("GetEmployeeIDByUserID~" + EmployeeID);
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    var emp = (from e in db.Users
                               where e.UserID == UserID
                               select new { e.EmployeeID }).FirstOrDefault();

                    EmployeeID = emp.EmployeeID;

                    _objCacheManager.Add("GetEmployeeIDByUserID~" + EmployeeID, EmployeeID);

                }
            }

            return EmployeeID;
        }

        public static void ResetPassword(int UserID, string NewPassword)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var emp = (from e in db.Users
                           where e.UserID == UserID
                           select e).FirstOrDefault();

                emp.PasswordHash = PasswordHash.CreateHash("Changeme" + emp.EmployeeID);
                //force user on login to change their password
                emp.LastPasswordChangeDate = DateTime.Now.AddDays(-90);

                db.SaveChanges();
            }
        }

        public static dynamic GetContactSearch(string filterString, string searchBy)
        {
            if (searchBy == "Name")
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    return (from c in db.Contacts
                            let ContactName = c.FirstName + " " + c.LastName
                            where ContactName.StartsWith(filterString)
                            let Address1 = string.IsNullOrEmpty(c.Address1) ? string.Empty : c.Address1 + " "
                            let City = string.IsNullOrEmpty(c.City) ? string.Empty : c.City + " "
                            let StateRegion = string.IsNullOrEmpty(c.StateRegion) == true ? string.Empty : c.StateRegion + " "

                            select new
                            {
                                c.ContactID,
                                c.FirstName,
                                c.LastName,
                                c.Phone,
                                Address = Address1 + City + StateRegion,
                                c.StoreNumber,
                                c.NTLoginID,
                                c.TransactionID
                            }).ToList();

                }
            }
            else if (searchBy == "Name2")
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    return (from c in db.Contacts
                            let ContactName = c.FirstName + " " + c.LastName
                            where ContactName.Contains(filterString)
                            let Address1 = string.IsNullOrEmpty(c.Address1) ? string.Empty : c.Address1 + " "
                            let City = string.IsNullOrEmpty(c.City) ? string.Empty : c.City + " "
                            let StateRegion = string.IsNullOrEmpty(c.StateRegion) == true ? string.Empty : c.StateRegion + " "

                            select new
                            {
                                c.ContactID,
                                c.FirstName,
                                c.LastName,
                                c.Phone,
                                Address = Address1 + City + StateRegion,
                                c.StoreNumber,
                                c.NTLoginID,
                                c.TransactionID
                            }).ToList();

                }
            }
            else if (searchBy == "Phone")
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    filterString = filterString.Replace(" ", string.Empty).Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Replace(".", string.Empty).Replace("_", string.Empty);

                    return (from c in db.Contacts
                            let tempPhone = c.Phone.Replace(" ", string.Empty).Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Replace(".", string.Empty).Replace("_", string.Empty)
                            where tempPhone.StartsWith(filterString)
                            let Address1 = string.IsNullOrEmpty(c.Address1) ? string.Empty : c.Address1 + " "
                            let City = string.IsNullOrEmpty(c.City) ? string.Empty : c.City + " "
                            let StateRegion = string.IsNullOrEmpty(c.StateRegion) ? string.Empty : c.StateRegion + " "

                            select new
                            {
                                c.ContactID,
                                c.FirstName,
                                c.LastName,
                                c.Phone,
                                Address = Address1 + City + StateRegion,
                                c.StoreNumber,
                                c.NTLoginID,
                                c.TransactionID
                            }).ToList();
                }
            }
            else if (searchBy == "Phone2")
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    filterString = filterString.Replace(" ", string.Empty).Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Replace(".", string.Empty).Replace("_", string.Empty);

                    return (from c in db.Contacts
                            let tempPhone = c.Phone.Replace(" ", string.Empty).Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Replace(".", string.Empty).Replace("_", string.Empty)
                            where tempPhone.Contains(filterString)
                            let Address1 = string.IsNullOrEmpty(c.Address1) ? string.Empty : c.Address1 + " "
                            let City = string.IsNullOrEmpty(c.City) ? string.Empty : c.City + " "
                            let StateRegion = string.IsNullOrEmpty(c.StateRegion) ? string.Empty : c.StateRegion + " "

                            select new
                            {
                                c.ContactID,
                                c.FirstName,
                                c.LastName,
                                c.Phone,
                                Address = Address1 + City + StateRegion,
                                c.StoreNumber,
                                c.NTLoginID,
                                c.TransactionID
                            }).ToList();
                }
            }

            return null;
        }

        public static dynamic GetZipCodeSearch(string filterString)
        {
            dynamic zipcodes;

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                zipcodes = (from z in db.ZipCodes
                            where z.ZipCode1.Contains(filterString)
                            let ZipCode = z.ZipCode1
                            select new { z.ZipCodeID, ZipCode }).ToList();
            }

            return zipcodes;
        }

        public static bool IsSignOffAllowed(int UserID, int FormID)
        {
            bool returnValue = false;

            if (CheckFormAccess(FormID, UserID))
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    returnValue = (from r in db.UserRoles
                                   where r.UserID == UserID && r.Role.RoleName == "AllowSignOff"
                                   select r).Any();
                }
            }

            return returnValue;
        }

        public static bool IsSignOffAllAllowed(int UserID, int FormID) //Gian 07/12/2014
        {
            bool returnValue = false;

            if (CheckFormAccess(FormID, UserID))
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    returnValue = (from r in db.UserRoles
                                   where r.UserID == UserID && r.Role.RoleName == "AllowSignOffAll"
                                   select r).Any();
                }
            }

            return returnValue;
        }

        public static bool IsUserAdmin(int UserID)
        {
            bool returnValue = false;

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                returnValue = (from r in db.UserRoles
                               where r.UserID == UserID && r.Role.RoleName == "AdminRole"
                               select r).Any();
            }

            return returnValue;
        }

        public static bool IsUserAllowFormAccessGrantor(int UserID)
        {
            bool returnValue = false;

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                returnValue = (from r in db.UserRoles
                               where r.UserID == UserID && r.Role.RoleName == "FormAccessGrantor"
                               select r).Any();
            }

            return returnValue;
        }

        public static bool IsUserAllowEditTransaction(int UserID)
        {
            bool returnValue = false;

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                returnValue = (from r in db.UserRoles
                               where r.UserID == UserID && r.Role.RoleName == "AllowEditTransaction"
                               select r).Any();
            }

            return returnValue;
        }

        public static bool IsUserAllowAssignUserRole(int UserID)
        {
            bool returnValue = false;


            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                returnValue = (from r in db.UserRoles
                               where r.UserID == UserID && r.Role.RoleName == "AssignUserRole"
                               select r).Any();
            }

            return returnValue;
        }

        public static dynamic GetFormsByUser(string EmployeeID)
        {
            dynamic forms;

            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            if (_objCacheManager.Contains("GetFormsByUser~" + EmployeeID))
            {
                forms = _objCacheManager.GetData("GetFormsByUser~" + EmployeeID);
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    forms = (from f in db.UserForms
                             where f.User.EmployeeID == EmployeeID && f.Form.HideFromList == false
                             select new { f.Form.FormID, f.Form.FormName, f.Form.FormDescription }).Distinct().OrderBy(f => f.FormName).ToList();

                    if (forms != null)
                    {
                        _objCacheManager.Add("GetFormsByUser~" + EmployeeID, forms);
                    }
                }
            }

            return forms;
        }

        public static string GetComcastXMLRequestHeader(string PhoneNumber)
        {
            string strXML = string.Empty;

            strXML = "<COMCASTREQUEST>";
            strXML += "<HEADER>";
            strXML += "<Name>COMCAST_getTrasfer</Name>";
            strXML += "<RequestStatus></RequestStatus>";
            strXML += "</HEADER>";
            strXML += "<BODY><getTrasfer><getTransferRequest>";
            strXML += "<ANI>" + PhoneNumber + "</ANI>";
            strXML += "<ACCOUNT/>";
            strXML += "<CALLERINTENT/>";
            strXML += "<DESCRIPTION/>";
            strXML += "<LANGUAGEPREFERENCE/>";
            strXML += "<MARKET/>";
            strXML += "<UOID/>";
            strXML += "<PHONENUMBER/>";
            strXML += "</getTransferRequest></getTrasfer></BODY>";
            strXML += "</COMCASTREQUEST>";

            return strXML;

        }
    }
}
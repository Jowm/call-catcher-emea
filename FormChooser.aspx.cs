﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.UI.WebControls;
using System.Linq;
using CallDispositionTool.Model;
using System.Web.Security;
using System.Web.UI;
using System.Collections.Generic;
using Telerik.Web.UI;
using System.Web.Configuration;
using CallDispositionTool.CTI;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Web;

namespace CallDispositionTool
{
    public partial class FormChooser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindForms();
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            HttpCookie ck = Request.Cookies["UserInfo"];
            string skinID = string.Empty;

            if (ck != null)
            {
                skinID = ck.Values["Skin"];
                this.skinManager1.Skin = skinID;
            }
            else
            {
                ck = new HttpCookie("UserInfo");
                ck.Values["Skin"] = this.skinManager1.Skin;
                Response.Cookies.Remove("UserInfo");
                Response.Cookies.Add(ck);
            }
        }

        void BindForms()
        {
            cboForms.DataSource = Helper.GetFormsByUser(Membership.GetUser().UserName);
            cboForms.DataBind();
        }

        protected void btnLaunchCTIPad_Click(object sender, EventArgs e)
        {
            // ADD CODE!!!   
            AuthorizationClient auth = new AuthorizationClient();
            string employeeID = this.User.Identity.Name;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("username", employeeID);
            parameters.Add("role", "agent");
            parameters.Add("agentid", Helper.GetAvayaLoginID(employeeID)); // Use Avaya Login ID for logged in agent (mandatory)
            parameters.Add("mgr_username", "mgr.of.logged.in.agent");  // Use supervisor's username  (optional)

            string url = this.Request.Url.ToString();
            int idx = url.IndexOf("/FormChooser.aspx");
            url = url.Substring(0, idx) + String.Format("/Form.aspx");
            //url += String.Format("?FormID=={0}", cboForms.SelectedItem.Value);
            parameters.Add("call_url", url);

            CacheFactory.GetCacheManager().Add("SelectedFormIDbyEmployeeID~" + employeeID, cboForms.SelectedItem.Value);
            Session["FormID"] = cboForms.SelectedItem.Value;
            //HttpCookie ck = new HttpCookie("FormChooser");
            //ck.Values["FormID"] = cboForms.SelectedItem.Value;
            //ck.Expires = DateTime.Now.AddYears(1);
            //Request.Cookies.Remove("FormChooser");
            //Request.Cookies.Add(ck);

            // Service method
            try
            {
                AccessUrl accessUrl = auth.GetAccessUrl(
                    WebConfigurationManager.AppSettings["CTIPadID"],
                    WebConfigurationManager.AppSettings["CTIPadSecret"],
                    WebConfigurationManager.AppSettings["CTIPadScope"],
                    WebConfigurationManager.AppSettings["CTIPadUrl"],
                    parameters);

                string js = string.Empty;

                js += "<script type = 'text/javascript'>";
                js += "window.open('";
                js += accessUrl.Url;
                js += "'); Close();";
                js += "</script>";

                ClientScript.RegisterStartupScript(this.GetType(), "script", js);

                //Response.Redirect(accessUrl.Url);
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                throw;
            }
        }
    }
}
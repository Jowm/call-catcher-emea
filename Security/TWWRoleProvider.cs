﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using CallDispositionTool.Model;

namespace TWW.Security
{
    public class TWWRoleProvider : RoleProvider
    {
        #region privatevariables

        private string _ApplicationName;
        private string _RemoteProviderName;

        #endregion

        public TWWRoleProvider()
        {
        }

        #region properties

        /// <summary>
        /// required implementation
        /// </summary>
        public override string ApplicationName
        {
            get
            {
                return _ApplicationName;
            }
            set
            {
                _ApplicationName = value;
            }
        }

        #endregion


        #region overridable fuctions

        /// <summary>
        /// handle the Initiate override and extract our parameters
        /// </summary>
        /// <param name="name">name of the provider</param>
        /// <param name="config">configuration collection</param>
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            _ApplicationName = config["applicationName"];
            if (string.IsNullOrEmpty(_ApplicationName))
            {
                _ApplicationName = ProviderUtility.GetDefaultAppName();
            }

            _RemoteProviderName = config["remoteProviderName"];

            base.Initialize(name, config);
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="usernames">a list of usernames</param>
        /// <param name="roleNames">a list of roles</param>
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {

        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="roleName">a role name</param>
        public override void CreateRole(string roleName)
        {

        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="roleName">a role</param>
        /// <param name="throwOnPopulatedRole">get upset of users are in a role</param>
        /// <returns></returns>
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            return false;
        }

        /// <summary>
        /// required implemention
        /// </summary>
        /// <param name="roleName">a role</param>
        /// <param name="usernameToMatch">a username to look for in the role</param>
        /// <returns></returns>
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            string sUserList = string.Empty;

            using (CallDispositionEntities dbContext = new CallDispositionEntities())
            {
                var query = from u in dbContext.UserRoles
                            where u.Role.RoleName == roleName && u.User.EmployeeID == usernameToMatch
                            select u;

                foreach (UserRole item in query)
                {
                    sUserList += item.User.EmployeeID + ",";                                                            
                }
            }

            if (sUserList.Length > 0)
                sUserList.Remove(sUserList.Length - 1, 1);

            return sUserList.Split(',');
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <returns></returns>
        public override string[] GetAllRoles()
        {
            string sRoleList = string.Empty;

            using (CallDispositionEntities dbContext = new CallDispositionEntities())
            {
                var query = from p in dbContext.Roles
                            select p;

                foreach (Role item in query)
                {
                    sRoleList += item.RoleName + ",";
                }
            }

            if (sRoleList.Length > 0)
                sRoleList.Remove(sRoleList.Length - 1, 1);

            return sRoleList.Split(',');
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">a username</param>
        /// <returns>a list of roles</returns>
        public override string[] GetRolesForUser(string username)
        {
            string tmpRoleNames = "";

            using (CallDispositionEntities dbContext = new CallDispositionEntities())
            {
                var query = from r in dbContext.UserRoles
                            where r.User.EmployeeID == username
                            select r;

                foreach (UserRole item in query)
                {
                    tmpRoleNames += item.Role.RoleName + ",";
                }
            }

            return tmpRoleNames.Split(',');
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="roleName">a role</param>
        /// <returns>a list of users</returns>
        public override string[] GetUsersInRole(string roleName)
        {
            string tmpUsers = "";

            using (CallDispositionEntities dbContext = new CallDispositionEntities())
            {
                var query = from r in dbContext.UserRoles
                            where r.Role.RoleName == roleName
                            select r;

                foreach (UserRole item in query)
                {
                    tmpUsers += item.User.EmployeeID + ",";
                }
            }

            return tmpUsers.Split(',');
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">a username</param>
        /// <param name="roleName">a role</param>
        /// <returns>true or false</returns>
        public override bool IsUserInRole(string username, string roleName)
        {
            bool retVal = false;

            using (CallDispositionEntities dbContext = new CallDispositionEntities())
            {
                int count = (from r in dbContext.UserRoles
                             where r.Role.RoleName == roleName && r.User.EmployeeID == username
                             select r).Count();

                if (count > 0)
                {
                    retVal = true;
                }
            }

            return retVal;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="usernames">a list of usernames</param>
        /// <param name="roleNames">a list of roles</param>
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {

        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="roleName">a role</param>
        /// <returns>true or false</returns>
        public override bool RoleExists(string roleName)
        {
            bool retVal = false;

            using (CallDispositionEntities dbContext = new CallDispositionEntities())
            {
                var count = (from r in dbContext.Roles
                             where r.RoleName == roleName
                             select r).Count();

                if (count > 0)
                {
                    retVal = true;
                }
            }

            return retVal;
        }

        #endregion

    }
}
﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.Collections.Specialized;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Web.Configuration;
using CallDispositionTool.Model;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace TWW.Security
{
    public class TWWMembershipProvider : MembershipProvider
    {

        #region privatevariables

        private bool _EnablePasswordReset;
        private bool _EnablePasswordRetrieval;
        private int _MaxInvalidPasswordAttempts;
        private int _MinRequiredNonalphanumericCharacters;
        private int _MinRequiredPasswordLength;
        private int _PasswordAttemptWindow;
        private MembershipPasswordFormat _PasswordFormat;
        private string _PasswordStrengthRegularExpression;
        private bool _RequiresQuestionAndAnswer;
        private bool _RequiresUniqueEmail;

        private string _ApplicationName;
        private string _RemoteProviderName;


        #endregion

        #region properties

        /// <summary>
        /// required implementation
        /// </summary>
        public override string ApplicationName
        {
            get
            {
                return _ApplicationName;
            }
            set
            {
                _ApplicationName = value;
            }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override bool EnablePasswordReset
        {
            get { return _EnablePasswordReset; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override bool EnablePasswordRetrieval
        {
            get { return _EnablePasswordRetrieval; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override int MaxInvalidPasswordAttempts
        {
            get { return _MaxInvalidPasswordAttempts; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return _MinRequiredNonalphanumericCharacters; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override int MinRequiredPasswordLength
        {
            get { return _MinRequiredPasswordLength; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override int PasswordAttemptWindow
        {
            get { return _PasswordAttemptWindow; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override MembershipPasswordFormat PasswordFormat
        {
            get { return _PasswordFormat; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override string PasswordStrengthRegularExpression
        {
            get { return _PasswordStrengthRegularExpression; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override bool RequiresQuestionAndAnswer
        {
            get { return _RequiresQuestionAndAnswer; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override bool RequiresUniqueEmail
        {
            get { return _RequiresUniqueEmail; }
        }

        #endregion

        public TWWMembershipProvider()
        {
        }

        #region function overrides

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            _ApplicationName = config["applicationName"];
            if (string.IsNullOrEmpty(_ApplicationName))
            {
                _ApplicationName = ProviderUtility.GetDefaultAppName();
            }

            _EnablePasswordRetrieval = ProviderUtility.GetBooleanValue(config, "enablePasswordRetrieval", false);
            _EnablePasswordReset = ProviderUtility.GetBooleanValue(config, "enablePasswordReset", true);
            _RequiresQuestionAndAnswer = ProviderUtility.GetBooleanValue(config, "requiresQuestionAndAnswer", true);
            _RequiresUniqueEmail = ProviderUtility.GetBooleanValue(config, "requiresUniqueEmail", true);
            _MaxInvalidPasswordAttempts = ProviderUtility.GetIntValue(config, "maxInvalidPasswordAttempts", 5, false, 0);
            _PasswordAttemptWindow = ProviderUtility.GetIntValue(config, "passwordAttemptWindow", 10, false, 0);
            _MinRequiredPasswordLength = ProviderUtility.GetIntValue(config, "minRequiredPasswordLength", 7, false, 0x80);
            _MinRequiredNonalphanumericCharacters = ProviderUtility.GetIntValue(config, "minRequiredNonalphanumericCharacters", 1, true, 0x80);
            _PasswordStrengthRegularExpression = config["passwordStrengthRegularExpression"];

            if (config["passwordFormat"] != null)
            {
                _PasswordFormat = (MembershipPasswordFormat)Enum.Parse(typeof(MembershipPasswordFormat), config["passwordFormat"]);
            }
            else
            {
                _PasswordFormat = MembershipPasswordFormat.Hashed;
            }

            _RemoteProviderName = config["remoteProviderName"];

            base.Initialize(name, config);
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">a username</param>
        /// <param name="oldPassword">original password</param>
        /// <param name="newPassword">new password</param>
        /// <returns>true or false</returns>
        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            bool returnValue = false;

            try
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    var user = (from u in db.Users
                                where u.EmployeeID == username
                                select u).FirstOrDefault();

                    if (user != null)
                    {
                        if (CallDispositionTool.Security.PasswordHash.ValidatePassword(oldPassword, user.PasswordHash) && CheckPasswordComplexity(newPassword))
                        {
                            user.LastPasswordChangeDate = DateTime.Now;
                            user.PasswordHash = CallDispositionTool.Security.PasswordHash.CreateHash(newPassword);
                            db.SaveChanges();
                            returnValue = true;
                        }
                    }
                }

                return returnValue;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">a username</param>
        /// <param name="password">the password</param>
        /// <param name="newPasswordQuestion">new question</param>
        /// <param name="newPasswordAnswer">new answer</param>
        /// <returns>true or false</returns>
        public override bool ChangePasswordQuestionAndAnswer(string username, string password,
          string newPasswordQuestion, string newPasswordAnswer)
        {
            bool returnValue = false;
            try
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    var user = (from u in db.Users
                                where u.EmployeeID == username
                                select u).FirstOrDefault();

                    if (user != null)
                    {
                        if (CallDispositionTool.Security.PasswordHash.ValidatePassword(password, user.PasswordHash))
                        {
                            user.PasswordRecoveryQuestion1 = newPasswordQuestion;
                            user.PasswordRecoveryAnswer1 = newPasswordAnswer;
                            user.LastPasswordChangeDate = DateTime.Now;
                            db.SaveChanges();
                            returnValue = true;
                        }
                    }
                }

                return returnValue;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">required implementation</param>
        /// <param name="password">required implementation</param>
        /// <param name="email">required implementation</param>
        /// <param name="passwordQuestion">required implementation</param>
        /// <param name="passwordAnswer">required implementation</param>
        /// <param name="isApproved">required implementation</param>
        /// <param name="providerUserKey">required implementation</param>
        /// <param name="status">required implementation</param>
        /// <returns>a user object</returns>
        public override MembershipUser CreateUser(string username, string password, string email,
          string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey,
          out MembershipCreateStatus status)
        {
            //  passwordQuestion, passwordAnswer, isApproved, providerUserKey, out newStatus));
            status = (MembershipCreateStatus)Enum.Parse(typeof(MembershipCreateStatus), null);
            return null;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">required implementation</param>
        /// <param name="deleteAllRelatedData">required implementation</param>
        /// <returns>required implementation</returns>
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            int num = 0;

            using (CallDispositionEntities dbContext = new CallDispositionEntities())
            {
                var user = (from u in dbContext.Users
                            where u.EmployeeID == username
                            select u).FirstOrDefault();

                if (user != null)
                {
                    dbContext.Users.DeleteObject(user);
                    num = dbContext.SaveChanges();
                }
            }

            return num > 0 ? true : false;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="emailToMatch">required implementation</param>
        /// <param name="pageIndex">required implementation</param>
        /// <param name="pageSize">required implementation</param>
        /// <param name="totalRecords">required implementation</param>
        /// <returns>required implementation</returns>
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            return null;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="usernameToMatch">required implementation</param>
        /// <param name="pageIndex">required implementation</param>
        /// <param name="pageSize">required implementation</param>
        /// <param name="totalRecords">required implementation</param>
        /// <returns>required implementation</returns>
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex,
          int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            return null;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="pageIndex">required implementation</param>
        /// <param name="pageSize">required implementation</param>
        /// <param name="totalRecords">required implementation</param>
        /// <returns>required implementation</returns>
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize,
          out int totalRecords)
        {
            totalRecords = 0;

            return null;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <returns>required implementation</returns>
        public override int GetNumberOfUsersOnline()
        {
            return 0;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">required implementation</param>
        /// <param name="answer">required implementation</param>
        /// <returns>required implementation</returns>
        public override string GetPassword(string username, string answer)
        {
            return null;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">required implementation</param>
        /// <param name="userIsOnline">required implementation</param>
        /// <returns>required implementation</returns>
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            MembershipUser user = null;

            //Create Instance of CacheManager 
            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            // Check If Key is in Cache Collection
            if (_objCacheManager.Contains("GetUser_Username~" + username) && !userIsOnline)
            {
                // Retrieve Cached Data
                user = (MembershipUser)_objCacheManager.GetData("GetUser_Username~" + username);
            }
            else
            {
                //Add object to cache
                using (CallDispositionEntities dbContext = new CallDispositionEntities())
                {
                    var emp = (from u in dbContext.Users
                               where u.EmployeeID == username
                               select u).FirstOrDefault();

                    if (emp != null)
                    {
                        if (userIsOnline)
                        {
                            emp.LastActivityDate = DateTime.Now;
                            dbContext.SaveChanges();
                        }

                        user = new MembershipUser("TWWMembershipProvider",
                            emp.EmployeeID,
                            emp.UserID,
                            emp.EmailAddress,
                            emp.PasswordRecoveryQuestion1,
                            null,
                            true,
                            emp.Inactive.HasValue ? emp.Inactive.Value : false,
                            (emp.CreatedOn.HasValue == true ? emp.CreatedOn.Value : DateTime.MinValue),
                            (emp.LastLoginDate.HasValue == true ? emp.LastLoginDate.Value : DateTime.MinValue),
                            (emp.LastActivityDate.HasValue == true ? emp.LastActivityDate.Value : DateTime.MinValue),
                            (emp.LastPasswordChangeDate.HasValue == true ? emp.LastPasswordChangeDate.Value : DateTime.MinValue),
                            DateTime.MinValue);

                        _objCacheManager.Add("GetUser_Username~" + username, user);
                    }
                }
            }

            return user;

        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="providerUserKey">required implementation</param>
        /// <param name="userIsOnline">required implementation</param>
        /// <returns>required implementation</returns>
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {

            MembershipUser user = null;

            //Create Instance of CacheManager 
            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            // Check If Key is in Cache Collection
            if (_objCacheManager.Contains("GetUser_ProviderUserKey~" + Convert.ToString(providerUserKey)))
            {
                // Retrieve Cached Data
                user = (MembershipUser)_objCacheManager.GetData("GetUser_ProviderUserKey~" + Convert.ToString(providerUserKey));
            }
            else
            {
                using (CallDispositionEntities dbContext = new CallDispositionEntities())
                {
                    int UserID = Convert.ToInt32(providerUserKey);

                    var emp = (from p in dbContext.Users
                               where p.UserID == UserID
                               select p).FirstOrDefault();

                    if (emp != null)
                    {

                        if (userIsOnline)
                        {
                            emp.LastActivityDate = DateTime.Now;
                            dbContext.SaveChanges();
                        }

                        user = new MembershipUser("TWWMembershipProvider",
                            emp.EmployeeID,
                            emp.UserID,
                            emp.EmailAddress,
                            emp.PasswordRecoveryQuestion1,
                            null,
                            true,
                            emp.Inactive.HasValue ? emp.Inactive.Value : false,
                            (emp.CreatedOn.HasValue == true ? emp.CreatedOn.Value : DateTime.MinValue),
                            (emp.LastLoginDate.HasValue == true ? emp.LastLoginDate.Value : DateTime.MinValue),
                            (emp.LastActivityDate.HasValue == true ? emp.LastActivityDate.Value : DateTime.MinValue),
                            (emp.LastPasswordChangeDate.HasValue == true ? emp.LastPasswordChangeDate.Value : DateTime.MinValue),
                            DateTime.MinValue);

                        _objCacheManager.Add("GetUser_ProviderUserKey~" + Convert.ToString(providerUserKey), user);
                    }
                }
            }

            return user;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="email">required implementation</param>
        /// <returns>required implementation</returns>
        public override string GetUserNameByEmail(string email)
        {
            try
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    var user = (from u in db.Users
                                where u.EmailAddress == email
                                select new { u.EmployeeID }).FirstOrDefault();

                    if (user != null)
                    {
                        return user.EmployeeID;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">required implementation</param>
        /// <param name="answer">required implementation</param>
        /// <returns>required implementation</returns>
        public override string ResetPassword(string username, string answer)
        {
            try
            {
                string newPassword = null;

                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    var user = (from u in db.Users
                                where u.EmployeeID == username
                                select u).FirstOrDefault();

                    if (user != null)
                    {
                        if (answer == user.PasswordRecoveryAnswer1.ToLower())
                        {
                            newPassword = Membership.GeneratePassword(Membership.MinRequiredPasswordLength, Membership.MinRequiredPasswordLength);
                            user.PasswordHash = CallDispositionTool.Security.PasswordHash.CreateHash(newPassword);
                            user.Inactive = false;
                            //force user on login to change their password
                            user.LastPasswordChangeDate = DateTime.Now.AddDays(-90);
                            CacheFactory.GetCacheManager().Remove("GetUser_Username~" + username);
                            CacheFactory.GetCacheManager().Remove("GetUser_ProviderUserKey~" + user.UserID.ToString());

                            UserPasswordHistory record = new UserPasswordHistory();

                            record.UserID = user.UserID;
                            record.PasswordHash = CallDispositionTool.Security.PasswordHash.CreateHash(username);
                            record.CreatedOn = DateTime.Now;
                            db.UserPasswordHistories.AddObject(record);
                            db.SaveChanges();

                            return newPassword;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CallDispositionTool.Helper.LogError(ex);
                throw;
            }

            return null;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="userName">required implementation</param>
        /// <returns>required implementation</returns>
        public override bool UnlockUser(string userName)
        {
            bool retValue = false;

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var user = (from u in db.Users
                            where u.EmployeeID == userName
                            select u).FirstOrDefault();

                if (user != null)
                {
                    user.Inactive = false;
                    db.SaveChanges();
                    retValue = true;
                }
                else
                {
                    retValue = false;
                }
            }

            return retValue;
        }
        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="user">required implementation</param>
        public override void UpdateUser(MembershipUser user)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                int UserID = Convert.ToInt32(user.ProviderUserKey);

                var item = (from u in db.Users
                            where u.UserID == UserID
                            select u).FirstOrDefault();

                if (item != null)
                {
                    DateTime minDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;

                    item.EmployeeID = user.UserName;
                    item.EmailAddress = user.Email;
                    item.LastLoginDate = user.LastLoginDate > minDate ? user.LastLoginDate : minDate;
                    item.LastActivityDate = user.LastActivityDate > minDate ? user.LastActivityDate : minDate;
                    item.LastPasswordChangeDate = user.LastPasswordChangedDate > minDate ? user.LastPasswordChangedDate : minDate;
                    item.LastPasswordLockoutDate = user.LastLockoutDate > minDate ? user.LastLockoutDate : minDate;
                    item.PasswordRecoveryQuestion1 = user.PasswordQuestion;
                    item.Inactive = user.IsLockedOut;

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        public override bool ValidateUser(string username, string password)
        {
            bool returnValue = false;

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                CallDispositionTool.Model.User user = (from u in db.Users
                                                       where u.EmployeeID == username && u.Inactive == false
                                                       select u).FirstOrDefault();

                if (user != null)
                {
                    if (user.LastLoginDate.HasValue)
                    {
                        if ((DateTime.Now - user.LastLoginDate.Value).TotalDays >= 90)
                        {
                            //Disable user when their lastlogin date is greater than 90 days
                            user.Inactive = true;
                            db.SaveChanges();
                            return false;
                        }
                    }
                    else if (user.Inactive == true)
                    {
                        return false;
                    }

                    returnValue = CallDispositionTool.Security.PasswordHash.ValidatePassword(password, user.PasswordHash);
                }
            }

            return returnValue;
        }

        private bool CheckPasswordComplexity(string password)
        {
            if (string.IsNullOrEmpty(password))
                return false;

            if (password.Length < this._MinRequiredPasswordLength)
                return false;

            int nonAlnumCount = 0;

            for (int i = 0; i < password.Length; i++)
            {
                if (!char.IsLetterOrDigit(password, i)) nonAlnumCount++;
            }

            if (nonAlnumCount < _MinRequiredNonalphanumericCharacters)
                return false;
            if (!string.IsNullOrEmpty(_PasswordStrengthRegularExpression) &&
                !Regex.IsMatch(password, _PasswordStrengthRegularExpression))
            {
                return false;
            }

            return true;
        }
        #endregion
    }
}
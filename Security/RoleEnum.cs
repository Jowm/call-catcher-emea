﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallDispositionTool.Security
{
    public enum RoleEnum
    {
        AdminRole = 1,
        ReportViewer = 2,
        PickListOptionManager = 3,
        DispositionManager = 4,
        UserAccessGrantor = 5,
        FormManager = 6,
        ReassignCallBack = 7,
        AssignUserRole = 8,
        FormAccessGrantor = 9
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using CallDispositionTool.UserControl;
using Telerik.Web.UI;
using System.Web.Security;
using System.Web.Configuration;
using Microsoft.AspNet.SignalR;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Web.Services;
using System.Configuration;
using System.Globalization;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Text;

namespace CallDispositionTool
{
    public partial class TransactionForm : System.Web.UI.Page
    {
        const int BHN_RETENTION = 7;
        List<CallDispositionTool.Model.Attribute> _Attributes;
        private int formID
        {
            get
            {
                if (ViewState["formID"] != null)
                    return Convert.ToInt32(ViewState["formID"]);
                return 0;
            }
            set
            {
                ViewState["formID"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            formID = Convert.ToInt32(Request.QueryString["FormID"]);

            if (!Page.IsPostBack)
            {
                Response.Cache.SetCacheability(System.Web.HttpCacheability.Private);
                Response.Cache.SetMaxAge(new TimeSpan(24, 0, 0));
                Response.Cache.SetExpires(DateTime.Now.AddHours(24));
                Response.Cache.SetAllowResponseInBrowserHistory(true);

                string formid = Request["FormID"];
                formID = Convert.ToInt32(Request["FormID"]);
                int iFormID;

                if (int.TryParse(formid, out iFormID))
                {
                    if (!Helper.CheckFormAccess(Convert.ToInt32(formid), Convert.ToInt32(Membership.GetUser().ProviderUserKey)))
                    {
                        Response.Redirect("~/Unauthorized.aspx");
                    }

                    if (Helper.HasCallBackFeature(iFormID))
                    {
                        CallBackUserControl1.FormID = iFormID;
                    }

                    if (Helper.HasContactFeature(iFormID))
                    {
                        pnlContact.Visible = true;
                        ContactUserControl1.FormID = iFormID;
                    }

                    if (Helper.HasLinkToLiberator(iFormID))
                    {
                        pnlLiberator.Visible = true;
                        hypLiberator.NavigateUrl = String.Format(WebConfigurationManager.AppSettings["LiberatorURL"], Helper.GetEmployeeName(User.Identity.Name), User.Identity.Name, DateTime.Now.Ticks.ToString());
                    }

                    if (Request["FormName"] != null)
                    {
                        Page.Title = Request["FormName"];
                    }
                    if (iFormID == UPS_SmartPad || iFormID == UK_SmartPad)
                    {
                        frmBody.Attributes.Add("class", "cssbody");
                        TabStrip1.CssClass = "SmartPadTabStrip";
                        tranHTML.Attributes.Add("class", "htmlSmartPad");
                        btnSubmitSmartPad.Visible = true;
                        btnSubmitSmartPad.Enabled = false;
                        btnDropCall.Visible = true;
                        btnCancel.Visible = true;
                        btnReset.Visible = false;
                        btnSubmit.Visible = false;
                        lblFormName.CssClass = "hSmartPad";
                    }
                    else
                    {
                        btnSubmitSmartPad.Visible = false;
                        btnDropCall.Visible = false;
                    }
                }
                else
                {
                    Response.Redirect("~/Unauthorized.aspx");
                }
            }
        }

        #region .SmartPad Custom


        int UPS_SmartPad = Convert.ToInt32(ConfigurationManager.AppSettings["SmartPadFormID"]);
        int UK_SmartPad = Convert.ToInt32(ConfigurationManager.AppSettings["UKSmartPadFormID"]);
        int SenukaiFormID = Convert.ToInt32(ConfigurationManager.AppSettings["SenukaiFormID"]);
        int PaldieskartesFormID = Convert.ToInt32(ConfigurationManager.AppSettings["PaldieskartesFormID"]);
        int SwedBankFormID = Convert.ToInt32(ConfigurationManager.AppSettings["SwedBankFormID"]);
        int KlientuKreipiniaiFormID = Convert.ToInt32(ConfigurationManager.AppSettings["KlientuKreipiniaiFormID"]);
        protected void grdSmartPadDetail_DataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            var dataItem = e.DetailTableView.ParentItem;

            if (e.DetailTableView.Name == "grdSmartPadDetail")
            {
                var tranID = dataItem.GetDataKeyValue("TransactionID").ToString();

                try
                {
                    e.DetailTableView.DataSource = getSmartPadDetail(tranID, false);

                }
                catch (Exception ex)
                {
                    ((RadWindowManager)Master.FindControl("RadWindowManager1")).RadAlert(ex.Message, 330, 180, "Error Message", "");
                }


            }
        }

        protected void grdSmartPad_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

            if (formID == UPS_SmartPad || formID == UK_SmartPad)
            {
                //if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
                //{
                try
                {
                    //low level sql datatable 
                    using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CallDisposition"].ConnectionString))
                    {
                        cn.Open();

                        string sql = @"GetTransactionsSmartPad";

                        SqlCommand cmd = new SqlCommand(sql, cn);
                        cmd.CommandTimeout = 20000;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FormID", formID);
                        cmd.Parameters.AddWithValue("@StartDate", Convert.ToDateTime("1/1/1900"));
                        cmd.Parameters.AddWithValue("@EndDate", Convert.ToDateTime("1/1/3000"));
                        cmd.Parameters.AddWithValue("@EmployeeID", Convert.ToInt32(Membership.GetUser().UserName));
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();

                        da.Fill(dt);

                        if (dt.Columns.Count > 0)
                        {
                            grdSmartPad.DataSource = dt;
                        }
                        else
                        {
                            grdSmartPad.DataSource = null;
                        }
                    }

                }
                catch (Exception ex)
                {
                    ((RadWindowManager)Master.FindControl("RadWindowManager1")).RadAlert(ex.Message, 330, 180, "Error Message", "");
                }
            }
            //}
        }


        private DataTable getSmartPadDetail(string tranID, bool isReport)
        {
            var dt = new DataTable();
            int formID = Convert.ToInt32(Request.QueryString["FormID"]);
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CallDisposition"].ConnectionString))
            {
                cn.Open();

                string sql = @"GetTransactionsSmartPadDetail";

                SqlCommand cmd = new SqlCommand(sql, cn);
                cmd.CommandTimeout = 20000;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FormID", formID);
                cmd.Parameters.AddWithValue("@StartDate", Convert.ToDateTime("1/1/1900"));
                cmd.Parameters.AddWithValue("@EndDate", Convert.ToDateTime("1/1/3000"));
                cmd.Parameters.AddWithValue("@EmployeeID", Convert.ToInt32(Membership.GetUser().UserName));
                cmd.Parameters.AddWithValue("@TransactionID", Convert.ToInt32(tranID));
                cmd.Parameters.AddWithValue("@IsReport", isReport);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
                return dt;
            }
        }

        protected void grdSmartPad_ItemCommand(object sender, GridCommandEventArgs e)
        {
            var command = e.CommandName;
            if (command == "ExportToExcel" || command == "ExportToCsv")
            {
                var dt = new DataTable();
                dt = getSmartPadDetail("0", true);
                var filename = formID == UPS_SmartPad ? "UPS Smart Pad - " : "UK Smart Pad - ";

                if (command == "ExportToExcel")
                {
                    ExcelHelper.ToExcel(dt, filename + Membership.GetUser().UserName + " " + DateTime.Now + ".xls", Page.Response);
                }
                else
                {
                    CSVHelper.ConvertToCSV(dt, filename + Membership.GetUser().UserName + " " + DateTime.Now + ".csv", Page.Response);
                }
            }
        }

        #endregion

        protected void Page_PreRender(object sender, EventArgs e)
        {
            HttpCookie ck = Request.Cookies["UserInfo"];
            string skinID = string.Empty;

            if (ck != null)
            {
                skinID = ck.Values["Skin"];
                this.skinManager1.Skin = skinID;
            }
            else
            {
                ck = new HttpCookie("UserInfo");
                ck.Values["Skin"] = this.skinManager1.Skin;
                Response.Cookies.Remove("UserInfo");
                Response.Cookies.Add(ck);
            }
        }

        //Matt - 08-18-2015 - Custom code for Smart Pad
        protected virtual void grdTransaction_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            if (e.Column.UniqueName == "Smart Pad")
            {
                e.Column.Visible = false;
            }

        }

        void ResetControls()
        {
            foreach (var item in _Attributes)
            {
                DataTypeIDEnum dt = (DataTypeIDEnum)item.DataTypeID;

                Control ctrl = formPlaceHolder.FindControl("Ctr_" + item.AttributeID);

                switch (dt)
                {
                    case DataTypeIDEnum.String:
                        RadTextBox txt = ctrl as RadTextBox;
                        txt.Text = string.Empty;
                        break;
                    case DataTypeIDEnum.Boolean:
                        CheckBox cb = ctrl as CheckBox;
                        cb.Checked = false;
                        break;
                    case DataTypeIDEnum.Numeric:
                        RadNumericTextBox num = ctrl as RadNumericTextBox;
                        num.Value = null;
                        break;
                    case DataTypeIDEnum.Date:
                        RadDatePicker dtp = ctrl as RadDatePicker;
                        dtp.SelectedDate = null;
                        break;
                    case DataTypeIDEnum.DropdownList:
                        RadComboBox cbo = ctrl as RadComboBox;
                        cbo.Text = string.Empty;
                        cbo.ClearSelection();
                        break;
                    case DataTypeIDEnum.Listbox:
                        RadListBox lst = ctrl as RadListBox;
                        lst.ClearSelection();

                        foreach (RadListBoxItem itm in lst.Items)
                        {
                            itm.Checked = false;
                        }
                        break;
                    case DataTypeIDEnum.Disposition:
                        DispositionUserControl dispo = ctrl as DispositionUserControl;
                        dispo.Rebind();
                        break;
                    case DataTypeIDEnum.DropdownTreeView:
                        DropdownTreeViewUserControl tree = ctrl as DropdownTreeViewUserControl;
                        tree.Rebind();
                        break;
                    case DataTypeIDEnum.Contact:
                        ContactUserControl contactUC = ctrl as ContactUserControl;
                        contactUC.ResetControl();
                        break;
                    case DataTypeIDEnum.TimerAuto:
                        TimerAutoUserControl timerAuto = ctrl as TimerAutoUserControl;
                        timerAuto.Reset();
                        break;
                    case DataTypeIDEnum.TimerManual:
                        TimerManualUserControl timerManual = ctrl as TimerManualUserControl;
                        timerManual.Reset();
                        break;
                    case DataTypeIDEnum.CustomerSatisfactionRadioButton:
                        CustomerSatisfactionUserControl csUC = ctrl as CustomerSatisfactionUserControl;
                        csUC.Reset();
                        break;
                    case DataTypeIDEnum.UserCaptionUserControl:
                        UserCaptionUserControl UserCaption = ctrl as UserCaptionUserControl;
                        UserCaption.Reset();
                        break;
                    case DataTypeIDEnum.AssignToUserControl:
                        AssignToUserControl AssignTo = ctrl as AssignToUserControl;
                        AssignTo.Reset();
                        break;
                    default:
                        break;
                }
            }
        }

        bool IsRecordContainsOnHold()
        {
            bool returnValue = false;

            foreach (var item in _Attributes)
            {
                DataTypeIDEnum dt = (DataTypeIDEnum)item.DataTypeID;

                Control ctrl = formPlaceHolder.FindControl("Ctr_" + item.AttributeID);

                switch (dt)
                {
                    case DataTypeIDEnum.Disposition:
                        DispositionUserControl d = ctrl as DispositionUserControl;
                        returnValue = d.DispositionValue.ToUpper().Contains("\\ON HOLD");
                        break;
                    case DataTypeIDEnum.DropdownTreeView:
                        DropdownTreeViewUserControl tree = ctrl as DropdownTreeViewUserControl;
                        returnValue = tree.DispositionValue.ToUpper().Contains("\\ON HOLD");
                        break;
                    default:
                        break;
                }

                if (returnValue)
                    return true;
            }

            return returnValue;
        }

        public void Dispo24_SelectedIndexChange(object sender, DropdownTreeViewUserControl.DispositionChangedEventArgs e)
        {
            DropdownTreeViewUserControl ctr24 = formPlaceHolder.FindControl("Ctr_24") as DropdownTreeViewUserControl;
            DropdownTreeViewUserControl ctr31 = formPlaceHolder.FindControl("Ctr_31") as DropdownTreeViewUserControl;

            RadAjaxManager manager = RadAjaxManager.GetCurrent(this.Page);
            manager.AjaxSettings.AddAjaxSetting(ctr24, pnlMain, LoadingPanel1);

            ctr31.FilterDispositionID = e.Dispo2;

            if (e.Dispo2.HasValue)
            {
                ctr31.AutoPostBack = false;
                ctr31.Enabled = true;
                ctr31.Rebind();
                //ctr31.ClearEntries();
            }
            else
            {
                ctr31.Enabled = false;
            }
        }

        public void Dispo24_SelectedIndexChange(object sender, DispositionUserControl.DispositionChangedEventArgs e)
        {
            DispositionUserControl ctr24 = formPlaceHolder.FindControl("Ctr_24") as DispositionUserControl;
            DispositionUserControl ctr31 = formPlaceHolder.FindControl("Ctr_31") as DispositionUserControl;

            RadAjaxManager manager = RadAjaxManager.GetCurrent(this.Page);
            manager.AjaxSettings.AddAjaxSetting(ctr24, pnlMain, LoadingPanel1);

            if (e.Dispo2.HasValue)
            {
                ctr31.FilterDispositionID = e.Dispo2;
                ctr31.Rebind();
            }
        }

        public void Ctr_239_OnSelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox Ctr_239 = formPlaceHolder.FindControl("Ctr_239") as RadComboBox;
            RadTextBox Ctr_485 = formPlaceHolder.FindControl("Ctr_485") as RadTextBox;

            if (Ctr_239.Text == "Ceka neparadas konta atlikums" || Ctr_239.Text == "Nav ieskaitita MAXIMA nauda" ||
                Ctr_239.Text == "Nav sanemta atlaide ar PALDIES" || Ctr_239.Text == "Nedarbojas karte" ||
                Ctr_239.Text == "No kartes noskaitita (pazudusi) MAXIMA nauda")
            {
                Ctr_485.Visible = true;
            }
            else if (Ctr_239.SelectedItem == null)
            {
                Ctr_485.Visible = false;
            }
            else if (Ctr_239.Text == "" || Ctr_239.Text == null)
            {
                Ctr_485.Visible = false;
            }
            else
            {
                Ctr_485.Visible = false;
                Ctr_239.Text = null;
            }
        }

        public void Ctr_477_OnSelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox Ctr_477 = formPlaceHolder.FindControl("Ctr_477") as RadComboBox;
            RadTextBox Ctr_480 = formPlaceHolder.FindControl("Ctr_480") as RadTextBox; // asmens kodas 480
            RadTextBox Ctr_483 = formPlaceHolder.FindControl("Ctr_483") as RadTextBox; // addresas 483
            RadTextBox Ctr_484 = formPlaceHolder.FindControl("Ctr_484") as RadTextBox; // automobilis 484
            RadTextBox Ctr_485 = formPlaceHolder.FindControl("Ctr_485") as RadTextBox; // valst numeris 485
            RadTextBox Ctr_487 = formPlaceHolder.FindControl("Ctr_487") as RadTextBox; // ivyko salis 487
            RadTextBox Ctr_489 = formPlaceHolder.FindControl("Ctr_489") as RadTextBox; // ivyko vieta 489

            if (Ctr_477.Text == "Kelioniu draudimas")
            {
                Ctr_480.Visible = true;
                Ctr_483.Visible = false;
                Ctr_484.Visible = false;
                Ctr_485.Visible = false;
                Ctr_487.Visible = true;
                Ctr_489.Visible = false;
            }
            else if (Ctr_477.Text == "Turto draudimas")
            {
                Ctr_480.Visible = false;
                Ctr_483.Visible = true;
                Ctr_484.Visible = false;
                Ctr_485.Visible = false;
                Ctr_487.Visible = false;
                Ctr_489.Visible = false;
            }
            else if (Ctr_477.Text == "Transporto priemoniu draudimas")
            {
                Ctr_480.Visible = false;
                Ctr_483.Visible = false;
                Ctr_484.Visible = true;
                Ctr_485.Visible = true;
                Ctr_487.Visible = false;
                Ctr_489.Visible = true;
            }
            else 
            {
                Ctr_480.Visible = false; Ctr_480.Text = "";
                Ctr_483.Visible = false; Ctr_483.Text = "";
                Ctr_484.Visible = false; Ctr_484.Text = "";
                Ctr_485.Visible = false; Ctr_485.Text = "";
                Ctr_487.Visible = false; Ctr_487.Text = "";
                Ctr_489.Visible = false; Ctr_489.Text = "";
            }
        }

        public void Problema_SelectedIndexChange(object sender, DispositionUserControl.DispositionChangedEventArgs e) //x2
        {
            DispositionUserControl Ctr_451 = formPlaceHolder.FindControl("Ctr_451") as DispositionUserControl;
            RadTextBox Ctr_462 = formPlaceHolder.FindControl("Ctr_462") as RadTextBox;
            TableRow problema = formPlaceHolder.FindControl("lol1") as TableRow;
        
            RadAjaxManager manager = RadAjaxManager.GetCurrent(this.Page);
            manager.AjaxSettings.AddAjaxSetting(Ctr_451, pnlMain, LoadingPanel1);
            if (Ctr_451.DispositionValue == "Paslaugos\\Bendra informacija")
            {
                Ctr_462.Enabled = false;
            }
        }

        public void Ctr_636_OnSelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox Ctr_636 = formPlaceHolder.FindControl("Ctr_636") as RadComboBox;

            string[] taip = {"Ctr_637", "Ctr_638", "Ctr_639", "Ctr_644", 
                                "Ctr_645", "Ctr_646", "Ctr_647", "Ctr_648"};

            string[] ne = {"Ctr_640", "Ctr_641", "Ctr_642", "Ctr_643", "Ctr_644", 
                              "Ctr_645", "Ctr_646", "Ctr_647", "Ctr_648"};

            if (Ctr_636.SelectedItem.Text == "Taip")
            {
                foreach (string id in ne)
                {
                    RadAjaxManager1.ResponseScripts.Add(String.Format("hideControls({0})", id));
                }

                foreach (string id in taip)
                {
                    RadAjaxManager1.ResponseScripts.Add(String.Format("showRequiredControls({0})", id));
                }
            }
            else if (Ctr_636.SelectedItem.Text == "Ne")
            {
                foreach (string id in taip)
                {
                    RadAjaxManager1.ResponseScripts.Add(String.Format("hideControls({0})", id));
                }

                foreach (string id in ne)
                {
                    RadAjaxManager1.ResponseScripts.Add(String.Format("showRequiredControls({0})", id));
                }
            }
            else
            {
                Ctr_636.SelectedIndex = 0;

                foreach (string id in taip)
                {
                    RadAjaxManager1.ResponseScripts.Add(String.Format("hideControls({0})", id));
                }

                foreach (string id in ne)
                {
                    RadAjaxManager1.ResponseScripts.Add(String.Format("hideControls({0})", id));
                }

            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            string formid = Request["FormID"];
            int iFormID;

            if (int.TryParse(formid, out iFormID))
            {
                //Custom SmartPad Requirements
                if (iFormID != UPS_SmartPad && iFormID != UK_SmartPad)
                {
                    btnSubmitSmartPad.Visible = false;
                    btnDropCall.Visible = false;
                    HeaderPlaceHolder.Controls.Add(new LiteralControl("<br /><br />"));
                }
                //Create Instance of CacheManager 
                CacheManager _objCacheManager = CacheFactory.GetCacheManager();

                //formPlaceHolderHeader
                if (_objCacheManager.Contains("GetAttributes~" + formid))
                {
                    _Attributes = (List<CallDispositionTool.Model.Attribute>)_objCacheManager.GetData("GetAttributes~" + formid);
                    lblFormName.Text = (string)_objCacheManager.GetData("GetFormNameByFormID~" + formid);
                }
                else
                {
                    using (CallDispositionEntities db = new CallDispositionEntities())
                    {
                        _Attributes = (from p in db.Attributes
                                       where p.FormID == iFormID && p.HideFromList == false
                                       orderby p.SortOrder
                                       select p).ToList();

                        lblFormName.Text = _Attributes.First().Form.FormName;

                        _objCacheManager.Add("GetAttributes~" + formid, _Attributes);
                        _objCacheManager.Add("GetFormNameByFormID~" + formid, lblFormName.Text);
                    }
                }

                formPlaceHolder.Controls.Add(new LiteralControl("<table style='width: 100%'>"));

                foreach (CallDispositionTool.Model.Attribute item in _Attributes)
                {
                    if (item.DataTypeID != (int)DataTypeIDEnum.AssignToUserControl)
                    {
                        if (item.AttributeName == "Problema")
                        {
                            formPlaceHolder.Controls.Add(new LiteralControl("<tr>"));
                            AddCustomAttribute(item, iFormID);
                            formPlaceHolder.Controls.Add(new LiteralControl("</tr>"));
                        }
                        else
                        {
                            formPlaceHolder.Controls.Add(new LiteralControl("<tr>"));
                            AddCustomAttribute(item, iFormID);
                            formPlaceHolder.Controls.Add(new LiteralControl("</tr>"));
                        }
                    }
                }
               
                formPlaceHolder.Controls.Add(new LiteralControl("</table>"));

                if (iFormID == SenukaiFormID)//x1
                {
                    DispositionUserControl Ctr_451 = formPlaceHolder.FindControl("Ctr_451") as DispositionUserControl;
                    RadTextBox Ctr_462 = formPlaceHolder.FindControl("Ctr_462") as RadTextBox;

                    try
                    {
                        if (Ctr_451.DispositionValue == "")
                        {
                            Ctr_451.dispoHandler += new DispositionUserControl.OnDispositionChangedEventHandler(Problema_SelectedIndexChange);

                            //Ctr_462.Enabled = false;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }

                //if (iFormID == PaldieskartesFormID)//x1
                //{
                //    // Joem - 2019-01-21
                //    //RadComboBox Ctr_239 = formPlaceHolder.FindControl("Ctr_239") as RadComboBox;
                //    //RadTextBox Ctr_485 = formPlaceHolder.FindControl("Ctr_485") as RadTextBox;

                //    //Ctr_239.CausesValidation = false;
                //    ////777
                //    //if (Ctr_239.Text == "")
                //    //{
                //    //    Ctr_239.AutoPostBack = true;
                //    //    //Ctr_477.OnClientSelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(Ctr_477_OnSelectedIndexChanged);
                //    //    Ctr_239.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(Ctr_239_OnSelectedIndexChanged);

                //    //    Ctr_485.Visible = false;
                //    //}

                    
                //}

                //swedbank condition
                if (iFormID == SwedBankFormID)
                {
                    RadComboBox Ctr_477 = formPlaceHolder.FindControl("Ctr_477") as RadComboBox;
                    RadTextBox Ctr_478 = formPlaceHolder.FindControl("Ctr_478") as RadTextBox;
                    RadTextBox Ctr_480 = formPlaceHolder.FindControl("Ctr_480") as RadTextBox; // asmens kodas 480
                    RadTextBox Ctr_483 = formPlaceHolder.FindControl("Ctr_483") as RadTextBox; // addresas 483
                    RadTextBox Ctr_484 = formPlaceHolder.FindControl("Ctr_484") as RadTextBox; // automobilis 484
                    RadTextBox Ctr_485 = formPlaceHolder.FindControl("Ctr_485") as RadTextBox; // valst numeris 485
                    RadTextBox Ctr_487 = formPlaceHolder.FindControl("Ctr_487") as RadTextBox; // ivyko salis 487
                    RadTextBox Ctr_489 = formPlaceHolder.FindControl("Ctr_489") as RadTextBox; // ivyko vieta 489

                    if (Ctr_477.Text == "")
                    {
                        Ctr_477.AutoPostBack = true;
                        Ctr_477.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(Ctr_477_OnSelectedIndexChanged);

                        Ctr_478.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        Ctr_478.Enabled = false;

                        Ctr_480.Visible = false; // asmens kodas
                        Ctr_483.Visible = false; // addresas
                        Ctr_484.Visible = false; // automobilis
                        Ctr_485.Visible = false; // valst numeris
                        Ctr_487.Visible = false; // ivyko salis
                        Ctr_489.Visible = false; // ivyko vieta
                    }
                }

                //Klientu Kreipiniai condition
                if (iFormID == KlientuKreipiniaiFormID)//x1
                {
                    RadComboBox Ctr_636 = formPlaceHolder.FindControl("Ctr_636") as RadComboBox;

                    Ctr_636.SelectedIndex = 0;

                    if (Ctr_636.Text == "")
                    {
                        Ctr_636.AutoPostBack = true;
                        Ctr_636.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(Ctr_636_OnSelectedIndexChanged);

                        string[] ids = { "Ctr_637", "Ctr_638", "Ctr_639", "Ctr_640", "Ctr_641", "Ctr_642", 
                                           "Ctr_643", "Ctr_644", "Ctr_645", "Ctr_646", "Ctr_647", "Ctr_648" };

                        foreach (string id in ids)
                        {
                            RadAjaxManager1.ResponseScripts.Add(String.Format("hideControls({0})", id));
                        }
                    }
                }

                //Assign an on change handler for custom BHN disposition control
                if (iFormID == BHN_RETENTION)
                {
                    DropdownTreeViewUserControl ctr24_Ddtv = formPlaceHolder.FindControl("Ctr_24") as DropdownTreeViewUserControl;
                    DropdownTreeViewUserControl ctr31_Ddtv = formPlaceHolder.FindControl("Ctr_31") as DropdownTreeViewUserControl;

                    DispositionUserControl ctr24_Cdd = formPlaceHolder.FindControl("Ctr_24") as DispositionUserControl;
                    DispositionUserControl ctr31_Cdd = formPlaceHolder.FindControl("Ctr_31") as DispositionUserControl;

                    
                    if (ctr24_Ddtv != null && ctr31_Ddtv != null)
                    {
                        ctr31_Ddtv.Enabled = false;
                        ctr24_Ddtv.AutoPostBack = true;
                        ctr24_Ddtv.dispoHandler += new DropdownTreeViewUserControl.OnDispositionChangedEventHandler(Dispo24_SelectedIndexChange);
                    }
                    else if (ctr24_Cdd != null && ctr31_Cdd != null)
                    {
                        ctr24_Cdd.dispoHandler += new DispositionUserControl.OnDispositionChangedEventHandler(Dispo24_SelectedIndexChange);
                    }
                }
            }
            else
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        bool isGrouping = false;
        public bool ShouldApplySortFilterOrGroup()
        {
            return grdTransaction.MasterTableView.FilterExpression != "" ||
                (grdTransaction.MasterTableView.GroupByExpressions.Count > 0 || isGrouping) ||
                grdTransaction.MasterTableView.SortExpressions.Count > 0;
        }

        protected void grdTransaction_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            int formID = Convert.ToInt32(Request.QueryString["FormID"]);
            if (formID != UPS_SmartPad && formID != UK_SmartPad && formID != SenukaiFormID)
            {
                if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
                {
                    //low level sql datatable 
                    using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CallDisposition"].ConnectionString))
                    {
                        cn.Open();

                        string sql = @"GetTransactionsByUser";
                        int transCount = 0;
                        int userID = Convert.ToInt32(Membership.GetUser().ProviderUserKey);

                        using (CallDispositionEntities db = new CallDispositionEntities())
                        {
                            transCount = (from t in db.Transactions
                                          where t.CreatedBy == userID && t.FormID == formID
                                          select t).Count();
                        }

                        grdTransaction.VirtualItemCount = transCount;

                        int startRowIndex = (ShouldApplySortFilterOrGroup()) ? 0 : grdTransaction.CurrentPageIndex * grdTransaction.PageSize;
                        int maximumRows = (ShouldApplySortFilterOrGroup()) ? transCount : grdTransaction.PageSize;

                        SqlCommand cmd = new SqlCommand(sql, cn);
                        cmd.CommandTimeout = 9000;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UserID", Convert.ToInt32(Membership.GetUser().ProviderUserKey));
                        cmd.Parameters.AddWithValue("@FormID", formID);
                        cmd.Parameters.AddWithValue("@StartRowIndex", startRowIndex);
                        cmd.Parameters.AddWithValue("@MaximumRows", maximumRows);
                        grdTransaction.AllowCustomPaging = !ShouldApplySortFilterOrGroup();

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();

                        da.Fill(dt);

                        try
                        {
                            if (dt.Columns.Count > 0)
                            {
                                grdTransaction.DataSource = dt;
                            }
                            else
                            {
                                grdTransaction.DataSource = null;
                            }
                        }
                        catch (Exception ex)
                        {
                            RadWindowManager1.RadAlert(ex.Message, 330, 180, "Error Message", "");
                        }
                    }
                }
            }

            else if (formID == SenukaiFormID)
            {
                if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
                {
                    //low level sql datatable 
                    using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CallDisposition"].ConnectionString))
                    {
                        cn.Open();

                        string sql = @"GetTransactionsByDateToday_Senukai";
                        int transCount = 0;
                        int userID = Convert.ToInt32(Membership.GetUser().ProviderUserKey);

                        using (CallDispositionEntities db = new CallDispositionEntities())
                        {
                            transCount = (from t in db.Transactions
                                          where t.CreatedBy == userID && t.FormID == formID
                                          select t).Count();
                        }

                        grdTransaction.VirtualItemCount = transCount;

                        int startRowIndex = (ShouldApplySortFilterOrGroup()) ? 0 : grdTransaction.CurrentPageIndex * grdTransaction.PageSize;
                        int maximumRows = (ShouldApplySortFilterOrGroup()) ? transCount : grdTransaction.PageSize;

                        SqlCommand cmd = new SqlCommand(sql, cn);
                        cmd.CommandTimeout = 9000;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FormID", formID);
                        grdTransaction.AllowCustomPaging = !ShouldApplySortFilterOrGroup();

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();

                        da.Fill(dt);

                        try
                        {
                            if (dt.Columns.Count > 0)
                            {
                                grdTransaction.DataSource = dt;
                            }
                            else
                            {
                                grdTransaction.DataSource = null;
                            }
                        }
                        catch (Exception ex)
                        {
                            RadWindowManager1.RadAlert(ex.Message, 330, 180, "Error Message", "");
                        }
                    }
                }

            }
        }

        void AddCustomAttribute(CallDispositionTool.Model.Attribute attribute, int formID)
        {

            var width = "100";

            if (formID != UPS_SmartPad && formID != UK_SmartPad)
            {
                //if ()
                {
                formPlaceHolder.Controls.Add(new LiteralControl(String.Format("<td style='vertical-align: text-top; width: 20%;'><strong><label for='{0}'>", "Ctr_" + attribute.AttributeID.ToString()) + attribute.AttributeName + "</label></strong></td>"));
          
                }
                //else
                {
                    //label
                }
                      width = "80";
            }

            //Add the UI as the right cell
            List<Control> UIControls = CreateCustomAttributeUI(attribute.AttributeID, attribute.AttributeName, attribute.DataTypeID, attribute.Mandatory, null);

            formPlaceHolder.Controls.Add(new LiteralControl("<td style='width: " + width + "%'>"));

            foreach (Control ctrl in UIControls)
            {
                formPlaceHolder.Controls.Add(ctrl);
            }

            formPlaceHolder.Controls.Add(new LiteralControl("</td>"));
        }

        List<Control> CreateCustomAttributeUI(int AttributeID, string AttributeName, int DataTypeID, bool? Mandatory, object AttributeValue)
        {
            List<Control> ctrls = new List<Control>();
            DataTypeIDEnum myEnum = (DataTypeIDEnum)DataTypeID;
            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            switch (myEnum)
            {
                case DataTypeIDEnum.String:
                    //Use Textbox
                    RadTextBox tb = new RadTextBox();
                    tb.ID = "Ctr_" + AttributeID.ToString();
                    tb.Width = new Unit("300px");

                    ctrls.Add(tb);

                    AttributeTextboxProperty atp;

                    if (_objCacheManager.Contains("AttributeTextboxProperty~" + AttributeID.ToString()))
                    {
                        atp = (AttributeTextboxProperty)_objCacheManager.GetData("AttributeTextboxProperty~" + AttributeID.ToString());
                    }
                    else
                    {
                        using (CallDispositionEntities db = new CallDispositionEntities())
                        {
                            atp = (from a in db.AttributeTextboxProperties
                                   where a.AttributeID == AttributeID
                                   select a).FirstOrDefault();

                            _objCacheManager.Add("AttributeTextboxProperty~" + AttributeID.ToString(), atp);
                        }
                    }

                    if (atp != null)
                    {
                        tb.MaxLength = atp.MaxLength;
                        tb.Columns = atp.MaxLength;

                        if (atp.TextMode == 1)
                        {
                            tb.TextMode = InputMode.MultiLine;
                            tb.Rows = 4;
                        }
                        else if (atp.TextMode == 2)
                        {
                            tb.TextMode = InputMode.Password;
                        }
                        else if (atp.TextMode == 3)
                        {
                            tb.TextMode = InputMode.SingleLine;
                        }

                        RegularExpressionValidator regexValidator = new RegularExpressionValidator();
                        regexValidator.ID = "ctrl_Regex_" + AttributeID.ToString();
                        regexValidator.ControlToValidate = tb.ID;
                        regexValidator.ErrorMessage = atp.ErrorMessage;

                        try
                        {
                            regexValidator.Display = ValidatorDisplay.Dynamic;
                            regexValidator.ValidationExpression = atp.RegularExpression;
                        }
                        catch (Exception ex)
                        {
                            Helper.LogError(ex);
                        }

                        regexValidator.SetFocusOnError = true;
                        ctrls.Add(new LiteralControl("<br />"));
                        ctrls.Add(regexValidator);
                    }

                    if (Mandatory.Value)
                    {
                        //ctrls.Add(new LiteralControl("<br />"));
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }

                    break;
                case DataTypeIDEnum.Boolean:
                    //Use Checkbox

                    CheckBox cb = new CheckBox();
                    cb.ID = "Ctr_" + AttributeID.ToString();

                    ctrls.Add(cb);

                    break;
                case DataTypeIDEnum.Numeric:
                    //Use Numeric Textbox
                    RadNumericTextBox numericTB = new RadNumericTextBox();
                    numericTB.ID = "Ctr_" + AttributeID.ToString();
                    numericTB.NumberFormat.DecimalDigits = 0;
                    numericTB.ShowSpinButtons = true;
                    numericTB.NumberFormat.GroupSeparator = "";
                    numericTB.Width = new Unit("300px");

                    ctrls.Add(numericTB);

                    if (Mandatory.Value)
                    {
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }

                    break;
                case DataTypeIDEnum.Date:
                    //Use Date Picker
                    RadDatePicker dp = new RadDatePicker();

                    dp.ID = "Ctr_" + AttributeID.ToString();

                    ctrls.Add(dp);

                    if (Mandatory.Value)
                    {
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }

                    break;
                case DataTypeIDEnum.DropdownList:
                    //Use dropdown list
                    RadComboBox ddl = new RadComboBox();
                    RadComboBoxItemCollection ddlItems = GetDropdownPicklistOption(AttributeID, ddl);

                    ddl.ID = "Ctr_" + AttributeID.ToString();
                    ddl.DropDownAutoWidth = RadComboBoxDropDownAutoWidth.Enabled;
                    ddl.MaxHeight = new Unit("250px");
                    ddl.MarkFirstMatch = true;

                    foreach (RadComboBoxItem item in ddlItems)
                    {
                        ddl.Items.Add(item);
                    }

                    ctrls.Add(ddl);

                    if (Mandatory.Value)
                    {
                        //ctrls.Add(new LiteralControl("<br />"));
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }
                    break;
                case DataTypeIDEnum.Listbox:
                    RadListBox lst = new RadListBox();
                    RadListBoxItemCollection lstItems = GetListboxPickListOption(AttributeID, lst);
                    lst.ID = "Ctr_" + AttributeID.ToString();
                    lst.SelectionMode = ListBoxSelectionMode.Multiple;
                    lst.CheckBoxes = true;
                    lst.Width = new Unit("300px");
                    lst.OnClientSelectedIndexChanged = "OnClientSelectedIndexChanged";

                    foreach (RadListBoxItem item in lstItems)
                    {
                        lst.Items.Add(item);
                    }

                    ctrls.Add(lst);

                    if (Mandatory.Value)
                    {
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }

                    break;
                case DataTypeIDEnum.Disposition:
                    //Use Disposition Control
                    DispositionUserControl dispoCtrl = LoadControl("~/UserControl/DispositionUserControl.ascx") as DispositionUserControl;

                    dispoCtrl.ID = "Ctr_" + AttributeID.ToString();
                    dispoCtrl.AttributeID = AttributeID;
                    ctrls.Add(dispoCtrl);

                    break;
                case DataTypeIDEnum.DropdownTreeView:
                    //Use Disposition Control
                    DropdownTreeViewUserControl dropdownTree = LoadControl("~/UserControl/DropdownTreeViewUserControl.ascx") as DropdownTreeViewUserControl;

                    dropdownTree.ID = "Ctr_" + AttributeID.ToString();
                    dropdownTree.AttributeID = AttributeID;
                    dropdownTree.AttributeName = AttributeName;

                    ctrls.Add(dropdownTree);

                    break;
                case DataTypeIDEnum.Contact:
                    //Use Contact User Control
                    ContactUserControl contact = LoadControl("~/UserControl/ContactUserControl.ascx") as ContactUserControl;

                    contact.ID = "Ctr_" + AttributeID.ToString();
                    contact.AttributeName = AttributeName;
                    contact.AttributeID = AttributeID;

                    ctrls.Add(contact);
                    break;
                case DataTypeIDEnum.TimerAuto:
                    //User Auto Timer control
                    TimerAutoUserControl timerAuto = LoadControl("~/UserControl/TimerAutoUserControl.ascx") as TimerAutoUserControl;

                    timerAuto.ID = "Ctr_" + AttributeID.ToString();
                    timerAuto.AttributeName = AttributeName;
                    timerAuto.AttributeID = AttributeID;

                    ctrls.Add(timerAuto);
                    break;
                case DataTypeIDEnum.TimerManual:
                    //User Auto Timer control
                    TimerManualUserControl timerManual = LoadControl("~/UserControl/TimerManualUserControl.ascx") as TimerManualUserControl;

                    timerManual.ID = "Ctr_" + AttributeID.ToString();
                    timerManual.AttributeName = AttributeName;
                    timerManual.AttributeID = AttributeID;

                    ctrls.Add(timerManual);
                    break;

                case DataTypeIDEnum.SmartPad:
                    // User Smart Pad
                    SmartPadUserControl smartPad = LoadControl("~/UserControl/SmartPadUserControl.ascx") as SmartPadUserControl;

                    smartPad.ID = "Ctr_" + AttributeID.ToString();
                    smartPad.AttributeName = AttributeName;
                    smartPad.AttributeID = AttributeID;

                    ctrls.Add(smartPad);

                    break;
                case DataTypeIDEnum.CustomerSatisfactionRadioButton:
                    CustomerSatisfactionUserControl csUC = LoadControl("~/UserControl/CustomerSatisfactionUserControl.ascx") as CustomerSatisfactionUserControl;
                    csUC.ID = "Ctr_" + AttributeID.ToString();
                    csUC.AttributeName = AttributeName;
                    csUC.AttributeID = AttributeID;

                    ctrls.Add(csUC);

                    //if (Mandatory.Value)
                    //{
                    //    ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    //}

                    break;
                case DataTypeIDEnum.UserCaptionUserControl:
                    UserCaptionUserControl UserCaption = LoadControl("~/UserControl/UserCaptionUserControl.ascx") as UserCaptionUserControl;
                    UserCaption.ID = "Ctr_" + AttributeID.ToString();
                    UserCaption.AttributeName = AttributeName;
                    UserCaption.AttributeID = AttributeID;

                    ctrls.Add(UserCaption);

                    //if (Mandatory.Value)
                    //{
                    //    ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    //}

                    break;
                case DataTypeIDEnum.AssignToUserControl:
                    AssignToUserControl AssignTo = LoadControl("~/UserControl/AssignToUserControl.ascx") as AssignToUserControl;
                    AssignTo.ID = "Ctr_" + AttributeID.ToString();
                    AssignTo.AttributeName = AttributeName;
                    AssignTo.AttributeID = AttributeID;

                    ctrls.Add(AssignTo);

                    //if (Mandatory.Value)
                    //{
                    //    ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    //}

                    break;
                default:
                    break;
            }

            return ctrls;
        }

        RequiredFieldValidator CreateRequiredFieldValidator(int AttributeID, string ErrorMessage, string InitialValue)
        {
            RequiredFieldValidator rfv = new RequiredFieldValidator();
            rfv.ID = "ReqVal_Ctr_" + AttributeID.ToString();
            rfv.ControlToValidate = "Ctr_" + AttributeID.ToString();
            rfv.Text = "*";
            rfv.ErrorMessage = ErrorMessage;
            rfv.InitialValue = InitialValue;
            rfv.Display = ValidatorDisplay.Dynamic;
            rfv.SetFocusOnError = true;

            return rfv;
        }

        RadComboBoxItemCollection GetDropdownPicklistOption(int AttributeID, RadComboBox ddl)
        {
            RadComboBoxItemCollection ddlItems = new RadComboBoxItemCollection(ddl);
            dynamic items;

            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            if (_objCacheManager.Contains("GetDropdownPicklistOption~" + AttributeID.ToString()))
            {
                items = _objCacheManager.GetData("GetDropdownPicklistOption~" + AttributeID.ToString());
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    items = (from i in db.PickListOptions
                             where i.AttributeID == AttributeID && (i.HideFromList == false || i.HideFromList == null)
                             orderby i.SortOrder, i.DisplayText ascending
                             select new { i.DisplayText, i.OptionValue }).ToList();

                    _objCacheManager.Add("GetDropdownPicklistOption~" + AttributeID.ToString(), items);
                }
            }

            ddlItems.Add(new RadComboBoxItem("", ""));

            foreach (var item in items)
            {
                ddlItems.Add(new RadComboBoxItem(item.DisplayText, item.OptionValue));
            }

            return ddlItems;
        }

        RadListBoxItemCollection GetListboxPickListOption(int AttributeID, RadListBox lst)
        {
            RadListBoxItemCollection lstItems = new RadListBoxItemCollection(lst);
            CacheManager _objCacheManager = CacheFactory.GetCacheManager();
            dynamic items;

            if (_objCacheManager.Contains("GetDropdownPicklistOption~" + AttributeID.ToString()))
            {
                items = _objCacheManager.GetData("GetListboxPickListOption~" + AttributeID.ToString());
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    items = (from i in db.PickListOptions
                             where i.AttributeID == AttributeID && (i.HideFromList == false || i.HideFromList == null)
                             orderby i.SortOrder, i.DisplayText ascending
                             select new { i.DisplayText, i.OptionValue }).ToList();

                    _objCacheManager.Add("GetListboxPickListOption~" + AttributeID.ToString(), items);
                }
            }

            foreach (var item in items)
            {
                lstItems.Add(new RadListBoxItem(item.DisplayText, item.OptionValue));
            }

            return lstItems;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //Gian - Add check if form is sign off, open form and pass form id
            int formID = Convert.ToInt32(Request["FormID"]);

            if (IsSpecialSignOff(formID) || IsRecordContainsOnHold())
            {
                //int formID = Convert.ToInt32(Request["FormID"]);
                Session["SignOffFormID"] = formID;

                RadAjaxManager1.ResponseScripts.Add(String.Format("showSignOffForm({0})", formID));
            }
            else
            {
                if (formID == UPS_SmartPad || formID == UK_SmartPad)
                {
                    SaveRecord(true, false);
                }
                else
                {
                    SaveRecord(false,false);
                }

            }
        }

        protected void btnDropCall_Click(object sender, EventArgs e)
        {
            SaveRecord(true, true);
        }

        bool IsSpecialSignOff(int formID)
        {
            bool specialSignOffForm = false;

            string signOffIDs = ConfigurationManager.AppSettings["FormsWithSpecialSignOff"].ToString();
            List<string> formIDList = signOffIDs.Split(',').ToList();

            if (formIDList.Contains(formID.ToString()))
            {
                specialSignOffForm = true;
            }

            return specialSignOffForm;
        }

        void SaveSmartPadValues(bool isDropCall)
        {


            int AttributeID;
            if (formID == UPS_SmartPad)
            {
                AttributeID = Convert.ToInt32(ConfigurationManager.AppSettings["SmartPadAttributeID"]);
            }
            else
            {
                AttributeID = Convert.ToInt32(ConfigurationManager.AppSettings["UKSmartPadAttributeID"]);
            }

            Control ctrl = formPlaceHolder.FindControl("Ctr_" + AttributeID);

            var pnlSmart1 = FindControl<Panel>(this.Controls, "pnlSmart1");
            var pnlSmart2 = FindControl<Panel>(this.Controls, "pnlSmart2");
            var hdnStart = FindControl<HiddenField>(this.Controls, "hdnStart");
            var hdnStartDrop = FindControl<HiddenField>(this.Controls, "hdnStartDrop");
            var hdnEnd = FindControl<HiddenField>(this.Controls, "hdnEnd");
            var txtCaseNotes = FindControl<RadTextBox>(this.Controls, "txtCaseNotes");
            SmartPadUserControl smartPad = ctrl as SmartPadUserControl;

            string durationInMinutes;

            var defaultTime = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd 00:00:00.000"));
            DateTime TimeStart = defaultTime,
                TimeEnd = defaultTime;

            if (hdnStart.Value != string.Empty || hdnStartDrop.Value != string.Empty)
            {
                TimeStart = Convert.ToDateTime(hdnStart.Value == "" ? hdnStartDrop.Value : hdnStart.Value);
                TimeEnd = Convert.ToDateTime(hdnEnd.Value);
            }

            TimeSpan span = new TimeSpan();
            span = TimeEnd.Subtract(TimeStart);
            durationInMinutes = string.Format("{0:D2}:{1:D2}:{2:D2}", span.Minutes, span.Seconds, span.Milliseconds);

            var strCallType = string.Empty;
            var strIssue = string.Empty;
            var savingValueTemp = durationInMinutes;

            if (smartPad.dtCallTypes.Rows.Count > 0)
            {
                foreach (DataRow row in smartPad.dtCallTypes.Rows)
                {
                    strCallType = row["CallType"].ToString();
                    strIssue = row["Issue"].ToString();
                    savingValueTemp += "\\Call Type: " + strCallType + "\\Issue: " + strIssue + "\\Case Notes:\n";
                }
            }
            {
                savingValueTemp += "\\Call Type:\\Issue:\\Case Notes:\n";
            }

            savingValueTemp += txtCaseNotes.Text + "\\"; //+smartPad.rdoValueDuring == string.Empty ? "None\\None" : smartPad.rdoValueDuring + "\\" + "None";

            if (smartPad.rdoValueDuring == string.Empty)
            {
                savingValueTemp += "None\\None";
            }
            else
            {
               savingValueTemp +=  smartPad.rdoValueDuring + "\\" + "None";
            }

            smartPad.savingValue = isDropCall == false ? "Finished" + "\\" + savingValueTemp : "Dropped" + "\\" + savingValueTemp;


            var chkDiscussed = FindControl<CheckBox>(this.Controls, "chkDiscussed");
            var cboCountry = FindControl<RadComboBox>(this.Controls, "cboCountry");
            var cboSite = FindControl<RadComboBox>(this.Controls, "cboSite");
            var cboSentiment = FindControl<RadComboBox>(this.Controls, "cboSentiment");
            var cboAccountHolder = FindControl<RadComboBox>(this.Controls, "cboAccountHolder");
            var cboClassification = FindControl<RadComboBox>(this.Controls, "cboClassification");
            var txtECMCaseNumber = FindControl<RadTextBox>(this.Controls, "txtECMCaseNumber");

            var cboFeeProcess = FindControl<RadComboBox>(this.Controls, "cboFeeProcess");
            var cboFeeApplicable = FindControl<RadComboBox>(this.Controls, "cboFeeApplicable");
            var cboFeeNotApplicable = FindControl<RadComboBox>(this.Controls, "cboFeeNotApplicable");
            var cboFeeApplicableNonAcct = FindControl<RadComboBox>(this.Controls, "cboFeeApplicableNonAcct");


            var country = chkDiscussed.Checked == true ? cboCountry.SelectedItem.Text : "N/A";
            var site = chkDiscussed.Checked == true ? cboSite.SelectedItem.Text : "N/A";
            var sentiment = chkDiscussed.Checked == true ? cboSentiment.SelectedItem.Text : "N/A";
            var accountholder = chkDiscussed.Checked == true ? cboAccountHolder.SelectedItem.Text : "N/A";
            var classification = chkDiscussed.Checked == true ? cboClassification.SelectedItem.Text : "N/A";
            var ECMNumber = chkDiscussed.Checked == true ? txtECMCaseNumber.Text : "N/A";
            var isPickupFeeDiscussed = chkDiscussed.Checked == true ? "Yes" : "No";

            var FeeProcess = chkDiscussed.Checked == true ? cboFeeProcess.SelectedItem.Text : "N/A";
            var FeeApplicable = chkDiscussed.Checked == true ? cboFeeApplicable.SelectedItem.Text : "N/A";
            var FeeNotApplicable = chkDiscussed.Checked == true ? cboFeeNotApplicable.SelectedItem.Text : "N/A";
            var FeeApplicableNonAcct = chkDiscussed.Checked == true ? cboFeeApplicableNonAcct.SelectedItem.Text : "N/A";


            ECMNumber = ECMNumber.Trim() == "" ? "None" : ECMNumber;
            var strPickupFee = "PickupFeeDiscussed: " + isPickupFeeDiscussed + "\\Country: " + country + "\\Site: " + site + "\\Sentiment: " +
                sentiment + "\\AccountHolder: " + accountholder + "\\Classification: " + classification + "\\ECMNumber: " + ECMNumber + "\\FeeProcess: " +
                FeeProcess + "\\FeeApplicable: " + FeeApplicable + "\\FeeNotApplicable: " + FeeNotApplicable + "\\FeeApplicableNonAcct: " + FeeApplicableNonAcct + "\\";


            if (formID == UK_SmartPad)
            {
                smartPad.savingValue = strPickupFee + smartPad.savingValue;
            }
        }

        void SaveRecord(bool isSmartPadForm, bool isDropCall)
        {
            pnlMain.Enabled = false;
            btnSubmit.Enabled = false;

            //for smartpad saving
            //if (isSmartPadForm)
            //{
            //    SaveSmartPadValues(isDropCall);
            //}



            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                Transaction tran = new Transaction();
                CallDispositionTool.Model.CallBack cb = new CallDispositionTool.Model.CallBack();
                CallBackHistoryLog log = new CallBackHistoryLog();
                TransactionCallData callData = new TransactionCallData();

                tran.FormID = Convert.ToInt32(Request["FormID"]);
                tran.CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                tran.CreatedOn = DateTime.Now;

                string _body = "";
                string _problema = "";
                string _topic = "";

                var _subject = "";

                foreach (CallDispositionTool.Model.Attribute item in _Attributes)
                {
                    tran.AttributeValues.Add(GetValueForAttribute(item.AttributeID, item.DataTypeID));
                    if (item.AttributeName != "Problema")
                    {


                        string[] MaximaForm = { "15", "16", "17", "18", "19", "20", "21", "22", "26", "50" };
                        if (MaximaForm.Contains(item.FormID.ToString()) == true)
                        {
                            //get value for email body
                            _body += String.Format("" + Environment.NewLine + " {0} : {1} " + Environment.NewLine + "",
                                    item.AttributeName,
                                        GetValueForAttribute(item.AttributeID, item.DataTypeID).Value.ToString());
                        }
                        else
                        {
                            //get value for email body
                            _body += String.Format("<br> <b> {0} </b>: {1} ",
                                    item.AttributeName,
                                        GetValueForAttribute(item.AttributeID, item.DataTypeID).Value.ToString());
                        }
                        if (item.AttributeID == 477)
                        {
                            _topic = GetValueForAttribute(item.AttributeID, item.DataTypeID).Value.ToString();
                        }

                        //if (item.AttributeID == 440)
                        if (item.AttributeID == 666)
                        {
                            _subject = GetValueForAttribute(item.AttributeID, item.DataTypeID).Value.ToString();
                        }
                    }
                    //else if (item.AttributeID == 477) // For SwedBank Topic Dropdown ID
                    //{

                    //}
                    else
                    {

                        //get value for email body
                        //_body += String.Format("<b> {0} </b> : {1} <br>",

                        if (item.FormID.ToString() == "37")
                        {
                            _body += String.Format("<br> <b> {0} </b>: {1} ",
                                    item.AttributeName,
                                        GetValueForAttribute(item.AttributeID, item.DataTypeID).Value.ToString());
                        }
                        else
                        {
                            _body += String.Format("" + Environment.NewLine + " {0} : {1} " + Environment.NewLine + "",
                                    item.AttributeName,
                                        GetValueForAttribute(item.AttributeID, item.DataTypeID).Value.ToString());
                        }
                        _problema = GetValueForAttribute(item.AttributeID, item.DataTypeID).Value.ToString();
                    }
                }

                if (chkCallBackRequired.SelectedItem != null)
                {
                    if (chkCallBackRequired.SelectedItem.Text == "Yes")
                    {
                        cb.CallBackDate = CallBackUserControl1.CallBackDateTime.Value;
                        cb.TimezoneID = CallBackUserControl1.TimeZoneID;
                        cb.ContactNumber = CallBackUserControl1.ContactNumber;
                        cb.CallBackReasonID = CallBackUserControl1.CallBackReasonID;

                        if (CallBackUserControl1.AssignedTo > 0)
                        {
                            cb.AssignedTo = CallBackUserControl1.AssignedTo;
                            cb.AssignedReasonID = CallBackUserControl1.AssignedReasonID;
                        }

                        cb.Notes = CallBackUserControl1.Notes;
                        cb.CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                        cb.CreatedOn = DateTime.Now;
                        cb.CallBackStatusID = 1;

                        log.Action = "Create";
                        log.ActionLog = "Call Back Created";
                        log.CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                        log.CreatedOn = DateTime.Now;

                        cb.CallBackHistoryLogs.Add(log);
                        tran.CallBacks.Add(cb);


                        //adding callback required in email body
                        var _timezone = (from p in db.Timezones where p.TimezoneID == cb.TimezoneID select new { p.Abbreviation }).First();
                        var _callbackreason = (from t in db.CallBackReasons where t.CallBackReasonID == cb.CallBackReasonID select new { t.Description }).First();

                        _body += "\r\nCall Back Required \r\n";
                        _body += String.Format("Call Back Date : {0} \r\n", cb.CallBackDate);
                        _body += String.Format("Time Zone : {0} \r\n", _timezone.Abbreviation);
                        _body += String.Format("Contact Number : {0} \r\n", cb.ContactNumber);
                        _body += String.Format("Call Back Reason : {0} \r\n", _callbackreason.Description);

                        if (CallBackUserControl1.AssignedTo > 0)
                        {
                            var _assignedto = (from p in db.Users where p.UserID == cb.AssignedTo select new { p.FirstName, p.LastName }).First();
                            var _assignedreasonid = (from p in db.CallBackAssignmentReasons where p.CallBackAssignmentReasonID == CallBackUserControl1.AssignedReasonID select new { p.Description }).First();

                            _body += String.Format("Specific Assigned To : {0},{1} \r\n", _assignedto.LastName, _assignedto.FirstName);
                            _body += String.Format("Reason for Assignment : {0} \r\n", _assignedreasonid.Description);
                        }
                    }
                }


                if (pnlContact.Visible)
                {
                    TransactionContact tc = new TransactionContact();

                    tc.ContactID = ContactUserControl1.ContactID;
                    tc.CreateDate = DateTime.Now;
                    tran.TransactionContact= tc;
                }

                if (chkCallBackRequired.SelectedItem != null)
                {
                    if (chkCallBackRequired.SelectedItem.Text == "Yes")
                    {
                        cb.CallBackDate = CallBackUserControl1.CallBackDateTime.Value;
                        cb.TimezoneID = CallBackUserControl1.TimeZoneID;
                        cb.ContactNumber = CallBackUserControl1.ContactNumber;
                        cb.CallBackReasonID = CallBackUserControl1.CallBackReasonID;

                        if (CallBackUserControl1.AssignedTo > 0)
                        {
                            cb.AssignedTo = CallBackUserControl1.AssignedTo;
                            cb.AssignedReasonID = CallBackUserControl1.AssignedReasonID;
                        }

                        cb.Notes = CallBackUserControl1.Notes;
                        cb.CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                        cb.CreatedOn = DateTime.Now;
                        cb.CallBackStatusID = 1;

                        log.Action = "Create";
                        log.ActionLog = "Call Back Created";
                        log.CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                        log.CreatedOn = DateTime.Now;

                        cb.CallBackHistoryLogs.Add(log);
                        tran.CallBacks.Add(cb);
                    }
                }

                //Validate and save call data info
                if (Request["CallerID"] != null && Request["DNIS"] != null && Request["CallID"] != null && Request["Queue"] != null && Request["DN"] != null)
                {
                    try
                    {
                        callData.CallerID = Request["CallerID"];
                        callData.DNIS = Request["DNIS"];
                        callData.CallID = Request["CallID"];
                        callData.Queue = Request["Queue"];
                        callData.DN = Request["DN"];
                        tran.TransactionCallData = callData;
                    }
                    catch (Exception ex)
                    {
                        Helper.LogError(ex);
                        RadWindowManager1.RadAlert(
                            ex.Message,
                            330,
                            180,
                            "Error",
                            "");
                    }
                }

                int SignOffUserID;
                if (int.TryParse(hdnSignOffUserID.Value, out SignOffUserID))
                {
                    if (SignOffUserID > 0)
                    {
                        TransactionSignOff ts = new TransactionSignOff();
                        ts.CreatedOn = DateTime.Now;
                        ts.UserID = SignOffUserID;
                        tran.TransactionSignOff = ts;
                    }
                }

                //db.Transactions.AddObject(tran);

                try
                {

                    //db.SaveChanges();
                    //commit database transaction
                    //send an email to recipient
                    if (_subject == "")
                    {
                        _subject = db.Forms.Where(p => p.FormID == tran.FormID).Select(p => p.FormDescription).FirstOrDefault().ToString();
                    }

                    //foreach (CallDispositionTool.Model.Attribute item in _Attributes)
                    //{
                    //    if (item.AttributeName == "Info veids")
                    //    {
                    //        //RadComboBox i = item as RadComboBox;
                    //        //_subject = i.SelectedItem.Text.ToString();
                    //        break;
                    //    }
                    //}

                    int _userid = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                    int _formid = Convert.ToInt32(Request["FormID"]);
                    string _transactionid = tran.TransactionID.ToString();
                    string sendreport = MailSending.Send(_subject, _body, _userid, _formid, _problema, _topic, _transactionid);

                    bool emailSent = Convert.ToBoolean(sendreport.Split('|')[0]);

                    tran.EmailSent = emailSent;
                    tran.ErrorMessage = (!emailSent) ? sendreport.Split('|')[1].ToString() : "";

                    db.Transactions.AddObject(tran);
                    db.SaveChanges();
                    
                    //For SmartPad
                    if (formID != UPS_SmartPad && formID != UK_SmartPad)
                    {
                        RadWindowManager1.RadAlert(
           "Transaction ID # <strong>" + tran.TransactionID.ToString() + " </strong> has been saved.",
           330,
           180,
           "Save Success",
           "PageReset");
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "RefreshPage", "window.location.reload();", true);
                    }
                }
                catch (Exception ex)
                {
                    string errorMsg = ex.Message;
                    if (ex.InnerException.Message != null)
                    {
                        errorMsg += Environment.NewLine + "Inner exception: " + ex.InnerException.Message;
                    }

                    RadWindowManager1.RadAlert(
                        errorMsg,
                        330,
                        180,
                        "Error",
                        "");

                    Helper.LogError(ex);
                }

                if (cb.AssignedTo.HasValue)
                {
                    try
                    {
                        //Send real time notification to assigned user
                        var context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
                        string msg = String.Format("You have pending call back.<br><strong>Contact Number:</strong>{3}. <br><strong>Call Back Date/Time:</strong> {1}<br>Click <a href='{2}/CallBack.aspx?CallBack={0}&Assigned=1'>here</a> to view details", cb.CallBackID, cb.CallBackDate.ToString(), WebConfigurationManager.AppSettings["ApplicationURL"], cb.ContactNumber);
                        context.Clients.All.showAssignedNotification(Helper.GetEmployeeIDByUserID(cb.AssignedTo.Value), msg);
                    }
                    catch (Exception ex)
                    {
                        RadWindowManager1.RadAlert(
                            ex.Message,
                            330,
                            180,
                            "Error",
                            "");

                        Helper.LogError(ex);
                    }
                }
            }
        }

        AttributeValue GetValueForAttribute(int AttributeID, int DataTypeID)
        {
            AttributeValue value = new AttributeValue();
            Control ctrl = formPlaceHolder.FindControl("Ctr_" + AttributeID);
            DataTypeIDEnum myEnum = (DataTypeIDEnum)DataTypeID;

            value.AttributeID = AttributeID;

            switch (myEnum)
            {
                case DataTypeIDEnum.String:
                    RadTextBox tb = ctrl as RadTextBox;
                    value.Value = tb.Text.Trim();
                    break;
                case DataTypeIDEnum.Boolean:
                    CheckBox cb = ctrl as CheckBox;
                    value.Value = cb.Checked.ToString();
                    break;
                case DataTypeIDEnum.Numeric:
                    RadNumericTextBox num = ctrl as RadNumericTextBox;
                    value.Value = num.Text;
                    break;
                case DataTypeIDEnum.Date:
                    RadDatePicker dtp = ctrl as RadDatePicker;
                    value.Value = dtp.SelectedDate.Value.ToString();
                    break;
                case DataTypeIDEnum.DropdownList:
                    RadComboBox cbo = ctrl as RadComboBox;
                    value.Value = cbo.SelectedItem.Text;
                    break;
                case DataTypeIDEnum.Listbox:
                    RadListBox lst = ctrl as RadListBox;

                    foreach (RadListBoxItem item in lst.CheckedItems)
                    {
                        value.Value += item.Text + "|";
                    }

                    if (value.Value.Length > 0)
                        value.Value.Remove(value.Value.Length - 1, 1);

                    break;
                case DataTypeIDEnum.Disposition:
                    DispositionUserControl disp = ctrl as DispositionUserControl;
                    value.Value = disp.DispositionValue;
                    break;
                case DataTypeIDEnum.DropdownTreeView:
                    DropdownTreeViewUserControl tree = ctrl as DropdownTreeViewUserControl;
                    value.Value = tree.DispositionValue;
                    break;
                case DataTypeIDEnum.Contact:
                    ContactUserControl contactUC = ctrl as ContactUserControl;
                    value.Value = contactUC.ContactInfo;
                    break;
                case DataTypeIDEnum.TimerAuto:
                    TimerAutoUserControl timerAuto = ctrl as TimerAutoUserControl;
                    value.Value = timerAuto.ElapsedTime;
                    break;
                case DataTypeIDEnum.TimerManual:
                    TimerManualUserControl timerManual = ctrl as TimerManualUserControl;
                    value.Value = timerManual.ElapsedTime;
                    break;
                case DataTypeIDEnum.SmartPad:
                    // User Smart Pad
                    SmartPadUserControl smartPad = ctrl as SmartPadUserControl;
                    value.Value = smartPad.savingValue;
                    break;
                case DataTypeIDEnum.CustomerSatisfactionRadioButton:
                    CustomerSatisfactionUserControl csUC = ctrl as CustomerSatisfactionUserControl;
                    value.Value = csUC.CustomerSatisfaction;
                    break;
                case DataTypeIDEnum.AssignToUserControl:
                    //AssignToUserControl atc = ctrl as AssignToUserControl;
                    value.Value = string.Empty;
                    break;
                case DataTypeIDEnum.UserCaptionUserControl:
                    UserCaptionUserControl ucuc = ctrl as UserCaptionUserControl;
                    value.Value = ucuc.Username;
                    break;


                default:
                    break;
            }

            return value;
        }

        protected void chkCallBackRequired_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chkCallBackRequired.SelectedIndex == 0)
            {
                CallBackUserControl1.Visible = true;
            }
            else if (chkCallBackRequired.SelectedIndex == 1)
            {
                CallBackUserControl1.Visible = false;
            }
        }

        protected void TabStrip1_TabClick(object sender, RadTabStripEventArgs e)
        {
            if (TabStrip1.SelectedIndex == 1)
            {
                grdTransaction.Rebind();
            }
        }

        protected void grdTransaction_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                ImageButton editLink = (ImageButton)e.Item.FindControl("EditLink");

                if (editLink != null)
                {
                    editLink.Attributes["href"] = "javascript:void(0);";
                    editLink.Attributes["target"] = "_blank";
                    editLink.OnClientClick = String.Format("return ViewTransaction('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["TransactionID"], e.Item.ItemIndex);
                }
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdTransaction.MasterTableView.SortExpressions.Clear();
                grdTransaction.MasterTableView.GroupByExpressions.Clear();
                grdTransaction.MasterTableView.CurrentPageIndex = grdTransaction.MasterTableView.PageCount - 1;
                grdTransaction.Rebind();
            }
            else if (e.Argument == "RefreshContact")
            {
                ContactUserControl1.RefreshContact(Convert.ToInt32(hdnContactID.Value));
            }
            else if (e.Argument == "SaveAndSignOffRecord")
            {
                SaveRecord(false, false);
            }
        }

        [WebMethod]
        public static SearchBoxItemData[] GetResults(SearchBoxContext context)
        {
            dynamic contacts = Helper.GetContactSearch(context.Text, context.SelectedContextItem.Key);
            List<SearchBoxItemData> result = new List<SearchBoxItemData>();

            foreach (var item in contacts)
            {
                SearchBoxItemData itemData = new SearchBoxItemData();
                itemData.Value = Convert.ToString(item.GetType().GetProperty("ContactID").GetValue(item, null));

                if (context.SelectedContextItem.Key == "Name")
                {
                    itemData.Text = item.GetType().GetProperty("FirstName").GetValue(item, null) + " " + item.GetType().GetProperty("LastName").GetValue(item, null) + " | " + item.GetType().GetProperty("Phone").GetValue(item, null);
                }
                else if (context.SelectedContextItem.Key == "Name2")
                {
                    itemData.Text = item.GetType().GetProperty("FirstName").GetValue(item, null) + " " + item.GetType().GetProperty("LastName").GetValue(item, null) + " | " + item.GetType().GetProperty("Phone").GetValue(item, null);
                }
                else if (context.SelectedContextItem.Key == "Phone")
                {
                    itemData.Text = item.GetType().GetProperty("Phone").GetValue(item, null) + " | " + item.GetType().GetProperty("FirstName").GetValue(item, null) + " " + item.GetType().GetProperty("LastName").GetValue(item, null);
                }
                else if (context.SelectedContextItem.Key == "Phone2")
                {
                    itemData.Text = item.GetType().GetProperty("Phone").GetValue(item, null) + " | " + item.GetType().GetProperty("FirstName").GetValue(item, null) + " " + item.GetType().GetProperty("LastName").GetValue(item, null);
                }

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["ContactID"] = item.GetType().GetProperty("ContactID").GetValue(item, null);
                dic["FirstName"] = item.GetType().GetProperty("FirstName").GetValue(item, null);
                dic["LastName"] = item.GetType().GetProperty("LastName").GetValue(item, null);
                dic["Phone"] = item.GetType().GetProperty("Phone").GetValue(item, null);
                dic["Address"] = item.GetType().GetProperty("Address").GetValue(item, null);
                dic["StoreNumber"] = item.GetType().GetProperty("StoreNumber").GetValue(item, null);
                dic["NTLoginID"] = item.GetType().GetProperty("NTLoginID").GetValue(item, null);
                dic["TransactionID"] = item.GetType().GetProperty("TransactionID").GetValue(item, null);

                itemData.DataItem = dic;

                result.Add(itemData);
            }

            return result.ToArray();
        }

        private static T FindControl<T>(ControlCollection controls, string controlId)
        {
            T ctrl = default(T);
            foreach (Control ctl in controls)
            {
                if (ctl.GetType() != typeof(Telerik.Web.UI.GridTableRow))
                {
                    if (ctl.ClientID.Length <= controlId.Length)
                    {
                        if (ctl.GetType() == typeof(T) && ctl.ClientID == controlId)
                        {
                            ctrl = (T)Convert.ChangeType(ctl, typeof(T), CultureInfo.InvariantCulture);
                            return ctrl;

                        }
                    }
                    else
                    {
                        if (ctl.GetType() == typeof(T) && ctl.ClientID.Substring(ctl.ClientID.Length - controlId.Length) == controlId)
                        {
                            ctrl = (T)Convert.ChangeType(ctl, typeof(T), CultureInfo.InvariantCulture);
                            return ctrl;

                        }
                    }
                }


                if (ctl.Controls.Count > 0 && ctrl == null)
                    ctrl = FindControl<T>(ctl.Controls, controlId);
            }
            return ctrl;
        }

    }
}
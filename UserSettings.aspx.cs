﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using CallDispositionTool.Model;
using System.Web.Security;

namespace CallDispositionTool
{
    public partial class UserSettings : NotificationPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                HttpCookie ck = new HttpCookie("UserInfo");

                if (ck != null)
                {
                    cboSkin.SelectedIndex = cboSkin.FindItemIndexByText(ck["Skin"]);
                }
            }
        }

        protected void cboSkin_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            HttpCookie ck = new HttpCookie("UserInfo");

            ck.Values["Skin"] = cboSkin.SelectedItem.Text;
            ck.Expires = DateTime.Now.AddYears(1);
            
            Request.Cookies.Remove("UserInfo");
            Request.Cookies.Add(ck);
            (Master.FindControl("skinManager1") as RadSkinManager).Skin = cboSkin.SelectedItem.Text;
        }
        protected void ChangePassword1_ChangedPassword(object sender, EventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                UserPasswordHistory record = new UserPasswordHistory();

                record.UserID = Convert.ToInt32(Membership.GetUser(ChangePassword1.UserName).ProviderUserKey);
                record.PasswordHash = Security.PasswordHash.CreateHash(ChangePassword1.NewPassword);
                record.CreatedOn = DateTime.Now;

                lblStatus.Text = string.Empty;

                db.UserPasswordHistories.AddObject(record);
                db.SaveChanges();
            }
        }

        bool HasDuplicatePassword(string EmployeeID, string oldPassword)
        {
            bool returnValue = false;

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var pwds = (from p in db.UserPasswordHistories
                            where p.User.EmployeeID == EmployeeID
                            orderby p.CreatedOn descending
                            select new { p.PasswordHash }).Take(7).ToList();

                foreach (var item in pwds)
                {
                    if (CallDispositionTool.Security.PasswordHash.ValidatePassword(oldPassword, item.PasswordHash))
                    {
                        returnValue = true;
                        break;
                    }
                }
            }

            return returnValue;
        }

        protected void ChangePassword1_ChangingPassword(object sender, LoginCancelEventArgs e)
        {
            if (HasDuplicatePassword(ChangePassword1.UserName, ChangePassword1.NewPassword))
            {
                lblStatus.Text = "Your new password must not equal to your past 7 old passwords";
                e.Cancel = true;
            }
            else
            {
                lblStatus.Text = "";
            }
        }

        protected void ChangePassword1_ContinueButtonClick(object sender, EventArgs e)
        {
            //FormsAuthentication.SignOut();
            Response.Redirect(String.Format("~/PasswordChangeQuestionAndAnswer.aspx?Username={0}", ChangePassword1.UserName));
        }

        protected void ChangePassword1_CancelButtonClick(object sender, EventArgs e)
        {
            Response.Redirect("~/Login.aspx");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace CallDispositionTool.Admin
{
    public partial class LogInAsAnotherUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Membership.ValidateUser(txtUsername.Text, txtPassword.Text))
            {
                MembershipUser loginAsUser = Membership.GetUser(txtLoginAsUsername.Text);

                if (loginAsUser != null)
                {
                    FormsAuthentication.RedirectFromLoginPage(txtLoginAsUsername.Text, false);
                }
                else
                {
                    lblErrorMessage.Text = "Only Admins can log into the site as another user. To login as yourself, please visit the standard Login page.";
                }
            }
            else
            {
                lblErrorMessage.Text = "Invalid Username or Password.";
            }
        }
    }
}
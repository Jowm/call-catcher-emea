﻿using System;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Linq;
using CallDispositionTool.Model;
using CallDispositionTool.Security;
using System.Collections.Generic;
using Telerik.Web.UI;
using Telerik.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace CallDispositionTool.Admin
{
    public partial class FormPropertyManagement : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            HttpCookie ck = Request.Cookies["UserInfo"];
            string skinID = string.Empty;

            if (ck != null)
            {
                skinID = ck.Values["Skin"];
                this.skinManager1.Skin = skinID;
            }
            else
            {
                ck = new HttpCookie("UserInfo");
                ck.Values["Skin"] = this.skinManager1.Skin;
                Response.Cookies.Remove("UserInfo");
                Response.Cookies.Add(ck);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindListBox();
            }
        }

        public void BindListBox()
        {
            int FormID;

            if (int.TryParse(Request["FormID"], out FormID))
            {
                if (FormID > 0)
                {
                    using (CallDispositionEntities db = new CallDispositionEntities())
                    {
                        lstSource.DataTextField = "PropertyName";
                        lstSource.DataValueField = "PropertyName";
                        lstDestination.DataTextField = "PropertyName";
                        lstDestination.DataValueField = "PropertyName";

                        var destination = (from p in db.FormPropertyLookups
                                           where (from fp in db.FormProperties
                                                  where fp.FormID == FormID
                                                  select fp.PropertyName).Contains(p.PropertyName)
                                           orderby p.PropertyName
                                           select new { p.FormPropertyLookupID, p.PropertyName }).ToList();

                        var source = (from p in db.FormPropertyLookups
                                      where !(from fp in db.FormProperties
                                              where fp.FormID == FormID
                                              select fp.PropertyName).Any<string>(a => a == p.PropertyName)
                                      orderby p.PropertyName
                                      select new { p.FormPropertyLookupID, p.PropertyName }).ToList();

                        lstSource.DataSource = source;
                        lstSource.DataBind();
                        lstDestination.DataSource = destination;
                        lstDestination.DataBind();
                    }
                }
            }
        }

        protected void lstDestination_OnInserted(object sender, RadListBoxEventArgs e)
        {
            int FormID;
            int CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);

            if (int.TryParse(Request["FormID"], out FormID))
            {
                if (FormID > 0)
                {
                    using (CallDispositionEntities db = new CallDispositionEntities())
                    {
                        foreach (RadListBoxItem item in e.Items)
                        {
                            FormProperty fp = new FormProperty();

                            fp.PropertyName = item.Value;
                            fp.FormID = FormID;
                            fp.CreatedBy = CreatedBy;
                            fp.CreatedOn = DateTime.Now;
                            db.FormProperties.AddObject(fp);
                        }

                        CacheFactory.GetCacheManager().Remove("HasContactFeature~" + FormID.ToString());
                        CacheFactory.GetCacheManager().Remove("HasLinkToLiberator~" + FormID.ToString());
                        CacheFactory.GetCacheManager().Remove("HasCallBackFeature~" + FormID.ToString());
                        CacheFactory.GetCacheManager().Remove("HasTransferCallFeature~" + FormID.ToString());
                        CacheFactory.GetCacheManager().Remove("HasAudienceManagerFeature~" + FormID.ToString());

                        db.SaveChanges();
                    }
                }
            }
        }

        protected void lstSource_OnInserted(object sender, RadListBoxEventArgs e)
        {
            int FormID;

            if (int.TryParse(Request["FormID"], out FormID))
            {
                if (FormID > 0)
                {
                    using (CallDispositionEntities db = new CallDispositionEntities())
                    {
                        int CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);

                        foreach (RadListBoxItem item in e.Items)
                        {
                            string PropertName = item.Value;

                            FormProperty fp = (from f in db.FormProperties
                                               where f.FormID == FormID && f.PropertyName.Contains(PropertName)
                                               select f).FirstOrDefault();

                            if (fp != null)
                            {
                                db.FormProperties.DeleteObject(fp);
                            }
                        }

                        CacheFactory.GetCacheManager().Remove("HasContactFeature~" + FormID.ToString());
                        CacheFactory.GetCacheManager().Remove("HasLinkToLiberator~" + FormID.ToString());
                        CacheFactory.GetCacheManager().Remove("HasCallBackFeature~" + FormID.ToString());
                        CacheFactory.GetCacheManager().Remove("HasTransferCallFeature~" + FormID.ToString());
                        CacheFactory.GetCacheManager().Remove("HasAudienceManagerFeature~" + FormID.ToString());

                        db.SaveChanges();
                    }
                }
            }
        }
    }
}
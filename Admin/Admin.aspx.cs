﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Linq;
using CallDispositionTool.Model;
using CallDispositionTool.Security;
using System.Collections.Generic;
using Telerik.Web.UI;
using System.Web.Security;

namespace CallDispositionTool.Admin
{
    public partial class AdminPage : FormPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    var myRoles = (from r in db.UserRoles
                                   where r.UserID == this.UserID
                                   select new { r.RoleID }).ToList();


                    foreach (var item in myRoles)
                    {
                        RoleEnum r = (RoleEnum)item.RoleID;

                        switch (r)
                        {
                            case RoleEnum.AdminRole:
                                TabStrip1.Tabs[0].Visible = true;
                                TabStrip1.Tabs[1].Visible = true;
                                TabStrip1.Tabs[2].Visible = true;
                                TabStrip1.Tabs[3].Visible = true;
                                break;
                            case RoleEnum.PickListOptionManager:
                                TabStrip1.Tabs[1].Visible = true;
                                break;
                            case RoleEnum.DispositionManager:
                                TabStrip1.Tabs[2].Visible = true;
                                break;
                            case RoleEnum.UserAccessGrantor:
                                TabStrip1.Tabs[0].Visible = true;
                                break;
                            case RoleEnum.FormManager:
                                TabStrip1.Tabs[3].Visible = true;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
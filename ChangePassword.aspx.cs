﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Net.Mail;
using CallDispositionTool.Model;

namespace CallDispositionTool
{
    public partial class ChangePassword : Page
    {
        #region _PasswordChangeNotificationBody
        const string _PasswordChangeNotificationBody = @"
                <html>
                    <body>
                    <h2>Your Password Has Been Change!</h2>
                    <p>
                    This email confirms that your password has been change.
                    </p>
                    <p>
                    To log on to the Transcom Call Disposition Tool, use the following credentials:
                    </p>
                    <table>
                    <tr>
                    <td>
                    <b>Username:</b>
                    </td>
                    <td>
                    {0}
                    </td>
                    </tr>
                    <tr>
                    <td>
                    <b>Password:</b>
                    </td>
                    <td>
                    {1}
                    </td>
                    </tr>
                    </table>
                    <p>
                    If you have any questions or encounter any problems logging in,
                    please contact a site administrator.
                    </p>
                    </body>
                </html>
            ";
        #endregion

        void SendPasswordChangeNotification(string UserEmailAddress, string userName, string password)
        {
            MailAddress mTo = new MailAddress(UserEmailAddress);
            MailAddress mFrom = new MailAddress("webforms@transcom.com");
            MailMessage m = new MailMessage(mFrom, mTo);

            m.Subject = "Transcom Call Disposition Tool - Password Reset Notification";
            m.Body = String.Format(_PasswordChangeNotificationBody, userName, password);
            m.IsBodyHtml = true;

            SmtpClient client = new SmtpClient();
            //client.EnableSsl = true;
            client.Send(m);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ChangePassword1_ChangedPassword(object sender, EventArgs e)
        {
            try
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    MembershipUser user = Membership.GetUser(ChangePassword1.UserName);

                    if (user != null)
                    {
                        UserPasswordHistory record = new UserPasswordHistory();

                        record.UserID = Convert.ToInt32(Membership.GetUser(ChangePassword1.UserName).ProviderUserKey);
                        record.PasswordHash = Security.PasswordHash.CreateHash(ChangePassword1.NewPassword);
                        record.CreatedOn = DateTime.Now;

                        lblStatus.Text = string.Empty;

                        db.UserPasswordHistories.AddObject(record);
                        db.SaveChanges();

                        try
                        {
                            SendPasswordChangeNotification(user.Email, user.UserName, user.PasswordQuestion);
                        }
                        catch (Exception ex)
                        {
                            Helper.LogError(ex);
                            lblStatus.Text = String.Format("Error Sending Email to {0}: {1}" + user.Email, ex.Message);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                lblStatus.Text = "Error: " + ex.Message;
            }
        }

        bool HasDuplicatePassword(string EmployeeID, string oldPassword)
        {
            bool returnValue = false;

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var pwds = (from p in db.UserPasswordHistories
                            where p.User.EmployeeID == EmployeeID
                            orderby p.CreatedOn descending
                            select new { p.PasswordHash }).Take(7).ToList();

                foreach (var item in pwds)
                {
                    if (CallDispositionTool.Security.PasswordHash.ValidatePassword(oldPassword, item.PasswordHash))
                    {
                        returnValue = true;
                        break;
                    }                                 
                }
            }

            return returnValue;
        }

        protected void ChangePassword1_SendingMail(object sender, MailMessageEventArgs e)
        {
            //MailAddress mTo = new MailAddress(e.Message.To.ToString());
            //MailAddress mFrom = new MailAddress(e.Message.From.ToString());
            //MailMessage m = new MailMessage(mFrom, mTo);  

            //m.Subject = e.Message.Subject;
            //m.Body = e.Message.Body;
            //m.IsBodyHtml = true;

            //SmtpClient client = new SmtpClient();
            ////client.EnableSsl = true;
            //client.Send(m);

            //e.Cancel = true;
        }

        protected void ChangePassword1_ChangingPassword(object sender, LoginCancelEventArgs e)
        {
            if (HasDuplicatePassword(ChangePassword1.UserName, ChangePassword1.NewPassword))
            {
                lblStatus.Text = "Your new password must not equal to your past 7 old passwords";
                e.Cancel = true;
            }
            else
            {
                lblStatus.Text = "";
            }
        }

        protected void ChangePassword1_ContinueButtonClick(object sender, EventArgs e)
        {
            //FormsAuthentication.SignOut();
            Response.Redirect(String.Format("~/PasswordChangeQuestionAndAnswer.aspx?Username={0}", ChangePassword1.UserName));
        }

        protected void ChangePassword1_CancelButtonClick(object sender, EventArgs e)
        {
            Response.Redirect("~/Login.aspx");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using CallDispositionTool.UserControl;
using Telerik.Web.UI;
using System.Web.Security;
using System.Web.Configuration;

namespace CallDispositionTool
{
    public partial class TransactionPopup : System.Web.UI.Page
    {
        List<CallDispositionTool.Model.Attribute> _Attributes;

        public decimal TransactionID { get; set; }
        public decimal CallBackID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            HttpCookie ck = Request.Cookies["UserInfo"];
            string skinID = string.Empty;

            if (ck != null)
            {
                skinID = ck.Values["Skin"];
                this.skinManager1.Skin = skinID;
            }
            else
            {
                ck = new HttpCookie("UserInfo");
                ck.Values["Skin"] = this.skinManager1.Skin;
                Response.Cookies.Remove("UserInfo");
                Response.Cookies.Add(ck);
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            CallBackID = Convert.ToInt32(Request["CallBackID"]);

            if (CallBackID > 0)
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    var query = (from p in db.CallBacks
                                 where p.CallBackID == CallBackID
                                 select new { p.TransactionID, p.Transaction.FormID }).FirstOrDefault();

                    int formID = query.FormID;
                    TransactionID = query.TransactionID;

                    _Attributes = (from p in db.Attributes
                                   where p.FormID == formID
                                   select p).ToList<CallDispositionTool.Model.Attribute>();

                    foreach (CallDispositionTool.Model.Attribute item in _Attributes)
                    {
                        AddCustomAttribute(item);
                    }
                }
            }
        }

        void AddCustomAttribute(CallDispositionTool.Model.Attribute attribute)
        {
            //Add a row to CustomUITable
            TableRow tr = new TableRow();
            TableCell tdName = new TableCell();

            //Add the name as the left cell
            tdName.Text = "<h4>" + attribute.AttributeName + "</h4>";
            tdName.VerticalAlign = VerticalAlign.Middle;
            tdName.Width = new Unit("100px");
            tr.Cells.Add(tdName);

            //Add the UI as the right cell
            //List<Control> UIControls = CreateCustomAttributeUI(AttributeID, AttributeName, DataTypeID, Mandatory, AttributeValue);
            TableCell tdValue = new TableCell();

            tdValue.VerticalAlign = VerticalAlign.Middle;
            tdValue.Text = GetAttributeValue(attribute.AttributeID, this.TransactionID);

            tr.Cells.Add(tdValue);

            CustomUITable.Rows.Add(tr);
        }

        string GetAttributeValue(int AttributeID, decimal TransactionID)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                AttributeValue attributeValue = (from p in db.AttributeValues
                                      where p.AttributeID == AttributeID && p.TransactionID == TransactionID
                                      select p).FirstOrDefault();

                return attributeValue.Value;
            }
        }
    }
}
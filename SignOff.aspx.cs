﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using System.Configuration;

namespace CallDispositionTool
{
    public partial class SignOff : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtUserName.Focus();

                int FormID = 0;
                int.TryParse(Session["SignOffFormID"].ToString(), out FormID);
                if (IsSpecialSignOffForm(FormID))
                {
                    lblHeader.InnerText = "Sign off";
                    lblInstructions.InnerText = "Please ask the person who will sign off to authenticate";
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            HttpCookie ck = Request.Cookies["UserInfo"];
            string skinID = string.Empty;

            if (ck != null)
            {
                skinID = ck.Values["Skin"];
                this.skinManager1.Skin = skinID;
            }
            else
            {
                ck = new HttpCookie("UserInfo");
                ck.Values["Skin"] = this.skinManager1.Skin;
                Response.Cookies.Remove("UserInfo");
                Response.Cookies.Add(ck);
            }
        }

        bool IsSpecialSignOffForm(int formID)
        {
            bool returnValue = false;

            string signOffIDs = ConfigurationManager.AppSettings["FormsWithSpecialSignOff"].ToString();
            List<string> formIDList = signOffIDs.Split(',').ToList();

            if (formIDList.Contains(formID.ToString()))
            {
                returnValue = true;
            }

            return returnValue;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            MembershipUser user = Membership.GetUser(txtUserName.Text);
            MembershipUser loggedInUser = Membership.GetUser();

            //Check if user exist
            if (user != null)
            {
                if (Membership.ValidateUser(txtUserName.Text.Trim(), txtPassword.Text.Trim()))
                {
                    user.LastLoginDate = DateTime.Now;
                    Membership.UpdateUser(user);
                    int UserID = Convert.ToInt32(user.ProviderUserKey);
                    int FormID = 0;
                    
                    int.TryParse(Session["SignOffFormID"].ToString(), out FormID);
                    hdnUserID.Value = UserID.ToString();

                    if (FormID > 0 && UserID > 0)
                    {
                        //if (Helper.IsSignOffAllowed(UserID, FormID))
                        if (Helper.IsSignOffAllAllowed(UserID, FormID))
                        {
                            RadWindowManager1.RadAlert(
                                "Sign Off Success!",
                                330,
                                180,
                                "Success",
                                String.Format("ProceedSignOff({0})", UserID));
                        }
                        else if (IsSpecialSignOffForm(FormID) && (user.ProviderUserKey != loggedInUser.ProviderUserKey) && Helper.IsSignOffAllowed(UserID, FormID))
                        {
                            RadWindowManager1.RadAlert(
                                "Sign Off Success!",
                                330,
                                180,
                                "Success",
                                String.Format("ProceedSignOff({0})", UserID));
                        }
                        else if (Helper.IsSignOffAllowed(UserID, FormID))
                        {
                            RadWindowManager1.RadAlert(
                                "Sign Off Success!",
                                330,
                                180,
                                "Success",
                                String.Format("ProceedSignOff({0})", UserID));
                        }
                        else
                        {
                            RadWindowManager1.RadAlert(
                                "User not allowed to sign off",
                                330,
                                180,
                                "Error Sign Off",
                                "");
                        }
                    }
                    else
                    {
                        RadWindowManager1.RadAlert(
                            "Invalid User or Form IDs, please close the browser and relogin to retain session.",
                            330,
                            180,
                            "Error Sign Off",
                            "");
                    }
                }
                else
                {
                    RadWindowManager1.RadAlert(
                        "Invalid Username or Password.",
                        330,
                        180,
                        "Error",
                        "");
                }
            }
            else
            {
                RadWindowManager1.RadAlert(
                    "Invalid Username or Password.",
                    330,
                    180,
                    "Error Login",
                    "");
            }
        }
    }
}
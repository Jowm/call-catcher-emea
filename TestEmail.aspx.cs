﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using CallDispositionTool.UserControl;
using Telerik.Web.UI;
using System.Web.Security;
using System.Web.Configuration;
using Microsoft.AspNet.SignalR;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace CallDispositionTool
{
    public partial class TestEmail : System.Web.UI.Page
    {
        List<CallDispositionTool.Model.Attribute> _Attributes;
        List<AttributeValue> _AttributeValues;

        const int BHN_RETENTION = 7;

        #region _FormEditNotificationBody
        const string _FormEditNotificationBody = @"
                <html>
                    <body>
                    <h2>Your Password Has Been Reset!</h2>
                    <p>
                    This email confirms that the form has been edited.
                    </p>
                    <p>
                    To log on to the Transcom Call Disposition Tool, use the following credentials:
                    </p>
                    <table>
                    <tr>
                        <td>
                            <b>Uzregistravo:</b>
                        </td>   
                    <td>
                    </td>
                    </tr>
                    </table>
                    <p>
                    If you have any questions or encounter any problems,
                    please contact a site administrator.
                    </p>
                    </body>
                </html>
            ";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Clicked(object sender, EventArgs e)
        {
            try
            {
                SendEmail();
            }
            catch (Exception ex)
            {
            }
        }

        void SendEmail()
        {
            MailAddressCollection TO_addressList = new MailAddressCollection();
            string emails = "martin.guco@transcom.com,noreply@transcom.com";
            //MailAddress mTo = new MailAddress("martin.guco@transcom.com,martin.guco@gmail.com");//emailAddress
            MailAddress mFrom = new MailAddress("noreply@transcom.com");
            //MailMessage m = new MailMessage(mFrom, mTo);
            /*            MailMessage m = new MailMessage();
                        m.From = mFrom;

                        foreach (string email in emails.Split(','))
                        {
                            //MailAddress mTo = new MailAddress(emails);
                            //m.To.Add(mTo);
                            m.To.Add(emails.Trim());
                        }
            */
            foreach (var curr_address in emails.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
            {
                MailAddress mytoAddress = new MailAddress(curr_address);
                TO_addressList.Add(mytoAddress);
            }

            MailMessage m = new MailMessage();

            m.From = mFrom;
            m.To.Add(TO_addressList.ToString());
            m.Subject = "Transcom Call Disposition Tool - Form Edit Notification";
            m.Body = String.Format(_FormEditNotificationBody);
            m.IsBodyHtml = true;
            SmtpClient client = new SmtpClient();
            //client.EnableSsl = true;
            client.Send(m);
            
        }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.UI.WebControls;
using System.Linq;
using CallDispositionTool.Model;
using System.Web.Security;
using System.Web.UI;
using System.Collections.Generic;
using Telerik.Web.UI;
using System.Web.Configuration;
using CallDispositionTool.CTI;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Web;

namespace CallDispositionTool
{
    public partial class FormSessionRedirect : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindForms();
            }
        }

        void BindForms()
        {
            cboForms.DataSource = Helper.GetFormsByUser(Membership.GetUser().UserName);
            cboForms.DataBind();
        }

        protected void btnLaunchForm_Click(object sender, EventArgs e)
        {
            CacheFactory.GetCacheManager().Add("SelectedFormIDbyEmployeeID~" + Membership.GetUser().UserName, cboForms.SelectedItem.Value);
            Session["FormID"] = cboForms.SelectedItem.Value;

            Response.Redirect(String.Format("~/Form.aspx?iframe={0}&CallerID={1}&DNIS={2}&CallID={3}&Queue={4}&BackURL=http{5}",
                Request["iframe"],
                Request["CallerID"],
                Request["DNIS"],
                Request["CallID"],
                Request["Queue"],
                Request["BackURL"]));
        }
    }
}
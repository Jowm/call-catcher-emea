﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Configuration;
using CallDispositionTool.Model;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Web.Security;
using System.Web.Configuration;
using Ionic.Zip;

public class MailSending
{
    public static string Send(string subject, string body, int? UserId, int? FormId, string problema, string TopicName, string TransactionID)
    {
        CallDispositionEntities db = new CallDispositionEntities();

        const string sender = "webforms@transcom.com";
        string recipient = "";

        if (IsUserExistForMail(UserId) && FormId != 37)
        //if (IsUserExistForMail(UserId))
        {
            //if (TopicName != null && TopicName != "")
            //{
            //    recipient = db.UserMails
            //    .Where(p => p.UserID == UserId && p.FormID == FormId && p.TopicName == TopicName)
            //    .Select(p => p.Recipient)
            //    .First();
            //}
            //else
            //{
            //    recipient = db.UserMails
            //    .Where(p => p.UserID == UserId && p.FormID == FormId)
            //    .Select(p => p.Recipient)
            //    .First();
            //}

            recipient = db.UserMails
                .Where(p => p.UserID == UserId && p.FormID == FormId)
                .Select(p => p.Recipient)
                .First();

            //recipient = "jowm824@gmail.com,john.orcullo@transcom.com";
            MailMessage _mail = new MailMessage();
            SmtpClient _smtpClient = new SmtpClient();

            string[] MaximaForm = { "15", "16", "17", "18", "19", "20", "21", "22", "26", "50" };
            if (MaximaForm.Contains(FormId.ToString()) == true)
            {
                _mail.IsBodyHtml = true;
                _mail.From = new MailAddress(sender);
                //_mail.To.Add("martin.guco@transcom.com");
                _mail.To.Add(recipient);
                _mail.Subject = subject;
                _mail.Body = "";

                /* The file storage in hdrive compose of nine folders, Lastnumber will determine which folder will the data be stored*/
                char Lastnumber = TransactionID[TransactionID.Length - 1];
                /* Textfile name must always be unique in order to create a text therefore we get the TransactionID which is database unique entity */
                string fileLocation = @"D:/MailFiles/" + Lastnumber.ToString() + "/" + TransactionID + ".txt";
                //string fileLocation = @"C:\Users\john.orcullo\Desktop\test/MailFiles/" + Lastnumber.ToString() + "/" + TransactionID + ".txt"; //test
                //    HttpContext.Current.Server.MapPath("~/MailFiles/" + Lastnumber.ToString() + "/data_" + TransactionID + ".txt");
                /* A code for creation of text file */
                System.IO.File.WriteAllText(fileLocation, body);

                /* A code for creation of zip file */
                using (ZipFile zip = new ZipFile())
                {

                    zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                    /* Contains the password to unlock the zip file that is attached in email. */
                    zip.AddFile(fileLocation).Password = "TranscomMaxima20!*";
                    zip.Encryption = EncryptionAlgorithm.WinZipAes256;
                    /* ZipFile name must always be unique in order to create a zip therefore we get the TransactionID which is database unique entity */
                    zip.Save(@"D:/MailFiles/" + Lastnumber.ToString() + "/" + TransactionID + ".zip");
                    //zip.Save(@"C:\Users\john.orcullo\Desktop\test/MailFiles/" + Lastnumber.ToString() + "/" + TransactionID + ".zip"); //test

                }
                /* Creation of string zipfile - this will contain the location of zipfile that is needed for email attachment */
                string zipfile = @"D:/MailFiles/" + Lastnumber.ToString() + "/" + TransactionID + ".zip";
                //string zipfile = @"C:\Users\john.orcullo\Desktop\test/MailFiles/" + Lastnumber.ToString() + "/" + TransactionID + ".zip"; //test
                Attachment data = new Attachment(zipfile);
                _mail.Attachments.Add(data);

                try
                {
                    _smtpClient.Send(_mail);
                    return "true";
                }
                catch (Exception ex)
                {
                    return "false" + ((ex.InnerException.Message != null || ex.InnerException.Message != "") ? "|" + ex.InnerException.Message : "|" + ex.Message);
                }
            }
            else
            {
                _mail.IsBodyHtml = true;
                _mail.From = new MailAddress(sender);
                //_mail.To.Add("martin.guco@transcom.com");
                _mail.To.Add(recipient);
                _mail.Subject = subject;
                _mail.Body = body;

                try
                {
                    _smtpClient.Send(_mail);
                    return "true";
                }
                catch (Exception ex)
                {
                    return "false" + ((ex.InnerException.Message != null || ex.InnerException.Message != "") ? "|" + ex.InnerException.Message : "|" + ex.Message);
                }
            }
        }

        if (FormId == 37)
        {
            int ProblemaAttributeID = Convert.ToInt32(ConfigurationManager.AppSettings["ProblemaAttributeID"]);
            SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CallDisposition"].ConnectionString);
            cn.Open();
            string sql = @"dbo.GetDispositionEmail";
            SqlCommand cmd = new SqlCommand(sql, cn);
            cmd.CommandTimeout = 9000;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AttributeID", ProblemaAttributeID);
            cmd.Parameters.AddWithValue("@ProblemaValue", problema);//_AttributeValues[2].Value

            string emails = Convert.ToString(cmd.ExecuteScalar());

            cn.Close();

            MailAddressCollection TO_addressList = new MailAddressCollection();

            MailAddress mFrom = new MailAddress("webforms@transcom.com");
            foreach (var curr_address in emails.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
            {
                MailAddress mytoAddress = new MailAddress(curr_address);
                TO_addressList.Add(mytoAddress);
            }

            MailMessage m = new MailMessage();
            SmtpClient client = new SmtpClient();

            m.IsBodyHtml = true;
            m.From = mFrom;
            //m.To.Add("martin.guco@transcom.com");
            m.To.Add(TO_addressList.ToString());
            m.Subject = string.Format("{0} - {1}", subject, problema);
            m.Body = body;

            try
            {
                client.Send(m);
                return "true";
            }
            catch (Exception ex)
            {
                return "false" + ((ex.InnerException.Message != null || ex.InnerException.Message != "") ? "|" + ex.InnerException.Message : "|" + ex.Message);
            }
        }

        return "true|Email [Off]";
    }

    private static bool IsUserExistForMail(int? UserId)
    {
        CallDispositionEntities db = new CallDispositionEntities();

        bool result = db.UserMails
            .Where(p => p.Active == true && p.UserID == UserId).Any();

        return result;
    }
}
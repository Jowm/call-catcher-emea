﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SmartPadUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.SmartPadUserControl" %>
<rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
</rad:RadAjaxLoadingPanel>
<rad:RadAjaxManagerProxy ID="ajaxManagerProxy1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="cboShipping">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                <rad:AjaxUpdatedControl ControlID="cboShipping2" />
                <rad:AjaxUpdatedControl ControlID="cboTracking" />
                <rad:AjaxUpdatedControl ControlID="cboTracking2" />
                <rad:AjaxUpdatedControl ControlID="cboUniversal" />
                <rad:AjaxUpdatedControl ControlID="cboUniversal2" />
                <rad:AjaxUpdatedControl ControlID="cboTransfer" />
                <rad:AjaxUpdatedControl ControlID="cboTransfer2" />
                <rad:AjaxUpdatedControl ControlID="labelMessage" />
                <rad:AjaxUpdatedControl ControlID="gridCallTypes" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="cboShipping2">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                <rad:AjaxUpdatedControl ControlID="cboTracking" />
                <rad:AjaxUpdatedControl ControlID="cboTracking2" />
                <rad:AjaxUpdatedControl ControlID="cboUniversal" />
                <rad:AjaxUpdatedControl ControlID="cboUniversal2" />
                <rad:AjaxUpdatedControl ControlID="cboTransfer" />
                <rad:AjaxUpdatedControl ControlID="cboTransfer2" />
                <rad:AjaxUpdatedControl ControlID="labelMessage" />
                <rad:AjaxUpdatedControl ControlID="gridCallTypes" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="cboTracking">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                <rad:AjaxUpdatedControl ControlID="cboShipping" />
                <rad:AjaxUpdatedControl ControlID="cboShipping2" />
                <rad:AjaxUpdatedControl ControlID="cboUniversal" />
                <rad:AjaxUpdatedControl ControlID="cboTracking2" />
                <rad:AjaxUpdatedControl ControlID="cboUniversal2" />
                <rad:AjaxUpdatedControl ControlID="cboTransfer" />
                <rad:AjaxUpdatedControl ControlID="cboTransfer2" />
                <rad:AjaxUpdatedControl ControlID="labelMessage" />
                <rad:AjaxUpdatedControl ControlID="gridCallTypes" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="cboTracking2">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                <rad:AjaxUpdatedControl ControlID="cboTracking" />
                <rad:AjaxUpdatedControl ControlID="cboShipping" />
                <rad:AjaxUpdatedControl ControlID="cboShipping2" />
                <rad:AjaxUpdatedControl ControlID="cboUniversal" />
                <rad:AjaxUpdatedControl ControlID="cboUniversal2" />
                <rad:AjaxUpdatedControl ControlID="cboTransfer" />
                <rad:AjaxUpdatedControl ControlID="cboTransfer2" />
                <rad:AjaxUpdatedControl ControlID="labelMessage" />
                <rad:AjaxUpdatedControl ControlID="gridCallTypes" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="cboUniversal">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                <rad:AjaxUpdatedControl ControlID="cboShipping" />
                <rad:AjaxUpdatedControl ControlID="cboShipping2" />
                <rad:AjaxUpdatedControl ControlID="cboTracking" />
                <rad:AjaxUpdatedControl ControlID="cboTracking2" />
                <rad:AjaxUpdatedControl ControlID="cboTransfer" />
                <rad:AjaxUpdatedControl ControlID="cboTransfer2" />
                <rad:AjaxUpdatedControl ControlID="labelMessage" />
                <rad:AjaxUpdatedControl ControlID="gridCallTypes" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="cboUniversal2">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                <rad:AjaxUpdatedControl ControlID="cboShipping" />
                <rad:AjaxUpdatedControl ControlID="cboShipping2" />
                <rad:AjaxUpdatedControl ControlID="cboTracking" />
                <rad:AjaxUpdatedControl ControlID="cboTracking2" />
                <rad:AjaxUpdatedControl ControlID="cboTransfer" />
                <rad:AjaxUpdatedControl ControlID="cboTransfer2" />
                <rad:AjaxUpdatedControl ControlID="labelMessage" />
                <rad:AjaxUpdatedControl ControlID="gridCallTypes" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="cboTracking2">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                <rad:AjaxUpdatedControl ControlID="cboTracking" />
                <rad:AjaxUpdatedControl ControlID="cboShipping" />
                <rad:AjaxUpdatedControl ControlID="cboShipping2" />
                <rad:AjaxUpdatedControl ControlID="cboUniversal" />
                <rad:AjaxUpdatedControl ControlID="cboUniversal2" />
                <rad:AjaxUpdatedControl ControlID="cboTransfer" />
                <rad:AjaxUpdatedControl ControlID="cboTransfer2" />
                <rad:AjaxUpdatedControl ControlID="labelMessage" />
                <rad:AjaxUpdatedControl ControlID="gridCallTypes" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="cboTransfer">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                <rad:AjaxUpdatedControl ControlID="cboShipping" />
                <rad:AjaxUpdatedControl ControlID="cboShipping2" />
                <rad:AjaxUpdatedControl ControlID="cboTracking" />
                <rad:AjaxUpdatedControl ControlID="cboTracking2" />
                <rad:AjaxUpdatedControl ControlID="cboUniversal" />
                <rad:AjaxUpdatedControl ControlID="cboUniversal2" />
                <rad:AjaxUpdatedControl ControlID="labelMessage" />
                <rad:AjaxUpdatedControl ControlID="gridCallTypes" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="cboTransfer2">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                <rad:AjaxUpdatedControl ControlID="cboShipping" />
                <rad:AjaxUpdatedControl ControlID="cboShipping2" />
                <rad:AjaxUpdatedControl ControlID="cboTracking" />
                <rad:AjaxUpdatedControl ControlID="cboTracking2" />
                <rad:AjaxUpdatedControl ControlID="cboUniversal" />
                <rad:AjaxUpdatedControl ControlID="cboUniversal2" />
                <rad:AjaxUpdatedControl ControlID="labelMessage" />
                <rad:AjaxUpdatedControl ControlID="gridCallTypes" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="gridCallTypes">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                <rad:AjaxUpdatedControl ControlID="gridCallTypes" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="imgAngry">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="imgSad">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="imgNormal">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="imgHappy">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="imgEcstatic">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="imgAngry2">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="imgSad2">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="imgNormal2">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="imgHappy2">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="imgEcstatic2">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rdoAngry">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rdoSad">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rdoNormal">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rdoHappy">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rdoEcstatic">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnEndCall">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="chkDiscussed">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<asp:Panel ID="pnlOuter" runat="server">
    <div>
        <asp:HiddenField ID="hdnStart" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnEnd" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnStartDrop" runat="server" ClientIDMode="Static" />
    </div>
    <asp:Panel ID="pnlSmart1" runat="server">
        <div class="row">
            <div class="block">
                <div class="row">
                    <div class="block">
                        <span class="lblSmartPad">What is your customer calling about?</span>
                    </div>
                </div>
                <br />
                <div class="row" runat="server" id="rowShipping">
                    <div class="block">
                        <table>
                            <tr>
                                <td>
                                    <img id="Img1" src="~/images/Call%20Driver%20Icons/shipping.png" alt="Angry" class="Emoticon"
                                        height="22" width="30" runat="server" />
                                </td>
                                <td>
                                    <rad:RadComboBox ID="cboShipping" runat="server" Visible="True" EmptyMessage="Shipping"
                                        DataTextField="Description" DataValueField="DispositionID" AppendDataBoundItems="true"
                                        OnSelectedIndexChanged="cbo_SelectedIndexChanged" AutoPostBack="true" CausesValidation="false"
                                        MarkFirstMatch="true" DropDownAutoWidth="Enabled" MaxHeight="250px" HighlightTemplatedItems="true"
                                        Sort="None" Width="230px" CssClass="drop">
                                        <ItemTemplate>
                                            <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td style="width: 90%;">
                                                        <%# DataBinder.Eval(Container, "Text")%>
                                                    </td>
                                                    <td style="width: 10%;">
                                                        <asp:Image ID="itemImage" runat="server" ImageUrl='<%# DataBinder.Eval(Container, "ImageUrl")%>' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="block">
                        <table>
                            <tr>
                                <td>
                                    <rad:RadComboBox ID="cboShipping2" runat="server" Visible="false" EmptyMessage="Shipping"
                                        DataTextField="Description" DataValueField="DispositionID" AppendDataBoundItems="true"
                                        OnSelectedIndexChanged="cbo_SelectedIndexChanged2" AutoPostBack="true" CausesValidation="false"
                                        MarkFirstMatch="true" DropDownAutoWidth="Enabled" MaxHeight="250px" HighlightTemplatedItems="true"
                                        Sort="None" Width="230px" CssClass="drop">
                                        <ItemTemplate>
                                            <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td style="width: 90%;">
                                                        <%# DataBinder.Eval(Container, "Text")%>
                                                    </td>
                                                    <td style="width: 10%;">
                                                        <asp:Image ID="itemImage" runat="server" ImageUrl='<%# DataBinder.Eval(Container, "ImageUrl")%>' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="block DTlink">
                        <asp:LinkButton ID="lnkShipping" runat="server"></asp:LinkButton>
                    </div>
                </div>
                <div class="row">
                    <div class="block">
                        <table>
                            <tr>
                                <td>
                                    <img id="Img8" src="~/images/Call%20Driver%20Icons/drop%20pin.png" alt="Angry" class="Emoticon"
                                        height="30" width="30" runat="server" />
                                </td>
                                <td>
                                    <rad:RadComboBox ID="cboTracking" runat="server" Visible="True" EmptyMessage="Tracking"
                                        DataTextField="Description" DataValueField="DispositionID" AppendDataBoundItems="true"
                                        OnSelectedIndexChanged="cbo_SelectedIndexChanged" AutoPostBack="true" CausesValidation="false"
                                        MarkFirstMatch="true" DropDownAutoWidth="Enabled" MaxHeight="250px" HighlightTemplatedItems="true"
                                        Sort="None" Width="230px" CssClass="drop">
                                        <ItemTemplate>
                                            <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td style="width: 90%;">
                                                        <%# DataBinder.Eval(Container, "Text")%>
                                                    </td>
                                                    <td style="width: 10%;">
                                                        <asp:Image ID="itemImage" runat="server" ImageUrl='<%# DataBinder.Eval(Container, "ImageUrl")%>' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="block">
                        <table>
                            <tr>
                                <td>
                                    <rad:RadComboBox ID="cboTracking2" runat="server" EmptyMessage="Tracking" DataTextField="Description"
                                        DataValueField="DispositionID" AppendDataBoundItems="true" Sort="None" OnSelectedIndexChanged="cbo_SelectedIndexChanged2"
                                        AutoPostBack="true" CausesValidation="false" MarkFirstMatch="true" DropDownAutoWidth="Enabled"
                                        MaxHeight="250px" HighlightTemplatedItems="true" Width="230px" CssClass="drop"
                                        Visible="false">
                                        <ItemTemplate>
                                            <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                <td style="width: 90%;">
                                                    <%# DataBinder.Eval(Container, "Text")%>
                                                </td>
                                                <td style="width: 10%;">
                                                    <asp:Image ID="itemImage" runat="server" ImageUrl='<%# DataBinder.Eval(Container, "ImageUrl")%>' />
                                                </td>
                                            </table>
                                        </ItemTemplate>
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="block DTlink">
                        <asp:LinkButton ID="lnkTracking" runat="server"></asp:LinkButton>
                    </div>
                </div>
                <div class="row" runat="server" id="rowUniversal">
                    <div class="block">
                        <table>
                            <tr>
                                <td>
                                    <img id="Img7" src="~/images/Call%20Driver%20Icons/info.png" alt="Angry" class="Emoticon"
                                        height="30" width="30" runat="server" />
                                </td>
                                <td>
                                    <rad:RadComboBox ID="cboUniversal" runat="server" Visible="True" EmptyMessage="Universal"
                                        DataTextField="Description" DataValueField="DispositionID" AppendDataBoundItems="true"
                                        Sort="None" OnSelectedIndexChanged="cbo_SelectedIndexChanged" AutoPostBack="true"
                                        CausesValidation="false" MarkFirstMatch="true" DropDownAutoWidth="Enabled" MaxHeight="250px"
                                        HighlightTemplatedItems="true" Width="230px" CssClass="drop">
                                        <ItemTemplate>
                                            <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                <td style="width: 90%;">
                                                    <%# DataBinder.Eval(Container, "Text")%>
                                                </td>
                                                <td style="width: 10%;">
                                                    <asp:Image ID="itemImage" runat="server" ImageUrl='<%# DataBinder.Eval(Container, "ImageUrl")%>' />
                                                </td>
                                            </table>
                                        </ItemTemplate>
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="block">
                        <table>
                            <tr>
                                <td>
                                    <rad:RadComboBox ID="cboUniversal2" runat="server" EmptyMessage="Universal" DataTextField="Description"
                                        DataValueField="DispositionID" AppendDataBoundItems="true" Sort="None" OnSelectedIndexChanged="cbo_SelectedIndexChanged2"
                                        AutoPostBack="true" CausesValidation="false" MarkFirstMatch="true" DropDownAutoWidth="Enabled"
                                        MaxHeight="250px" HighlightTemplatedItems="true" Width="230px" CssClass="drop"
                                        Visible="false">
                                        <ItemTemplate>
                                            <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                <td style="width: 90%;">
                                                    <%# DataBinder.Eval(Container, "Text")%>
                                                </td>
                                                <td style="width: 10%;">
                                                    <asp:Image ID="itemImage" runat="server" ImageUrl='<%# DataBinder.Eval(Container, "ImageUrl")%>' />
                                                </td>
                                            </table>
                                        </ItemTemplate>
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="block DTlink">
                        <asp:LinkButton ID="lnkUniversal" runat="server"></asp:LinkButton>
                    </div>
                </div>
                <div class="row">
                    <div class="block">
                        <table>
                            <tr>
                                <td>
                                    <img id="Img2" src="~/images/Call%20Driver%20Icons/transfer2.png" alt="Angry" class="Emoticon"
                                        height="30" width="30" runat="server" />
                                </td>
                                <td>
                                    <rad:RadComboBox ID="cboTransfer" runat="server" Visible="True" EmptyMessage="Transfer"
                                        DataTextField="Description" DataValueField="DispositionID" AppendDataBoundItems="true"
                                        Sort="None" OnSelectedIndexChanged="cbo_SelectedIndexChanged" AutoPostBack="true"
                                        CausesValidation="false" MarkFirstMatch="true" DropDownAutoWidth="Enabled" MaxHeight="250px"
                                        HighlightTemplatedItems="true" Width="230px" CssClass="drop">
                                        <ItemTemplate>
                                            <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                <td style="width: 90%;">
                                                    <%# DataBinder.Eval(Container, "Text")%>
                                                </td>
                                                <td style="width: 10%;">
                                                    <asp:Image ID="itemImage" runat="server" ImageUrl='<%# DataBinder.Eval(Container, "ImageUrl")%>' />
                                                </td>
                                            </table>
                                        </ItemTemplate>
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="block">
                        <table>
                            <tr>
                                <td>
                                    <rad:RadComboBox ID="cboTransfer2" runat="server" EmptyMessage="Transfer" DataTextField="Description"
                                        DataValueField="DispositionID" AppendDataBoundItems="true" Sort="None" OnSelectedIndexChanged="cbo_SelectedIndexChanged2"
                                        AutoPostBack="true" CausesValidation="false" MarkFirstMatch="true" DropDownAutoWidth="Enabled"
                                        MaxHeight="250px" HighlightTemplatedItems="true" Width="230px" CssClass="drop"
                                        Visible="false">
                                        <ItemTemplate>
                                            <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                <td style="width: 90%;">
                                                    <%# DataBinder.Eval(Container, "Text")%>
                                                </td>
                                                <td style="width: 10%;">
                                                    <asp:Image ID="itemImage" runat="server" ImageUrl='<%# DataBinder.Eval(Container, "ImageUrl")%>' />
                                                </td>
                                            </table>
                                        </ItemTemplate>
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="block DTlink">
                        <asp:LinkButton ID="lnkTransfer" runat="server"></asp:LinkButton>
                    </div>
                </div>
                <div class="row">
                    <div class="block">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <table width="100%" class="tblSmart1">
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <span class="lblSmartPad">How is your customer feeling?</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Panel ID="pnlRadioEmoticon" runat="server">
                            <asp:HiddenField ID="hiddenEmoticonValue" runat="server" />
                            <table width="50%">
                                <tr>
                                    <td>
                                        <label for="rdoAngry">
                                            <asp:ImageButton ID="imgAngry" src="images/Emoticons/Icon%205.png" alt="Angry" class="Emoticon"
                                                Height="30" Width="30" runat="server" OnClick="img_Click" CausesValidation="false" />
                                        </label>
                                    </td>
                                    <td>
                                        <label for="rdoSad">
                                            <asp:ImageButton ID="imgSad" src="images/Emoticons/Icon%204.png" alt="Sad" class="Emoticon"
                                                Height="30" Width="30" runat="server" OnClick="img_Click" CausesValidation="false" /></label>
                                    </td>
                                    <td>
                                        <label for="rdoNormal">
                                            <asp:ImageButton ID="imgNormal" src="images/Emoticons/Icon%203.png" alt="Normal"
                                                class="Emoticon" Height="30" Width="30" runat="server" OnClick="img_Click" CausesValidation="false" /></label>
                                    </td>
                                    <td>
                                        <label for="rdoHappy">
                                            <asp:ImageButton ID="imgHappy" src="images/Emoticons/Icon%202.png" alt="Happy" class="Emoticon"
                                                Height="30" Width="30" runat="server" OnClick="img_Click" CausesValidation="false" /></label>
                                    </td>
                                    <td>
                                        <label for="rdoEcstatic">
                                            <asp:ImageButton ID="imgEcstatic" src="images/Emoticons/Icon%201.png" alt="Ecstatic"
                                                class="Emoticon" Height="30" Width="30" runat="server" OnClick="img_Click" CausesValidation="false" /></label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center">
                                        <asp:RadioButton ID="rdoAngry" runat="server" GroupName="rdoList" OnCheckedChanged="rdoID_CheckedChanged"
                                            AutoPostBack="true" CausesValidation="false" />
                                    </td>
                                    <td style="text-align: center">
                                        <asp:RadioButton ID="rdoSad" runat="server" GroupName="rdoList" OnCheckedChanged="rdoID_CheckedChanged"
                                            AutoPostBack="true" CausesValidation="false" />
                                    </td>
                                    <td style="text-align: center">
                                        <asp:RadioButton ID="rdoNormal" runat="server" GroupName="rdoList" OnCheckedChanged="rdoID_CheckedChanged"
                                            AutoPostBack="true" CausesValidation="false" />
                                    </td>
                                    <td style="text-align: center">
                                        <asp:RadioButton ID="rdoHappy" runat="server" GroupName="rdoList" OnCheckedChanged="rdoID_CheckedChanged"
                                            AutoPostBack="true" CausesValidation="false" />
                                    </td>
                                    <td style="text-align: center">
                                        <asp:RadioButton ID="rdoEcstatic" runat="server" GroupName="rdoList" OnCheckedChanged="rdoID_CheckedChanged"
                                            AutoPostBack="true" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                            <asp:Literal runat="server" ID="labelMessage"></asp:Literal>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <span class="lblSmartPad">Your Case Notes</span>
                    </td>
                    <%--              <td colspan="1" align="right">
                        <rad:RadButton ID="btnDropCall" runat="server" Text="Drop Call" OnClick="btnDropCall_Click"
                            CssClass="endcall btnSmartPad" DisabledButtonCssClass="btnSmartPad_disabled"
                            ForeColor="White">
                        </rad:RadButton>
                    </td>--%>
                </tr>
                <tr>
                    <td colspan="3">
                        <rad:RadTextBox runat="server" TextMode="MultiLine" Rows="15" Width="100%" ID="txtCaseNotes"
                            Font-Size="12" CssClass="casenotes" MaxLength="7000">
                        </rad:RadTextBox>
                        <asp:RegularExpressionValidator runat="server" ID="revCaseNotes" ControlToValidate="txtCaseNotes"
                            Text="-" ErrorMessage="Invalid Character/s in Case Notes (\)" ValidationExpression="^[^\\]{0,1024}$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <%--         <tr>
                    <td colspan="3">
                        <div class="textarea-container">
               
                            <asp:PlaceHolder ID="plceHldr" runat="server"></asp:PlaceHolder>
                        </div>
                    </td>
                </tr>--%>
                <%--       <tr>
                    <td colspan="3" align="left">
                        <rad:RadButton ID="btnEndCall" runat="server" Text="End Call" OnClick="btnEndCall_Click"
                            CssClass="endcall btnSmartPad" DisabledButtonCssClass="btnSmartPad_disabled"
                            ValidationGroup="EndCall" ForeColor="White">
                        </rad:RadButton>
                    </td>
                </tr>--%>
            </table>
        </div>
        <asp:Panel ID="pnlUK" runat="server">
            <div class="row">
                <div class="block blockheader">
                    <asp:CheckBox ID="chkDiscussed" runat="server" OnCheckedChanged="chkDiscussed_OnCheckedChanged"
                        AutoPostBack="True" />
                    <span class="lblSmartPad">Pick Up Fee Discussed?</span>
                </div>
            </div>
            <div class="row">
                <asp:Panel ID="pnlSmart2" runat="server" Visible="false" CssClass="pnlSmart2">
                    <div class="row">
                        <table width="100%" class="tblSmart1">
                            <tr>
                                <td>
                                    Country:
                                </td>
                                <td>
                                    <rad:RadComboBox ID="cboCountry" runat="server" Visible="True" EmptyMessage="Country"
                                        CausesValidation="false" MarkFirstMatch="true" DropDownAutoWidth="Enabled" MaxHeight="250px"
                                        HighlightTemplatedItems="true" Width="150px" CssClass="drop" Enabled="false">
                                        <Items>
                                            <rad:RadComboBoxItem runat="server" Text="United Kingdom" Selected="true" />
                                        </Items>
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    GBS CS Site:
                                </td>
                                <td>
                                    <rad:RadComboBox ID="cboSite" runat="server" Visible="True" Sort="None" CausesValidation="false"
                                        DropDownAutoWidth="Enabled" MaxHeight="250px" Width="80px" CssClass="drop" Enabled="false">
                                        <Items>
                                            <rad:RadComboBoxItem runat="server" Text="Manila" Selected="true" />
                                        </Items>
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Sentiment:
                                    <asp:RequiredFieldValidator ID="rfvSentiment" runat="server" ControlToValidate="cboSentiment"
                                        ErrorMessage="Select sentiment." Text="*" Enabled="false"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <rad:RadComboBox ID="cboSentiment" runat="server" Visible="True" EmptyMessage="[Select]"
                                        DataTextField="Description" DataValueField="Description" AppendDataBoundItems="true"
                                        Sort="None" CausesValidation="false" MarkFirstMatch="true" DropDownAutoWidth="Enabled"
                                        MaxHeight="100px" HighlightTemplatedItems="true" Width="100px" CssClass="drop">
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Account Holder/Non-Account:
                                    <asp:RequiredFieldValidator ID="rfvAccountHolder" runat="server" ControlToValidate="cboAccountHolder"
                                        ErrorMessage="Indicate if account holder or not." Text="*" Enabled="false"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <rad:RadComboBox ID="cboAccountHolder" runat="server" Visible="True" Sort="None"
                                        EmptyMessage="[Select]" AppendDataBoundItems="true" DataTextField="Description"
                                        DataValueField="Description" CausesValidation="false" DropDownAutoWidth="Disabled"
                                        MaxHeight="250px" Width="70px" CssClass="drop" HighlightTemplatedItems="true">
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Classification:
                                    <asp:RequiredFieldValidator ID="rfvClassification" runat="server" ControlToValidate="cboClassification"
                                        ErrorMessage="Select classification" Text="*" Enabled="false"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <rad:RadComboBox ID="cboClassification" runat="server" Visible="True" EmptyMessage="[Select]"
                                        DataTextField="Description" DataValueField="Description" AppendDataBoundItems="true"
                                        Sort="None" CausesValidation="false" MarkFirstMatch="true" DropDownAutoWidth="Enabled"
                                        MaxHeight="250px" HighlightTemplatedItems="true" Width="270px" CssClass="drop">
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ECM Case Number:
                                </td>
                                <td>
                                    <rad:RadTextBox ID="txtECMCaseNumber" runat="server" MaxLength="20">
                                    </rad:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RegularExpressionValidator runat="server" ID="revECMCaseNumber" ControlToValidate="txtECMCaseNumber"
                                        Text="-" ErrorMessage="Invalid Character/s in ECM (\)" ValidationExpression="^[^\\]{0,1024}$"></asp:RegularExpressionValidator>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    PU Fee Process:
                                    <asp:RequiredFieldValidator ID="rfvFeeProcess" runat="server" ControlToValidate="cboFeeProcess"
                                        ErrorMessage="Select Fee Process" Text="*" Enabled="false"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <rad:RadComboBox ID="cboFeeProcess" runat="server" Visible="True" EmptyMessage="[Select]"
                                        DataTextField="Description" DataValueField="Description" AppendDataBoundItems="true"
                                        Sort="None" CausesValidation="false" MarkFirstMatch="true" DropDownAutoWidth="Enabled"
                                        MaxHeight="250px" HighlightTemplatedItems="true" Width="270px" CssClass="drop">
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    PU Fee Applicable:
                                    <asp:RequiredFieldValidator ID="rfvApplicable" runat="server" ControlToValidate="cboFeeApplicable"
                                        ErrorMessage="Select Fee Applicable" Text="*" Enabled="false"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <rad:RadComboBox ID="cboFeeApplicable" runat="server" Visible="True" EmptyMessage="[Select]"
                                        DataTextField="Description" DataValueField="Description" AppendDataBoundItems="true"
                                        Sort="None" CausesValidation="false" MarkFirstMatch="true" DropDownAutoWidth="Enabled"
                                        MaxHeight="250px" HighlightTemplatedItems="true" Width="270px" CssClass="drop">
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    PU Fee Not Applicable:
                                    <asp:RequiredFieldValidator ID="rfvNotApplicable" runat="server" ControlToValidate="cboFeeNotApplicable"
                                        ErrorMessage="Select Fee Not Applicable" Text="*" Enabled="false"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <rad:RadComboBox ID="cboFeeNotApplicable" runat="server" Visible="True" EmptyMessage="[Select]"
                                        DataTextField="Description" DataValueField="Description" AppendDataBoundItems="true"
                                        Sort="None" CausesValidation="false" MarkFirstMatch="true" DropDownAutoWidth="Enabled"
                                        MaxHeight="250px" HighlightTemplatedItems="true" Width="270px" CssClass="drop">
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    PU Fee Applicable - Non Acct:
                                    <asp:RequiredFieldValidator ID="rfvApplicableNonAcct" runat="server" ControlToValidate="cboFeeApplicableNonAcct"
                                        ErrorMessage="Select Fee Not Applicable" Text="*" Enabled="false"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <rad:RadComboBox ID="cboFeeApplicableNonAcct" runat="server" Visible="True" EmptyMessage="[Select]"
                                        DataTextField="Description" DataValueField="Description" AppendDataBoundItems="true"
                                        Sort="None" CausesValidation="false" MarkFirstMatch="true" DropDownAutoWidth="Enabled"
                                        MaxHeight="250px" HighlightTemplatedItems="true" Width="270px" CssClass="drop">
                                    </rad:RadComboBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </div>
            <div class="row">
                <div class="block blockheader">
                </div>
            </div>
        </asp:Panel>
        <div class="row">
            <div class="block blockheader">
                <span class="lblSmartPad">Call Types</span>
            </div>
        </div>
        <div class="row">
            <div class="block">
                <telerik:RadGrid runat="server" ID="gridCallTypes" AutoGenerateColumns="False" AllowPaging="true"
                    PageSize="5" GridLines="None" Skin="Metro" Width="100%" OnNeedDataSource="gridCallTypes_NeedDataSource"
                    OnItemCommand="gridCallTypes_ItemCommand">
                    <ClientSettings EnableRowHoverStyle="True">
                    </ClientSettings>
                    <PagerStyle AlwaysVisible="True" Mode="Slider" />
                    <MasterTableView AutoGenerateColumns="False" EnableNoRecordsTemplate="True" NoMasterRecordsText="No Call Type selected."
                        ShowHeadersWhenNoRecords="True" TableLayout="Auto" ShowHeader="false" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center"
                        DataKeyNames="ID">
                        <Columns>
                            <telerik:GridBoundColumn UniqueName="CallType" HeaderText="CallType" DataField="CallType"
                                AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Issue" HeaderText="Issue" DataField="Issue"
                                AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="500">
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="50">
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </div>
        </div>
    </asp:Panel>
</asp:Panel>

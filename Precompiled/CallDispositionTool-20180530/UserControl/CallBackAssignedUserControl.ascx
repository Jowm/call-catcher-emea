﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CallBackAssignedUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.CallBackAssignedUserControl" %>
<div style="padding: 10px 10px 10px 10px">
    <rad:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function OpenTransaction(CallBackID) {
                var oWnd = radopen("TransactionPopup.aspx?CallBackID=" + CallBackID, "RadWindow1");
            }

            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg) {
                    //var cityName = arg.cityName;
                    //var seldate = arg.selDate;
                    //$get("order").innerHTML = "You chose to fly to <strong>" + cityName + "</strong> on <strong>" + seldate + "</strong>";
                }
            }
        </script>
    </rad:RadScriptBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientClose="OnClientClose" ShowContentDuringLoad="false"
                Height="600px" Width="800px" AutoSizeBehaviors="Default">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManagerProxy ID="ajaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="grdCallBack">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdCallBack" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="cboCallBackStatus">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdCallBack" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <table>
        <tr>
            <td>
                <strong>Status:</strong>
            </td>
            <td>
                <rad:RadComboBox ID="cboCallBackStatus" runat="server" DataSourceID="dsCallBackStatus"
                    DataTextField="CallBackStatus" DataValueField="CallBackStatusID" AutoPostBack="true"
                    OnSelectedIndexChanged="cboCallBackStatus_SelectedIndexChanged1">
                </rad:RadComboBox>
            </td>
        </tr>
    </table>
    <rad:RadGrid ID="grdCallBack" runat="server" AutoGenerateColumns="false" AllowSorting="true"
        Width="100%" OnItemDataBound="grdCallBack_ItemDataBound" OnNeedDataSource="grdCallBack_NeedDataSource">
        <PagerStyle Mode="NextPrevNumericAndAdvanced" />
        <ClientSettings AllowColumnsReorder="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Resizing AllowColumnResize="true" AllowResizeToFit="true" ResizeGridOnColumnResize="true" />
        </ClientSettings>
        <ItemStyle Wrap="false"></ItemStyle>
        <MasterTableView CommandItemDisplay="Top" PageSize="15" AllowPaging="true" TableLayout="Auto"
            AllowAutomaticDeletes="true" AllowAutomaticInserts="true" AllowAutomaticUpdates="true"
            DataKeyNames="CallBackID">
            <NestedViewTemplate>
                <rad:RadTabStrip ID="TabStripCallBack" runat="server" SelectedIndex="0" MultiPageID="MultiPageCallBack"
                    ShowBaseLine="true" Width="100%">
                    <Tabs>
                        <rad:RadTab Text="Transaction" runat="server">
                        </rad:RadTab>
                        <rad:RadTab Text="Call Back History Audit" runat="server">
                        </rad:RadTab>
                    </Tabs>
                </rad:RadTabStrip>
                <div style="border: 0.1pt solid gray; border-width: 0.5pt; border-top-style: none;
                    padding: 10px 10px 10px 10px">
                    <rad:RadMultiPage ID="MultiPageCallBack" runat="server" SelectedIndex="0" RenderSelectedPageOnly="False"
                        Width="100%">
                        <rad:RadPageView ID="PageViewTransaction" runat="server">
                            <div style="padding: 10px 10px 10px 10px">
                                Click
                                <asp:HyperLink ID="lnkTrans" runat="server" Text="Here"></asp:HyperLink>
                                to view the transaction.
                            </div>
                        </rad:RadPageView>
                        <rad:RadPageView ID="PageViewUserRole" runat="server">
                            <asp:Label ID="lblCallBackID" Font-Bold="true" Font-Italic="true" Text='<%# Eval("CallBackID") %>'
                                Visible="false" runat="server" />
                            <rad:RadGrid ID="grdCallBackHistory" runat="server" Width="100%" AllowAutomaticDeletes="true"
                                AllowAutomaticInserts="true" AllowAutomaticUpdates="true" DataSourceID="dsCallBackHistory">
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                                <MasterTableView Name="CallBackHistory" DataKeyNames="CallBackHistoryLogID,CallBackID"
                                    AutoGenerateColumns="False" CommandItemDisplay="Top" AllowSorting="true" DataSourceID="dsCallBackHistory">
                                    <CommandItemSettings ShowAddNewRecordButton="false" />
                                    <Columns>
                                        <rad:GridBoundColumn HeaderText="Action" DataField="Action" SortExpression="Action">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn HeaderText="Log" DataField="ActionLog" SortExpression="ActionLog">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn HeaderText="Logged By" DataField="User.EmployeeID" SortExpression="User.EmployeeID">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn HeaderText="CreatedOn" DataField="CreatedOn" SortExpression="CreatedOn">
                                        </rad:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                            </rad:RadGrid>
                            <asp:EntityDataSource ID="dsCallBackHistory" runat="server" ConnectionString="name=CallDispositionEntities"
                                DefaultContainerName="CallDispositionEntities" EntitySetName="CallBackHistoryLogs"
                                AutoGenerateWhereClause="true" Include="User">
                                <WhereParameters>
                                    <asp:ControlParameter ControlID="lblCallBackID" PropertyName="Text" Name="CallBackID"
                                        Type="Int32" />
                                </WhereParameters>
                            </asp:EntityDataSource>
                        </rad:RadPageView>
                    </rad:RadMultiPage>
                </div>
            </NestedViewTemplate>
            <CommandItemSettings ShowAddNewRecordButton="false" />
            <SortExpressions>
                <rad:GridSortExpression FieldName="CallBackID" SortOrder="Descending" />
            </SortExpressions>
            <Columns>
                <rad:GridTemplateColumn AllowFiltering="false" HeaderText="Status">
                    <ItemTemplate>
                        <rad:RadComboBox ID="cboCallBackStatus" runat="server" DataSourceID="dsCallBackStatus"
                            SelectedValue='<%# Bind("CallBackStatusID") %>' DataTextField="CallBackStatus"
                            DataValueField="CallBackStatusID" Width="100px" AppendDataBoundItems="true" AutoPostBack="true"
                            OnSelectedIndexChanged="cboCallBackStatus_SelectedIndexChanged">
                            <Items>
                                <rad:RadComboBoxItem Text="" Value="" />
                            </Items>
                        </rad:RadComboBox>
                    </ItemTemplate>
                    <HeaderStyle Width="110px" />
                </rad:GridTemplateColumn>
                <rad:GridDateTimeColumn UniqueName="ActualCallBackDate" HeaderText="Actual Call Back Date/Time"
                    DataField="ActualCallBackDate" SortExpression="ActualCallBackDate">
                </rad:GridDateTimeColumn>
                <rad:GridDateTimeColumn UniqueName="Timezone" HeaderText="Timezone"
                    DataField="Timezone" SortExpression="Timezone">
                </rad:GridDateTimeColumn>
                <rad:GridDateTimeColumn UniqueName="AdjustedCallBackDate" HeaderText="Adjusted CallBack Date/Time" DataField="AdjustedCallBackDate"
                    SortExpression="AdjustedCallBackDate" ReadOnly="true">
                </rad:GridDateTimeColumn>
                <rad:GridBoundColumn UniqueName="ContactNumber" HeaderText="Contact Number" DataField="ContactNumber"
                    SortExpression="ContactNumber">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn UniqueName="CallBackReason" HeaderText="CallBack Reason" DataField="CallBackReason"
                    SortExpression="CallBackReason">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn UniqueName="CreatedBy" HeaderText="Created By" DataField="CreatedBy"
                    SortExpression="CreatedBy">
                </rad:GridBoundColumn>
                <rad:GridDateTimeColumn UniqueName="CreatedOn" HeaderText="Created On" DataField="CreatedOn"
                    SortExpression="CreatedOn" ReadOnly="true">
                </rad:GridDateTimeColumn>
            </Columns>
        </MasterTableView>
    </rad:RadGrid>
    <asp:EntityDataSource ID="dsCallBackReason" runat="server" ConnectionString="name=CallDispositionEntities"
        DefaultContainerName="CallDispositionEntities" EntitySetName="CallBackReasons">
    </asp:EntityDataSource>
    <asp:EntityDataSource ID="dsTimezone" runat="server" ConnectionString="name=CallDispositionEntities"
        DefaultContainerName="CallDispositionEntities" EntitySetName="Timezones">
    </asp:EntityDataSource>
    <asp:EntityDataSource ID="dsCallBackStatus" runat="server" ConnectionString="name=CallDispositionEntities"
        DefaultContainerName="CallDispositionEntities" EntitySetName="CallBackStatus"
        EnableFlattening="False">
    </asp:EntityDataSource>
    <asp:EntityDataSource ID="dsCallBack" runat="server" ConnectionString="name=CallDispositionEntities"
        DefaultContainerName="CallDispositionEntities" EnableDelete="True" EnableFlattening="False"
        EnableUpdate="True" EntitySetName="CallBacks" AutoGenerateOrderByClause="True"
        AutoGenerateWhereClause="true">
        <WhereParameters>
            <asp:Parameter Name="CreatedBy" Type="Int32" />
            <asp:Parameter Name="CallBackStatusID" Type="Int32" />
        </WhereParameters>
    </asp:EntityDataSource>
</div>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.ContactUserControl" %>
<style type="text/css">
    .clsHidden
    {
        display: none;
    }
</style>
<script type="text/javascript">
    function ShowAddContact() {
        var windowURL = 'Contact.aspx?Mode=Add';
        window.open(windowURL, "AddContact", 'width=430,height=600,resizable=no,scrollbars=no').focus();
    }
    function OnClientButtonCommand(sender, args) {
        if (args.get_commandName() == "NewContact") {
            ShowAddContact();
        }
    }
</script>
<div>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManagerProxy ID="ajaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="searchBox1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlContactInfo" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <rad:RadSearchBox ID="searchBox1" runat="server" DataSourceID="SqlDataSource1" DataTextField="ContactName"
        DataKeyNames="ContactID,ContactName,Phone,MobilePhone,Address,Email" DataValueField="ContactID"
        DataContextKeyField="FirstName" Width="300px" EmptyMessage="Type to search" EnableAutoComplete="true"
        OnSearch="searchBox1_Search" ShowSearchButton="true" OnDataBound="searchBox1_DataBound"
        OnButtonCommand="searchBox1_ButtonCommand" Height="30px" OnClientButtonCommand="OnClientButtonCommand">
        <Buttons>
            <rad:SearchBoxButton ID="btnReset" runat="server" CommandName="Reset" CommandArgument="Reset"
                Position="Right" ToolTip="Reset" AlternateText="Reset" ImageUrl="~/images/close.png" />
            <rad:SearchBoxButton ID="btnAddNewContact" runat="server" CommandName="NewContact"
                CommandArgument="NewContact" Position="Right" ToolTip="New Contact" AlternateText="New Contact"
                ImageUrl="~/images/AddRecord.gif" />
        </Buttons>
        <DropDownSettings Height="250px" Width="400px">
            <ItemTemplate>
                <table>
                    <tr>
                        <td colspan="2">
                            <h2>
                                <%# DataBinder.Eval(Container.DataItem, "FirstName") %>
                                <%# DataBinder.Eval(Container.DataItem, "LastName") %></h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Phone:</strong>
                        </td>
                        <td>
                            <%# DataBinder.Eval(Container.DataItem, "Phone")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Mobile:</strong>
                        </td>
                        <td>
                            <%# DataBinder.Eval(Container.DataItem, "MobilePhone")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Address:</strong>
                        </td>
                        <td>
                            <%# DataBinder.Eval(Container.DataItem, "Address")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Email:</strong>
                        </td>
                        <td>
                            <%# DataBinder.Eval(Container.DataItem, "Email")%>
                        </td>
                    </tr>
                </table>
                <hr />
            </ItemTemplate>
        </DropDownSettings>
    </rad:RadSearchBox>
    <asp:TextBox ID="txtContactID" runat="server" CssClass="clsHidden"></asp:TextBox>
    <asp:RequiredFieldValidator ID="reqContact" runat="server" ErrorMessage="" Text="*"
        ControlToValidate="txtContactID"></asp:RequiredFieldValidator>
    <asp:Panel ID="pnlContactInfo" runat="server" Visible="false">
        <fieldset>
            <legend>
                <h2>
                    <asp:Label ID="lblContactName" runat="server"></asp:Label>
                </h2>
            </legend>
            <table>
                <tr>
                    <td>
                        <strong>Phone:</strong>
                    </td>
                    <td>
                        <asp:Label ID="lblPhone" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Mobile:</strong>
                    </td>
                    <td>
                        <asp:Label ID="lblMobile" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Address:</strong>
                    </td>
                    <td>
                        <asp:Label ID="lblAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Email:</strong>
                    </td>
                    <td>
                        <asp:Label ID="lblEmail" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:cn_CallDisposition %>"
        SelectCommand="SELECT [ContactID], [FirstName] + ' ' +  [LastName] as ContactName, FirstName, LastName, Phone, MobilePhone, Address1 + ' ' + Address2 AS Address, Email FROM [Contact]">
    </asp:SqlDataSource>
</div>

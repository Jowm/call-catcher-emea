﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Form.aspx.cs" Inherits="CallDispositionTool.Form" %>

<%@ OutputCache Duration="1440" VaryByParam="FormID" %>
<%@ Register Src="UserControl/CallBackUserControl.ascx" TagName="CallBackUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/ContactPanelUserControl.ascx" TagName="ContactPanelUserControl"
    TagPrefix="uc2" %>
<%@ Register Src="UserControl/AudienceManagerUserControl.ascx" TagName="AudienceManagerUserControl"
    TagPrefix="uc3" %>
<%@ Register Src="UserControl/TransferCallUserControl.ascx" TagName="TransferCallUserControl"
    TagPrefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script src='Scripts/jquery-1.6.4.min.js' type="text/javascript"></script>
    <!-- Google Analytics -->
    <script src="Scripts/GAnalytics.js" type="text/javascript"></script>
    <script src="Scripts/postmessage.js" type="text/javascript"></script>
    <script src="Scripts/CTIPadSDK.js" type="text/javascript"></script>
    <link href="CSS/FormStyle.css" rel="stylesheet" type="text/css" />
    <title>Call Catcher</title>
    <meta http-equiv="Cache-Control" content="cache">
    <meta http-equiv="Pragma" content="cache">
</head>
<body>
    <form id="form1" runat="server">
    <rad:RadFormDecorator ID="formDecorator1" runat="server" EnableRoundedCorners="true" />
    <rad:RadScriptManager ID="ScriptManager1" runat="server">
    </rad:RadScriptManager>
    <rad:RadSkinManager ID="skinManager1" runat="server" PersistenceMode="Cookie">
    </rad:RadSkinManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        ShowContentDuringLoad="false" Width="680px" Height="500px" VisibleOnPageLoad="false"
        VisibleStatusbar="false" DestroyOnClose="true" ReloadOnShow="true" Modal="true"
        AutoSize="true" InitialBehaviors="Maximize" RenderMode="Lightweight">
    </telerik:RadWindowManager>
    <rad:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function SearchBoxSetContact(contact) {
                if (contact) {
                    document.getElementById('<%= hdnContactID.ClientID %>').value = contact;
                    $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("RefreshContact");
                }
            }
            
            function PageReset() {
                CloseTab();
            }

            function PageReset(args) {
                location.href = window.location.href;
            }

            function startTrackProgress(grid) {
                if (!window._trackStart) {
                    window._trackStart = new Date();
                }
            }

            function endTrackProgress(grid) {
                var duration = getBindDuration();
                setStatusLabelVisiblity(grid, false);
                setDurationVisiblity(grid, true, duration);
            }

            function getBindDuration() {
                var result = { min: 0, sec: 0, msec: 0 };

                if (window._trackStart) {
                    var bindDuration = (new Date()) - window._trackStart;
                    window._trackStart = null;

                    result.min = parseInt(bindDuration / (1000 * 60));
                    result.sec = parseInt((bindDuration / 1000) % 60);
                    result.msec = parseInt((bindDuration % 1000) / 10);
                }

                result.min = result.min < 10 ? "0" + result.min : result.min + "";
                result.sec = result.sec < 10 ? "0" + result.sec : result.sec + "";
                result.msec = result.msec < 10 ? "0" + result.msec : result.msec + "";

                return result;
            }

            function onResponseEnd(sender, eventArgs) {
                var duration = getBindDuration();

                var grid = $find("<%= grdTransaction.ClientID %>");

                if (grid) {
                    var footer = grid.get_masterTableViewFooter().get_element();
                    footer.rows[0].cells[1].innerHTML = 'Time Elapsed: ' + duration.min + ":" + duration.sec + ":" + duration.msec; // Set the footer text 
                }
            }

            function onRequestStart(sender, args) {
                startTrackProgress();

                if (args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }

            function showNotification() {
                var notification = $find("<%=RadNotification1.ClientID %>");
                setTimeout(function () {
                    notification.show();
                }, 0);
            }

            function CheckIfShow(sender, args) {
                var summaryElem = document.getElementById("<%= ValidationSummary1.ClientID%>");
                //check if summary is visible - if not, there are not errors, do not notify and continue with postback
                var noErrors = summaryElem.style.display == "none";
                args.set_cancel(noErrors);
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow)
                    oWindow = window.RadWindow; //Will work in Moz in all cases, including clasic dialog     
                else if (window.frameElement.radWindow)
                    oWindow = window.frameElement.radWindow; //IE (and Moz as well)     
                return oWindow;
            }

            function ShowEditForm(transactionID, rowIndex) {
                <%
                    var AllowEditTransaction = CallDispositionTool.Helper.IsUserAllowEditTransaction(Convert.ToInt32(Membership.GetUser().ProviderUserKey));

                    if (AllowEditTransaction) {
                %>
                var windowURL = '<%= ResolveClientUrl("~/TransactionEdit.aspx") %>' + '?TransactionID=' + transactionID;
                var grid = $find("<%= grdTransaction.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element(); grid.get_masterTableView().selectItem(rowControl, true);
                var oWnd = radopen(windowURL, null);
                oWnd.set_title("Transaction Edit - " + transactionID);
                oWnd.maximize();
                oWnd.center();

                <% }
                    else {
                %>
                var windowURL = '<%= ResolveClientUrl("~/TransactionEdit.aspx") %>?Mode=View' + '&TransactionID=' + transactionID;
                var grid = $find("<%= grdTransaction.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element(); grid.get_masterTableView().selectItem(rowControl, true);
                var oWnd = radopen(windowURL, null);
                oWnd.set_title("Transaction View - " + transactionID);
                oWnd.maximize();
                oWnd.center();

                <% } %>

                return false;
            }

            function refreshGrid() {
                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("Rebind");
            }

            function showSignOffForm(FormID) {
                var windowURL = '<%= ResolveClientUrl("~/SignOff.aspx") %>' + '?FormID=' + FormID;
                var oWnd = radopen(windowURL, null);
                oWnd.setSize(400, 300);
                oWnd.set_title("Sign Off Required");
                oWnd.center();
            }

            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();

                if (arg) {
                    if (arg.SignOffUserID > 0) {
                        document.getElementById('<%= hdnSignOffUserID.ClientID %>').value = arg.SignOffUserID;
                        $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("SaveAndSignOffRecord");
                    }
                }
            }

            function OnClientSelectedIndexChanged(sender, args) {
                args.get_item().set_checked(args.get_item().get_selected());
            }                     
        </script>
    </rad:RadScriptBlock>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <telerik:RadWindowManager ID="RadWindowManager2" runat="server" EnableShadow="true"
        Width="680px" Height="500px" VisibleOnPageLoad="false" VisibleStatusbar="false"
        Modal="true" RenderMode="Lightweight" OnClientClose="OnClientClose" DestroyOnClose="true"
        Animation="Fade">
    </telerik:RadWindowManager>
    <rad:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="onRequestStart" OnResponseEnd="onResponseEnd" />
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="TabStrip1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="TabStrip1" />
                    <rad:AjaxUpdatedControl ControlID="MultiPage1" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="grdTransaction" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="grdTransaction">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdTransaction" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="RadFilter1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlReport" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSubmit">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="btnNew" />
                    <rad:AjaxUpdatedControl ControlID="lblStatus" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnReset">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="btnNew" />
                    <rad:AjaxUpdatedControl ControlID="lblStatus" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="btnNew" />
                    <rad:AjaxUpdatedControl ControlID="lblStatus" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="chkCallBackRequired">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <div style="margin: 0 auto 0 auto; width: 100%; height: 100%">
        <h2>
            <asp:Label ID="lblFormName" runat="server"></asp:Label></h2>
        <br />
        <br />
        <br />
        <rad:RadTabStrip ID="TabStrip1" runat="server" SelectedIndex="0" ShowBaseLine="True"
            AutoPostBack="True" MultiPageID="MultiPage1" CausesValidation="False" Width="100%"
            OnTabClick="TabStrip1_TabClick">
            <Tabs>
                <rad:RadTab Text="Create">
                </rad:RadTab>
                <rad:RadTab Text="History">
                </rad:RadTab>
            </Tabs>
        </rad:RadTabStrip>
        <telerik:RadNotification ID="RadNotification1" runat="server" Width="380" Animation="Slide"
            OnClientShowing="CheckIfShow" EnableRoundedCorners="true" EnableShadow="true"
            LoadContentOn="PageLoad" Title="Validation errors" OffsetX="-20" OffsetY="-20"
            TitleIcon="warning" EnableAriaSupport="true" ShowSound="warning" AutoCloseDelay="7000"
            RenderMode="Lightweight">
            <ContentTemplate>
                <div style="background-color: White; padding: 5px 5px 5px 5px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
                </div>
            </ContentTemplate>
        </telerik:RadNotification>
        <div style="border: 1pt solid gray; border-top-style: none; width: 100%; height: 100%;">
            <rad:RadMultiPage ID="MultiPage1" runat="server" SelectedIndex="0" RenderSelectedPageOnly="true"
                Width="100%">
                <rad:RadPageView ID="PageViewCreate" runat="server" Selected="true">
                    <div style="padding: 10px 10px 10px 10px">
                        <asp:Panel ID="pnlMain" runat="server" DefaultButton="btnSubmit">
                            <fieldset style="padding: 10px 10px 10px 10px;">
                                <div style="float: right">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Panel ID="pnlAudienceManager" runat="server" Visible="false">
                                                    <div style="float: right">
                                                        <uc3:AudienceManagerUserControl ID="AudienceManagerUserControl1" runat="server" />
                                                        <p>
                                                        </p>
                                                    </div>
                                                </asp:Panel>
                                            </td>
                                            <td>
                                                <asp:Panel ID="pnlTransferCall" runat="server" Visible="false">
                                                    <div style="float: right">
                                                        <uc4:TransferCallUserControl ID="TransferCallUserControl1" runat="server" />
                                                        <p>
                                                        </p>
                                                    </div>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <asp:Panel ID="pnlContact" runat="server" Visible="false">
                                    <asp:HiddenField ID="hdnContactID" runat="server" />
                                    <uc2:ContactPanelUserControl ID="ContactUserControl1" runat="server" Visible="true" />
                                </asp:Panel>
                                <asp:PlaceHolder ID="formPlaceHolder" runat="server"></asp:PlaceHolder>
                                <asp:HiddenField ID="hdnSignOffUserID" runat="server" />
                                <asp:Panel ID="pnlCallBack" runat="server" Visible="false">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 20%">
                                                <strong>
                                                    <label for="chkCallBackRequired">
                                                        Call Back Required?</label></strong>
                                            </td>
                                            <td style="width: 80%">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButtonList ID="chkCallBackRequired" runat="server" AutoPostBack="true"
                                                                RepeatLayout="Table" RepeatDirection="Horizontal" OnSelectedIndexChanged="chkCallBackRequired_SelectedIndexChanged">
                                                                <asp:ListItem Text="Yes" Value="False">
                                                                </asp:ListItem>
                                                                <asp:ListItem Text="No" Value="True">
                                                                </asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td>
                                                            <asp:RequiredFieldValidator ID="reqCB" runat="server" ControlToValidate="chkCallBackRequired"
                                                                Display="Static" ErrorMessage="Please enter value for Call Back Required" Text="*"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <uc1:CallBackUserControl ID="CallBackUserControl1" runat="server" Visible="false" />
                                </asp:Panel>
                                <asp:Panel ID="pnlLiberator" runat="server" Visible="false">
                                    <div style="float: right">
                                        <a>Click
                                            <asp:HyperLink ID="hypLiberator" Text="here" runat="server" Target="_search"></asp:HyperLink>
                                            to open Sony Website Bug Report Form</a></div>
                                </asp:Panel>
                                <br />
                                <hr />
                                <div style="float: left">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                                    OnClientClick="showNotification();" Width="100px" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnReset" runat="server" Text="Reset" CausesValidation="false" OnClick="btnReset_Click"
                                                    Width="100px" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </fieldset>
                        </asp:Panel>
                        <br />
                    </div>
                </rad:RadPageView>
                <rad:RadPageView ID="PagViewHistory" runat="server">
                    <div style="padding: 10px 10px 10px 10px" id="gridContainer">
                        <asp:Panel ID="pnlReport" runat="server">
                            <rad:RadFilter runat="server" ID="RadFilter1" FilterContainerID="grdTransaction"
                                ShowApplyButton="true" ApplyButtonText="Apply Filter" Visible="true">
                            </rad:RadFilter>
                            <br />
                            <rad:RadGrid ID="grdTransaction" runat="server" AutoGenerateColumns="True" AllowSorting="true"
                                Width="100%" OnNeedDataSource="grdTransaction_NeedDataSource" OnItemDataBound="grdTransaction_ItemDataBound">
                                <PagerStyle Mode="NextPrevNumericAndAdvanced" />
                                <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true">
                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                    <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                                </ClientSettings>
                                <ExportSettings ExportOnlyData="true" IgnorePaging="true" FileName="Report">
                                </ExportSettings>
                                <ItemStyle Wrap="false"></ItemStyle>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true"></Selecting>
                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                    <Resizing AllowResizeToFit="true" />
                                </ClientSettings>
                                <MasterTableView CommandItemDisplay="Top" PageSize="15" AllowPaging="true" EnableHeaderContextMenu="true"
                                    EnableHeaderContextFilterMenu="true" DataKeyNames="TransactionID">
                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToExcelButton="true"
                                        ShowExportToCsvButton="true" />
                                    <SortExpressions>
                                        <rad:GridSortExpression FieldName="TransactionID" SortOrder="Descending" />
                                    </SortExpressions>
                                    <Columns>
                                        <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="EditLink" runat="server" Text="Edit" ImageUrl="~/images/Edit.gif"
                                                    CommandName="FormEdit" ToolTip="Edit Record"></asp:ImageButton>
                                            </ItemTemplate>
                                            <HeaderStyle Width="30px" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                            </rad:RadGrid>
                        </asp:Panel>
                    </div>
                </rad:RadPageView>
            </rad:RadMultiPage>
        </div>
    </div>
    </form>
</body>
</html>

﻿function CloseTab() {
    var idFrame = GetParamByName('iframe');
    pm({
        target: window.parent,
        url: "http://10.31.70.172/ctipad2/",
        type: "message1",
        data: { tabToClose: idFrame }
    });
}

function TransferCall(target) {
    pm({
        target: window.parent,
        url: "http://10.31.70.172/ctipad2/",
        type: "messageTCall",
        data: { tcTarget: target }
    });
}

function GetParamByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results === null) {
        return "";
    }
    else {
        return results[1];
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="LogInAsAnotherUser.aspx.cs" Inherits="CallDispositionTool.Admin.LogInAsAnotherUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <asp:Panel ID="pnlLogin" runat="server" DefaultButton="btnSubmit">
        <div id="formWrap2">
            <div class="profileImage">
            </div>
            <div class="profileLogin">
                <h1>
                    Login</h1>
                <p>
                </p>
                <hr />
                <table>
                    <tr>
                        <td>
                            <h3>
                                <label for="txtUsername">
                                    <span>Admin Username:</span>
                                </label>
                            </h3>
                        </td>
                        <td>
                            <rad:RadTextBox ID="txtUsername" runat="server" Width="150px">
                            </rad:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsername"
                                Display="Dynamic" ErrorMessage="Username is required" SetFocusOnError="True"
                                ForeColor="#CC0000">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h3>
                                <label for="txtUsername">
                                    <span>Admin Password:</span></label></h3>
                        </td>
                        <td>
                            <rad:RadTextBox ID="txtPassword" runat="server" TextMode="Password" Width="150px">
                            </rad:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword"
                                Display="Dynamic" ErrorMessage="Password is required" SetFocusOnError="True"
                                ForeColor="#CC0000">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h3>
                                <label for="txtLoginAsUsername">
                                    <span>Login As Username:</span>
                                </label>
                            </h3>
                        </td>
                        <td>
                            <rad:RadTextBox ID="txtLoginAsUsername" runat="server" Width="150px">
                            </rad:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtLoginAsUsername"
                                Display="Dynamic" ErrorMessage="Login As Username required" SetFocusOnError="True"
                                ForeColor="#CC0000">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnSubmit" runat="server" Text="Log On" Width="100px" OnClick="btnSubmit_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <p>
                            </p>
                            <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
</asp:Content>

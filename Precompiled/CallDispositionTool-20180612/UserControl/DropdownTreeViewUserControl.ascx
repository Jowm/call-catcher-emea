﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DropdownTreeViewUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.DropdownTreeViewUserControl" %>
<div>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManagerProxy ID="ajaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="ddtDisposition">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="ddtDisposition" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <table>
        <tr>
            <td>
                <rad:RadDropDownTree ID="ddtDisposition" runat="server" DefaultMessage="Please select"
                    Width="300px" DataFieldParentID="ParentDispositionID" DataTextField="Description"
                    DataValueField="DispositionID" DataFieldID="DispositionID" TextMode="FullPath"
                    ExpandNodeOnSingleClick="true" FullPathDelimiter="\" OnEntryAdded="ddtDisposition_EntryAdded"
                    OnClientDropDownOpening="OnClientDropDownOpening" OnClientEntryAdded="OnClientEntryAdded"
                    AccessKey="P" OnNodeDataBound="ddtDisposition_NodeDataBound">
                    <ButtonSettings ShowClear="True" />
                    <DropDownSettings Width="300px" Height="250px" />
                </rad:RadDropDownTree>
                <%--                <rad:RadDropDownTree ID="ddtDisposition" runat="server" DefaultMessage="Please select"
                    Width="300px" DataFieldParentID="ParentDispositionID" DataTextField="Description"
                    DataValueField="DispositionID" DataFieldID="DispositionID" TextMode="FullPath"
                    ExpandNodeOnSingleClick="false" FullPathDelimiter="\" OnEntryAdded="ddtDisposition_EntryAdded"
                    OnClientEntryAdded="OnClientEntryAdded" OnClientDropDownOpening="OnClientDropDownOpening">
                    <ButtonSettings ShowClear="True" />
                    <DropDownSettings AutoWidth="Enabled" Height="250px" />
                    <DataBindings>
                        <rad:DropDownNodeBinding ExpandMode="WebService" Depth="0" />
                        <rad:DropDownNodeBinding ExpandMode="WebService" Depth="1" />
                    </DataBindings>
                    <WebServiceSettings Path="DropdownTreeWS.asmx" Method="LoadNodes" />
                    <ClientNodeTemplate>
                        <div>
                           <span style="vertical-align: middle;">#= Text #</span>
                        </div>
                    </ClientNodeTemplate>
                </rad:RadDropDownTree>--%>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="reqDisposition" runat="server" ErrorMessage="Please select a value for Disposition"
                    Text="*" ControlToValidate="ddtDisposition" SetFocusOnError="true" Display="Static"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    function OnClientEntryAdded(sender, eventArgs) {
        setTimeout(function () { sender.closeDropDown(); }, 100);
    }

    function OnClientDropDownOpening(sender, eventArgs) {
        var tree = sender;
        tree.get_entries().clear();
    }
</script>

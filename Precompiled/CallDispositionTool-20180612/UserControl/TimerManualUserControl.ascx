﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimerManualUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.TimerManualUserControl" %>
<h1>
    <time>00:00:00</time>
</h1>
<asp:HiddenField ID="hiddenElapsed" runat="server" ClientIDMode="Static" />
<asp:Button ID="btnStartTimer" runat="server" ClientIDMode="Static" Text="Start" />
<asp:Button ID="btnStopTimer" runat="server" ClientIDMode="Static" Text="Stop" />
<asp:Button ID="btnClearTimer" runat="server" ClientIDMode="Static" Text="Clear" />

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContactDetails.aspx.cs"
    Inherits="CallDispositionTool.ContactDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script src="Scripts/GAnalytics.js" type="text/javascript"></script>
    <link href="CSS/FormStyle.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <title>Contact Details</title>
    <meta http-equiv="Cache-Control" content="cache">
    <meta http-equiv="Pragma" content="cache">
</head>
<body>
    <form id="form1" runat="server">
    <rad:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function on_RequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0 ||
                    args.get_eventTarget().indexOf("FormEdit") >= 0
                    ) {
                    args.set_enableAjax(false);
                }
            }

            function ShowContactDetails(contactID) {
                window.location.href = "ContactDetails.aspx?ContactID=" + contactID;
            }

            function ClosePopup(contact) {
                if (contact)
                    window.opener.SearchBoxSetContact(contact);
                self.close();
            }

            function Test() {
                window.opener.TestFunction();
                self.close();
            }
        </script>
    </rad:RadScriptBlock>
    <rad:RadCodeBlock ID="CodeBlock1" runat="server">
        <script type="text/javascript">
            $(function () {
                $("body").css("display", "none");
                $("body").fadeIn(1000);
            });
        </script>
    </rad:RadCodeBlock>
    <rad:RadSkinManager ID="skinManager1" runat="server" PersistenceMode="Cookie">
    </rad:RadSkinManager>
    <rad:RadFormDecorator ID="formDecorator1" runat="server" EnableRoundedCorners="true"
        RenderMode="Lightweight" ControlsToSkip="H4H5H6" />
    <rad:RadScriptManager ID="ScriptManager1" runat="server">
    </rad:RadScriptManager>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        ShowContentDuringLoad="false" Width="680px" Height="500px" VisibleOnPageLoad="false"
        VisibleStatusbar="false" DestroyOnClose="true" ReloadOnShow="true" Modal="true"
        AutoSize="true" InitialBehaviors="Maximize" RenderMode="Lightweight">
    </telerik:RadWindowManager>
    <rad:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="on_RequestStart" />
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="grdContactExtended">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdContactExtended" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnEdit">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSubmit">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlNew" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="txtEditPhone">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="cboEditZipCode" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="cboEditCity" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="cboEditStateRegion" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="txtEditPostalCode">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="cboEditCity" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="cboEditStateRegion" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="cboEditCity">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="cboEditStateRegion" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="txtPhone">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="cboZipCode" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="cboCity" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="cboStateRegion" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="cboZipCode">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="cboCity" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="cboStateRegion" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="cboCity">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="cboStateRegion" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnEditSubmit">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnEditCancel">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <div>
        <asp:Panel ID="pnlMain" runat="server">
            <h2>
                <asp:Label ID="lblContactName" runat="server"></asp:Label></h2>
            <br />
            <br />
            <br />
            <rad:RadTabStrip ID="TabStrip1" runat="server" SelectedIndex="0" ShowBaseLine="True"
                MultiPageID="MultiPage1" CausesValidation="False" Width="100%">
                <Tabs>
                    <rad:RadTab Text="Details">
                    </rad:RadTab>
                    <rad:RadTab Text="Additional Info">
                    </rad:RadTab>
                    <rad:RadTab Text="New" Visible="false">
                    </rad:RadTab>
                </Tabs>
            </rad:RadTabStrip>
            <div style="border: 1pt solid gray; border-top-style: none; width: 100%; height: 100%;">
                <rad:RadMultiPage ID="MultiPage1" runat="server" SelectedIndex="0" RenderSelectedPageOnly="false"
                    Width="100%">
                    <rad:RadPageView ID="PageViewDetails" runat="server">
                        <div style="padding: 10px 10px 10px 10px">
                            <asp:Panel ID="pnlInfo" runat="server">
                                <div style="float: right">
                                    <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/images/Edit.gif" AlternateText="Perform Edit"
                                        OnClick="btnEdit_Click" ToolTip="Edit Contact" />
                                </div>
                                <table>
                                    <tr>
                                        <td>
                                            <strong>First Name:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Last Name:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblLastName" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Phone:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPhone" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Address 1:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblAddress1" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Store Number:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblStoreNumber" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>NT Login ID:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblNTLoginID" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Transaction ID:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTransactionID" runat="server"></asp:Label>
                                        </td>
                                    </tr>
<%--                                    <tr>
                                        <td>
                                            <strong>Corp/SysPrin:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCorpSysPrin" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Account Number:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblAccountNumber" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <strong>City:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCity" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>State/Region:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblStateRegion" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Postal Code:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPostalCode" runat="server"></asp:Label>
                                        </td>
                                    </tr>--%>
                                    <%--                                    <tr>
                                        <td>
                                            <strong>Comments:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblComments" runat="server"></asp:Label>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td>
                                            <strong>Created By:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Created On</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCreatedOn" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnEditSubmit">
                                <table>
                                    <tr>
                                        <td>
                                            <strong>First Name:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtEditFirstName" runat="server">
                                            </rad:RadTextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEditFirstName"
                                                ErrorMessage="*" Display="Dynamic" ValidationGroup="New"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Last Name:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtEditLastName" runat="server">
                                            </rad:RadTextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEditLastName"
                                                ErrorMessage="*" Display="Dynamic" ValidationGroup="New"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Phone:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtEditPhone" runat="server" 
                                                AutoPostBack="true">
                                            </rad:RadTextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtEditPhone"
                                                ErrorMessage="*" Display="Dynamic" ValidationGroup="New"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Address 1:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtEditAddress1" runat="server" TextMode="MultiLine" Rows="4">
                                            </rad:RadTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Store Number:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtEditStoreNumber" runat="server" Rows="4">
                                            </rad:RadTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>NT Login ID:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtEditNTLoginID" runat="server" Rows="4">
                                            </rad:RadTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Transaction ID:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtEditTransactionID" runat="server" Rows="4">
                                            </rad:RadTextBox>
                                        </td>
                                    </tr>
<%--                                    <tr>
                                        <td>
                                            <strong>Corp/SysPrin:</strong>
                                        </td>
                                        <td>
                                            <rad:RadComboBox ID="cboEditCorpSysPrin" runat="server" MarkFirstMatch="true" DataTextField="CorpSysPrin"
                                                DataValueField="CorpSysPrinID" AppendDataBoundItems="true" EmptyMessage="Please specify"
                                                AllowCustomText="true">
                                                <Items>
                                                    <rad:RadComboBoxItem Text="" Value="" />
                                                </Items>
                                            </rad:RadComboBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="cboEditCorpSysPrin"
                                                ErrorMessage="*" Display="Dynamic" ValidationGroup="New"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Account Number:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtEditAccountNumber" runat="server">
                                            </rad:RadTextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEditAccountNumber"
                                                ErrorMessage="*" Display="Dynamic" ValidationGroup="New"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <strong>Postal Code:</strong>
                                        </td>
                                        <td>
                                            <rad:RadComboBox ID="cboEditZipCode" runat="server" EmptyMessage="Please specify"
                                                MarkFirstMatch="true" AllowCustomText="true" AppendDataBoundItems="true" DataTextField="City"
                                                DataValueField="City" OnSelectedIndexChanged="cboEditZipCode_SelectedIndexChanged"
                                                AutoPostBack="true">
                                                <Items>
                                                    <rad:RadComboBoxItem Text="" Value="" />
                                                </Items>
                                            </rad:RadComboBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*"
                                                Display="Dynamic" ControlToValidate="cboEditZipCode" ValidationGroup="New">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>City:</strong>
                                        </td>
                                        <td>
                                            <rad:RadComboBox ID="cboEditCity" runat="server" EmptyMessage="Please specify" MarkFirstMatch="true"
                                                AllowCustomText="true" AppendDataBoundItems="true" DataTextField="City" DataValueField="City"
                                                OnSelectedIndexChanged="cboEditCity_SelectedIndexChanged" AutoPostBack="true">
                                                <Items>
                                                    <rad:RadComboBoxItem Text="" Value="" />
                                                </Items>
                                            </rad:RadComboBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                                                Display="Dynamic" ControlToValidate="cboEditCity">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>State/Region:</strong>
                                        </td>
                                        <td>
                                            <rad:RadComboBox ID="cboEditStateRegion" runat="server" EmptyMessage="Please specify"
                                                AllowCustomText="true" MarkFirstMatch="true" AppendDataBoundItems="true" DataTextField="State"
                                                DataValueField="State">
                                                <Items>
                                                    <rad:RadComboBoxItem Text="" Value="" />
                                                </Items>
                                            </rad:RadComboBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                                                Display="Dynamic" ControlToValidate="cboEditStateRegion">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
--%>                                    <%--                                    <tr>
                                        <td>
                                            <strong>Comments:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtEditComment" runat="server" TextMode="MultiLine" Rows="4">
                                            </rad:RadTextBox>
                                        </td>
                                    </tr>
                                    --%>
                                </table>
                                <asp:Button ID="btnEditSubmit" runat="server" Text="Submit" OnClick="btnEditSubmit_Click" />
                                <asp:Button ID="btnEditCancel" runat="server" Text="Cancel" OnClick="btnEditCancel_Click" />
                            </asp:Panel>
                        </div>
                    </rad:RadPageView>
                    <rad:RadPageView ID="PageViewAdditional" runat="server">
                        <div style="padding: 10px 10px 10px 10px">
                            <rad:RadGrid ID="grdContactExtended" runat="server" AutoGenerateColumns="false" AllowSorting="true"
                                AllowPaging="true" Width="100%" OnInsertCommand="grdContactExtendedInfo_InsertCommand"
                                OnDeleteCommand="grdContactExtendedInfo_DeleteCommand" OnNeedDataSource="grdContactExtendedInfo_NeedDataSource"
                                OnUpdateCommand="grdContactExtendedInfo_UpdateCommand">
                                <MasterTableView CommandItemDisplay="Top" DataKeyNames="ContactExtendedInfoID" AllowAutomaticInserts="true">
                                    <EditFormSettings>
                                        <EditColumn ButtonType="ImageButton">
                                        </EditColumn>
                                    </EditFormSettings>
                                    <Columns>
                                        <rad:GridEditCommandColumn ButtonType="ImageButton">
                                            <HeaderStyle Width="25px" />
                                        </rad:GridEditCommandColumn>
                                        <rad:GridButtonColumn ButtonType="ImageButton" ImageUrl="~/Images/Delete.gif" CommandName="Delete"
                                            ConfirmText="Are you sure you want to delete this record?">
                                            <HeaderStyle Width="25px" />
                                        </rad:GridButtonColumn>
                                        <rad:GridBoundColumn UniqueName="KeyInfo" DataField="KeyInfo" HeaderText="Key" SortExpression="KeyInfo"
                                            DataType="System.String">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn UniqueName="Value" DataField="Value" HeaderText="Value" SortExpression="Value"
                                            DataType="System.String">
                                        </rad:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                            </rad:RadGrid>
                        </div>
                    </rad:RadPageView>
                    <rad:RadPageView ID="PageViewNew" runat="server">
                        <div style="padding: 10px 10px 10px 10px">
                            <asp:Panel ID="pnlNew" runat="server" DefaultButton="btnSubmit">
                                <table>
                                    <tr>
                                        <td>
                                            <strong>First Name:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtFirstName" runat="server">
                                            </rad:RadTextBox>
                                            <asp:RequiredFieldValidator ID="reqFN" runat="server" ControlToValidate="txtFirstName"
                                                ErrorMessage="*" Display="Dynamic" ValidationGroup="New"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Last Name:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtLastName" runat="server">
                                            </rad:RadTextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtLastName"
                                                ErrorMessage="*" Display="Dynamic" ValidationGroup="New"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
<%--                                    <tr>
                                        <td>
                                            <strong>Corp/SysPrin:</strong>
                                        </td>
                                        <td>
                                            <rad:RadComboBox ID="cboCorpSysPrin" runat="server" MarkFirstMatch="true" DataTextField="CorpSysPrin"
                                                AllowCustomText="true" DataValueField="CorpSysPrinID" AppendDataBoundItems="true"
                                                EmptyMessage="Please specify">
                                                <Items>
                                                    <rad:RadComboBoxItem Text="" Value="" Selected="True" />
                                                    <rad:RadComboBoxItem Text="---" Value="---" Selected="True" />
                                                </Items>
                                            </rad:RadComboBox>
                                            <asp:RequiredFieldValidator ID="reqCorpySysPrin" runat="server" ControlToValidate="cboCorpSysPrin"
                                                ErrorMessage="*" Display="Dynamic" ValidationGroup="New"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Account Number:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtAccountNumber" runat="server">
                                            </rad:RadTextBox>
                                            <asp:RequiredFieldValidator ID="reqAccountNumber" runat="server" ControlToValidate="txtAccountNumber"
                                                ErrorMessage="*" Display="Dynamic" ValidationGroup="New"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
--%>                                    <tr>
                                        <td>
                                            <strong>Phone:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtPhone" runat="server">
                                            </rad:RadTextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPhone"
                                                ErrorMessage="*" Display="Dynamic" ValidationGroup="New"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Address 1:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtAddress1" runat="server" TextMode="MultiLine">
                                            </rad:RadTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Store Number:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtStoreNumber" runat="server">
                                            </rad:RadTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>NT Login ID:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtNTLoginID" runat="server">
                                            </rad:RadTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Transaction ID:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtTransactionID" runat="server">
                                            </rad:RadTextBox>
                                        </td>
                                    </tr>

<%--                                    <tr>
                                        <td>
                                            <strong>Postal Code:</strong>
                                        </td>
                                        <td>
                                            <rad:RadComboBox ID="cboZipCode" runat="server" EmptyMessage="Please specify" MarkFirstMatch="true"
                                                AllowCustomText="true" AppendDataBoundItems="true" DataTextField="City" DataValueField="City"
                                                OnSelectedIndexChanged="cboZipCode_SelectedIndexChanged" AutoPostBack="true">
                                                <Items>
                                                    <rad:RadComboBoxItem Text="" Value="" />
                                                </Items>
                                            </rad:RadComboBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                                                Display="Dynamic" ControlToValidate="cboZipCode" ValidationGroup="New">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>City:</strong>
                                        </td>
                                        <td>
                                            <rad:RadComboBox ID="cboCity" runat="server" EmptyMessage="Please specify" MarkFirstMatch="true"
                                                AllowCustomText="true" AppendDataBoundItems="true" DataTextField="City" DataValueField="City"
                                                OnSelectedIndexChanged="cboCity_SelectedIndexChanged" AutoPostBack="true">
                                                <Items>
                                                    <rad:RadComboBoxItem Text="" Value="" />
                                                </Items>
                                            </rad:RadComboBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                                                Display="Dynamic" ControlToValidate="cboCity" ValidationGroup="New">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>State/Region:</strong>
                                        </td>
                                        <td>
                                            <rad:RadComboBox ID="cboStateRegion" runat="server" EmptyMessage="Please specify"
                                                AllowCustomText="true" MarkFirstMatch="true" AppendDataBoundItems="true" DataTextField="State"
                                                DataValueField="State">
                                                <Items>
                                                    <rad:RadComboBoxItem Text="" Value="" />
                                                </Items>
                                            </rad:RadComboBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                                                Display="Dynamic" ControlToValidate="cboStateRegion" ValidationGroup="New">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
--%>                                    <%--                                    <tr>
                                        <td>
                                            <strong>Comments:</strong>
                                        </td>
                                        <td>
                                            <rad:RadTextBox ID="txtComments" runat="server" TextMode="MultiLine" Rows="4">
                                            </rad:RadTextBox>
                                        </td>
                                    </tr>
                                    --%>
                                </table>
                                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_OnClick" Text="Submit"
                                    ValidationGroup="New" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="ClosePopup()"
                                    ValidationGroup="New" />
                            </asp:Panel>
                        </div>
                    </rad:RadPageView>
                </rad:RadMultiPage>
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>

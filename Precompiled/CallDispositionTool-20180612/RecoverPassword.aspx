﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="RecoverPassword.aspx.cs" Inherits="CallDispositionTool.RecoverPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="viewStep1" runat="server">
            <h2>
                Forgot Password?</h2>
            <br />
            <br />
            <p>
                Enter your User Name to reset your password.</p>
            <label for="txtUserName">
                <strong>Username:</strong>
            </label>
            <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                ID="reqUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage="Enter Username"
                Display="Dynamic" ValidationGroup="SubmitUsername"></asp:RequiredFieldValidator>
            <br />
            <rad:RadCaptcha ID="RadCaptcha1" runat="server" ValidationGroup="SubmitUsername"
                ErrorMessage="The code you entered is not valid." Display="Dynamic" EnableRefreshImage="true"
                CaptchaImage-EnableCaptchaAudio="true">
                <CaptchaImage EnableCaptchaAudio="true">
                </CaptchaImage>
            </rad:RadCaptcha>
            <br />
            <div style="color: Red">
                <asp:Label ID="lblStatus" runat="server"></asp:Label>
            </div>
            <br />
            <asp:Button ID="btnSubmit" runat="server" Text="Next" OnClick="btnSubmit_Click" ValidationGroup="SubmitUsername"
                CausesValidation="true" />
        </asp:View>
        <asp:View ID="viewStep2" runat="server">
            <h2>
                Reset password</h2>
            <br />
            <br />
            <p>
                Enter the answer for the question below.</p>
            <label for="txtQuestion">
                Question:
            </label>
            <asp:TextBox ID="txtQuestion" runat="server" Enabled="false" Width="350px"> </asp:TextBox>
            <br />
            <label for="txtAnswer">
                Answer:
            </label>
            <asp:TextBox ID="txtAnswer" runat="server" Width="150px"></asp:TextBox><asp:RequiredFieldValidator
                ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAnswer" ErrorMessage="Please enter the answer."
                ValidationGroup="SubmitAnswer" Display="Dynamic"></asp:RequiredFieldValidator>
            <br />
            <rad:RadCaptcha ID="RadCaptcha2" runat="server" ValidationGroup="SubmitAnswer" ErrorMessage="The code you entered is not valid."
                Display="Dynamic" EnableRefreshImage="true" CaptchaImage-EnableCaptchaAudio="true">
                <CaptchaImage EnableCaptchaAudio="true"></CaptchaImage>
            </rad:RadCaptcha>
            <br />
            <div style="color: Red">
                <asp:Label ID="lblStatus2" runat="server"></asp:Label>
            </div>
            <br />
            <asp:Button ID="btnSubmit2" runat="server" Text="Reset Password" OnClick="btnSubmit2_Click"
                CausesValidation="true" ValidationGroup="SubmitAnswer" />
        </asp:View>
        <asp:View ID="viewStep3" runat="server">
            <h2>
                Your password has been reset</h2>
            <br />
            <br />
            <p>
                Your new password is indicated below. Click finish to begin login, you are prompted to change password after successful login.</p>
            <label for="txtPassword">
                New Password:
            </label>
            <asp:TextBox ID="txtNewPassword" runat="server" Enabled="false"> </asp:TextBox>
            <br />
            <asp:Button ID="btnFinish" runat="server" Text="Finish" OnClick="btnFinish_Click" />
        </asp:View>
    </asp:MultiView>
</asp:Content>

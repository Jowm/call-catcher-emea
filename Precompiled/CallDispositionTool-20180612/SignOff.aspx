﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignOff.aspx.cs" Inherits="CallDispositionTool.SignOff" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="Scripts/GAnalytics.js" type="text/javascript"></script>
    <link href="CSS/FormStyle.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <title>Sign Off</title>
    <meta http-equiv="Cache-Control" content="cache">
    <meta http-equiv="Pragma" content="cache">
</head>
<body>
    <form id="form1" runat="server">
    <rad:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow)
                    oWindow = window.RadWindow; //Will work in Moz in all cases, including clasic dialog     
                else if (window.frameElement.radWindow)
                    oWindow = window.frameElement.radWindow; //IE (and Moz as well)     
                return oWindow;
            }

            function CloseDialog() {
                GetRadWindow().Close();
            }

            function ProceedSignOff(SignOffUserID) {
                var oArg = new Object();

                oArg.SignOffUserID = document.getElementById('<%= hdnUserID.ClientID %>').value;
                GetRadWindow().close(oArg);
            }
        </script>
    </rad:RadScriptBlock>
    <rad:RadCodeBlock ID="CodeBlock1" runat="server">
        <script type="text/javascript">
            $(function () {
                $("body").css("display", "none");
                $("body").fadeIn(1000);
            });
        </script>
    </rad:RadCodeBlock>
    <rad:RadSkinManager ID="skinManager1" runat="server" PersistenceMode="Cookie">
    </rad:RadSkinManager>
    <rad:RadFormDecorator ID="formDecorator1" runat="server" EnableRoundedCorners="true"
        RenderMode="Lightweight" ControlsToSkip="H4H5H6" />
    <rad:RadScriptManager ID="ScriptManager1" runat="server">
    </rad:RadScriptManager>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        ShowContentDuringLoad="false"  VisibleOnPageLoad="false"
        VisibleStatusbar="false" ReloadOnShow="true" Modal="true"
        RenderMode="Lightweight" DestroyOnClose="true">
    </telerik:RadWindowManager>
    <div>
        <div style="padding: 20px; text-align: left;">
            <h2 runat="server" id="lblHeader">Reauthentication</h2>

            <br />
            <br />
            <label runat="server" id="lblInstructions">
                Please ask your Team Leader for Reauthentication</label>

            <br />
            <br />
            <strong>
                <label for="radTL_ID">
                    Username:</label>
            </strong>
            <br />
            <rad:RadTextBox runat="server" ID="txtUserName" MaxLength="20" AutoCompleteType="None"
                AutoComplete="Off">
            </rad:RadTextBox>
            <asp:RequiredFieldValidator ID="reqradTL_ID" runat="server" ErrorMessage="*" ControlToValidate="txtUserName"></asp:RequiredFieldValidator>
            <br />
            <strong>
                <label for="txtPassword">
                    Password:</label>
            </strong>
            <br />
            <asp:HiddenField ID="hdnUserID" runat="server" />
            <rad:RadTextBox runat="server" ID="txtPassword" TextMode="Password" MaxLength="20"
                ValidationGroup="tlsign" AutoCompleteType="None" AutoComplete="Off">
            </rad:RadTextBox> 
            <asp:RequiredFieldValidator ID="reqPassword" runat="server" ErrorMessage="*"
                ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
            <br />
            <br />
            <asp:Button runat="server" ID="btnSubmit" Text="Sign & Save" OnClick="btnSubmit_Click"
                CausesValidation="true"></asp:Button>
            <asp:Button ID="btnCancelSignOff" runat="server" Text="Cancel" OnClientClick="return CloseDialog();"
                CausesValidation="false" />
        </div>
    </div>
    </form>
</body>
</html>

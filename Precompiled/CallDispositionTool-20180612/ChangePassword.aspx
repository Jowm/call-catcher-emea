﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="ChangePassword.aspx.cs" Inherits="CallDispositionTool.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <asp:ChangePassword ID="ChangePassword1" runat="server" DisplayUserName="true" OnChangedPassword="ChangePassword1_ChangedPassword"
        OnSendingMail="ChangePassword1_SendingMail" OnChangingPassword="ChangePassword1_ChangingPassword"
        OnContinueButtonClick="ChangePassword1_ContinueButtonClick" OnCancelButtonClick="ChangePassword1_CancelButtonClick">
        <LabelStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="11pt" />
        <%--        <MailDefinition BodyFileName="~/MailFiles/ChangePasswordMail.txt" From="noreply@transcom.com"
            IsBodyHtml="True" Subject="Transcom Call Dispostion Tool Password Change Notification">
        </MailDefinition>
        --%>
        <TitleTextStyle Width="13pt" />
    </asp:ChangePassword>
    <br />
    <h4>
        <asp:Label ID="lblStatus" runat="server" ForeColor="Red" Visible="false"></asp:Label></h4>
</asp:Content>

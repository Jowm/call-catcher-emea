﻿<%@ Page Title="Transcom Call Disposition Tool - Home" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CallDispositionTool.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <div style="width: 100%; margin: 20px 0 0 0;">
        <rad:RadDockLayout ID="radDockLayout1" runat="server">
            <table style="width: 100%">
                <tr>
                    <td style="vertical-align: top; width: 50%">
                        <rad:RadDockZone runat="server" ID="RadDockZone1" EnableAnimation="true" Width="100%">
                            <rad:RadDock ID="radDock1" runat="server" Title="My Tracker" Width="100%">
                            </rad:RadDock>
                        </rad:RadDockZone>
                    </td>
                    <td style="vertical-align: top; width: 50%">
                        <rad:RadDockZone runat="server" ID="RadDockZone3" EnableAnimation="true" Width="95%">
                            <rad:RadDock ID="radDock3" runat="server" Title="My Call Back" Width="100%">
                            </rad:RadDock>
                        </rad:RadDockZone>
                        <rad:RadDockZone runat="server" ID="RadDockZone2" EnableAnimation="true" Width="95%">
                            <rad:RadDock ID="radDock2" runat="server" Title="CTI Widget">
                            </rad:RadDock>
                        </rad:RadDockZone>
                    </td>
                </tr>
            </table>
        </rad:RadDockLayout>
    </div>
</asp:Content>

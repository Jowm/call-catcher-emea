﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Admin.aspx.cs" Inherits="CallDispositionTool.Admin.AdminPage" %>

<%@ Register Src="../UserControl/UserAdminUserControl.ascx" TagName="UserAdminUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/PickListOptionAdminUserControl.ascx" TagName="PickListOptionAdminUserControl"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControl/DispositionAdminUserControl.ascx" TagName="DispositionAdminUserControl"
    TagPrefix="uc3" %>
<%@ Register Src="../UserControl/FormManageUserControl.ascx" TagName="FormManageUserControl"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <rad:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }
        </script>
    </rad:RadScriptBlock>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManager ID="ajaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="TabStrip1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="TabStrip1" />
                    <rad:AjaxUpdatedControl ControlID="MultiPage1" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <div style="margin: 0 auto 0 auto; width: 100%;">
        <h2>
            Admin</h2>
        <br />
        <br />
        <br />
        <rad:RadTabStrip ID="TabStrip1" runat="server" SelectedIndex="0" ShowBaseLine="true"
            AutoPostBack="true" MultiPageID="MultiPage1" CausesValidation="false" Width="867px">
            <Tabs>
                <rad:RadTab Text="User Access" Visible="false">
                </rad:RadTab>
                <rad:RadTab Text="Pick List Option" Visible="false">
                </rad:RadTab>
                <rad:RadTab Text="Disposition" Visible="false">
                </rad:RadTab>
                <rad:RadTab Text="Form Management" Visible="false">
                </rad:RadTab>
            </Tabs>
        </rad:RadTabStrip>
        <div style="border: 1pt solid gray; border-top-style: none; width: 867px">
            <rad:RadMultiPage ID="MultiPage1" runat="server" SelectedIndex="0" RenderSelectedPageOnly="true"
                Width="867px">
                <rad:RadPageView ID="PageViewUser" runat="server" Selected="true">
                    <uc1:UserAdminUserControl ID="UserAdminUserControl1" runat="server" />
                </rad:RadPageView>
                <rad:RadPageView ID="PageViewPickListOption" runat="server">
                    <uc2:PickListOptionAdminUserControl ID="PickListOptionAdminUserControl1" runat="server" />
                </rad:RadPageView>
                <rad:RadPageView ID="PageViewDisposition" runat="server">
                    <uc3:DispositionAdminUserControl ID="DispositionAdminUserControl1" runat="server" />
                </rad:RadPageView>
                <rad:RadPageView ID="PageViewForm" runat="server">
                    <uc4:FormManageUserControl ID="FormManageUserControl1" runat="server" />
                </rad:RadPageView>
                <rad:RadPageView ID="PageViewUserForm" runat="server">
                    <uc4:FormManageUserControl ID="FormManageUserControl2" runat="server" />
                </rad:RadPageView>
                <rad:RadPageView ID="PageViewUserRole" runat="server">
                    <uc4:FormManageUserControl ID="FormManageUserControl3" runat="server" />
                </rad:RadPageView>
            </rad:RadMultiPage>
        </div>
        <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />
        <asp:GridView ID="Menu1" runat="server" AutoGenerateColumns="false" CssClass="DDGridView"
            RowStyle-CssClass="td" HeaderStyle-CssClass="th" CellPadding="6">
            <Columns>
                <asp:TemplateField HeaderText="Table Name" SortExpression="TableName">
                    <ItemTemplate>
                        <asp:DynamicHyperLink ID="HyperLink1" runat="server"><%# Eval("DisplayName") %></asp:DynamicHyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>

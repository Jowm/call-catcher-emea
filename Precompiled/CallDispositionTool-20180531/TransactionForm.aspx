﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransactionForm.aspx.cs"
    Inherits="CallDispositionTool.TransactionForm" %>

<%@ OutputCache Duration="1440" VaryByParam="FormID" Location="Client" %>
<%@ Register Src="UserControl/CallBackUserControl.ascx" TagName="CallBackUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/ContactPanelUserControl.ascx" TagName="ContactPanelUserControl"
    TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" runat="server" id="tranHTML">
<head id="Head1" runat="server">
    <script src='Scripts/jquery-1.6.4.min.js' type="text/javascript"></script>
    <script src="Scripts/GAnalytics.js" type="text/javascript"></script>
    <script src="Scripts/postmessage.js" type="text/javascript"></script>
    <script src="Scripts/CTIPadSDK.js" type="text/javascript"></script>
    <link href="CSS/FormStyle.css" rel="stylesheet" type="text/css" />
    <title>Transaction Form</title>
    <meta http-equiv="Cache-Control" content="cache">
    <meta http-equiv="Pragma" content="cache">
</head>
<body id="frmBody" runat="server">
    <form id="form1" runat="server">
    <rad:RadCodeBlock ID="CodeBlock1" runat="server">
        <script type="text/javascript">
            $(function () {
                $("body").css("display", "none");
                $("body").fadeIn(1000);
            });
        </script>
    </rad:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel runat="server" ID="ralpConfiguration">
    </telerik:RadAjaxLoadingPanel>
    <rad:RadSkinManager ID="skinManager1" runat="server" PersistenceMode="Cookie">
    </rad:RadSkinManager>
    <rad:RadFormDecorator ID="formDecorator1" runat="server" EnableRoundedCorners="true"
        RenderMode="Lightweight" ControlsToSkip="H4H5H6" />
    <rad:RadScriptManager ID="ScriptManager1" runat="server">
    </rad:RadScriptManager>
    <rad:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function SearchBoxSetContact(contact) {
                if (contact) {
                    document.getElementById('<%= hdnContactID.ClientID %>').value = contact;
                    $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("RefreshContact");
                }
            }
            
            function PageReset(args) {
                location.href = window.location.href;
            }

            function startTrackProgress(grid) {
                if (!window._trackStart) {
                    window._trackStart = new Date();
                }
            }

            function endTrackProgress(grid) {
                var duration = getBindDuration();
                setStatusLabelVisiblity(grid, false);
                setDurationVisiblity(grid, true, duration);
            }

            function onRequestStart(sender, args) {
                startTrackProgress();

                if (args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }

            function showNotification() {
                var notification = $find("<%=RadNotification1.ClientID %>");
                setTimeout(function () {
                    notification.show();
                }, 0);
            }

            function showSignOffForm(FormID) {
                var windowURL = '<%= ResolveClientUrl("~/SignOff.aspx") %>' + '?FormID=' + FormID;
                var oWnd = radopen(windowURL, null);
                oWnd.setSize(400, 300);
                oWnd.set_title("Sign Off Required");
                oWnd.center();
            }

            function CheckIfShow(sender, args) {
                var summaryElem = document.getElementById("<%= ValidationSummary1.ClientID%>");
                //check if summary is visible - if not, there are not errors, do not notify and continue with postback
                var noErrors = summaryElem.style.display == "none";
                //validateSignOff(noErrors);
                args.set_cancel(noErrors);
            }

            function ViewTransaction(transactionID, rowIndex) {
                <%
                    var AllowEditTransaction = CallDispositionTool.Helper.IsUserAllowEditTransaction(Convert.ToInt32(Membership.GetUser().ProviderUserKey));

                    if (AllowEditTransaction) {
                %>
                var windowURL = '<%= ResolveClientUrl("~/TransactionEdit.aspx") %>' + '?TransactionID=' + transactionID;
                var grid = $find("<%= grdTransaction.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element(); grid.get_masterTableView().selectItem(rowControl, true);
                var oWnd = radopen(windowURL, null);
                oWnd.set_title("Transaction Edit - " + transactionID);
                oWnd.maximize();
                oWnd.center();

                <% }
                    else {
                %>
                var windowURL = '<%= ResolveClientUrl("~/TransactionEdit.aspx") %>?Mode=View' + '&TransactionID=' + transactionID;
                var grid = $find("<%= grdTransaction.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element(); grid.get_masterTableView().selectItem(rowControl, true);
                var oWnd = radopen(windowURL, null);
                oWnd.set_title("Transaction View - " + transactionID);
                oWnd.maximize();
                oWnd.center();

                <% } %>

                return false;
            }

            function refreshGrid() {
                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("Rebind");
            }

            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();

                if (arg) {
                    if (arg.SignOffUserID > 0) {
                        document.getElementById('<%= hdnSignOffUserID.ClientID %>').value = arg.SignOffUserID;
                        $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("SaveAndSignOffRecord");
                    }
                    else if (arg.ContactID > 0) {
                        document.getElementById('<%= hdnContactID.ClientID %>').value = arg.ContactID;
                        $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("RefreshContact");
                    }
                }
            }

            function OnClientSelectedIndexChanged(sender, args) {
                args.get_item().set_checked(args.get_item().get_selected());
            }
            
            function AssignContact(val) {
                var $ = $telerik.$;
                var searchBox = $find("rsbInput ");

                if (searchBox) {
                    alert(searchBox.get_text());
                }
            } 
            Date.prototype.today = function () { 
    return (((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1)  +"/"+ ((this.getDate() < 10)?"0":"") + this.getDate() +"/"+ this.getFullYear();
}

// For the time now
Date.prototype.timeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}

var SetEndTime = function() {
     var datetime = new Date().today() + " " + new Date().timeNow();
            $('input#hdnEnd').attr('value', datetime);
              setTimeout(SetEndTime, 3000); 
};

var smartPadctr = 0;

function divClicked() {
    var divHtml = $(this).html();
    var editableText = $("<textarea />");
    editableText.val(divHtml);
    $(this).replaceWith(editableText);
    editableText.focus();
    // setup the blur event for this new textarea
    editableText.blur(editableTextBlurred);
}

function editableTextBlurred() {
    var html = $(this).val();
    var viewableText = $("<div>");
    viewableText.html(html);
    $(this).replaceWith(viewableText);
    // setup the click event for this new div
    viewableText.click(divClicked);
}

            function pageLoad() {
                var $ = $telerik.$;

                //prevent showing the required field validator when enter was pressed on the textbox
                $(".rsbInput ").keypress(function (event) {
                    if (event.which == 13) {
                        return false;
                    }
                });

                var h1 = document.getElementsByTagName('h1')[0],
                    start = document.getElementById('start'),
                    stop = document.getElementById('stop'),
                    clear = document.getElementById('clear'),
                    seconds = 0, minutes = 0, hours = 0,
                    t;

                function add() {
                    seconds++;
                    if (seconds >= 60) {
                        seconds = 0;
                        minutes++;
                        if (minutes >= 60) {
                            minutes = 0;
                            hours++;
                        }
                    }
    
                    h1.textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);
                    $('input#hiddenElapsed').attr('value', h1.textContent);

                    timer();
                }

                function timer() {
                    t = setTimeout(add, 1000);
                    return false;
                }
                timer();

                /* Start button */
                $("#btnStartTimer").onclick = timer;               
                //start.onclick = timer;

                /* Stop button */
                $("#btnStopTimer").onclick = function() {
                    clearTimeout(t);
                    return false;
                }
                //stop.onclick = function() {
                //    clearTimeout(t);
                //}

                /* Clear button */
                $("#btnClearTimer").onclick = function() {
                    h1.textContent = "00:00:00";
                    $('input#hiddenElapsed').attr('value', "00:00:00");
                    seconds = 0; minutes = 0; hours = 0;
                    return false;
                }
                //clear.onclick = function() {
                //    h1.textContent = "00:00:00";
                //    $('input#hiddenElapsed').attr('value', "00:00:00");
                //    seconds = 0; minutes = 0; hours = 0;
                //}

                 SetEndTime();

                $(".casenotes").one("keypress", function () {
                    var datetime = new Date().today() + " " + new Date().timeNow();
                    $('input#hdnStart').attr('value', datetime);
                });

                $(".drop").change(function () {
                    if (smartPadctr == 0){
                        var datetime = new Date().today() + " " + new Date().timeNow();
                        $('input#hdnStartDrop').attr('value', datetime);
                        smartPadctr++;
                    }
                });

            }
        </script>
    </rad:RadScriptBlock>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Width="680px" Height="500px" VisibleOnPageLoad="false" VisibleStatusbar="false"
        Modal="true" RenderMode="Lightweight" OnClientClose="OnClientClose" DestroyOnClose="true"
        Animation="Fade">
    </telerik:RadWindowManager>
    <rad:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="TabStrip1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="TabStrip1" />
                    <rad:AjaxUpdatedControl ControlID="MultiPage1" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="grdTransaction" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="grdTransaction">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdTransaction" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="grdSmartPad">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdSmartPad" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="RadFilter1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlReport" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSubmit">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="btnNew" />
                    <rad:AjaxUpdatedControl ControlID="lblStatus" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSubmitSmartPad">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="btnNew" />
                    <rad:AjaxUpdatedControl ControlID="lblStatus" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnReset">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="btnNew" />
                    <rad:AjaxUpdatedControl ControlID="lblStatus" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="btnNew" />
                    <rad:AjaxUpdatedControl ControlID="lblStatus" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="btnNew" />
                    <rad:AjaxUpdatedControl ControlID="lblStatus" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="chkCallBackRequired">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <rad:RadNotification ID="RadNotification1" runat="server" Width="380" Animation="Slide"
        OnClientShowing="CheckIfShow" EnableRoundedCorners="true" EnableShadow="true"
        LoadContentOn="PageLoad" Title="Validation errors" OffsetX="-20" OffsetY="-20"
        TitleIcon="warning" EnableAriaSupport="true" ShowSound="warning" AutoCloseDelay="3000"
        RenderMode="Lightweight">
        <ContentTemplate>
            <div style="background-color: White; padding: 5px 5px 5px 5px">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList">
                </asp:ValidationSummary>
            </div>
        </ContentTemplate>
    </rad:RadNotification>
    <div style="margin: 0 auto 0 auto; width: 100%; height: 100%">
        <h2>
            <asp:Label ID="lblFormName" runat="server"></asp:Label></h2>
        <asp:PlaceHolder ID="HeaderPlaceHolder" runat="server"></asp:PlaceHolder>
        <br />
        <asp:Label ID="lblErrorMessage" Text="" runat="server" />
        <br />
        <rad:RadTabStrip ID="TabStrip1" runat="server" SelectedIndex="0" ShowBaseLine="True"
            AutoPostBack="True" MultiPageID="MultiPage1" CausesValidation="False" Width="100%"
            OnTabClick="TabStrip1_TabClick">
            <Tabs>
                <rad:RadTab Text="Create" Selected="True">
                </rad:RadTab>
                <rad:RadTab Text="History">
                </rad:RadTab>
            </Tabs>
        </rad:RadTabStrip>
        <div style="border: 1pt solid gray; border-top-style: none; width: 100%; height: 100%;">
            <rad:RadMultiPage ID="MultiPage1" runat="server" SelectedIndex="0" RenderSelectedPageOnly="true"
                Width="100%">
                <rad:RadPageView ID="PageViewCreate" runat="server" Selected="true">
                    <div style="padding: 10px 10px 10px 10px">
                        <asp:Panel ID="pnlMain" runat="server" DefaultButton="btnSubmit" Width="100%">
                            <fieldset style="padding: 10px 10px 10px 10px;" width="100%">
                                <asp:Panel ID="pnlContact" runat="server" Visible="false">
                                    <asp:HiddenField ID="hdnContactID" runat="server" />
                                    <uc2:ContactPanelUserControl ID="ContactUserControl1" runat="server" Visible="true" />
                                </asp:Panel>
                                <asp:PlaceHolder ID="formPlaceHolder" runat="server"></asp:PlaceHolder>
                                <asp:HiddenField ID="hdnSignOffUserID" runat="server" />
                                <asp:Panel ID="pnlCallBack" runat="server" Visible="false">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 20%">
                                                <strong>
                                                    <label for="chkCallBackRequired">
                                                        Call Back Required?</label></strong>
                                            </td>
                                            <td style="width: 80%">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButtonList ID="chkCallBackRequired" runat="server" AutoPostBack="true"
                                                                RepeatLayout="Table" RepeatDirection="Horizontal" OnSelectedIndexChanged="chkCallBackRequired_SelectedIndexChanged">
                                                                <asp:ListItem Text="Yes" Value="False">
                                                                </asp:ListItem>
                                                                <asp:ListItem Text="No" Value="True">
                                                                </asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td>
                                                            <asp:RequiredFieldValidator ID="reqCB" runat="server" ControlToValidate="chkCallBackRequired"
                                                                Display="Static" ErrorMessage="Please enter value for Call Back Required" Text="*"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <uc1:CallBackUserControl ID="CallBackUserControl1" runat="server" Visible="false" />
                                </asp:Panel>
                                <asp:Panel ID="pnlLiberator" runat="server" Visible="false">
                                    <div style="float: right">
                                        <a>Click
                                            <asp:HyperLink ID="hypLiberator" Text="here" runat="server" Target="_search"></asp:HyperLink>
                                            to open Sony Website Bug Report Form</a></div>
                                </asp:Panel>
                                <br />
                                <hr />
                                <div style="float: left">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                                    OnClientClick="showNotification();" Width="100px" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnReset" runat="server" Text="Reset" CausesValidation="false" OnClientClick="PageReset();"
                                                    Width="100px" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 100%; text-align: center;">
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="PageReset();"
                                        CausesValidation="false" Width="100px" CssClass="btnLeft rfdSkinnedButton" Visible="false" />
                                    <asp:Button ID="btnDropCall" runat="server" Text="Drop Call" CausesValidation="false"
                                        Width="100px" CssClass="btnCenter rfdSkinnedButton" Visible="false" OnClick="btnDropCall_Click" />
                                    <asp:Button ID="btnSubmitSmartPad" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                        OnClientClick="showNotification();" Width="100px" Style="float: right;" ClientIDMode="Predictable" />
                                </div>
                            </fieldset>
                        </asp:Panel>
                        <br />
                    </div>
                </rad:RadPageView>
                <rad:RadPageView ID="PagViewHistory" runat="server">
                    <asp:Panel ID="pnlReport" runat="server">
                        <div style="padding: 10px 10px 10px 10px" id="gridContainer">
                            <rad:RadFilter runat="server" ID="RadFilter1" FilterContainerID="grdTransaction"
                                ShowApplyButton="true" ApplyButtonText="Apply Filter" Visible="true">
                            </rad:RadFilter>
                            <br />
                            <rad:RadGrid ID="grdTransaction" runat="server" AutoGenerateColumns="True" AllowSorting="true"
                                AllowCustomPaging="True" AllowPaging="True" Width="100%" OnNeedDataSource="grdTransaction_NeedDataSource"
                                OnItemDataBound="grdTransaction_ItemDataBound" OnColumnCreated="grdTransaction_ColumnCreated">
                                <PagerStyle Mode="NextPrevNumericAndAdvanced" AlwaysVisible="true" />
                                <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true">
                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                    <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                                </ClientSettings>
                                <ExportSettings ExportOnlyData="true" IgnorePaging="true" FileName="Report">
                                </ExportSettings>
                                <ItemStyle Wrap="false"></ItemStyle>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true"></Selecting>
                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                    <Resizing AllowResizeToFit="true" />
                                </ClientSettings>
                                <MasterTableView CommandItemDisplay="Top" PageSize="15" AllowPaging="true" EnableHeaderContextMenu="true"
                                    EnableHeaderContextFilterMenu="true" DataKeyNames="TransactionID" IsFilterItemExpanded="false"
                                    TableLayout="Fixed" ShowFooter="true">
                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToExcelButton="true"
                                        ShowExportToCsvButton="true" />
                                    <SortExpressions>
                                        <rad:GridSortExpression FieldName="TransactionID" SortOrder="Descending" />
                                    </SortExpressions>
                                    <Columns>
                                        <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false"
                                            Exportable="false">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="EditLink" runat="server" Text="Edit" ImageUrl="~/images/Eye16.png"
                                                    CommandName="FormEdit" ToolTip="View Details"></asp:ImageButton>
                                            </ItemTemplate>
                                            <HeaderStyle Width="30px" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                            </rad:RadGrid>
                            <telerik:RadGrid ID="grdSmartPad" runat="server" OnNeedDataSource="grdSmartPad_NeedDataSource"
                                AllowPaging="True" EnableViewState="true" OnDetailTableDataBind="grdSmartPadDetail_DataBind"
                                OnItemCommand="grdSmartPad_ItemCommand">
                                <ExportSettings ExportOnlyData="true" IgnorePaging="true" FileName="Report">
                                </ExportSettings>
                                <GroupingSettings CaseSensitive="False" />
                                <FilterItemStyle HorizontalAlign="Center" />
                                <SortingSettings EnableSkinSortStyles="false" />
                                <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="True" EnableHeaderContextMenu="true"
                                    DataKeyNames="TransactionID" PageSize="15" TableLayout="Fixed" ShowFooter="true">
                                    <CommandItemSettings ShowExportToExcelButton="True" ShowExportToCsvButton="True"
                                        ShowAddNewRecordButton="false" ShowRefreshButton="True" />
                                    <DetailTables>
                                        <rad:GridTableView Name="grdSmartPadDetail" runat="server" AutoGenerateColumns="True"
                                            AllowSorting="true" AllowPaging="true" Width="100%" EnableViewState="true">
                                        </rad:GridTableView>
                                    </DetailTables>
                                </MasterTableView>
                            </telerik:RadGrid>
                        </div>
                    </asp:Panel>
                </rad:RadPageView>
            </rad:RadMultiPage>
        </div>
    </div>
    </form>
</body>
</html>

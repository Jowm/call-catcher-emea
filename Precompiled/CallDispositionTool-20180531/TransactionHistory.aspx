﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransactionHistory.aspx.cs"
    Inherits="CallDispositionTool.TransactionHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script src="Scripts/GAnalytics.js" type="text/javascript"></script>
    <link href="CSS/FormStyle.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <title>Transaction History</title>
    <meta http-equiv="Cache-Control" content="cache">
    <meta http-equiv="Pragma" content="cache">
</head>
<body>
    <form id="form1" runat="server">
    <rad:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function on_RequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0 ||
                    args.get_eventTarget().indexOf("FormEdit") >= 0
                    ) {
                    args.set_enableAjax(false);
                }
            }

            function ShowEditForm(transactionID, rowIndex) {
                var windowURL = '<%= ResolveClientUrl("~/TransactionEdit.aspx") %>' + '?Mode=View&TransactionID=' + transactionID;
                var grid = $find("<%= grdTransaction.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element(); grid.get_masterTableView().selectItem(rowControl, true);
                var oWnd = radopen(windowURL, null);
                oWnd.set_title("Transaction Edit - " + transactionID);
                oWnd.center();

                return false;
            }

            function refreshGrid() {
                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("Rebind");
            }
        </script>
    </rad:RadScriptBlock>
    <rad:RadCodeBlock ID="CodeBlock1" runat="server">
        <script type="text/javascript">
            $(function () {
                $("body").css("display", "none");
                $("body").fadeIn(1000);
            });
        </script>
    </rad:RadCodeBlock>
    <rad:RadSkinManager ID="skinManager1" runat="server" PersistenceMode="Cookie">
    </rad:RadSkinManager>
    <rad:RadFormDecorator ID="formDecorator1" runat="server" EnableRoundedCorners="true"
        RenderMode="Lightweight" ControlsToSkip="H4H5H6" />
    <rad:RadScriptManager ID="ScriptManager1" runat="server">
    </rad:RadScriptManager>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        ShowContentDuringLoad="false" Width="680px" Height="500px" VisibleOnPageLoad="false"
        VisibleStatusbar="false" DestroyOnClose="true" ReloadOnShow="true" Modal="true"
        AutoSize="true" InitialBehaviors="Maximize" RenderMode="Lightweight">
    </telerik:RadWindowManager>
    <rad:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="on_RequestStart" />
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="TabStrip1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="TabStrip1" />
                    <rad:AjaxUpdatedControl ControlID="MultiPage1" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdTransaction" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="grdTransaction">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdTransaction" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="RadFilter1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlReport" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <div>
        <h2>
            <asp:Label ID="lblContactName" runat="server"></asp:Label></h2>
        <br />
        <br />
        <br />
        <rad:RadTabStrip ID="TabStrip1" runat="server" SelectedIndex="0" ShowBaseLine="True"
            AutoPostBack="True" MultiPageID="MultiPage1" CausesValidation="False" Width="100%"
            OnTabClick="TabStrip1_TabClick">
            <Tabs>
                <rad:RadTab Text="Transactions">
                </rad:RadTab>
            </Tabs>
        </rad:RadTabStrip>
        <div style="border: 1pt solid gray; border-top-style: none; width: 100%; height: 100%;">
            <rad:RadMultiPage ID="MultiPage1" runat="server" SelectedIndex="0" RenderSelectedPageOnly="true"
                Width="100%">
                <rad:RadPageView ID="PageViewTranscaction" runat="server">
                    <asp:Panel ID="pnlReport" runat="server">
                        <div style="padding: 10px 10px 10px 10px" id="gridContainer">
                            <rad:RadFilter runat="server" ID="RadFilter1" FilterContainerID="grdTransaction"
                                ShowApplyButton="true" ApplyButtonText="Apply Filter" Visible="true">
                            </rad:RadFilter>
                            <br />
                            <rad:RadGrid ID="grdTransaction" runat="server" AutoGenerateColumns="True" AllowSorting="true"
                                AllowPaging="True" Width="100%" OnNeedDataSource="grdTransaction_NeedDataSource"
                                OnItemDataBound="grdTransaction_ItemDataBound">
                                <PagerStyle Mode="NextPrevNumericAndAdvanced" AlwaysVisible="true" />
                                <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true">
                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                    <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                                </ClientSettings>
                                <ExportSettings ExportOnlyData="true" IgnorePaging="true" FileName="Report">
                                </ExportSettings>
                                <ItemStyle Wrap="false"></ItemStyle>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true"></Selecting>
                                    <Scrolling UseStaticHeaders="true" />
                                    <Resizing AllowResizeToFit="true" />
                                </ClientSettings>
                                <MasterTableView CommandItemDisplay="Top" PageSize="15" AllowPaging="true" EnableHeaderContextMenu="true"
                                    EnableHeaderContextFilterMenu="true" DataKeyNames="TransactionID" IsFilterItemExpanded="false">
                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToExcelButton="true"
                                        ShowExportToCsvButton="true" />
                                    <SortExpressions>
                                        <rad:GridSortExpression FieldName="TransactionID" SortOrder="Descending" />
                                    </SortExpressions>
                                    <Columns>
                                        <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false"
                                            Exportable="false">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="EditLink" runat="server" Text="Edit" ImageUrl="~/images/Eye16.png"
                                                    CommandName="FormEdit" ToolTip="View Details"></asp:ImageButton>
                                            </ItemTemplate>
                                            <HeaderStyle Width="30px" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                            </rad:RadGrid>
                        </div>
                    </asp:Panel>
                </rad:RadPageView>
            </rad:RadMultiPage>
        </div>
    </div>
    </form>
</body>
</html>

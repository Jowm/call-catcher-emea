﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="CallDispositionTool.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <div style="min-height: 400px">
        <asp:Panel ID="pnlLogin" runat="server" DefaultButton="btnSubmit">
            <div id="formWrap">
                <div class="profileImage">
                </div>
                <div class="profileLogin">
                    <h1>
                        Login</h1>
                    <p>
                    </p>
                    <hr />
                    <table>
                        <tr>
                            <td>
                                <h3>
                                    <label for="txtUsername">
                                        <span>Username:</span>
                                    </label>
                                </h3>
                            </td>
                            <td>
                                <rad:radtextbox id="txtUsername" runat="server" width="150px" autocomplete="Off">
                                </rad:radtextbox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsername"
                                    Display="Dynamic" ErrorMessage="Username is required" SetFocusOnError="True"
                                    ForeColor="#CC0000">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h3>
                                    <label for="txtUsername">
                                        <span>Password:</span></label></h3>
                            </td>
                            <td>
                                <rad:radtextbox id="txtPassword" runat="server" textmode="Password" width="150px"
                                    autocomplete="Off">
                                </rad:radtextbox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword"
                                    Display="Dynamic" ErrorMessage="Password is required" SetFocusOnError="True"
                                    ForeColor="#CC0000">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <a>Forgot password? Click</a> <a href="~/RecoverPassword.aspx" runat="server">here</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" ClientIDMode="Static" Text="Log On" Width="100px"
                                    OnClick="btnSubmit_Click" OnClientClick="return ValidateAndShowSpinner();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <p>
                                </p>
                                <strong style="color: Red">
                                    <asp:Label ID="lblErrorMessage" runat="server"></asp:Label></strong>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>

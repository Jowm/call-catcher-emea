﻿<%@ Page Title="Contacts" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Contact.aspx.cs" Inherits="CallDispositionTool.Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <rad:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function refreshGrid() {
            }

            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }

            function OpenTransaction(CallBackID) {
                var oWnd = radopen("TransactionPopup.aspx?CallBackID=" + CallBackID, "RadWindow1");
            }

            function OnClientClose(oWnd, args) {
                var arg = args.get_argument();
                if (arg) {
                }
            }

            function PopUpShowing(sender, args) {
                setTimeout(function () {
                    var popUp = args.get_popUp();
                    var screenSize = $telerik.getViewPortSize();
                    popUp.style.top = ((screenSize.height - popUp.offsetHeight) / 2) + "px";
                    popUp.style.left = ((screenSize.width - popUp.offsetWidth) / 2) + "px";
                }, 1)
            }

            function ShowEditForm(transactionID, rowIndex) {
                var windowURL = '<%= ResolveClientUrl("~/TransactionEdit.aspx") %>' + '?TransactionID=' + transactionID;
                var oWnd = radopen(windowURL, null);
                oWnd.set_title("Transaction Edit - " + transactionID);
                oWnd.center();

                return false;
            }

            function pageLoad() {
                var grid = $find('<%= grdContacts.ClientID %>');
                var columns = grid.get_masterTableView().get_columns();
                for (var i = 0; i < columns.length; i++) {
                    columns[i].resizeToFit();
                }
            }
        </script>
    </rad:RadScriptBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true" Modal="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientClose="OnClientClose" ShowContentDuringLoad="false"
                Height="600px" Width="800px" AutoSizeBehaviors="Default" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManager ID="ajaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="grdContacts">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdContacts" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="RadFilter1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlContact" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <asp:Panel ID="pnlContact" runat="server">
        <h2>
            Contacts</h2>
        <br />
        <br />
        <rad:RadFilter runat="server" ID="RadFilter1" FilterContainerID="grdContacts" ShowApplyButton="true">
        </rad:RadFilter>
        <br />
        <rad:RadGrid ID="grdContacts" runat="server" AutoGenerateColumns="true" AllowSorting="true"
            AllowPaging="true" Width="868px" DataSourceID="dsContact" OnPreRender="grdContacts_PreRender"
            AllowCustomPaging="True" OnInsertCommand="grdContacts_InsertCommand" OnUpdateCommand="grdContacts_UpdateCommand"
            OnDeleteCommand="grdContacts_DeleteCommand" VirtualItemCount="10000">
            <ExportSettings ExportOnlyData="true" IgnorePaging="true" FileName="Contacts">
            </ExportSettings>
            <ClientSettings>
                <Scrolling UseStaticHeaders="true" />
                <Resizing AllowColumnResize="true" ShowRowIndicatorColumn="true" />
                <ClientEvents OnPopUpShowing="PopUpShowing" />
            </ClientSettings>
            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="false" EnableHeaderContextMenu="true"
                DataKeyNames="ContactID" EditMode="PopUp" AllowCustomPaging="true" AllowPaging="true"
                PageSize="15">
                <CommandItemSettings ShowExportToExcelButton="true" ShowExportToCsvButton="true" />
                <EditFormSettings EditColumn-ButtonType="PushButton" FormCaptionStyle-Font-Size="12pt"
                    CaptionFormatString="Edit Contact Info: {0}" CaptionDataField="ContactID" InsertCaption="Add Contact Info">
                    <EditColumn ButtonType="PushButton" FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                    <PopUpSettings Modal="true" ScrollBars="Auto" ShowCaptionInEditForm="false" />
                </EditFormSettings>
                <PagerStyle Mode="NextPrevNumericAndAdvanced" AlwaysVisible="true" />
                <Columns>
                    <rad:GridEditCommandColumn ButtonType="ImageButton">
                        <HeaderStyle Width="25px" />
                    </rad:GridEditCommandColumn>
                    <rad:GridButtonColumn ButtonType="ImageButton" ImageUrl="~/Images/Delete.gif" CommandName="Delete"
                        ConfirmText="Are you sure you want to delete this contact?">
                        <HeaderStyle Width="25px" />
                    </rad:GridButtonColumn>
                    <rad:GridBoundColumn UniqueName="ContactID" DataField="ContactID" HeaderText="Contact ID"
                        SortExpression="ContactID" DataType="System.Int32" ReadOnly="true">
                        <HeaderStyle Width="90px" />
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="FirstName" DataField="FirstName" HeaderText="First Name"
                        SortExpression="FirstName" DataType="System.String">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="LastName" DataField="LastName" HeaderText="Last Name"
                        SortExpression="LastName" DataType="System.String">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="Tags" DataField="Tags" HeaderText="Tags" SortExpression="Tags"
                        DataType="System.String" Display="false">
                    </rad:GridBoundColumn>
                    <rad:GridDropDownColumn UniqueName="CompanyID" DataField="CompanyID" HeaderText="Company"
                        ListTextField="CompanyName" ListValueField="CompanyID" DataSourceID="dsCompany"
                        DropDownControlType="RadComboBox" EmptyListItemText="" EmptyListItemValue=""
                        EnableEmptyListItem="true" Display="false">
                    </rad:GridDropDownColumn>
                    <rad:GridBoundColumn UniqueName="Title" DataField="Title" HeaderText="Title" SortExpression="Title"
                        DataType="System.String">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="Email" DataField="Email" HeaderText="Email" SortExpression="Email"
                        DataType="System.String">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="Phone" DataField="Phone" HeaderText="Phone" SortExpression="Phone"
                        DataType="System.String">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="MobilePhone" DataField="MobilePhone" HeaderText="Mobile Phone"
                        SortExpression="MobilePhone" DataType="System.String">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="Fax" DataField="Fax" HeaderText="Fax" SortExpression="Fax"
                        DataType="System.String" Display="false">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="Address1" DataField="Address1" HeaderText="Address 1"
                        SortExpression="Address1" DataType="System.String">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="Address2" DataField="Address2" HeaderText="Address 2"
                        SortExpression="Address2" DataType="System.String" Display="false">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="City" DataField="City" HeaderText="City" SortExpression="City"
                        DataType="System.String" Display="false">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="StateRegion" DataField="StateRegion" HeaderText="State/Region"
                        SortExpression="StateRegion" DataType="System.String" Display="false">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="Country" DataField="Country" HeaderText="Country"
                        SortExpression="Country" DataType="System.String" Display="false">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="PostalCode" DataField="PostalCode" HeaderText="Postal Code"
                        SortExpression="PostalCode" DataType="System.String" Display="false">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="Comments" DataField="Comments" HeaderText="Comments"
                        SortExpression="Comments" DataType="System.String" Display="false">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="CreatedBy" DataField="CreatedBy" HeaderText="Created By"
                        SortExpression="CreatedBy" DataType="System.String" ReadOnly="true" Display="false">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="CreateDate" DataField="CreateDate" HeaderText="Create Date"
                        SortExpression="CreateDate" DataType="System.DateTime" ReadOnly="true" Display="false">
                    </rad:GridBoundColumn>
                </Columns>
                <SortExpressions>
                    <rad:GridSortExpression FieldName="ContactID" SortOrder="Descending" />
                </SortExpressions>
                <NestedViewTemplate>
                    <div style="padding: 10px 10px 10px 10px">
                        <rad:RadTabStrip ID="TabStripContact" runat="server" SelectedIndex="0" MultiPageID="MultiPageContact"
                            ShowBaseLine="true" Width="100%" AutoPostBack="true">
                            <Tabs>
                                <rad:RadTab Text="Contact Details" runat="server" Visible="true">
                                </rad:RadTab>
                                <rad:RadTab Text="Additional Info" runat="server">
                                </rad:RadTab>
                                <rad:RadTab Text="Transaction History" runat="server">
                                </rad:RadTab>
                            </Tabs>
                        </rad:RadTabStrip>
                        <div style="border: 0.1pt solid gray; border-width: 0.5pt; border-top-style: none;
                            padding: 10px 10px 10px 10px">
                            <rad:RadMultiPage ID="MultiPageContact" runat="server" SelectedIndex="0" RenderSelectedPageOnly="True"
                                Width="100%">
                                <rad:RadPageView ID="PageViewAttribute" runat="server">
                                    <fieldset style="padding: 10px;">
                                        <legend style="padding: 5px"><b>Details info for Contact:&nbsp; &nbsp;
                                            <%#Eval("FirstName") %>
                                            &nbsp;<%#Eval("LastName") %></b></legend>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        First Name:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label1" runat="server" Text='<%#Bind("FirstName") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Last Name:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label2" runat="server" Text='<%#Bind("LastName") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Tags:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label3" runat="server" Text='<%#Bind("Tags") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Company:
                                                    </td>
                                                    <td>
                                                        <rad:RadComboBox ID="cboCompany" runat="server" DataSourceID="dsCompany" DataTextField="CompanyName"
                                                            AppendDataBoundItems="true" DataValueField="CompanyID" SelectedValue='<%#Bind("CompanyID") %>'>
                                                            <Items>
                                                                <rad:RadComboBoxItem Text="" Value="" />
                                                            </Items>
                                                        </rad:RadComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Title:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label5" runat="server" Text='<%#Bind("Title") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Email:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label6" runat="server" Text='<%#Bind("Email") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Phone:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label7" runat="server" Text='<%#Bind("Phone") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Mobile Phone:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label8" runat="server" Text='<%#Bind("MobilePhone") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Fax:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label9" runat="server" Text='<%#Bind("Fax") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Address 1:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label10" runat="server" Text='<%#Bind("Address1") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Address 2:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label11" runat="server" Text='<%#Bind("Address2") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        City:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label12" runat="server" Text='<%#Bind("City") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        State/Region:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label13" runat="server" Text='<%#Bind("StateRegion") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Country:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label14" runat="server" Text='<%#Bind("Country") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Postal Code:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label15" runat="server" Text='<%#Bind("PostalCode") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Comments:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label16" runat="server" Text='<%#Bind("Comments") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </fieldset>
                                </rad:RadPageView>
                                <rad:RadPageView ID="PageViewAdditionalInfo" runat="server">
                                    <rad:RadGrid ID="grdContactExtended" runat="server" AutoGenerateColumns="false" AllowSorting="true"
                                        AllowPaging="true" Width="100%" OnInsertCommand="grdContactExtendedInfo_InsertCommand"
                                        OnDeleteCommand="grdContactExtendedInfo_DeleteCommand" OnNeedDataSource="grdContactExtendedInfo_NeedDataSource"
                                        OnUpdateCommand="grdContactExtendedInfo_UpdateCommand" OnItemCommand="grdContactExtendedInfo_ItemCommand">
                                        <MasterTableView CommandItemDisplay="Top" DataKeyNames="ContactExtendedInfoID">
                                            <Columns>
                                                <rad:GridEditCommandColumn ButtonType="ImageButton">
                                                    <HeaderStyle Width="25px" />
                                                </rad:GridEditCommandColumn>
                                                <rad:GridButtonColumn ButtonType="ImageButton" ImageUrl="~/Images/Delete.gif" CommandName="Delete"
                                                    ConfirmText="Are you sure you want to delete this record?">
                                                    <HeaderStyle Width="25px" />
                                                </rad:GridButtonColumn>
                                                <rad:GridBoundColumn UniqueName="KeyInfo" DataField="KeyInfo" HeaderText="Key" SortExpression="KeyInfo"
                                                    DataType="System.String">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn UniqueName="Value" DataField="Value" HeaderText="Value" SortExpression="Value"
                                                    DataType="System.String">
                                                </rad:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </rad:RadGrid>
                                </rad:RadPageView>
                                <rad:RadPageView ID="PageViewTransactionHistory" runat="server">
                                    <rad:RadGrid ID="grdTransactionHistory" runat="server" AutoGenerateColumns="true"
                                        AllowSorting="true" AllowPaging="true" Width="100%" OnNeedDataSource="grdTransactionHistory_NeedDataSource"
                                        OnItemDataBound="grdTransactionHistory_ItemDataBound">
                                        <MasterTableView CommandItemDisplay="Top" DataKeyNames="TransactionID">
                                            <CommandItemSettings ShowAddNewRecordButton="false" />
                                            <Columns>
                                                <rad:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="EditLink" runat="server" Text="Edit" ImageUrl="~/images/Edit.gif"
                                                            CommandName="FormEdit" ToolTip="Edit Record"></asp:ImageButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="30px" />
                                                </rad:GridTemplateColumn>
                                            </Columns>
                                            <SortExpressions>
                                                <rad:GridSortExpression FieldName="TransactionID" SortOrder="Descending" />
                                            </SortExpressions>
                                        </MasterTableView>
                                    </rad:RadGrid>
                                </rad:RadPageView>
                            </rad:RadMultiPage>
                        </div>
                    </div>
                </NestedViewTemplate>
            </MasterTableView>
        </rad:RadGrid>
    </asp:Panel>
    <asp:EntityDataSource ID="dsCompany" runat="server" ConnectionString="name=CallDispositionEntities"
        DefaultContainerName="CallDispositionEntities" EntitySetName="Companies">
    </asp:EntityDataSource>
    <asp:EntityDataSource ID="dsContact" runat="server" ConnectionString="name=CallDispositionEntities"
        DefaultContainerName="CallDispositionEntities" EntitySetName="Contacts">
    </asp:EntityDataSource>
</asp:Content>

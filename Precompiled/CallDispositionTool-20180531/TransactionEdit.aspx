﻿<%@ Page Language="C#" AutoEventWireup="true" CodePage = 65001 CodeBehind="TransactionEdit.aspx.cs"
    Inherits="CallDispositionTool.TransactionEdit" %>
    <%  
Session.CodePage = 65001;
Response.Charset = "utf-8";
Session.LCID = 1033; %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Transaction Edit</title>
    <!-- Google Analytics -->
    <script src="Scripts/GAnalytics.js" type="text/javascript"></script>
    <link href="CSS/FormStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <rad:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow)
                    oWindow = window.RadWindow; //Will work in Moz in all cases, including clasic dialog     
                else if (window.frameElement.radWindow)
                    oWindow = window.frameElement.radWindow; //IE (and Moz as well)     
                return oWindow;
            }

            function CloseWindow(args) {
                GetRadWindow().BrowserWindow.refreshGrid();
                GetRadWindow().close();
                return false;
            }

            function CloseWithoutRefresh() {
                GetRadWindow().close();
                return false;
            }

            function showNotification() {
                var notification = $find("<%=RadNotification1.ClientID %>");
                setTimeout(function () {
                    notification.show();
                }, 0);
            }

            function CheckIfShow(sender, args) {
                var summaryElem = document.getElementById("<%= ValidationSummary1.ClientID%>");
                //check if summary is visible - if not, there are not errors, do not notify and continue with postback
                var noErrors = summaryElem.style.display == "none";
                args.set_cancel(noErrors);
            }
        </script>
    </rad:RadScriptBlock>
    <rad:RadSkinManager ID="skinManager1" runat="server" PersistenceMode="Cookie">
    </rad:RadSkinManager>
    <rad:RadFormDecorator ID="formDecorator1" runat="server" EnableRoundedCorners="true"
        RenderMode="Lightweight" ControlsToSkip="H4H5H6" />
    <rad:RadScriptManager ID="ScriptManager1" runat="server">
    </rad:RadScriptManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        ShowContentDuringLoad="false" Width="680px" Height="600px" VisibleOnPageLoad="false"
        VisibleStatusbar="false" DestroyOnClose="true" Modal="true" ReloadOnShow="true">
    </telerik:RadWindowManager>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <telerik:RadNotification ID="RadNotification1" runat="server" Width="380" Animation="Slide"
        OnClientShowing="CheckIfShow" EnableRoundedCorners="true" EnableShadow="true"
        LoadContentOn="PageLoad" Title="Validation errors" OffsetX="-20" OffsetY="-20"
        TitleIcon="warning" EnableAriaSupport="true" ShowSound="warning" AutoCloseDelay="7000">
        <ContentTemplate>
            <div style="background-color: White; padding: 5px 5px 5px 5px">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
            </div>
        </ContentTemplate>
    </telerik:RadNotification>
    <rad:RadAjaxManager ID="ajaxManager1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="btnSubmit">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="btnNew" />
                    <rad:AjaxUpdatedControl ControlID="lblStatus" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <div style="padding: 10px 10px 10px 10px; width: 100%">
        <asp:Panel ID="pnlMain" runat="server" DefaultButton="btnSubmit" Width="100%">
            <h2>
                <asp:Label ID="lblFormName" runat="server"></asp:Label>
            </h2>
            <br />
            <br />
            <br />
            <asp:PlaceHolder ID="formPlaceHolder" runat="server"></asp:PlaceHolder>
            <br />
            <hr />
            <div style="float: left">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Clicked" OnClientClick="showNotification();" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="return CloseWithoutRefresh();" />
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>

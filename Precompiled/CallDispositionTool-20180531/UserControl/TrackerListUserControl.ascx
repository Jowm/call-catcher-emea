﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrackerListUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.TrackerListUserControl" %>
<rad:RadAjaxManager ID="ajaxManager1" runat="server">
</rad:RadAjaxManager>
<script>
    function ShowForm(formID, formName) {
        if (formID) {
            var windowURL = '<%= ResolveClientUrl("~/TransactionForm.aspx") %>' + '?FormID=' + formID + '&FormName=' + formName;
            window.open(windowURL, formID, 'width=680,height=420,resizable=yes,scrollbars=yes').focus();
        }
    }
</script>
<div class="box_content">
    <div class="box_title">
        <div class="title_icon">
            <img src="images/mini_icon2.gif" alt="" title="" /></div>
        <h2>
            My <span class="dark_blue">Tracker</span></h2>
    </div>
    <asp:Repeater ID="repTracker" runat="server" OnItemDataBound="repTracker_ItemDataBound">
        <ItemTemplate>
            <div class="box_text_content">
                <img src="images/checked.gif" alt="" title="" class="box_icon" />
                <a class="details" style="cursor: pointer" onclick='ShowForm("<%# Eval("FormID") %>", "<%# Eval("FormName") %>")'>
                    Tracker ID:
                    <%# Eval("FormID") %></a>
                <div class="box_text">
                    <%# Eval("FormName") %>
                </div>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            <asp:Label ID="lblErrorMsg" CssClass="details" runat="server" Text="No items to show"
                Visible="false"></asp:Label>
        </FooterTemplate>
    </asp:Repeater>
</div>
<div class="clear">
</div>
<input type="hidden" id="hdClient" runat="server" />
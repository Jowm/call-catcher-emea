﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserFormAdminUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.UserFormAdminUserControl" %>
<rad:RadAjaxLoadingPanel ID="ajaxloadingPanel1" runat="server">
</rad:RadAjaxLoadingPanel>
<rad:RadAjaxPanel ID="ajaxPanel1" runat="server" LoadingPanelID="ajaxloadingPanel1">
    <table>
        <tr>
            <td>
                <strong>
                    <label for="lstSource">
                        Source</label></strong>
                <br />
                <rad:RadListBox ID="lstSource" runat="server" Width="300px" Height="200px" SelectionMode="Multiple"
                    AllowTransfer="true" TransferToID="lstDestination" AllowReorder="false" EnableDragAndDrop="true"
                    AllowTransferDuplicates="false" TransferMode="Move" OnInserted="lstSource_OnInserted"
                    AutoPostBackOnTransfer="true" RenderMode="Lightweight">
                    <ButtonSettings Position="Right" />
                    <EmptyMessageTemplate>
                        <a>No records found</a>
                    </EmptyMessageTemplate>
                </rad:RadListBox>
            </td>
            <td>
                <strong>
                    <label for="lstDestination">
                        Destination</label></strong>
                <br />
                <rad:RadListBox ID="lstDestination" runat="server" Width="300px" Height="200px" SelectionMode="Multiple"
                    AllowTransfer="true" TransferToID="lstSource" AllowReorder="false" EnableDragAndDrop="true"
                    OnInserted="lstDestination_OnInserted" AllowTransferDuplicates="false" TransferMode="Move"
                    AutoPostBackOnTransfer="true" RenderMode="Lightweight">
                    <ButtonSettings Position="Left" />
                    <EmptyMessageTemplate>
                        <a>No records found</a>
                    </EmptyMessageTemplate>
                </rad:RadListBox>
            </td>
        </tr>
    </table>
</rad:RadAjaxPanel>

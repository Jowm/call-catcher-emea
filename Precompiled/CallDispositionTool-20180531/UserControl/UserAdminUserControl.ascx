﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserAdminUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.UserAdminUserControl" %>
<%@ Register Src="UserFormAdminUserControl.ascx" TagName="UserFormAdminUserControl"
    TagPrefix="uc1" %>
<div style="padding: 10px 10px 10px 10px">
    <rad:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function ShowUserAccessPage(userID) {
                var windowURL = '<%= ResolveClientUrl("~/UserAccess.aspx") %>' + '?UserID=' + userID;
                var grid = $find("<%= grdUsers.ClientID %>");
                var oWnd = radopen(windowURL, null);
                oWnd.set_title("User Access - UserID: " + userID);
                oWnd.set_modal(true);
                oWnd.center();

                return false;
            }
        </script>
    </rad:RadScriptBlock>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManagerProxy ID="ajaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="grdUsers">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdUsers" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="RadFilter1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadFilter1" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <rad:RadFilter runat="server" ID="RadFilter1" FilterContainerID="grdUsers" ShowApplyButton="false">
    </rad:RadFilter>
    <br />
    <rad:RadGrid ID="grdUsers" runat="server" AutoGenerateColumns="false" AllowSorting="true"
        AllowFilteringByColumn="true" Width="100%" OnItemCommand="grdUsers_ItemCommand"
        AllowPaging="True" OnNeedDataSource="grdUsers_NeedDataSource" OnDeleteCommand="grdUsers_DeleteCommand"
        OnInsertCommand="grdUsers_InsertCommand" OnUpdateCommand="grdUsers_UpdateCommand"
        OnItemCreated="grdUsers_ItemCreated">
        <PagerStyle Mode="NextPrevNumericAndAdvanced" AlwaysVisible="true" />
        <ClientSettings AllowColumnsReorder="true" EnableAlternatingItems="true">
            <Scrolling UseStaticHeaders="true" />
            <Resizing AllowColumnResize="true" AllowResizeToFit="true" ResizeGridOnColumnResize="true"
                ClipCellContentOnResize="true" />
        </ClientSettings>
        <ExportSettings ExportOnlyData="true" IgnorePaging="true" FileName="Report">
        </ExportSettings>
        <ItemStyle Wrap="true"></ItemStyle>
        <MasterTableView CommandItemDisplay="Top" PageSize="15" TableLayout="Auto" IsFilterItemExpanded="false"
            DataKeyNames="UserID" EditMode="PopUp" EnableHeaderContextMenu="true">
            <CommandItemTemplate>
                <telerik:RadToolBar runat="server" ID="RadToolBar1" AutoPostBack="true">
                    <Items>
                        <telerik:RadToolBarButton Text="Add User" CommandName="InitInsert" Visible='<%# !grdUsers.MasterTableView.IsItemInserted %>'
                            ImageUrl="<%# GetAddRecordIcon() %>" ImagePosition="Right">
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton Text="Apply filter" CommandName="FilterRadGrid" ImageUrl="<%# GetFilterIcon() %>"
                            ImagePosition="Right">
                        </telerik:RadToolBarButton>
                    </Items>
                </telerik:RadToolBar>
            </CommandItemTemplate>
            <CommandItemSettings ShowExportToExcelButton="true" AddNewRecordText="Add User" ShowExportToCsvButton="true" />
            <SortExpressions>
                <rad:GridSortExpression FieldName="UserID" SortOrder="Descending" />
            </SortExpressions>
            <EditFormSettings EditColumn-ButtonType="PushButton">
                <EditColumn ButtonType="PushButton" FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
                <PopUpSettings Modal="true" ScrollBars="Auto" ShowCaptionInEditForm="true" />
            </EditFormSettings>
            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
            </RowIndicatorColumn>
            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
            </ExpandCollapseColumn>
            <Columns>
                <rad:GridEditCommandColumn UniqueName="EditColumn1" ButtonType="ImageButton">
                    <HeaderStyle Width="30px" />
                </rad:GridEditCommandColumn>
                <rad:GridTemplateColumn UniqueName="EditUserAccessColumn" AllowFiltering="false" ReadOnly="true" >
                    <ItemTemplate>
                        <asp:ImageButton ID="EditUserAccess" runat="server" ImageUrl="../Images/Access16.png"
                            AlternateText="View/Edit User Access" ToolTip="View/Edit User Access"  />
                    </ItemTemplate>
                    <HeaderStyle Width="30px" />
                </rad:GridTemplateColumn>
                <rad:GridButtonColumn UniqueName="ResetPassword" ButtonType="ImageButton" ImageUrl="../Images/Unlock16.png"
                    CommandName="ResetPassword" ConfirmDialogType="RadWindow" ConfirmText="Are you sure you want to reset password?"
                    Text="Reset Password">
                    <HeaderStyle Width="30px" />
                </rad:GridButtonColumn>
                <rad:GridBoundColumn UniqueName="EmployeeID" HeaderText="Employee ID" DataField="EmployeeID"
                    SortExpression="EmployeeID" DataType="System.String">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn UniqueName="FirstName" HeaderText="First Name" DataField="FirstName"
                    SortExpression="FirstName" DataType="System.String">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn UniqueName="LastName" HeaderText="Last Name" DataField="LastName"
                    SortExpression="LastName" DataType="System.String">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn UniqueName="EmailAddress" HeaderText="Email Address" DataField="EmailAddress"
                    SortExpression="EmailAddress" DataType="System.String">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn UniqueName="AvayaLoginID" HeaderText="Avaya Login ID" DataField="AvayaLogin"
                    SortExpression="AvayaLogin" DataType="System.String">
                </rad:GridBoundColumn>
                <rad:GridCheckBoxColumn UniqueName="Inactive" HeaderText="Inactive" DataField="Inactive"
                    SortExpression="Inactive" DataType="System.Boolean">
                </rad:GridCheckBoxColumn>
                <rad:GridDateTimeColumn UniqueName="LastLoginDate" HeaderText="Last Login Date" DataField="LastLoginDate"
                    SortExpression="LastLoginDate" ReadOnly="true" DataType="System.DateTime">
                </rad:GridDateTimeColumn>
                <rad:GridDateTimeColumn UniqueName="LastActivityDate" HeaderText="Last Activity Date"
                    DataField="LastActivityDate" SortExpression="LastLoginDate" ReadOnly="true" DataType="System.DateTime">
                </rad:GridDateTimeColumn>
                <rad:GridDateTimeColumn UniqueName="CreatedOn" HeaderText="Created On" DataField="CreatedOn"
                    SortExpression="CreatedOn" ReadOnly="true" DataType="System.DateTime">
                </rad:GridDateTimeColumn>
            </Columns>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
    </rad:RadGrid>
</div>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="ClearCache.aspx.cs" Inherits="CallDispositionTool.Admin.ClearCache" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <asp:Button ID="btnClearCache" runat="server" Text="Clear Cache" 
        onclick="btnClearCache_Click" />
    <br />
    <asp:Label ID="lblStatus" runat="server"></asp:Label>
</asp:Content>

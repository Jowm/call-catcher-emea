﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LaunchPad.aspx.cs" Inherits="CallDispositionTool.CTI.LaunchPad" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!--Reference the SignalR library. -->
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.signalR-1.0.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src='/signalr/hubs'></script>
    <title>Launch Pad</title>
</head>
<body>
    <form id="form1" runat="server">
    <rad:RadFormDecorator ID="formDecorator1" runat="server" EnableRoundedCorners="true" />
    <rad:RadScriptManager ID="ScriptManager1" runat="server">
    </rad:RadScriptManager>
    <div>
        <table>
            <tr>
                <td>
                    Server Status:
                </td>
                <td>
                    <asp:Label ID="lblServerStatus" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Agent Status:
                </td>
                <td>
                    <asp:Label ID="lblAgentStatus" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Extension Status:
                </td>
                <td>
                    <asp:Label ID="lblExtensionStatus" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Call Status:
                </td>
                <td>
                    <asp:Label ID="lblCallStatus" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <rad:RadCodeBlock ID="CodeBlock1" runat="server">
        <!--Add script to update the page and send messages.-->
        <script type="text/javascript">
            $(function () {
                //Declare a proxy to reference the hub. 
                var chat = $.connection.notificationHub;

                chat.client.ctiServerStatusInfoMessage = function (agentID, event) {
                    if (agentID == '<%= HttpContext.Current.User.Identity.Name %>') {
                        $('#<%= lblServerStatus.ClientID %>').html(event.Event)
                    }
                };

                chat.client.agentEventInfoMessage = function (agentID, event) {
                    if (agentID == '<%= HttpContext.Current.User.Identity.Name %>') {
                        $('#<%= lblAgentStatus.ClientID %>').html(event.Event)
                    }
                };

                chat.client.extensionStatusInfoMessage = function (agentID, event) {
                    if (agentID == '<%= HttpContext.Current.User.Identity.Name %>') {
                        $('#<%= lblExtensionStatus.ClientID %>').html(event.Event)
                    }
                };

                chat.client.callEventInfoMessage = function (agentID, event) {
                    if (agentID == '<%= HttpContext.Current.User.Identity.Name %>') {
                        $('#<%= lblCallStatus.ClientID %>').html(event.Event)
                    }
                };

                $.connection.hub.start();
            });
        </script>
    </rad:RadCodeBlock>
    </form>
</body>
</html>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerSatisfactionUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.CustomerSatisfactionUserControl" %>
<table>
    <tr>
        <td>
            <asp:RadioButtonList ID="radCustomerSatisfaction" runat="server" RepeatLayout="Table"
                RepeatDirection="Horizontal">
                <asp:ListItem Text='<img src="images/Emoticons/Icon%201.png" alt="Very Good" height="30" width="30" />'
                    Value="Very Good" />
                <asp:ListItem Text='<img src="images/Emoticons/Icon%202.png" alt="Good" height="30" width="30" />'
                    Value="Good" />
                <asp:ListItem Text='<img src="images/Emoticons/Icon%203.png" alt="Average" height="30" width="30" />'
                    Value="Average" />
                <asp:ListItem Text='<img src="images/Emoticons/Icon%204.png" alt="Poor" height="30" width="30" />'
                    Value="Poor" />
                <asp:ListItem Text='<img src="images/Emoticons/Icon%205.png" alt="Very Poor" height="30" width="30" />'
                    Value="Very Poor" />
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="req1" runat="server" ControlToValidate="radCustomerSatisfaction" ErrorMessage="Please select a choice" Text="*"></asp:RequiredFieldValidator>
        </td>
    </tr>
</table>

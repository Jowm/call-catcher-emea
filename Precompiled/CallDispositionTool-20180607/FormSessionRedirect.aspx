﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormSessionRedirect.aspx.cs"
    Inherits="CallDispositionTool.FormSessionRedirect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function Close() {
            GetRadWindow().close();
        }
    </script>
    <rad:RadSkinManager ID="skinManager1" runat="server" PersistenceMode="Cookie">
    </rad:RadSkinManager>
    <rad:RadFormDecorator ID="formDecorator1" runat="server" EnableRoundedCorners="true"
        RenderMode="Lightweight" ControlsToSkip="H4H5H6" />
    <rad:RadScriptManager ID="ScriptManager1" runat="server">
    </rad:RadScriptManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    <div style="padding: 10px 10px 10px 10px; margin: 0 auto 0 auto;">
        <asp:Panel ID="panel1" runat="server" DefaultButton="btnSubmit">
            <table>
                <tr>
                    <td>
                        <strong>
                            <label for="btnLaunchCTIPad">
                                Select Form:</label></strong>
                    </td>
                    <td>
                        <rad:RadComboBox ID="cboForms" runat="server" DataTextField="FormName" DataValueField="FormID"
                            Width="200px" AppendDataBoundItems="true" DropDownAutoWidth="Enabled">
                            <Items>
                            </Items>
                        </rad:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <div style="float: left">
                            <asp:Button ID="btnSubmit" runat="server" Text="Launch Form" OnClick="btnLaunchForm_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>

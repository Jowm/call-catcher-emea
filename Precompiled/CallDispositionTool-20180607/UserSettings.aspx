﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="UserSettings.aspx.cs" Inherits="CallDispositionTool.UserSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <rad:RadTabStrip ID="tabStrip1" runat="server" SelectedIndex="0" MultiPageID="MultiPage1" CausesValidation="false"
        ShowBaseLine="true" Width="867px">
        <Tabs>
            <rad:RadTab Text="Theme" Selected="true">
            </rad:RadTab>
            <rad:RadTab Text="Change Password">
            </rad:RadTab>
        </Tabs>
    </rad:RadTabStrip>
    <div style="border: 1pt solid gray; border-top-style: none; width: 867px">
        <rad:RadMultiPage ID="MultiPage1" runat="server" SelectedIndex="0">
            <rad:RadPageView ID="PageView1" runat="server" Selected="true">
                <div style="width: 100%; height: 300px; padding: 40px 10px 10px 10px">
                    <table>
                        <tr>
                            <td>
                                <label>Select Theme</label>
                            </td>
                            <td>
                                <rad:RadComboBox ID="cboSkin" runat="server" OnSelectedIndexChanged="cboSkin_SelectedIndexChanged"
                                    AutoPostBack="true" EmptyMessage="Please Specify">
                                    <Items>
                                        <rad:RadComboBoxItem Text="" Value="" />
                                        <rad:RadComboBoxItem Text="Default" runat="server" />
                                        <rad:RadComboBoxItem Text="Web20" runat="server" />
                                        <rad:RadComboBoxItem Text="WebBlue" runat="server" />
                                        <rad:RadComboBoxItem Text="Metro" runat="server" />
                                        <rad:RadComboBoxItem Text="Silk" runat="server" />
                                        <rad:RadComboBoxItem Text="Hay" runat="server" />
                                        <rad:RadComboBoxItem Text="Glow" runat="server" />
                                        <rad:RadComboBoxItem Text="Outlook" runat="server" />
                                        <rad:RadComboBoxItem Text="Office2007" runat="server" />
                                        <rad:RadComboBoxItem Text="Vista" runat="server" />
                                        <rad:RadComboBoxItem Text="Windows7" runat="server" />
                                        <rad:RadComboBoxItem Text="Office2010Blue" runat="server" />
                                        <rad:RadComboBoxItem Text="Sunset" runat="server" />
                                    </Items>
                                </rad:RadComboBox>
                                <asp:RequiredFieldValidator ID="reqSkin" runat="server" ErrorMessage="Please select skin"
                                    Text="*" ControlToValidate="cboSkin"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                    <span>Note: To take effect on other pages please refresh by pressing F5.</span >
                </div>
            </rad:RadPageView>
            <rad:RadPageView ID="PageView2" runat="server">
                <div style="width: 100%; height: 300px; padding: 10px 10px 10px 10px">
                    <asp:ChangePassword ID="ChangePassword1" runat="server" DisplayUserName="true" 
                        OnChangingPassword="ChangePassword1_ChangingPassword"
                        OnContinueButtonClick="ChangePassword1_ContinueButtonClick" OnChangedPassword="ChangePassword1_ChangedPassword" >
                        <LabelStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="11pt" />
                        <MailDefinition BodyFileName="~/MailFiles/ChangePasswordMail.txt" From="noreply@transcom.com"
                            IsBodyHtml="True" Subject="Transcom Call Dispostion Tool Password Change Notification">
                        </MailDefinition>
                        <TitleTextStyle Width="13pt" />
                    </asp:ChangePassword>
                    <br />
                    <h4>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="Red"></asp:Label></h4>
                </div>
            </rad:RadPageView>
        </rad:RadMultiPage>
    </div>
</asp:Content>

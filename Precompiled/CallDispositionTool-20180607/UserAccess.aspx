﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserAccess.aspx.cs" Inherits="CallDispositionTool.UserAccess" %>

<%@ Register Src="~/UserControl/UserFormAdminUserControl.ascx" TagName="UserFormAdminUserControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/UserRoleAdminUserControl.ascx" TagName="UserRoleAdminUserControl"  TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- Google Analytics -->
    <script src="Scripts/GAnalytics.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.ba-postmessage.js" type="text/javascript"></script>
    <script src="Scripts/CTIPadSDK.js" type="text/javascript"></script>
    <link href="CSS/FormStyle.css" rel="stylesheet" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <rad:RadAjaxLoadingPanel ID="ajaxloadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManager ID="ajaxManager1" runat="server" DefaultLoadingPanelID="loadingPanel1">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="userFormAdminUserControl1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="userFormAdminUserControl1" LoadingPanelID="ajaxloadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="userRoleAdminUserControl1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="userRoleAdminUserControl1" LoadingPanelID="ajaxloadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <rad:RadFormDecorator ID="formDecorator1" runat="server" EnableRoundedCorners="true" />
    <rad:RadScriptManager ID="ScriptManager1" runat="server">
    </rad:RadScriptManager>
    <rad:RadSkinManager ID="skinManager1" runat="server" PersistenceMode="Cookie">
    </rad:RadSkinManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        ShowContentDuringLoad="false" Width="680px" Height="500px" VisibleOnPageLoad="false"
        VisibleStatusbar="false" DestroyOnClose="true" ReloadOnShow="true" Modal="true"
        AutoSize="true" InitialBehaviors="Maximize" RenderMode="Lightweight">
    </telerik:RadWindowManager>
    <rad:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow)
                    oWindow = window.RadWindow; //Will work in Moz in all cases, including clasic dialog     
                else if (window.frameElement.radWindow)
                    oWindow = window.frameElement.radWindow; //IE (and Moz as well)     
                return oWindow;
            }

            function OnClientClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();

                if (arg) {
                }
            }        
        </script>
    </rad:RadScriptBlock>
    <telerik:RadWindowManager ID="RadWindowManager2" runat="server" EnableShadow="true"
        Width="680px" Height="500px" VisibleOnPageLoad="false" VisibleStatusbar="false"
        Modal="true" RenderMode="Lightweight" OnClientClose="OnClientClose" DestroyOnClose="true"
        Animation="Fade">
    </telerik:RadWindowManager>
    <div style="width: 100%; height: 400px;">
        <rad:RadTabStrip ID="TabStripUser" runat="server" SelectedIndex="0" MultiPageID="MultiPageUser"
            ShowBaseLine="true" Width="100%">
            <Tabs>
                <rad:RadTab Text="User Forms" runat="server" Visible="false">
                </rad:RadTab>
                <rad:RadTab Text="User Roles" runat="server" Visible="false">
                </rad:RadTab>
            </Tabs>
        </rad:RadTabStrip>
        <div style="border: 0.1pt solid gray; border-width: 0.5pt; border-top-style: none;
            padding: 10px 10px 10px 10px">
            <rad:RadMultiPage ID="MultiPageUser" runat="server" SelectedIndex="0" RenderSelectedPageOnly="False"
                Width="100%" Visible="true">
                <rad:RadPageView ID="PageViewFormAccess" runat="server" Visible="false">
                    <uc1:userformadminusercontrol id="userFormAdminUserControl1" runat="server" />
                </rad:RadPageView>
                <rad:RadPageView ID="PageViewUserRole" runat="server" Visible="false">
                    <uc2:userroleadminusercontrol id="userRoleAdminUserControl1" runat="server" />
                </rad:RadPageView>
            </rad:RadMultiPage>
        </div>
    </div>
    </form>
</body>
</html>

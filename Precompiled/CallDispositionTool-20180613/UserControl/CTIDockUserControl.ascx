﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CTIDockUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.CTIDockUserControl" %>
<%@ OutputCache Duration="1440" VaryByParam="none" %>

<div class="box_content">
    <rad:RadWindowManager ID="radWindowManager1" runat="server">
        <Windows>
            <rad:RadWindow ID="radWindow1" runat="server" Title="Form Chooser" EnableShadow="true"
                OpenerElementID="btnBegin" ShowContentDuringLoad="false" ReloadOnShow="false"
                Height="300px" Width="400px" NavigateUrl="~/FormChooser.aspx" Modal="true" RenderMode="Lightweight"
                Behaviors="Close" VisibleStatusbar="false">
            </rad:RadWindow>
        </Windows>
    </rad:RadWindowManager>
    <div class="box_title">
        <div class="title_icon">
            <img runat="server" src="~/images/mini_icon2.gif" alt="" title="" /></div>
        <h2>
            CTI <span class="dark_blue">Pad</span></h2>
    </div>
    <div class="box_text_content">
        <img runat="server" src="~/images/checked.gif" alt="" title="" class="box_icon" />
        <a id="btnBegin" class="details" style="cursor: pointer">To Begin, click here</a>
    </div>
</div>
<div class="clear">
</div>
<input type="hidden" id="hdClient" runat="server" />
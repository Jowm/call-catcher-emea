﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DispositionAdminUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.DispositionAdminUserControl" %>
<div style="padding: 10px 10px 10px 10px">
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManagerProxy ID="ajaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="cboAttribute">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="trvDisposition" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="trvDisposition">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="trvDisposition" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnExport">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="trvDisposition" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <a>Select Disposition: </a>
    <rad:RadComboBox ID="cboAttribute" runat="server" DataTextField="AttributeName" DataValueField="AttributeID"
        AutoPostBack="true" DropDownAutoWidth="Enabled" OnSelectedIndexChanged="cboAttribute_SelectedIndexChanged">
    </rad:RadComboBox>
    <p>
    </p>
    <p>
    </p>
    <asp:Button ID="btnExport" runat="server" Text="Export to Excel" OnClick="btnExport_Click" />
    <br />
    <rad:RadTreeList ID="trvDisposition" runat="server" AllowPaging="true" DataKeyNames="DispositionID"
        ParentDataKeyNames="ParentDispositionID" AutoGenerateColumns="false" AllowMultiItemSelection="false"
        EditMode="PopUp" OnChildItemsDataBind="trvDisposition_ChildItemsDataBind" OnNeedDataSource="trvDisposition_NeedDataSource"
        AllowLoadOnDemand="true" OnDeleteCommand="trvDisposition_DeleteCommand" OnInsertCommand="trvDisposition_InsertCommand"
        OnUpdateCommand="trvDisposition_UpdateCommand" ShowTreeLines="true">
        <ExportSettings
            ExportMode="RemoveAll"
            IgnorePaging="true" 
            OpenInNewWindow="false"
            FileName="Disposition">
        </ExportSettings>  
        <EditFormSettings>
            <PopUpSettings Modal="true" ShowCaptionInEditForm="true" ScrollBars="Auto" />
        </EditFormSettings>
        <Columns>
            <rad:TreeListEditCommandColumn UniqueName="EditCommandColumn" ButtonType="ImageButton"
                HeaderStyle-Width="80px">
                <ItemStyle CssClass="MyImageButton"></ItemStyle>
            </rad:TreeListEditCommandColumn>
            <rad:TreeListBoundColumn UniqueName="DispositionID" HeaderText="ID" DataField="DispositionID"
                SortExpression="DispositionID" ReadOnly="true" DataType="System.Int32">
            </rad:TreeListBoundColumn>
            <rad:TreeListBoundColumn DataField="Description" UniqueName="Description" HeaderText="Description"
                DataType="System.String">
            </rad:TreeListBoundColumn>
            <rad:TreeListBoundColumn UniqueName="ParentDispositionID" HeaderText="Parent ID"
                DataField="ParentDispositionID" SortExpression="ParentDispositionID" ReadOnly="true"
                DataType="System.Int32">
            </rad:TreeListBoundColumn>
            <rad:TreeListNumericColumn UniqueName="FilterDispositionID" HeaderText="Filter Disposition ID"
                DataField="FilterDispositionID" SortExpression="FilterDispositionID" NumericType="Number"
                DataType="System.Int32">
            </rad:TreeListNumericColumn>
            <rad:TreeListCheckBoxColumn DataField="HideFromList" UniqueName="HideFromList" HeaderText="HideFromList"
                DataType="System.Boolean">
            </rad:TreeListCheckBoxColumn>
            <rad:TreeListDateTimeColumn DataField="CreateDate" UniqueName="CreatedOn" HeaderText="CreatedOn"
                ReadOnly="true" DataType="System.DateTime">
            </rad:TreeListDateTimeColumn>
        </Columns>
    </rad:RadTreeList>
</div>

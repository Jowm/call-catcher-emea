﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PickListOptionAdminUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.PickListOptionAdminUserControl" %>
<div style="padding: 10px 10px 10px 10px">
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManagerProxy ID="ajaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="cboAttribute">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdPickListOption" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="grdPickListOption">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdPickListOption" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <strong>
        <label for="cboAttribute">
            Select Pick List Option:</label>
    </strong>
    <rad:RadComboBox ID="cboAttribute" runat="server" AutoPostBack="true" DropDownWidth="400px"
        AppendDataBoundItems="true" DropDownAutoWidth="Enabled" Height="300px">
    </rad:RadComboBox>
    <p>
    </p>
    <rad:RadGrid ID="grdPickListOption" runat="server" Width="100%" AllowAutomaticDeletes="True"
        AllowAutomaticInserts="True" AllowAutomaticUpdates="True" DataSourceID="dsPickListOption"
        CellSpacing="0" AutoGenerateEditColumn="true" AllowPaging="true">
        <ClientSettings AllowColumnsReorder="true" EnableAlternatingItems="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Resizing AllowColumnResize="true" AllowResizeToFit="true" ResizeGridOnColumnResize="true"
                ClipCellContentOnResize="true" />
        </ClientSettings>
        <MasterTableView Name="PickListOptionID" DataKeyNames="PickListOptionID" AutoGenerateColumns="False"
            CommandItemDisplay="Top" AllowSorting="true" DataSourceID="dsPickListOption"
            EditMode="PopUp">
            <CommandItemSettings ShowExportToExcelButton="true" AddNewRecordText="Add Record"
                ShowExportToCsvButton="true" />
            <EditFormSettings EditColumn-ButtonType="PushButton">
                <PopUpSettings Modal="true" />
            </EditFormSettings>
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <Columns>
                <rad:GridButtonColumn ButtonType="LinkButton" ConfirmDialogType="RadWindow" Text="Delete"
                    CommandName="Delete" ConfirmTitle="Delete Record" ConfirmText="Are you sure you want to delete this item?">
                    <HeaderStyle Width="60px" />
                </rad:GridButtonColumn>
                <rad:GridBoundColumn HeaderText="Display Text" DataField="DisplayText" SortExpression="DisplayText">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn HeaderText="Option Value" DataField="OptionValue" SortExpression="OptionValue">
                </rad:GridBoundColumn>
                <rad:GridNumericColumn HeaderText="SortOrder" DataField="SortOrder" SortExpression="SortOrder"
                    NumericType="Number" DataType="System.Int32" DecimalDigits="0">
                </rad:GridNumericColumn>
                <rad:GridCheckBoxColumn HeaderText="HideFromList" DataField="HideFromList" SortExpression="HideFromList"
                    DataType="System.Boolean">
                </rad:GridCheckBoxColumn>
                <rad:GridBoundColumn HeaderText="CreatedOn" DataField="CreatedOn" SortExpression="CreatedOn"
                    ReadOnly="true">
                </rad:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </rad:RadGrid>
    <asp:EntityDataSource ID="dsPickListOption" runat="server" ConnectionString="name=CallDispositionEntities"
        DefaultContainerName="CallDispositionEntities" EnableDelete="True" EnableFlattening="False"
        EnableInsert="True" EnableUpdate="True" EntitySetName="PickListOptions" AutoGenerateOrderByClause="True"
        AutoGenerateWhereClause="True" EntityTypeFilter="" Select="" Where="" OnInserting="dsPickListOption_Inserting">
        <WhereParameters>
            <asp:ControlParameter ControlID="cboAttribute" Name="AttributeID" PropertyName="SelectedValue"
                Type="Int32" />
        </WhereParameters>
    </asp:EntityDataSource>
    <style>
        .rcbHeader ul, .rcbFooter ul, .rcbItem ul, .rcbHovered ul, .rcbDisabled ul
        {
            width: 100%;
            display: inline-block;
            margin: 0;
            padding: 0;
            list-style-type: none;
        }
        
        .col1, .col2
        {
            margin: 0;
            padding: 0 5px 0 0;
            width: 110px;
            line-height: 14px;
            float: left;
        }
    </style>
</div>

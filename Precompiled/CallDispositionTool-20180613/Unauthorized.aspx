﻿<%@ Page Title="Unauthorized Access" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Unauthorized.aspx.cs" Inherits="CallDispositionTool.Unauthorized" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <div style="height: 300px">
        <h2>
            Unauthorized Access</h2>
        <br />
        <br />
        <h4>
            You have attempted to access a page that you are not authorized to view.
        </h4>
        <h4>
            If you have any questions, please contact the site administrator.
        </h4>
    </div>
</asp:Content>

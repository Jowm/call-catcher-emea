﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormChooser.aspx.cs" Inherits="CallDispositionTool.FormChooser" %>

<%@ OutputCache Duration="1440" VaryByParam="*" Location="Client" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Form Chooser</title>
    <!-- Google Analytics -->
    <script src="Scripts/GAnalytics.js" type="text/javascript"></script>
    <link href="CSS/FormStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function Close() {
            GetRadWindow().close();
        }
    </script>
    <rad:RadSkinManager ID="skinManager1" runat="server" PersistenceMode="Cookie">
    </rad:RadSkinManager>
    <rad:RadFormDecorator ID="formDecorator1" runat="server" EnableRoundedCorners="true"
        RenderMode="Lightweight" ControlsToSkip="H4H5H6" />
    <rad:RadScriptManager ID="ScriptManager1" runat="server">
    </rad:RadScriptManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    <div style="padding: 10px 10px 10px 10px">
        <asp:Panel ID="panel1" runat="server" DefaultButton="btnLaunchCTIPad">
            <table>
                <tr>
                    <td>
                        <strong>
                            <label for="btnLaunchCTIPad">
                                Select Form:</label></strong>
                    </td>
                    <td>
                        <rad:RadComboBox ID="cboForms" runat="server" DataTextField="FormName" DataValueField="FormID"
                            Width="200px" AppendDataBoundItems="true" DropDownAutoWidth="Enabled">
                            <Items>
                            </Items>
                        </rad:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <div style="float: left">
                            <asp:Button ID="btnLaunchCTIPad" runat="server" Text="Lauch CTI Pad" OnClick="btnLaunchCTIPad_Click" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="Close()" />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DispositionUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.DispositionUserControl" %>
<div>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManagerProxy ID="ajaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="cboDispo1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlDisposition" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="cboDispo2">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlDisposition" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="cboDispo3">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlDisposition" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="cboDispo4">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlDisposition" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="cboDispo5">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlDisposition" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <asp:Panel ID="pnlDisposition" runat="server">
        <asp:HiddenField ID="hdnFilterDispositionID" runat="server" />
        <rad:RadComboBox ID="cboDispo1" runat="server" AutoPostBack="True" EmptyMessage="Please Specify"
            DataTextField="Description" DataValueField="DispositionID" AppendDataBoundItems="true"
            CausesValidation="false" OnSelectedIndexChanged="cboDispo1_SelectedIndexChanged"
            MarkFirstMatch="true" DropDownAutoWidth="Enabled" MaxHeight="250px">
            <Items>
                <rad:RadComboBoxItem Text="" Value="" />
            </Items>
        </rad:RadComboBox>
        <asp:RequiredFieldValidator ID="reqDisposition1" runat="server" ControlToValidate="cboDispo1"
            ErrorMessage="Input here" Text="*" Display="Dynamic"></asp:RequiredFieldValidator>
        <rad:RadComboBox ID="cboDispo2" runat="server" Visible="False" EmptyMessage="Please Specify"
            DataTextField="Description" DataValueField="DispositionID" AppendDataBoundItems="true"
            OnSelectedIndexChanged="cboDispo2_SelectedIndexChanged" AutoPostBack="true" CausesValidation="false"
            MarkFirstMatch="true" DropDownAutoWidth="Enabled" MaxHeight="250px">
            <Items>
                <rad:RadComboBoxItem Text="" Value="" />
            </Items>
        </rad:RadComboBox>
        <asp:RequiredFieldValidator ID="reqDisposition2" runat="server" ControlToValidate="cboDispo2"
            ErrorMessage="Input here" Visible="false" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
        <rad:RadComboBox ID="cboDispo3" runat="server" Visible="False" EmptyMessage="Please Specify"
            DataTextField="Description" DataValueField="DispositionID" AppendDataBoundItems="true"
            AutoPostBack="True" OnSelectedIndexChanged="cboDispo3_SelectedIndexChanged" CausesValidation="false"
            MarkFirstMatch="true" DropDownAutoWidth="Enabled" MaxHeight="250px">
            <Items>
                <rad:RadComboBoxItem Text="" Value="" />
            </Items>
        </rad:RadComboBox>
        <asp:RequiredFieldValidator ID="reqDisposition3" runat="server" ControlToValidate="cboDispo3"
            ErrorMessage="Input here" Visible="false" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
        <rad:RadComboBox ID="cboDispo4" runat="server" Visible="False" EmptyMessage="Please Specify"
            DataTextField="Description" DataValueField="DispositionID" AutoPostBack="true"
            OnSelectedIndexChanged="cboDispo4_SelectedIndexChanged" AppendDataBoundItems="true"
            MarkFirstMatch="true" DropDownAutoWidth="Enabled" CausesValidation="false" MaxHeight="250px">
            <Items>
                <rad:RadComboBoxItem Text="" Value="" />
            </Items>
        </rad:RadComboBox>
        <asp:RequiredFieldValidator ID="reqDisposition4" runat="server" ControlToValidate="cboDispo4"
            ErrorMessage="Input here" Visible="false" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
        <rad:RadComboBox ID="cboDispo5" runat="server" Visible="False" EmptyMessage="Please Specify"
            DataTextField="Description" DataValueField="DispositionID" AutoPostBack="true"
            OnSelectedIndexChanged="cboDispo5_SelectedIndexChanged" AppendDataBoundItems="true"
            MarkFirstMatch="true" DropDownAutoWidth="Enabled" CausesValidation="false" MaxHeight="250px">
            <Items>
                <rad:RadComboBoxItem Text="" Value="" />
            </Items>
        </rad:RadComboBox>
        <asp:RequiredFieldValidator ID="reqDisposition5" runat="server" ControlToValidate="cboDispo5"
            ErrorMessage="Input here" Visible="false" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
    </asp:Panel>
</div>

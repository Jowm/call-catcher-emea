﻿<%@ Page Title="Report" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Report.aspx.cs" Inherits="CallDispositionTool.Report.Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <rad:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function startTrackProgress(grid) {
                if (!window._trackStart) {
                    window._trackStart = new Date();
                }
            }

            function endTrackProgress(grid) {
                var duration = getBindDuration();
                setStatusLabelVisiblity(grid, false);
                setDurationVisiblity(grid, true, duration);
            }

            function getBindDuration() {
                var result = { min: 0, sec: 0, msec: 0 };

                if (window._trackStart) {
                    var bindDuration = (new Date()) - window._trackStart;
                    window._trackStart = null;

                    result.min = parseInt(bindDuration / (1000 * 60));
                    result.sec = parseInt((bindDuration / 1000) % 60);
                    result.msec = parseInt((bindDuration % 1000) / 10);
                }

                result.min = result.min < 10 ? "0" + result.min : result.min + "";
                result.sec = result.sec < 10 ? "0" + result.sec : result.sec + "";
                result.msec = result.msec < 10 ? "0" + result.msec : result.msec + "";

                return result;
            }

            function onResponseEnd(sender, eventArgs) {
                var duration = getBindDuration();

                //var grid = $find("<%= grdReport.ClientID %>");
                //var footer = grid.get_masterTableViewFooter().get_element();
                //footer.rows[0].cells[1].innerHTML = 'Time Elapsed: ' + duration.min + ":" + duration.sec + ":" + duration.msec; // Set the footer text 
            }

            function onRequestStart(sender, args) {
                startTrackProgress();

                if (args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0 ||
                    args.get_eventTarget().indexOf("Button1") >= 0) {
                    args.set_enableAjax(false);
                }
            }

            function ShowEditForm(transactionID, rowIndex) {
                //alert(transactionID);
                var windowURL = '<%= ResolveClientUrl("~/TransactionEdit.aspx") %>' + '?TransactionID=' + transactionID;
                //location.href = windowURL;
                var grid = $find("<%= grdReport.ClientID %>");
                var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element(); grid.get_masterTableView().selectItem(rowControl, true);
                var oWnd = radopen(windowURL, null);
                oWnd.set_modal();
                oWnd.set_title("Transaction Edit - " + transactionID);
                oWnd.center();

                return false;
            }

            function refreshGrid() {
                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("Rebind");
            }
        </script>
    </rad:RadScriptBlock>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="onRequestStart" OnResponseEnd="onResponseEnd" />
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="btnSubmit">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlReport" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="Button1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlReport" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdReport" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="grdReport">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdReport" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="grdSmartPad">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdSmartPad" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="grdSmartPadDetail" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="grdSmartPadDetail">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdSmartPad" LoadingPanelID="LoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="grdSmartPadDetail" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="RadFilter1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlReport" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <rad:RadWindowManager ID="radWindowManager1" runat="server" Modal="true">
    </rad:RadWindowManager>
    <asp:Panel ID="pnlReport" runat="server" DefaultButton="btnSubmit">
        <div style="width: 100%" id="gridContainer">
            <h1>
                Reports</h1>
            <br />
            <table>
                <tr>
                    <td>
                        <strong>Select Report: </strong>
                    </td>
                    <td>
                        <rad:RadComboBox ID="cboForm" runat="server" AppendDataBoundItems="true" EmptyMessage="Select Report"
                            DropDownAutoWidth="Enabled">
                            <Items>
                                <rad:RadComboBoxItem Text="" Value="" />
                            </Items>
                        </rad:RadComboBox>
                        <asp:RequiredFieldValidator ID="req1" runat="server" ControlToValidate="cboForm"
                            Display="Dynamic" Text="*" ErrorMessage="Please specify the report"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <strong>Start Date: </strong>
                    </td>
                    <td>
                        <rad:RadDatePicker ID="dtpStartDate" runat="server">
                        </rad:RadDatePicker>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="dtpStartDate"
                            Display="Dynamic" Text="*" ErrorMessage="Please enter Start Date"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <strong>End Date: </strong>
                    </td>
                    <td>
                        <rad:RadDatePicker ID="dtpEndDate" runat="server">
                        </rad:RadDatePicker>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="dtpEndDate"
                            Display="Dynamic" Text="*" ErrorMessage="Please enter End Date"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:Button ID="btnSubmit" runat="server" Text="Generate" Width="100px" OnClick="btnSubmit_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Include Call Data?</strong>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rblIncludeCallData" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                            <asp:ListItem Text="No" Value="False"></asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="rblIncludeCallData"
                            Display="Dynamic" Text="*" ErrorMessage="Select value for include call data"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <strong>Include Contact Details?</strong>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rblIncludeContactDetails" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                            <asp:ListItem Text="No" Value="False"></asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="rblIncludeContactDetails"
                            Display="Dynamic" Text="*" ErrorMessage="Select value for include contact details"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <strong>Include Sign Off Details?</strong>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rblIncludeSignOffDetails" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                            <asp:ListItem Text="No" Value="False" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="rblIncludeSignOffDetails"
                            Display="Dynamic" Text="*" ErrorMessage="Select value for include sign details"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Form fields alphabetical</strong>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rblFormFieldsAlphabetical" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Yes" Value="True" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="No" Value="False"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <rad:RadFilter runat="server" ID="RadFilter1" FilterContainerID="grdReport" ShowApplyButton="true"
                ApplyButtonText="Apply Filter" Visible="false">
            </rad:RadFilter>
            <br />
            <rad:RadGrid ID="grdReport" runat="server" AutoGenerateColumns="true" AllowSorting="true"
                AllowPaging="true" AllowCustomPaging="true" Width="868px" OnNeedDataSource="grdReport_NeedDataSource"
                OnItemDataBound="grdReport_ItemDataBound" VirtualItemCount="10000" OnColumnCreated="grdReport_ColumnCreated">
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" FileName="Report">
                </ExportSettings>
                <ClientSettings>
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                </ClientSettings>
                <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="true" EnableHeaderContextMenu="true"
                    DataKeyNames="TransactionID" PageSize="15" TableLayout="Fixed" ShowFooter="true">
                    <PagerStyle AlwaysVisible="true" Mode="NextPrevNumericAndAdvanced" />
                    <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToExcelButton="true"
                        ShowExportToCsvButton="true" />
                    <SortExpressions>
                        <rad:GridSortExpression FieldName="TransactionID" SortOrder="Descending" />
                    </SortExpressions>
                    <Columns>
                        <telerik:GridTemplateColumn UniqueName="TemplateEditColumn" AllowFiltering="false"
                            Exportable="false">
                            <ItemTemplate>
                                <asp:ImageButton ID="EditLink" runat="server" Text="Edit" ImageUrl="~/images/Edit.gif"
                                    CommandName="FormEdit" ToolTip="Edit Record"></asp:ImageButton>
                            </ItemTemplate>
                            <HeaderStyle Width="30px" />
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
            </rad:RadGrid>
            <telerik:RadGrid ID="grdSmartPad" runat="server" OnNeedDataSource="grdSmartPad_NeedDataSource"
                AllowPaging="True" EnableViewState="true" OnDetailTableDataBind="grdSmartPadDetail_DataBind"
                OnItemCommand="grdSmartPad_ItemCommand">
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" FileName="Report">
                </ExportSettings>
                <GroupingSettings CaseSensitive="False" />
                <FilterItemStyle HorizontalAlign="Center" />
                <SortingSettings EnableSkinSortStyles="false" />
                <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="True" EnableHeaderContextMenu="true"
                    DataKeyNames="TransactionID" PageSize="15" TableLayout="Fixed" ShowFooter="true">
                    <CommandItemSettings ShowExportToExcelButton="True" ShowExportToCsvButton="True" ShowAddNewRecordButton="false"
                        ShowRefreshButton="True" />
                    <DetailTables>
                        <rad:GridTableView Name="grdSmartPadDetail" runat="server" AutoGenerateColumns="True"
                            AllowSorting="true" AllowPaging="true" Width="100%" EnableViewState="true">
                        </rad:GridTableView>
                    </DetailTables>
                </MasterTableView>
            </telerik:RadGrid>
            <%--<asp:Button Text="Export to Excel" ID="Button1" OnClick="Button1_Click" runat="server" />--%>
        </div>
    </asp:Panel>
</asp:Content>

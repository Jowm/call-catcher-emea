﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Linq;
using CallDispositionTool.Model;
using CallDispositionTool.Security;
using System.Collections.Generic;
using Telerik.Web.UI;
using Telerik.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace CallDispositionTool
{
    public partial class Contact : NotificationPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void grdContacts_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && Request.QueryString["Mode"] == "Add")
            {
                grdContacts.MasterTableView.IsItemInserted = true;
                grdContacts.MasterTableView.Rebind();
            }
        }

        protected void grdContacts_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    int contactCount = (from c in db.Contacts
                                       select c).Count();

                    grdContacts.VirtualItemCount = contactCount;

                    int startRowIndex = grdContacts.CurrentPageIndex * grdContacts.PageSize;
                    int maximumRows = grdContacts.PageSize;

                    grdContacts.AllowCustomPaging = true;

                    grdContacts.DataSource = (from c in db.Contacts
                                              select c).OrderBy(c => c.ContactID).Skip(startRowIndex).Take(maximumRows).ToList();
                }
            }
        }

        protected void grdContacts_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                CallDispositionTool.Model.Contact obj = new CallDispositionTool.Model.Contact();

                obj.FirstName = values["FirstName"] as string;
                obj.LastName = values["LastName"] as string;
                obj.Tags = values["Tags"] as string;

                if (values["CompanyID"] != null)
                    obj.CompanyID = Convert.ToInt32(values["CompanyID"]);

                obj.Title = values["Title"] as string;
                obj.Email = values["Email"] as string;
                obj.Phone = values["Phone"] as string;
                obj.MobilePhone = values["MobilePhone"] as string;
                obj.Fax = values["Fax"] as string;
                obj.Address1 = values["Address1"] as string;
                obj.Address2 = values["Address2"] as string;
                obj.City = values["City"] as string;
                obj.StateRegion = values["StateRegion"] as string;
                obj.Country = values["Country"] as string;
                obj.PostalCode = values["PostalCode"] as string;
                obj.CreateDate = DateTime.Now;
                obj.CreatedBy = Membership.GetUser().ProviderUserKey as Int32?;

                try
                {
                    db.Contacts.AddObject(obj);
                    db.SaveChanges();
                    grdContacts.Controls.Add(new LiteralControl(string.Format("<strong style='color: green'>Record for {0} {1} successfully inserted</strong>", obj.FirstName, obj.LastName)));
                }
                catch (Exception ex)
                {
                    grdContacts.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error inserting Contact: {0}\nInner Exception: {1}</strong>", ex.Message, ex.InnerException.Message)));
                    e.Canceled = true;
                }
            }
        }

        protected void grdContacts_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            var contactID = (decimal)editableItem.GetDataKeyValue("ContactID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var contact = (from u in db.Contacts
                               where u.ContactID == contactID
                               select u).FirstOrDefault();

                if (contact != null)
                {
                    //update entity's state
                    editableItem.UpdateValues(contact);

                    try
                    {
                        db.SaveChanges();
                        CacheFactory.GetCacheManager().Flush();
                        grdContacts.Controls.Add(new LiteralControl(string.Format("<strong style='color: green'>Record for {0} {1} successfully updated: {0}</strong>", contact.FirstName, contact.LastName)));
                    }
                    catch (Exception ex)
                    {
                        grdContacts.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error Updating Contact: {0}\nInner Exception: {1}</strong>", ex.Message, ex.InnerException.Message)));
                        e.Canceled = true;
                    }
                }
            }
        }

        protected void grdContacts_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            decimal contactID = (decimal)((GridDataItem)e.Item).GetDataKeyValue("ContactID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var contact = (from c in db.Contacts
                               where c.ContactID == contactID
                               select c).FirstOrDefault();

                if (contact != null)
                {
                    try
                    {
                        db.Contacts.DeleteObject(contact);
                        db.SaveChanges();
                        grdContacts.Controls.Add(new LiteralControl(string.Format("<strong style='color: green'>Contact ID {0} has been deleted</strong>", contact.ContactID)));
                    }
                    catch (Exception ex)
                    {
                        grdContacts.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error deleting Contact: {0}\nInner Exception: {1}</strong>", ex.Message, ex.InnerException.Message)));
                        e.Canceled = true;
                    }
                }
            }
        }

        protected void grdContactExtendedInfo_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            decimal id = (decimal)((GridDataItem)e.Item).GetDataKeyValue("ContactExtendedInfoID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var obj = (from u in db.ContactExtendedInfoes
                           where u.ContactExtendedInfoID == id
                           select u).FirstOrDefault();

                if (obj != null)
                {
                    try
                    {
                        db.ContactExtendedInfoes.DeleteObject(obj);
                        db.SaveChanges();
                        grdContacts.Controls.Add(new LiteralControl(string.Format("<strong style='color: Green'>Contact ID {0} has been deleted</strong>", obj.ContactExtendedInfoID)));
                    }
                    catch (Exception ex)
                    {
                        grdContacts.Controls.Add(new LiteralControl(string.Format("<strong style='color: Red'>Error deleting contact: {0}\nInner Exception: {1}</strong>", ex.Message, ex.InnerException.Message)));
                    }
                }
            }
        }

        protected void grdContactExtendedInfo_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                GridDataItem parentItem = ((sender as RadGrid).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;
                decimal id = Convert.ToDecimal(parentItem.GetDataKeyValue("ContactID"));

                var infos = (from o in db.ContactExtendedInfoes
                             where o.ContactID == id
                             select o).ToList();

                (sender as RadGrid).DataSource = infos;
            }
        }

        protected void grdTransactionHistory_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                GridDataItem parentItem = ((sender as RadGrid).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;
                decimal id = Convert.ToDecimal(parentItem.GetDataKeyValue("ContactID"));

                var ds = (from tc in db.TransactionContacts
                          where tc.ContactID == id
                          select new { tc.TransactionID, tc.Transaction.Form.FormName, tc.Transaction.User.EmployeeID, tc.Transaction.CreatedOn }).ToList();

                (sender as RadGrid).DataSource = ds;
            }
        }

        protected void grdContactExtendedInfo_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.ExpandCollapseCommandName && !e.Item.Expanded)
            {
                foreach (GridItem item in e.Item.OwnerTableView.Items)
                {
                    if (item.Expanded && item != e.Item)
                    {
                        item.Expanded = false;
                    }
                }

                GridDataItem parentItem = e.Item as GridDataItem;
                RadGrid grdContactExtendedInfo = parentItem.ChildItem.FindControl("grdContactExtended") as RadGrid;

                if (grdContactExtendedInfo != null)
                    grdContactExtendedInfo.Rebind();

                RadGrid grdTransactionHistory = parentItem.ChildItem.FindControl("grdTransactionHistory") as RadGrid;

                if (grdTransactionHistory != null)
                    grdTransactionHistory.Rebind();

            }
            else if (e.CommandName == "FilterRadGrid")
            {
                RadFilter1.FireApplyCommand();
            }
        }

        protected void grdContactExtendedInfo_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);
            GridDataItem parentItem = ((sender as RadGrid).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;
            decimal id = Convert.ToDecimal(parentItem.GetDataKeyValue("ContactID"));

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                ContactExtendedInfo info = new ContactExtendedInfo();

                info.ContactID = id;
                info.KeyInfo = values["KeyInfo"] as string;
                info.Value = values["Value"] as string;
                info.CreateDate = DateTime.Now;
                info.CreatedBy = Membership.GetUser().ProviderUserKey as int?;

                try
                {
                    db.ContactExtendedInfoes.AddObject(info);
                    db.SaveChanges();
                    grdContacts.Controls.Add(new LiteralControl(string.Format("<strong style='color: Green'>Record has been inserted for Key:{0} Value:{1}</strong>", info.KeyInfo, info.Value)));
                }
                catch (Exception ex)
                {
                    Helper.LogError(ex);
                    grdContacts.Controls.Add(new LiteralControl(string.Format("<strong style='color: Red'>Error deleting info: {0}\nInner Exception: {1}</strong>", ex.Message, ex.InnerException.Message)));
                }
            }
        }

        protected void grdTransactionHistory_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                ImageButton editLink = (ImageButton)e.Item.FindControl("EditLink");
                editLink.Attributes["href"] = "javascript:void(0);";
                editLink.Attributes["target"] = "_blank";
                editLink.OnClientClick = String.Format("return ShowEditForm('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["TransactionID"], e.Item.ItemIndex);
            }
        }

        protected void grdContactExtendedInfo_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            decimal id = (decimal)editableItem.GetDataKeyValue("ContactExtendedInfoID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var info = (from u in db.ContactExtendedInfoes
                            where u.ContactExtendedInfoID == id
                            select u).FirstOrDefault();

                if (info != null)
                {
                    //update entity's state
                    editableItem.UpdateValues(info);

                    try
                    {
                        db.SaveChanges();
                        grdContacts.Controls.Add(new LiteralControl(string.Format("<strong style='color: Green'>Record for Key: {0} has been updated: {0}</strong>", info.KeyInfo)));
                    }
                    catch (Exception ex)
                    {
                        grdContacts.Controls.Add(new LiteralControl(string.Format("Error Updating : {0}", ex.Message)));
                    }
                }
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using CallDispositionTool.UserControl;
using Telerik.Web.UI;
using System.Web.Security;
using System.Web.Configuration;
using Microsoft.AspNet.SignalR;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace CallDispositionTool
{
    public partial class TransactionHistory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                grdTransaction.Rebind();

                try
                {
                    lblContactName.Text = Request["ContactName"];
                }
                catch (Exception)
                {
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            HttpCookie ck = Request.Cookies["UserInfo"];
            string skinID = string.Empty;

            if (ck != null)
            {
                skinID = ck.Values["Skin"];
                this.skinManager1.Skin = skinID;
            }
            else
            {
                ck = new HttpCookie("UserInfo");
                ck.Values["Skin"] = this.skinManager1.Skin;
                Response.Cookies.Remove("UserInfo");
                Response.Cookies.Add(ck);
            }
        }

        protected void grdTransaction_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                ImageButton editLink = (ImageButton)e.Item.FindControl("EditLink");
                editLink.Attributes["href"] = "javascript:void(0);";
                editLink.Attributes["target"] = "_blank";
                editLink.OnClientClick = String.Format("return ShowEditForm('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["TransactionID"], e.Item.ItemIndex);
            }
        }

        bool isGrouping = false;
        public bool ShouldApplySortFilterOrGroup()
        {
            return grdTransaction.MasterTableView.FilterExpression != "" ||
                (grdTransaction.MasterTableView.GroupByExpressions.Count > 0 || isGrouping) ||
                grdTransaction.MasterTableView.SortExpressions.Count > 0;
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdTransaction.MasterTableView.SortExpressions.Clear();
                grdTransaction.MasterTableView.GroupByExpressions.Clear();
                grdTransaction.MasterTableView.CurrentPageIndex = grdTransaction.MasterTableView.PageCount - 1;
                grdTransaction.Rebind();
            }
        }

        protected void TabStrip1_TabClick(object sender, RadTabStripEventArgs e)
        {
            if (TabStrip1.SelectedIndex == 0)
            {
                grdTransaction.Rebind();
            }
        }

        protected void grdTransaction_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
            {
                ////low level sql datatable 
                //using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CallDisposition"].ConnectionString))
                //{
                //    cn.Open();

                //    string sql = @"GetTransactionsByContact";
                //    int transCount = 0;
                //    int contactID = Convert.ToInt32(Request.QueryString["ContactID"]);
                //    int formID = Convert.ToInt32(Request.QueryString["FormID"]);

                //    using (CallDispositionEntities db = new CallDispositionEntities())
                //    {
                //        transCount = (from t in db.TransactionContacts
                //                      where t.ContactID == contactID && t.Transaction.FormID == formID
                //                      select t).Count();
                //    }

                //    int startRowIndex = (ShouldApplySortFilterOrGroup()) ? 0 : grdTransaction.CurrentPageIndex * grdTransaction.PageSize;
                //    int maximumRows = (ShouldApplySortFilterOrGroup()) ? transCount : grdTransaction.PageSize;

                //    SqlCommand cmd = new SqlCommand(sql, cn);
                //    cmd.CommandTimeout = 9000;
                //    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //    cmd.Parameters.AddWithValue("@ContactID", contactID);
                //    cmd.Parameters.AddWithValue("@FormID", formID);
                //    cmd.Parameters.AddWithValue("@StartRowIndex", startRowIndex);
                //    cmd.Parameters.AddWithValue("@MaximumRows", maximumRows);
                //    grdTransaction.AllowCustomPaging = !ShouldApplySortFilterOrGroup();

                //    SqlDataAdapter da = new SqlDataAdapter(cmd);
                //    DataTable dt = new DataTable();

                //    da.Fill(dt);

                //    try
                //    {
                //        if (dt.Columns.Count > 0)
                //        {
                //            grdTransaction.DataSource = dt;
                //        }
                //        else
                //        {
                //            grdTransaction.DataSource = null;
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        RadWindowManager1.RadAlert(ex.Message, 330, 180, "Error Message", "");
                //    }
                //}

                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    decimal contactID = Convert.ToInt32(Request.QueryString["ContactID"]);

                    var ds = (from tc in db.TransactionContacts
                              join t in db.Transactions on tc.TransactionID equals t.TransactionID
                              join f in db.Forms on t.FormID equals f.FormID
                              join u in db.Users on t.CreatedBy equals u.UserID
                              join av in db.AttributeValues on tc.TransactionID equals av.TransactionID
                              join a in db.Attributes on av.AttributeID equals a.AttributeID
                              where tc.ContactID == contactID
                              let SourceOfCallAndDisposition = a.AttributeName == "Source of Call and Disposition" ? av.Value : string.Empty
                              let Comments = a.AttributeName.Contains("Comment") ? av.Value : string.Empty
                              let CreatedBy = u.EmployeeID
                              select new {
                                  tc.TransactionID,
                                  f.FormName,
                                  SourceOfCallAndDisposition,
                                  Comments,
                                  CreatedBy,
                                  t.CreatedOn
                              } into x
                              group x by new {
                                  x.TransactionID,
                                  x.FormName,
                                  x.CreatedBy,
                                  x.CreatedOn
                              } into g
                              select new {
                                  g.Key.TransactionID,
                                  g.Key.FormName,
                                  SourceOfCallAndDisposition = g.Max(t => t.SourceOfCallAndDisposition),
                                  Comments = g.Max(t => t.Comments),
                                  g.Key.CreatedBy,
                                  g.Key.CreatedOn }
                              ).ToList();

                    (sender as RadGrid).DataSource = ds;
                }
            }
        }
    }
}
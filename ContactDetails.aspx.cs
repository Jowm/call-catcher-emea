﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using CallDispositionTool.UserControl;
using Telerik.Web.UI;
using System.Web.Security;
using System.Web.Configuration;
using Microsoft.AspNet.SignalR;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Web.Services;
using System.Collections;

namespace CallDispositionTool
{
    public partial class ContactDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string contactID;
                int iContactID;

                if (Request["Mode"] == "Add")
                {
                    TabStrip1.Tabs[0].Visible = false;
                    TabStrip1.Tabs[1].Visible = false;
                    TabStrip1.Tabs[2].Visible = true;
                    TabStrip1.Tabs[2].Selected = true;
                    MultiPage1.SelectedIndex = 2;
                    lblContactName.Text = "Add Contact";
                    //BindCorpSysPrin();
                }
                else
                {
                    contactID = Request["ContactID"];

                    if (int.TryParse(contactID, out iContactID))
                    {
                        BindDetails(iContactID);
                        grdContactExtended.Rebind();
                    }
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            HttpCookie ck = Request.Cookies["UserInfo"];
            string skinID = string.Empty;

            if (ck != null)
            {
                skinID = ck.Values["Skin"];
                this.skinManager1.Skin = skinID;
            }
            else
            {
                ck = new HttpCookie("UserInfo");
                ck.Values["Skin"] = this.skinManager1.Skin;
                Response.Cookies.Remove("UserInfo");
                Response.Cookies.Add(ck);
            }
        }

        //void BindCorpSysPrin()
        //{
        //    using (CallDispositionEntities db = new CallDispositionEntities())
        //    {
        //        var items = (from c in db.CorpSysPrins
        //                     where (c.HideFromList == false || !c.HideFromList.HasValue)
        //                     select new { c.CorpSysPrinID, c.CorpSysPrin1 }).OrderBy(c => c.CorpSysPrin1).ToList();

        //        cboCorpSysPrin.Items.Clear();
        //        cboEditCorpSysPrin.Items.Clear();
        //        cboCorpSysPrin.Items.Add(new RadComboBoxItem("", ""));
        //        cboEditCorpSysPrin.Items.Add(new RadComboBoxItem("", ""));

        //        foreach (var item in items)
        //        { 
        //            cboEditCorpSysPrin.Items.Add(new RadComboBoxItem(item.CorpSysPrin1, item.CorpSysPrinID.ToString()));
        //            cboCorpSysPrin.Items.Add(new RadComboBoxItem(item.CorpSysPrin1, item.CorpSysPrinID.ToString()));
        //        }
        //    }
        //}

        void BindDetails(decimal ContactID)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var contact = (from c in db.Contacts
                               where c.ContactID == ContactID
                               select c).FirstOrDefault();

                if (contact != null)
                {
                    lblContactName.Text = contact.FirstName + " " + contact.LastName;
                    lblFirstName.Text = contact.FirstName;
                    lblLastName.Text = contact.LastName;
                    //lblCorpSysPrin.Text = contact.CorpSysPrin;
                    //lblAccountNumber.Text = contact.AccountNumber;
                    lblPhone.Text = contact.Phone;
                    lblAddress1.Text = contact.Address1;
                    lblStoreNumber.Text = contact.StoreNumber;
                    lblNTLoginID.Text = contact.NTLoginID;
                    lblTransactionID.Text = contact.TransactionID;
                    //lblCity.Text = contact.City;
                    //lblStateRegion.Text = contact.StateRegion;
                    //lblPostalCode.Text = contact.PostalCode;
                    //lblComments.Text = contact.Comments;
                    lblCreatedBy.Text = contact.User.FirstName + " " + contact.User.LastName;
                    lblCreatedOn.Text = contact.CreateDate.ToString();
                }
            }
        }

        protected void grdContactExtendedInfo_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            decimal id = (decimal)((GridDataItem)e.Item).GetDataKeyValue("ContactExtendedInfoID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var obj = (from u in db.ContactExtendedInfoes
                           where u.ContactExtendedInfoID == id
                           select u).FirstOrDefault();

                if (obj != null)
                {
                    try
                    {
                        db.ContactExtendedInfoes.DeleteObject(obj);
                        db.SaveChanges();
                        grdContactExtended.Controls.Add(new LiteralControl(string.Format("<strong style='color: Green'>Record ID {0} has been deleted</strong>", obj.ContactExtendedInfoID)));
                    }
                    catch (Exception ex)
                    {
                        grdContactExtended.Controls.Add(new LiteralControl(string.Format("<strong style='color: Red'>Error deleting record: {0}\nInner Exception: {1}</strong>", ex.Message, ex.InnerException.Message)));
                    }
                }
            }
        }

        protected void grdContactExtendedInfo_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);
            decimal id = Convert.ToDecimal(Request["ContactID"]);

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                ContactExtendedInfo info = new ContactExtendedInfo();

                info.ContactID = id;
                info.KeyInfo = values["KeyInfo"] as string;
                info.Value = values["Value"] as string;
                info.CreateDate = DateTime.Now;
                info.CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);

                try
                {
                    db.ContactExtendedInfoes.AddObject(info);
                    db.SaveChanges();
                    grdContactExtended.Controls.Add(new LiteralControl(string.Format("<strong style='color: Green'>Record has been inserted for Key:{0} Value:{1}</strong>", info.KeyInfo, info.Value)));
                }
                catch (Exception ex)
                {
                    Helper.LogError(ex);
                    grdContactExtended.Controls.Add(new LiteralControl(string.Format("<strong style='color: Red'>Error deleting info: {0}\nInner Exception: {1}</strong>", ex.Message, ex.InnerException.Message)));
                }
            }
        }

        protected void grdContactExtendedInfo_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                decimal id = Convert.ToDecimal(Request["ContactID"]);

                var infos = (from o in db.ContactExtendedInfoes
                             where o.ContactID == id
                             select o).ToList();

                (sender as RadGrid).DataSource = infos;
            }
        }

        protected void grdContactExtendedInfo_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            decimal id = (decimal)editableItem.GetDataKeyValue("ContactExtendedInfoID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var info = (from u in db.ContactExtendedInfoes
                            where u.ContactExtendedInfoID == id
                            select u).FirstOrDefault();

                if (info != null)
                {
                    //update entity's state
                    editableItem.UpdateValues(info);

                    try
                    {
                        db.SaveChanges();
                        grdContactExtended.Controls.Add(new LiteralControl(string.Format("<strong style='color: Green'>Record for Key: {0} has been updated: {0}</strong>", info.KeyInfo)));
                    }
                    catch (Exception ex)
                    {
                        grdContactExtended.Controls.Add(new LiteralControl(string.Format("Error Updating : {0}", ex.Message)));
                    }
                }
            }
        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    Model.Contact contact = new Model.Contact();

                    contact.FirstName = txtFirstName.Text;
                    contact.LastName = txtLastName.Text;
                    contact.Phone = txtPhone.Text;
                    //contact.CorpSysPrin = cboCorpSysPrin.SelectedItem.Text;
                    //contact.AccountNumber = txtAccountNumber.Text;
                    contact.Address1 = txtAddress1.Text;
                    contact.StoreNumber = txtStoreNumber.Text;
                    contact.NTLoginID = txtNTLoginID.Text;
                    contact.TransactionID = txtTransactionID.Text;
                    //contact.City = cboCity.Text;
                    //contact.StateRegion = cboStateRegion.Text;
                    //contact.PostalCode = cboZipCode.Text;
                    //contact.Comments = txtComments.Text;
                    contact.CreateDate = DateTime.Now;
                    contact.CreatedBy = (int)Membership.GetUser().ProviderUserKey;

                    db.Contacts.AddObject(contact);
                    db.SaveChanges();
                    pnlNew.Enabled = false;

                    RadWindowManager1.RadAlert(
                        "New contact has been saved. Contact ID <strong># " + contact.ContactID + " </strong>",
                        330,
                        180,
                        "Save Success",
                        String.Format("ClosePopup('{0}')", contact.ContactID.ToString()));
                }
            }
            catch (Exception ex)
            {
                RadWindowManager1.RadAlert(
                    String.Format("<strong style='color: Red'>Error saving record\nError: {0}\nInner Exception: {1} </strong>", ex.Message, ex.InnerException.Message),
                    330,
                    180,
                    "Error",
                    "");
                pnlNew.Enabled = false;
                Helper.LogError(ex);
            }
        }

        //protected void searchBoxCity_Search(object sender, Telerik.Web.UI.SearchBoxEventArgs e)
        //{
        //    if (e.DataItem != null)
        //    {
        //        BindCity();
        //        cboStateRegion.Items.Clear();
        //    }
        //}

        //protected void txtEditPostalCode_Search(object sender, Telerik.Web.UI.SearchBoxEventArgs e)
        //{
        //    if (e.DataItem != null)
        //    {
        //        BindEditCity();
        //        cboEditStateRegion.Items.Clear();
        //    }
        //}

        //void BindCity()
        //{
        //    using (CallDispositionEntities db = new CallDispositionEntities())
        //    {
        //        cboCity.Items.Clear();
        //        cboStateRegion.Items.Clear();

        //        var cities = (from c in db.ZipCodes
        //                      where c.ZipCode1 == cboZipCode.Text
        //                      select new { c.City }).Distinct().ToList();

        //        cboCity.Items.Add(new RadComboBoxItem("", ""));

        //        foreach (var item in cities)
        //        {
        //            cboCity.Items.Add(new RadComboBoxItem(item.City, item.City));
        //        }
        //    }
        //}

        //void BindEditCity()
        //{
        //    using (CallDispositionEntities db = new CallDispositionEntities())
        //    {
        //        cboEditCity.Items.Clear();
        //        cboEditStateRegion.Items.Clear();

        //        var cities = (from c in db.ZipCodes
        //                      where c.ZipCode1 == cboEditZipCode.Text
        //                      select new { c.City }).Distinct().ToList();

        //        if (cities != null)
        //        {
        //            cboEditCity.Items.Add(new RadComboBoxItem("", ""));

        //            foreach (var item in cities)
        //            {
        //                cboEditCity.Items.Add(new RadComboBoxItem(item.City, item.City));
        //            }
        //        }
        //    }
        //}

        //void BindStateRegion()
        //{
        //    using (CallDispositionEntities db = new CallDispositionEntities())
        //    {
        //        cboStateRegion.Items.Clear();

        //        var states = (from c in db.ZipCodes
        //                      where c.City.Trim() == cboCity.Text.Trim() && c.ZipCode1 == cboZipCode.Text.Trim()
        //                      select new { c.State }).Distinct().ToList();

        //        if (states != null)
        //        {
        //            cboStateRegion.Items.Add(new RadComboBoxItem("", ""));

        //            foreach (var item in states)
        //            {
        //                cboStateRegion.Items.Add(new RadComboBoxItem(item.State, item.State));
        //            }
        //        }
        //    }
        //}

        //void BindEditStateRegion()
        //{
        //    using (CallDispositionEntities db = new CallDispositionEntities())
        //    {
        //        cboEditStateRegion.Items.Clear();

        //        var states = (from c in db.ZipCodes
        //                      where c.City.Trim() == cboEditCity.Text.Trim()
        //                      select new { c.State }).Distinct().ToList();

        //        if (states != null)
        //        {
        //            cboEditStateRegion.Items.Add(new RadComboBoxItem("", ""));

        //            foreach (var item in states)
        //            {
        //                cboEditStateRegion.Items.Add(new RadComboBoxItem(item.State, item.State));
        //            }
        //        }
        //    }
        //}

        //[WebMethod]
        //public static SearchBoxItemData[] GetCityResults(SearchBoxContext context)
        //{
        //    dynamic zipcodes = Helper.GetZipCodeSearch(context.Text);
        //    List<SearchBoxItemData> result = new List<SearchBoxItemData>();

        //    foreach (var item in zipcodes)
        //    {
        //        SearchBoxItemData itemData = new SearchBoxItemData();
        //        Dictionary<string, object> dic = new Dictionary<string, object>();

        //        itemData.Value = Convert.ToString(item.GetType().GetProperty("ZipCodeID").GetValue(item, null));
        //        itemData.Text = item.GetType().GetProperty("ZipCode").GetValue(item, null);
        //        dic["ZipCodeID"] = item.GetType().GetProperty("ZipCodeID").GetValue(item, null);
        //        dic["ZipCode"] = item.GetType().GetProperty("ZipCode").GetValue(item, null);
        //        itemData.DataItem = dic;
        //        result.Add(itemData);
        //    }

        //    return result.ToArray();
        //}

        //void BindZipCodes(string AreaCode)
        //{
        //    using (CallDispositionEntities db = new CallDispositionEntities())
        //    {
        //        cboZipCode.Items.Clear();

        //        var zipcodes = (from z in db.ZipCodes
        //                        where z.AreaCode == AreaCode
        //                        select new { ZipCode = z.ZipCode1 }).Distinct().ToList();

        //        if (zipcodes != null)
        //        {
        //            cboZipCode.Items.Add(new RadComboBoxItem("", ""));

        //            foreach (var item in zipcodes)
        //            {
        //                cboZipCode.Items.Add(new RadComboBoxItem(item.ZipCode, item.ZipCode));
        //            }
        //        }
        //    }
        //}

        //void BindEditZipCodes(string AreaCode)
        //{
        //    using (CallDispositionEntities db = new CallDispositionEntities())
        //    {
        //        cboEditZipCode.Items.Clear();

        //        var zipcodes = (from z in db.ZipCodes
        //                        where z.AreaCode == AreaCode
        //                        select new { ZipCode = z.ZipCode1 }).Distinct().ToList();

        //        if (zipcodes != null)
        //        {
        //            cboEditZipCode.Items.Add(new RadComboBoxItem("", ""));

        //            foreach (var item in zipcodes)
        //            {
        //                cboEditZipCode.Items.Add(new RadComboBoxItem(item.ZipCode, item.ZipCode));
        //            }
        //        }
        //    }
        //}

        //protected void cboZipCode_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        //{
        //    BindCity();
        //}

        //protected void cboEditZipCode_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        //{
        //    BindEditCity();
        //}

        //protected void cboCity_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        //{
        //    BindStateRegion();
        //}

        //protected void cboEditCity_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        //{
        //    BindEditStateRegion();
        //}

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            pnlEdit.Visible = true;
            pnlInfo.Visible = false;

            //BindCorpSysPrin();
            txtEditFirstName.Text = lblFirstName.Text;
            txtEditLastName.Text = lblLastName.Text;
            //cboEditCorpSysPrin.SelectedIndex = cboEditCorpSysPrin.FindItemIndexByText(lblCorpSysPrin.Text);
            //cboEditCorpSysPrin.Text = lblCorpSysPrin.Text;
            //txtEditAccountNumber.Text = lblAccountNumber.Text;
            txtEditPhone.Text = lblPhone.Text;
            txtEditAddress1.Text = lblAddress1.Text;
            txtEditStoreNumber.Text = lblStoreNumber.Text;
            txtEditNTLoginID.Text = lblNTLoginID.Text;
            txtEditTransactionID.Text = lblTransactionID.Text;
            //cboEditZipCode.Text = lblPostalCode.Text;
            //cboEditCity.SelectedIndex = cboEditCity.FindItemIndexByText(lblCity.Text);
            //cboEditCity.Text = lblCity.Text;
            //cboEditStateRegion.SelectedIndex = cboEditStateRegion.FindItemIndexByText(lblStateRegion.Text);
            //cboEditStateRegion.Text = lblStateRegion.Text;
            //txtEditComment.Text = lblComments.Text;
        }

        protected void btnEditCancel_Click(object sender, EventArgs e)
        {
            pnlEdit.Visible = false;
            pnlInfo.Visible = true;
        }

        protected void btnTest_OnClick(object sender, EventArgs e)
        {

        }

        protected void btnEditSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    decimal contactID;

                    if (decimal.TryParse(Request["ContactID"], out contactID))
                    {
                        Model.Contact contact = (from c in db.Contacts
                                                 where c.ContactID == contactID
                                                 select c).FirstOrDefault();

                        if (contact != null)
                        {
                            contact.FirstName = txtEditFirstName.Text;
                            contact.LastName = txtEditLastName.Text;
                            contact.Phone = txtEditPhone.Text;
                            //contact.CorpSysPrin = cboEditCorpSysPrin.SelectedItem.Text;
                            //contact.AccountNumber = txtEditAccountNumber.Text;
                            contact.Address1 = txtEditAddress1.Text;
                            //contact.City = cboEditCity.Text;
                            //contact.StateRegion = cboEditStateRegion.Text;
                            //contact.PostalCode = cboEditZipCode.SelectedItem.Text;
                            //contact.Comments = txtEditComment.Text;

                            db.SaveChanges();
                            BindDetails(contactID);
                            pnlEdit.Visible = false;
                            pnlInfo.Visible = true;

                        }
                        else
                        {
                            RadWindowManager1.RadAlert("No contacts to edit.", 330, 180, "Error Message", "");
                        }
                    }
                    else
                    {
                        RadWindowManager1.RadAlert("Invalid Contact ID", 330, 180, "Error Message", "");
                    }
                }

            }
            catch (Exception ex)
            {
                RadWindowManager1.RadAlert(string.Format("Error: {0}\nInner Exception: {1}", ex.Message, ex.InnerException), 330, 180, "Error Message", "");
                Helper.LogError(ex);
            }
        }

        //protected void txtEditPhone_TextChanged(object sender, EventArgs e)
        //{
        //    string sAreaCode = txtEditPhone.Text.Replace("(", "").Replace(")", "").Replace("+", "").Replace(" ", "").Replace("-", "");

        //    if (txtEditPhone.Text.Length >= 3)
        //    {
        //        sAreaCode = sAreaCode.Substring(0, 3);
        //        BindEditZipCodes(sAreaCode);
        //        cboEditZipCode.Text = txtEditPhone.Text.Substring(0, 3);
        //        cboEditCity.ClearSelection();
        //        cboEditStateRegion.ClearSelection();
        //    }
        //}

        //protected void txtPhone_TextChanged(object sender, EventArgs e)
        //{
        //    string sAreaCode = txtPhone.Text.Replace("(", "").Replace(")", "").Replace("+", "").Replace(" ", "").Replace("-", "");

        //    if (sAreaCode.Length >= 3)
        //    {
        //        sAreaCode = sAreaCode.Substring(0, 3);
        //        BindZipCodes(sAreaCode);
        //        cboCity.ClearSelection();
        //        cboStateRegion.ClearSelection();
        //    }
        //}
    }
}
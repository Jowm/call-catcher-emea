﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="PasswordChangeQuestionAndAnswer.aspx.cs" Inherits="CallDispositionTool.PasswordChangeQuestionAndAnswer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <h2>
        Change Passwords Question and Answer</h2>
    <br />
    <br />
    <table>
        <tr>
            <td>
                Username:
            </td>
            <td>
                <asp:TextBox ID="txtUsername" runat="server" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Password Question:
            </td>
            <td>
                <rad:RadComboBox ID="cboPasswordQuestion" runat="server" AppendDataBoundItems="true"
                    DropDownAutoWidth="Enabled" Width="350px">
                    <Items>
                        <rad:RadComboBoxItem Text="" Value="" />
                        <rad:RadComboBoxItem Text="What was the name of your elementary / primary school?"
                            Value="What was the name of your elementary / primary school?" />
                        <rad:RadComboBoxItem Text="What is the name of the company of your first job?" Value="What is the name of the company of your first job?" />
                        <rad:RadComboBoxItem Text="What was your favorite place to visit as a child?" Value="What was your favorite place to visit as a child?" />
                        <rad:RadComboBoxItem Text="What is your spouse's mother's maiden name?" Value="What is your spouse's mother's maiden name?" />
                        <rad:RadComboBoxItem Text="What is the country of your ultimate dream vacation?"
                            Value="What is the country of your ultimate dream vacation?" />
                        <rad:RadComboBoxItem Text="What is the name of your favorite childhood teacher?"
                            Value="What is the name of your favorite childhood teacher?" />
                        <rad:RadComboBoxItem Text="To what city did you go on your honeymoon? " Value="To what city did you go on your honeymoon? " />
                        <rad:RadComboBoxItem Text="What time of the day were you born?" Value="What time of the day were you born?" />
                        <rad:RadComboBoxItem Text="What was your dream job as a child? " Value="What was your dream job as a child? " />
                        <rad:RadComboBoxItem Text="What is the street number of the house you grew up in?"
                            Value="What is the street number of the house you grew up in?" />
                        <rad:RadComboBoxItem Text="What is the license plate (registration) of your dad's first car?"
                            Value="What is the license plate (registration) of your dad's first car?" />
                        <rad:RadComboBoxItem Text="Who was your childhood hero? " Value="Who was your childhood hero? " />
                        <rad:RadComboBoxItem Text="What was the first concert you attended?" Value="What was the first concert you attended?" />
                        <rad:RadComboBoxItem Text="What are the last 5 digits of your credit card?" Value="What are the last 5 digits of your credit card?" />
                        <rad:RadComboBoxItem Text="What are the last 5 of your Social Security number?" Value="What are the last 5 of your Social Security number?" />
                        <rad:RadComboBoxItem Text="What is your current car registration number?" Value="What is your current car registration number?" />
                        <rad:RadComboBoxItem Text="What are the last 5 digits of your driver's license number?"
                            Value="What are the last 5 digits of your drivers license number?" />
                        <rad:RadComboBoxItem Text="What month and day is your anniversary? (e.g., January 2)"
                            Value="What month and day is your anniversary? (e.g., January 2)" />
                        <rad:RadComboBoxItem Text="What is your grandmother's first name?" Value="What is your grandmother's first name?" />
                        <rad:RadComboBoxItem Text="What is your mother's middle name? " Value="What is your mother's middle name? " />
                        <rad:RadComboBoxItem Text="What is the last name of your favorite high school teacher?"
                            Value="What is the last name of your favorite high school teacher?" />
                        <rad:RadComboBoxItem Text="What was the make and model of your first car?" Value="What was the make and model of your first car?" />
                        <rad:RadComboBoxItem Text="Where did you vacation last year?" Value="Where did you vacation last year?" />
                        <rad:RadComboBoxItem Text="What is the name of your grandmother's dog? " Value="What is the name of your grandmother's dog? " />
                        <rad:RadComboBoxItem Text="What is the name, breed, and color of current pet?" Value="What is the name, breed, and color of current pet?" />
                        <rad:RadComboBoxItem Text="What is your preferred musical genre? " Value="What is your preferred musical genre? " />
                        <rad:RadComboBoxItem Text="In what city and country do you want to retire? " Value="In what city and country do you want to retire? " />
                        <rad:RadComboBoxItem Text="What is the name of the first undergraduate college you attended?"
                            Value="What is the name of the first undergraduate college you attended?" />
                        <rad:RadComboBoxItem Text="What was your high school mascot?" Value="What was your high school mascot?" />
                        <rad:RadComboBoxItem Text="What year did you graduate from High School?" Value="What year did you graduate from High School?" />
                        <rad:RadComboBoxItem Text="What is the name of the first school you attended?" Value="What is the name of the first school you attended?" />
                    </Items>
                </rad:RadComboBox>
                <asp:RequiredFieldValidator ID="reqPasswordQuestion" runat="server" ControlToValidate="cboPasswordQuestion"
                    ErrorMessage="Please choose"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Password Answer:
            </td>
            <td>
                <asp:TextBox ID="txtAnswer" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqPasswordAnswer" runat="server" ControlToValidate="txtAnswer"
                    ErrorMessage="Please choose"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                <asp:Button ID="btnContinue" runat="server" Text="Continue" Visible="false" OnClick="btnContinue_Click" />
            </td>
        </tr>
        <br />
        <div style="color: Green">
            <asp:Label ID="lblStatus" runat="server"></asp:Label>
        </div>
    </table>
</asp:Content>

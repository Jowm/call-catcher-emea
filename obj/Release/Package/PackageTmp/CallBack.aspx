﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="CallBack.aspx.cs" Inherits="CallDispositionTool.CallBack" %>

<%@ Register Src="UserControl/CallBackManagerUserControl.ascx" TagName="CallBackManagerUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/CallBackAssignedUserControl.ascx" TagName="CallBackAssignedUserControl"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <rad:RadAjaxPanel ID="pnlTimer" runat="server">
        <rad:RadNotification ID="RadNotification1" runat="server" Animation="Slide" Width="330"
            Height="130" EnableRoundedCorners="true" EnableShadow="true" Title="Call Back Notification"
            ShowCloseButton="true" ShowTitleMenu="true" Text="You have new call back. Click here to view details"
            Style="z-index: 35000">
            <NotificationMenu>
                <Items>
                    <rad:RadMenuItem Text="Mark this Call ">
                    </rad:RadMenuItem>
                </Items>
            </NotificationMenu>
        </rad:RadNotification>
    </rad:RadAjaxPanel>
    <rad:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }
        </script>
    </rad:RadScriptBlock>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManager ID="ajaxManager1" runat="server">
        <ClientEvents />
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="TabStrip1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="TabStrip1" />
                    <rad:AjaxUpdatedControl ControlID="MultiPage1" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <div style="margin: 0 auto 0 auto; width: 100%;">
        <h2>
            Call Back</h2>
        <br />
        <br />
        <br />
    </div>
    <rad:RadTabStrip ID="TabStrip1" runat="server" SelectedIndex="0" ShowBaseLine="True"
        AutoPostBack="True" MultiPageID="MultiPage1" CausesValidation="False" 
        Width="867px" ontabclick="TabStrip1_TabClick">
        <Tabs>
            <rad:RadTab Text="My Call Back" ToolTip="Shows your created call backs">
            </rad:RadTab>
            <rad:RadTab Text="Assigned Call Back" ToolTip="Shows the assigned from others call backs">
            </rad:RadTab>
        </Tabs>
    </rad:RadTabStrip>
    <div style="border: 1pt solid gray; border-top-style: none; width: 867px">
        <rad:RadMultiPage ID="MultiPage1" runat="server" SelectedIndex="0" RenderSelectedPageOnly="true"
            Width="867px">
            <rad:RadPageView ID="PageViewMyOwnCallBack" runat="server" Selected="true">
                <uc1:CallBackManagerUserControl ID="CallBackManagerUserControl1" runat="server" />
            </rad:RadPageView>
            <rad:RadPageView ID="PageViewAssignedCallBack" runat="server">
                <uc2:CallBackAssignedUserControl ID="CallBackAssignedUserControl1" runat="server" />
            </rad:RadPageView>
        </rad:RadMultiPage>
    </div>
</asp:Content>

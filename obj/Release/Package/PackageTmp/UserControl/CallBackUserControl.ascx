﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CallBackUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.CallBackUserControl" %>
<div>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManagerProxy ID="ajaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="radAssignedTo">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlCallBack" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <asp:Panel ID="pnlCallBack" runat="server">
        <fieldset style="width: 400px">
            <legend><strong>Call Back Details </strong></legend>
            <table style="width: 100%">
                <tr>
                    <td style="width: 150px">
                        <strong>
                            <label for="dtpCallBackDate">
                                Call Back Date/Time:</label></strong>
                    </td>
                    <td>
                        <rad:RadDateTimePicker ID="dtpCallBackDate" runat="server" Width="200px">
                        </rad:RadDateTimePicker>
                        <asp:RequiredFieldValidator ID="req1" runat="server" ControlToValidate="dtpCallBackDate"
                            Display="Static" Text="*" ErrorMessage="Required field for Call Back Date/Time"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <label for="cboTimeZone">
                                Time Zone:</label></strong>
                    </td>
                    <td>
                        <rad:RadComboBox ID="cboTimeZone" runat="server" AppendDataBoundItems="true" Width="130px"
                            DataSourceID="dsTimezone" DataTextField="Abbreviation" DataValueField="TimezoneID"
                            EmptyMessage="Select Timezone">
                            <Items>
                                <rad:RadComboBoxItem Text="" Value="" />
                            </Items>
                        </rad:RadComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cboTimeZone"
                            Text="*" ErrorMessage="Required field for Timezone" Display="Static"></asp:RequiredFieldValidator>
                        <asp:EntityDataSource ID="dsTimezone" runat="server" ConnectionString="name=CallDispositionEntities"
                            DefaultContainerName="CallDispositionEntities" EnableFlattening="False" EntitySetName="Timezones"
                            Select="it.[TimezoneID], it.[Abbreviation]">
                        </asp:EntityDataSource>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <label for="txtContactNumber">
                                Contact Number:</label></strong>
                    </td>
                    <td>
                        <rad:RadTextBox ID="txtContactNumber" runat="server">
                        </rad:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <label for="cboCallBackReason">
                                Call Back Reason:</label></strong>
                    </td>
                    <td>
                        <rad:RadComboBox ID="cboCallBackReason" runat="server" DataSourceID="dsCallBackReason"
                            DataTextField="Description" DataValueField="CallBackReasonID" AppendDataBoundItems="true"
                            EmptyMessage="Select Call Back Reason">
                            <Items>
                                <rad:RadComboBoxItem Text="" Value="" />
                            </Items>
                        </rad:RadComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cboCallBackReason"
                            ErrorMessage="Required field for Call Back Reason" Display="Static" Text="*"></asp:RequiredFieldValidator>
                        <asp:EntityDataSource ID="dsCallBackReason" runat="server" ConnectionString="name=CallDispositionEntities"
                            DefaultContainerName="CallDispositionEntities" EnableFlattening="False" EntitySetName="CallBackReasons"
                            Select="it.[CallBackReasonID], it.[Description]">
                        </asp:EntityDataSource>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <label for="cboAssignedTo">
                                Assigned To:</label></strong>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="radAssignedTo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="radAssignedTo_SelectedIndexChanged"
                            AppendDataBoundItems="true">
                            <asp:ListItem Text="Self" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Others" Value="1"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="pnlAssignment" runat="server" Visible="false">
                <table>
                    <tr>
                        <td style="width: 150px">
                            <strong>
                                <label for="cboAssignedTo">
                                    Specify Assigned To:</label></strong>
                        </td>
                        <td>
                            <rad:RadComboBox ID="cboAssignedTo" runat="server" AppendDataBoundItems="True" EmptyMessage="Specify Person to assign"
                                Height="300px">
                                <Items>
                                    <rad:RadComboBoxItem Text="" Value="" />
                                </Items>
                            </rad:RadComboBox>
                            <asp:RequiredFieldValidator ID="reqAssignedTo" runat="server" ControlToValidate="cboAssignedTo"
                                ErrorMessage="Required field for Assigned To" Display="Static" Text="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>
                                <label for="cboAssignedReason">
                                    Reason for Assignment:</label></strong>
                        </td>
                        <td>
                            <rad:RadComboBox ID="cboAssignedReason" runat="server" AppendDataBoundItems="True"
                                EmptyMessage="Select reason for assignment" DataSourceID="dsReasonForAssignment"
                                DataTextField="Description" DataValueField="CallBackAssignmentReasonID">
                                <Items>
                                    <rad:RadComboBoxItem Text="" Value="" />
                                </Items>
                            </rad:RadComboBox>
                            <asp:RequiredFieldValidator ID="reqAssignReason" runat="server" ControlToValidate="cboAssignedReason"
                                ErrorMessage="Required field for assignment reason"></asp:RequiredFieldValidator>
                            <asp:EntityDataSource ID="dsReasonForAssignment" runat="server" ConnectionString="name=CallDispositionEntities"
                                DefaultContainerName="CallDispositionEntities" EnableFlattening="False" EntitySetName="CallBackAssignmentReasons"
                                Select="it.[CallBackAssignmentReasonID], it.[Description]">
                            </asp:EntityDataSource>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <table>
                <tr>
                    <td style="width: 100px">
                        <strong>
                            <label for="txtNotes">
                                <span>Notes</span></label></strong>
                    </td>
                    <td>
                        <rad:RadTextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="4">
                        </rad:RadTextBox>
                    </td>
                </tr>
            </table>
            <p>
            </p>
        </fieldset>
    </asp:Panel>
</div>

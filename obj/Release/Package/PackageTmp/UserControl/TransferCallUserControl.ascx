﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TransferCallUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.TransferCallUserControl" %>
<script type="text/javascript">
    function ConfirmTransferCall()
    {
        if (confirm('Are you sure you want to Transfer the call to CSAT?') == true) {
            var csatNumber = '35765';

            if (csatNumber) {
                TransferCall(csatNumber);
                alert('Call has been transfered.');
            }
        }

        return false;
    }
</script>
<div>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Width="680px" Height="500px" VisibleOnPageLoad="false" VisibleStatusbar="false"
        Modal="true" RenderMode="Lightweight" OnClientClose="OnClientClose" DestroyOnClose="true"
        Animation="Fade">
    </telerik:RadWindowManager>
    <rad:RadAjaxManagerProxy ID="ajaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="btnTransferCall">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnTransferCall" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <asp:ImageButton ID="btnTransferCall" runat="server" ImageUrl="~/images/TransferCall32x32.png"
        AlternateText="Transfer Call to CSAT" ToolTip="Transfer Call to CSAT" OnClick="btnTransferCall_OnClick" OnClientClick="return ConfirmTransferCall()"
        CausesValidation="false" />
</div>

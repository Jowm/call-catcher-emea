﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmoticonRadioButtonList.ascx.cs"
    Inherits="CallDispositionTool.UserControl.EmoticonRadioButtonList" %>
<rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
</rad:RadAjaxLoadingPanel>
<rad:RadAjaxManagerProxy ID="ajaxManagerProxy1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="rdoAngry">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlRadioEmoticon" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rdoSad">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlRadioEmoticon" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rdoNormal">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlRadioEmoticon" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rdoHappy">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlRadioEmoticon" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rdoEcstatic">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlRadioEmoticon" LoadingPanelID="LoadingPanel1" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<asp:Panel ID="pnlRadioEmoticon" runat="server">
    <asp:HiddenField ID="hiddenEmoticonValue" runat="server" />
    <table width="50%">
        <tr>
            <td>
                <img src="~/images/Emoticons/Icon%205.png" alt="Angry" class="Emoticon" height="30"
                    width="30" runat="server" />
            </td>
            <td>
                <img src="~/images/Emoticons/Icon%204.png" alt="Sad" class="Emoticon" height="30"
                    width="30" runat="server" />
            </td>
            <td>
                <img src="~/images/Emoticons/Icon%203.png" alt="Normal" class="Emoticon" height="30"
                    width="30" runat="server" />
            </td>
            <td>
                <img src="~/images/Emoticons/Icon%202.png" alt="Happy" class="Emoticon" height="30"
                    width="30" runat="server" />
            </td>
            <td>
                <img src="~/images/Emoticons/Icon%201.png" alt="Ecstatic" class="Emoticon" height="30"
                    width="30" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:RadioButton ID="rdoAngry" runat="server" GroupName="rdoList" OnCheckedChanged="rdoID_CheckedChanged"
                    AutoPostBack="true" />
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="rdoSad" runat="server" GroupName="rdoList" OnCheckedChanged="rdoID_CheckedChanged"
                    AutoPostBack="true" />
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="rdoNormal" runat="server" GroupName="rdoList" OnCheckedChanged="rdoID_CheckedChanged"
                    AutoPostBack="true" />
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="rdoHappy" runat="server" GroupName="rdoList" OnCheckedChanged="rdoID_CheckedChanged"
                    AutoPostBack="true" />
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="rdoEcstatic" runat="server" GroupName="rdoList" OnCheckedChanged="rdoID_CheckedChanged"
                    AutoPostBack="true" />
            </td>
        </tr>
    </table>
    <asp:Literal runat="server" EnableViewState="False" ID="labelMessage"></asp:Literal>
</asp:Panel>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CallBackDockUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.CallBackDockUserControl" %>
<div class="box_content">
    <div class="box_title">
        <div class="title_icon">
            <img src="images/mini_icon1.gif" alt="" title="" /></div>
        <h2>
            My <span class="dark_blue">Callbacks</span></h2>
    </div>
    <asp:Repeater ID="repCallback" runat="server" OnItemDataBound="repCallBack_ItemDataBound">
        <ItemTemplate>
            <div class="box_text_content">
                <img src="images/calendar.gif" alt="" title="Call Back" class="box_icon" />
                <a href="CallBack.aspx?CallBackID=<%# Eval("CallBackID") %>" class="details" onclick="ShowSpinner();">
                    Call Back ID:
                    <%# Eval("CallBackID")%></a>
                <div class="box_text">
                    <strong>Contact:</strong>
                    <%# Eval("ContactNumber")%>
                    <br />
                    <strong>Reason:</strong>
                    <%# Eval("Description")%>
                </div>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            <br />
            <asp:Label ID="lblErrorMsg" CssClass="details" runat="server" Text="No items to show"
                Visible="false"></asp:Label>
        </FooterTemplate>
    </asp:Repeater>
</div>
<div class="clear">
</div>
<input type="hidden" id="hdClient" runat="server" />
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using System.Data.Objects;
using System.Web.Security;

namespace CallDispositionTool.DNC
{
    public partial class DNC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindClientDropdown();
            }
        }

        void BindClientDropdown() {
            cboClient.Items.Clear();
            cboClient.Items.Add(new Telerik.Web.UI.RadComboBoxItem("", ""));
            
            foreach (var item in GetClients())
            {
                cboClient.Items.Add(new Telerik.Web.UI.RadComboBoxItem(item.ClientName, item.ClientID.ToString()));                
            }
        }

        dynamic GetClients()
        {
            using (DNCEntities db = new DNCEntities())
            {
                return db.GetClients().ToList<Client>();
            }
        }

        void SaveSingleDNCRequest(Int64 PhoneNumber, Int32 ClientID, Int32 CIMNumber, Int32 SourceID)
        {
            using (DNCEntities db = new DNCEntities())
            {
                db.SaveSingleDNCRequest(PhoneNumber, ClientID, CIMNumber, SourceID);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                SaveSingleDNCRequest(Convert.ToInt64(txtPhoneNumber.Text), Convert.ToInt32(cboClient.SelectedItem.Value), Convert.ToInt32(Membership.GetUser().UserName), 3);
                lblStatus.Text = "DNC submission has been saved!";
            }
            catch (Exception ex)
            {
                lblStatus.Text = "Error Saving: " + ex.Message;
            }
        }
    }
}
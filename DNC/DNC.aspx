﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="DNC.aspx.cs" Inherits="CallDispositionTool.DNC.DNC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <asp:Panel ID="pnlDNC" runat="server" DefaultButton="btnSubmit">
        <h2>
            DNC Confirmation Generator</h2>
        <br />
        <br />
        <p>
            Please use this application to record requests from customers to have their telephone
            number added to an external client or Transcom Do Not Call list.
            <ul>
                <li>1. Select the appropriate Client name from the drop-down list. </li>
                <li>2. Enter the customer's phone number in the Phone Number field. </li>
                <li>3. Click the Submit button. </li>
                <li>4. When the grid reloads, provide the customer with the Confirmation number that
                    is generated. </li>
                <li>5. Repeat steps 1 through 4 for each Client DNC list to which the customer is requesting
                    their number added. <strong>If the number you are adding is from an Outbound campaign
                        and the customer is asking to be added to both the specific Client DNC list as well
                        as the NuComm DNC list, record the Confirmation number generated from the Client
                        specific request in the designated field in the dialer. If the customer is only
                        requesting to be added to the NuComm DNC list, record that Confirmation Number in
                        the dialer.</strong> </li>
            </ul>
        </p>
        <table>
            <tr>
                <td>
                    Select A Client:
                </td>
                <td>
                    <rad:RadComboBox ID="cboClient" runat="server" DropDownAutoWidth="Enabled" EmptyMessage="Select Client">
                    </rad:RadComboBox>
                    <asp:RequiredFieldValidator ID="reqClient" runat="server" ControlToValidate="cboClient"
                        Display="Dynamic" ErrorMessage="Enter Client"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Phone Number:
                </td>
                <td>
                    <rad:RadMaskedTextBox ID="txtPhoneNumber" runat="server">
                    </rad:RadMaskedTextBox>
                    <asp:RequiredFieldValidator ID="reqPhoneNumber" runat="server" ControlToValidate="txtPhoneNumber"
                        Display="Dynamic" ErrorMessage="Enter Phone Number"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regexPhoneNumber" runat="server" ControlToValidate="txtPhoneNumber"
                        Display="Dynamic" ValidationExpression="(?:\([2-9][0-8]\d\)\ ?|[2-9][0-8]\d[\-\ \.\/]?)[2-9]\d{2}[- \.\/]?\d{4}\b" ErrorMessage="Invalid NANP Telephone Number"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                </td>
            </tr>
        </table>
        <p style="color: Green">
            <asp:Label ID="lblStatus" runat="server"></asp:Label>
        </p>
    </asp:Panel>
</asp:Content>

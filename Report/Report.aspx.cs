﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Web.Configuration;
using Telerik.Web.UI;
using System.Configuration;

namespace CallDispositionTool.Report
{
    public partial class Report : NotificationPage
    {
        int UPS_SmartPad = Convert.ToInt32(ConfigurationManager.AppSettings["SmartPadFormID"]);
        int UK_SmartPad = Convert.ToInt32(ConfigurationManager.AppSettings["UKSmartPadFormID"]);

        private DateTime dateStart
        {
            get
            {
                if (ViewState["dateStart"] != null)
                    return Convert.ToDateTime(ViewState["dateStart"]);
                return Convert.ToDateTime("1/1/1990");
            }
            set
            {
                ViewState["dateStart"] = value;
            }
        }

        private DateTime dateEnd
        {
            get
            {
                if (ViewState["dateEnd"] != null)
                    return Convert.ToDateTime(ViewState["dateEnd"]);
                return Convert.ToDateTime("1/1/1990");
            }
            set
            {
                ViewState["dateEnd"] = value;
            }
        }

        private int formID
        {
            get
            {
                if (ViewState["formID"] != null)
                    return Convert.ToInt32(ViewState["formID"]);
                return 0;
            }
            set
            {
                ViewState["formID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindFormsDropdown();
                dtpStartDate.SelectedDate = DateTime.Now;
                dtpEndDate.SelectedDate = DateTime.Now;
            }
        }

        void BindFormsDropdown()
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var forms = (from f in db.UserForms
                             where f.UserID == this.UserID
                             select new { f.Form.FormName, f.FormID }).Distinct().ToList().OrderBy(f => f.FormName);

                cboForm.Items.Clear();
                cboForm.Items.Add(new Telerik.Web.UI.RadComboBoxItem("", ""));

                foreach (var item in forms)
                {
                    cboForm.Items.Add(new Telerik.Web.UI.RadComboBoxItem(item.FormName, item.FormID.ToString()));
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(cboForm.SelectedValue) == UPS_SmartPad || Convert.ToInt32(cboForm.SelectedValue) == UK_SmartPad)
            {
                dateStart = Convert.ToDateTime(dtpStartDate.SelectedDate);
                dateEnd = Convert.ToDateTime(dtpEndDate.SelectedDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59));
                formID = Convert.ToInt32(cboForm.SelectedValue);
                grdSmartPad.Rebind();
                grdSmartPad.Visible = true;
                grdReport.Visible = false;
            }
            else
            {
                grdReport.Rebind();
                grdSmartPad.Visible = false;
                grdReport.Visible = true;
            }

            RadFilter1.Visible = true;

        }

        protected void grdReport_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                ImageButton editLink = (ImageButton)e.Item.FindControl("EditLink");
                editLink.Attributes["href"] = "javascript:void(0);";
                editLink.Attributes["target"] = "_blank";
                editLink.OnClientClick = String.Format("return ShowEditForm('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["TransactionID"], e.Item.ItemIndex);
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdReport.MasterTableView.SortExpressions.Clear();
                grdReport.MasterTableView.GroupByExpressions.Clear();
                grdReport.MasterTableView.CurrentPageIndex = grdReport.MasterTableView.PageCount - 1;
                grdReport.Rebind();
            }
        }

        bool isGrouping = false;
        public bool ShouldApplySortFilterOrGroup()
        {
            return grdReport.MasterTableView.FilterExpression != "" ||
                (grdReport.MasterTableView.GroupByExpressions.Count > 0 || isGrouping) ||
                grdReport.MasterTableView.SortExpressions.Count > 0;
        }
        //Matt - 08-18-2015 - Custom code for Smart Pad
        protected virtual void grdReport_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            if (e.Column.UniqueName == "Smart Pad")
            {
                e.Column.Visible = false;
            }

        }

        protected void grdSmartPadDetail_DataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            var dataItem = e.DetailTableView.ParentItem;

            if (e.DetailTableView.Name == "grdSmartPadDetail")
            {
                var tranID = dataItem.GetDataKeyValue("TransactionID").ToString();

                try
                {
                    //low level sql datatable 
                    e.DetailTableView.DataSource = getSmartPadDetail(tranID, false);
                }
                catch (Exception ex)
                {
                    ((RadWindowManager)Master.FindControl("RadWindowManager1")).RadAlert(ex.Message, 330, 180, "Error Message", "");
                }


            }
        }

        private DataTable getSmartPadDetail(string tranID, bool isReport)
        {
            var dt = new DataTable();

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CallDisposition"].ConnectionString))
            {
                cn.Open();

                string sql = @"GetTransactionsSmartPadDetail";

                SqlCommand cmd = new SqlCommand(sql, cn);
                cmd.CommandTimeout = 20000;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FormID", formID);
                cmd.Parameters.AddWithValue("@StartDate", dateStart);
                cmd.Parameters.AddWithValue("@EndDate", dateEnd);
                cmd.Parameters.AddWithValue("@EmployeeID", "0");
                cmd.Parameters.AddWithValue("@TransactionID", Convert.ToInt32(tranID));
                cmd.Parameters.AddWithValue("@IsReport", isReport);

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
                return dt;
            }
        }

        protected void grdSmartPad_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (cboForm.SelectedValue != string.Empty)
            {
                if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
                {

                    try
                    {
                        //low level sql datatable 
                        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CallDisposition"].ConnectionString))
                        {
                            cn.Open();

                            string sql = @"GetTransactionsSmartPad";

                            SqlCommand cmd = new SqlCommand(sql, cn);
                            cmd.CommandTimeout = 20000;
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@FormID", formID);
                            cmd.Parameters.AddWithValue("@StartDate", dateStart);
                            cmd.Parameters.AddWithValue("@EndDate", dateEnd);
                            cmd.Parameters.AddWithValue("@EmployeeID", "0");
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            DataTable dt = new DataTable();

                            da.Fill(dt);

                            if (dt.Columns.Count > 0)
                            {
                                grdSmartPad.DataSource = dt;
                            }
                            else
                            {
                                grdSmartPad.DataSource = null;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        ((RadWindowManager)Master.FindControl("RadWindowManager1")).RadAlert(ex.Message, 330, 180, "Error Message", "");
                    }
                }
            }
        }


        protected void grdSmartPad_ItemCommand(object sender, GridCommandEventArgs e)
        {
            var command = e.CommandName;
            if (command == "ExportToExcel" || command == "ExportToCsv")
            {
                var dt = new DataTable();
                dt = getSmartPadDetail("0", true);
                var filename = formID == UPS_SmartPad ? "UPS Smart Pad " : "UK Smart Pad ";

                if (command == "ExportToExcel")
                {
                    ExcelHelper.ToExcel(dt, filename + DateTime.Now + ".xls", Page.Response);
                }
                else
                {
                    CSVHelper.ConvertToCSV(dt, filename + DateTime.Now + ".csv", Page.Response);
                }
            }
        }


        protected void grdReport_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {



            if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
            {
                try
                {
                    //low level sql datatable 
                    using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CallDisposition"].ConnectionString))
                    {
                        cn.Open();
                        string sql;
                        string abc = cboForm.SelectedItem.Value;
                        if (abc == "26" || abc == "21" || abc == "20" || abc == "19" || abc == "18" || abc == "17" || abc == "16" || abc == "15")
                        {
                            sql = @"GetTransactionsByDate_MaximaForms";
                        }
                        else
                        {
                             sql = @"GetTransactionsByDate";
                        }
                        
                        int transCount = 0;
                        int formID = Convert.ToInt32(cboForm.SelectedItem.Value);
                        DateTime endDateOffset = dtpEndDate.SelectedDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59);

                        using (CallDispositionEntities db = new CallDispositionEntities())
                        {
                            transCount = (from t in db.Transactions
                                          where t.FormID == formID && (t.CreatedOn >= dtpStartDate.SelectedDate && t.CreatedOn <= endDateOffset)
                                          select t).Count();
                        }

                        grdReport.VirtualItemCount = transCount;
                        grdReport.EnableLinqExpressions = false;
                        int startRowIndex = (ShouldApplySortFilterOrGroup()) ? 0 : grdReport.CurrentPageIndex * grdReport.PageSize;
                        int maximumRows = (ShouldApplySortFilterOrGroup()) ? transCount : grdReport.PageSize;
                        //int maximumRows = grdReport.PageSize;

                        SqlCommand cmd = new SqlCommand(sql, cn);
                        cmd.CommandTimeout = 20000;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FormID", cboForm.SelectedItem.Value);
                        cmd.Parameters.AddWithValue("@StartDate", dtpStartDate.SelectedDate);
                        cmd.Parameters.AddWithValue("@EndDate", dtpEndDate.SelectedDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59));
                        cmd.Parameters.AddWithValue("@StartRowIndex", startRowIndex);
                        cmd.Parameters.AddWithValue("@MaximumRows", maximumRows);
                        cmd.Parameters.AddWithValue("@IncludeCallData", Convert.ToBoolean(rblIncludeCallData.SelectedItem.Value));
                        cmd.Parameters.AddWithValue("@IncludeContact", Convert.ToBoolean(rblIncludeContactDetails.SelectedItem.Value));
                        cmd.Parameters.AddWithValue("@IncludeSignOffDetail", Convert.ToBoolean(rblIncludeSignOffDetails.SelectedItem.Value));
                        cmd.Parameters.AddWithValue("@FormFieldsAlphabetical", Convert.ToBoolean(rblFormFieldsAlphabetical.SelectedItem.Value));

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();

                        da.Fill(dt);

                        grdReport.AllowCustomPaging = !ShouldApplySortFilterOrGroup();

                        if (dt.Columns.Count > 0)
                        {
                            grdReport.DataSource = dt;
                        }
                        else
                        {
                            grdReport.DataSource = null;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ((RadWindowManager)Master.FindControl("RadWindowManager1")).RadAlert(ex.Message, 330, 180, "Error Message", "");
                }
            }

        }

        //        class TransactionItem
        //        {
        //            public decimal TransactionID { get; set; }
        //            public string EmployeeID { get; set; }
        //            public DateTime? CreatedOn { get; set; }
        //            //public System.Data.Objects.DataClasses.EntityCollection<Model.AttributeValue> AttributeValues { get; set; }
        //            public IEnumerable<AttributeDataItem> AttributeDataItems { get; set; }
        //        }

        //        class AttributeDataItem
        //        {
        //            public string AttributeName { get; set; }
        //            public string AttributeValue { get; set; }
        //        }

        //        protected void TransformerClassGenerationEventHandler(object sender, ClassGenerationEventArgs e)
        //        {
        //            string s = @"
        //                <div style=""font-family:Courier New; font-size:10pt;"">
        //                <br/>
        //                Base Path: " + e.BasePath + @"
        //                <br/>
        //                CodeFile: " + e.CodeFile;

        //            if (e.HasError)
        //                s += "<br>Failed To create Pivot class";

        //            s += "</div>";

        //            ((RadWindowManager)Master.FindControl("RadWindowManager1")).RadAlert(s.Replace("'", ""), 430, 280, "Error Message", "");
        //        }

        //public static Dictionary<TKey1, Dictionary<TKey2, TValue>> Pivot3<TSource, TKey1, TKey2, TValue>(this IEnumerable<TSource> source, Func<TSource, TKey1> key1Selector, Func<TSource, TKey2> key2Selector, Func<IEnumerable<TSource>, TValue> aggregate)
        //{
        //    return source.GroupBy(key1Selector).Select(
        //        x => new
        //        {
        //            X = x.Key,
        //            Y = source.GroupBy(key2Selector).Select(
        //                z => new
        //                {
        //                    Z = z.Key,
        //                    V = aggregate(from item in source
        //                                  where key1Selector(item).Equals(x.Key)
        //                                  && key2Selector(item).Equals(z.Key)
        //                                  select item
        //                    )

        //                }
        //            ).ToDictionary(e => e.Z, o => o.V)
        //        }
        //    ).ToDictionary(e => e.X, o => o.Y);
        //}

        //protected void grdReport_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        //{
        //    if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
        //    {
        //        try
        //        {
        //            using (CallDispositionEntities db = new CallDispositionEntities())
        //            {
        //                int transCount = 0;
        //                int formID = Convert.ToInt32(cboForm.SelectedItem.Value);
        //                DateTime endDateOffset = dtpEndDate.SelectedDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59);

        //                transCount = (from t in db.Transactions
        //                              where t.FormID == formID && (t.CreatedOn >= dtpStartDate.SelectedDate && t.CreatedOn <= endDateOffset)
        //                              select t).Count();

        //                grdReport.VirtualItemCount = transCount;

        //                int startRowIndex = (ShouldApplySortFilterOrGroup()) ? 0 : grdReport.CurrentPageIndex * grdReport.PageSize;
        //                int maximumRows = (ShouldApplySortFilterOrGroup()) ? transCount : grdReport.PageSize;

        //                var columns = (from c in db.Attributes
        //                               where c.FormID == formID
        //                               select c).ToList();

        //                var pivotTable = (from t in db.Transactions
        //                                  where t.FormID == formID && (t.CreatedOn >= dtpStartDate.SelectedDate && t.CreatedOn <= endDateOffset)
        //                                  select new Transactio

        //                var attributeValues = (from av in db.AttributeValues
        //                                       where av.Attribute.FormID == formID
        //                                       select new {
        //                                           av.AttributeValueID,
        //                                           av.AttributeID,
        //                                           av.Value
        //                                       }).ToList();

        //                var rawData = (from t in db.Transactions
        //                               join u in db.Users on t.CreatedBy equals u.UserID
        //                               where t.FormID == formID && (t.CreatedOn >= dtpStartDate.SelectedDate && t.CreatedOn <= endDateOffset)
        //                               orderby t.TransactionID descending
        //                               select new
        //                               {
        //                                   t.TransactionID,
        //                                   u.EmployeeID,
        //                                   t.CreatedOn
        //                               }).ToList();

        //                var result = rawData.GroupBy(t => t.TransactionID).Select(g => new
        //                {
        //                    TransactionID = g.Key,
        //                    AttributeName = columns.GroupJoin(g, 
        //                }).ToList();


        //                //var report = (from t in db.Transactions
        //                //              where t.FormID == formID && (t.CreatedOn >= dtpStartDate.SelectedDate && t.CreatedOn <= endDateOffset)
        //                //              orderby t.TransactionID descending
        //                //              let AttributeDataItems = (from av in db.AttributeValues
        //                //                                        join a in db.Attributes on av.AttributeID equals a.AttributeID
        //                //                                        where av.TransactionID == t.TransactionID
        //                //                                        select new AttributeDataItem
        //                //                                        {
        //                //                                            AttributeName = a.AttributeName,
        //                //                                            AttributeValue = av.Value
        //                //                                        })

        //                //              select new TransactionItem
        //                //              {
        //                //                  TransactionID = t.TransactionID,
        //                //                  EmployeeID = t.User.EmployeeID,
        //                //                  CreatedOn = t.CreatedOn,
        //                //                  AttributeDataItems = AttributeDataItems
        //                //              }).Skip(startRowIndex).Take(maximumRows).ToList();


        //                if (report != null)
        //                {
        //                    var reporPivot = report.Pivot(X => X.AttributeDataItems, y => y.AttributeName, z => z.AttributeValue, false).ToArray();
        //                    //Pivot3(report, key => key.TransactionID, 
        //                    grdReport.DataSource = reporPivot;
        //                }
        //                else 
        //                {
        //                    grdReport.DataSource = null;
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ((RadWindowManager)Master.FindControl("RadWindowManager1")).RadAlert(ex.Message.Replace("'", ""), 430, 280, "Error Message", "");
        //        }
        //    }
        //}

    }
}
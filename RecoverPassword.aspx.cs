﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Web.Security;
using CallDispositionTool.Model;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace CallDispositionTool
{
    public partial class RecoverPassword : System.Web.UI.Page
    {
        #region _PasswordChangeNotificationBody
        const string _PasswordChangeNotificationBody = @"
                <html>
                    <body>
                    <h2>Your Password Has Been Reset!</h2>
                    <p>
                    This email confirms that your password has been reset.
                    </p>
                    <p>
                    To log on to the Transcom Call Disposition Tool, use the following credentials:
                    </p>
                    <table>
                    <tr>
                    <td>
                    <b>Username:</b>
                    </td>
                    <td>
                    {0}
                    </td>
                    </tr>
                    <tr>
                    <td>
                    <b>Password:</b>
                    </td>
                    <td>
                    {1}
                    </td>
                    </tr>
                    </table>
                    <p>
                    If you have any questions or encounter any problems logging in,
                    please contact a site administrator.
                    </p>
                    </body>
                </html>
            ";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
        }

        //protected void PasswordRecovery1_SendingMail(object sender, MailMessageEventArgs e)
        //{
        //    MailAddress mTo = new MailAddress(e.Message.To.ToString());
        //    MailAddress mFrom = new MailAddress(e.Message.From.ToString());
        //    MailMessage m = new MailMessage(mFrom, mTo);

        //    m.Subject = e.Message.Subject;
        //    m.Body = e.Message.Body;

        //    SmtpClient client = new SmtpClient();
        //    //client.EnableSsl = true;
        //    client.Send(m);

        //    e.Cancel = true;
        //}

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            MembershipUser user = Membership.GetUser(txtUserName.Text);

            if (!RadCaptcha1.IsValid)
            {
                lblStatus.Text = "Invalid Code!";
            }
            else if (user == null)
            {
                lblStatus.Text = "Invalid Username!";
            }
            else if (user.IsLockedOut)
                lblStatus.Text = "Invalid Username!";
            else
            {
                txtQuestion.Text = user.PasswordQuestion;

                if (user.PasswordQuestion == "NULL")
                {
                    txtQuestion.Text = string.Empty;
                }
                else
                {
                    txtQuestion.Text = user.PasswordQuestion;
                }

                MultiView1.SetActiveView(viewStep2);
            }
        }

        protected void btnSubmit2_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    using (CallDispositionEntities db = new CallDispositionEntities())
            //    {
            //        var user = (from u in db.Users
            //                    where u.EmployeeID == txtUserName.Text
            //                    select u).FirstOrDefault();

            //        if (user != null)
            //        {
            //            if (txtAnswer.Text.ToLower() == user.PasswordRecoveryAnswer1.ToLower())
            //            {
            //                txtNewPassword.Text = Membership.GeneratePassword(7, 1);
            //                user.PasswordHash = CallDispositionTool.Security.PasswordHash.CreateHash(txtNewPassword.Text);
            //                //force user on login to change their password
            //                user.LastPasswordChangeDate = DateTime.Now;
            //                CacheFactory.GetCacheManager().Remove("GetUser_Username~" + txtUserName.Text);
            //                CacheFactory.GetCacheManager().Remove("GetUser_ProviderUserKey~" + user.UserID.ToString());

            //                SendPasswordChangeNotification(user.EmailAddress);

            //                UserPasswordHistory record = new UserPasswordHistory();

            //                record.UserID = Convert.ToInt32(Membership.GetUser(txtUserName.Text).ProviderUserKey);
            //                record.PasswordHash = Security.PasswordHash.CreateHash(txtNewPassword.Text);
            //                record.CreatedOn = DateTime.Now;
            //                db.UserPasswordHistories.AddObject(record);
            //                db.SaveChanges();

            //                MultiView1.SetActiveView(viewStep3);
            //            }
            //            else
            //            {
            //                lblStatus2.Text = "Wrong Password Answer Challenge!";
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Helper.LogError(ex);
            //    lblStatus2.Text = "Error: " + ex.Message;
            //}

            try
            {
                MembershipUser user = Membership.GetUser(txtUserName.Text);

                if (user != null)
                {
                    string newPassword = null;

                    newPassword = user.ResetPassword(txtAnswer.Text);

                    if (newPassword != null)
                    {
                        txtNewPassword.Text = newPassword;

                        try
                        {
                            SendPasswordChangeNotification(user.Email);
                        }
                        catch (Exception ex)
                        {
                            Helper.LogError(ex);
                        }

                        MultiView1.SetActiveView(viewStep3);
                    }
                    else
                    {
                        lblStatus2.Text = "Wrong Password Answer Challenge!";
                    }
                }
                else
                {
                    lblStatus2.Text = "Invalid User!";
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                lblStatus2.Text = "Error: " + ex.Message;
            }
        }

        void SendPasswordChangeNotification(string UserEmailAddress)
        {
            MailAddress mTo = new MailAddress(UserEmailAddress);
            MailAddress mFrom = new MailAddress("noreply@transcom.com");
            MailMessage m = new MailMessage(mFrom, mTo);

            m.Subject = "Transcom Call Disposition Tool - Password Reset Notification";
            m.Body = String.Format(_PasswordChangeNotificationBody, txtUserName.Text, txtNewPassword.Text);
            m.IsBodyHtml = true;

            SmtpClient client = new SmtpClient();
            //client.EnableSsl = true;
            client.Send(m);
        }

        protected void btnFinish_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("Login.aspx");
        }
    }
}
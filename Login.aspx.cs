﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;

namespace CallDispositionTool
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated && !string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                // This is an unauthorized, authenticated request...
                Response.Redirect("~/Unauthorized.aspx");

            if (!Page.IsPostBack)
            {
                txtUsername.Text = string.Empty;
                txtPassword.Text = string.Empty;
                txtUsername.Focus();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            MembershipUser user = Membership.GetUser(txtUsername.Text);

            try
            {
                //Check if user exist
                if (user != null)
                {
                    if (Membership.ValidateUser(txtUsername.Text.Trim(), txtPassword.Text.Trim()))
                    {
                        if ((DateTime.Now - user.LastPasswordChangedDate).TotalDays >= 90)
                        {
                            // Inform user password expired + redirect user to change password screen
                            Response.Redirect("~/ChangePassword.aspx");
                        }

                        user.LastLoginDate = DateTime.Now;
                        Membership.UpdateUser(user);

                        FormsAuthentication.SetAuthCookie(txtUsername.Text, true);
                        FormsAuthentication.Authenticate(txtUsername.Text.Trim(), txtPassword.Text.Trim());
                        FormsAuthentication.RedirectFromLoginPage(txtUsername.Text.Trim(), true);
                    }
                    else
                    {
                        lblErrorMessage.Text = "Invalid Password.";
                    }
                }
                else
                {
                    lblErrorMessage.Text = "Invalid Username or Password!";
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex, "Error Login Submit");
                //((RadWindowManager)Master.FindControl("RadWindowManager1")).RadAlert(String.Format("{0}\nInner Exception: {1}", ex.Message.Replace("'", ""), ex.InnerException.Message.Replace("'", "")), 330, 180, "Error", "");
            }
        }

    }
}
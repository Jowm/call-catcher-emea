﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.Security;
using CallDispositionTool.Model;
using System.Web.Configuration;

namespace CallDispositionTool
{
    public class NotificationPage : Page
    {
        public int UserID
        {
            get
            {
                return (int)Membership.GetUser().ProviderUserKey;
            }
        }

        public DateTime ClientTime
        {
            get
            {
                int minutesOffSet = 0;

                HttpCookie cookieLT = HttpContext.Current.Request.Cookies["localtime"];

                if (cookieLT != null)
                    minutesOffSet = Convert.ToInt32(cookieLT.Value);

                TimeZone localZone = TimeZone.CurrentTimeZone;
                DateTime ClientTime = localZone.ToUniversalTime(DateTime.Now).AddMinutes(minutesOffSet);

                return ClientTime;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            DateTime ct = this.ClientTime;

            ((Label)Master.FindControl("lblServerTime")).Text = DateTime.Now.ToString() + " - " + TimeZone.CurrentTimeZone.StandardName;
            ((Label)Master.FindControl("lblClientTIme")).Text = ct.ToString();

            try
            {
                int callbackCount = Helper.GetOutstandingCallBack(UserID);

                if (callbackCount > 0)
                {
                    ((Master.FindControl("LoginViewCallBackMenu") as LoginView).FindControl("bubble") as Label).Text = callbackCount.ToString();
                }
                else
                {
                    ((Master.FindControl("LoginViewCallBackMenu") as LoginView).FindControl("bubble") as Label).Visible = false;
                }
            }
            catch (Exception)
            {
            }

            //Set pending available callback prior to latest time
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var firstCallBack = (from p in db.CallBacks
                                     where (p.CreatedBy == this.UserID || p.AssignedTo == this.UserID) && (p.CallBackStatu.CallBackStatus == "Outstanding" || !p.CallBackStatusID.HasValue) && p.CallBackDate >= ct
                                     orderby p.CallBackDate ascending
                                     select new { p.CallBackID, p.CallBackDate, p.ContactNumber, p.Timezone.UtcOffset }).FirstOrDefault();

                if (firstCallBack != null)
                {
                    if (firstCallBack.CallBackDate > ct)
                    {
                        TimeSpan span = firstCallBack.CallBackDate.AddHours(firstCallBack.UtcOffset) - ct;

                        (Master.FindControl("RadNotification1") as RadNotification).Text = String.Format("You have pending call back.<br><strong>Contact Number:</strong>{2}. <br><strong>Call Back Date/Time:</strong> {1}<br>Click <a href='CallBack.aspx?CallBack={0}'>here</a> to view details",
                            firstCallBack.CallBackID,
                            firstCallBack.CallBackDate.ToString(),
                            firstCallBack.ContactNumber);

                        //Run the countdown and show the Notification
                        ClientScript.RegisterStartupScript(this.GetType(), "notify",
                            String.Format("javascript:CountDownNotification({0})",
                            Convert.ToInt32(span.TotalSeconds).ToString()), true);
                    }
                }
            }

            base.OnLoad(e);
        }
    }
}
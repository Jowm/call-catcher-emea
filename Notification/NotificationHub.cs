﻿using System;
using System.Web;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR.Infrastructure;
using System.Threading;
using System.Threading.Tasks;

[HubName("notificationHub")]
public class NotificationHub : Microsoft.AspNet.SignalR.Hub
{
    private static int connectionCount = 0;

    public void SendMessage(string message)
    {
        this.Clients.All.addMessage(message);
    }

    public void Send(string name, string message)
    {
        // Call the broadcastMessage method to update clients.
        this.Clients.All.broadcastMessage(name, message);
    }

    public void SetCallBackBubbleValue(string employeeid, string value)
    {
        this.Clients.All.broadcastMessage(employeeid, value);
    }

    public override Task OnConnected()
    {
        Interlocked.Increment(ref connectionCount);
        return base.OnConnected();
    }

    public override Task OnReconnected()
    {
        Interlocked.Increment(ref connectionCount);
        return base.OnReconnected();
    }

    public override Task OnDisconnected()
    {
        Interlocked.Decrement(ref connectionCount);
        return base.OnDisconnected();
    }
}

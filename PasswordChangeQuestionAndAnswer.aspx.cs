﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using CallDispositionTool.Model;

namespace CallDispositionTool
{
    public partial class PasswordChangeQuestionAndAnswer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtUsername.Text = Membership.GetUser().UserName;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    var user = (from u in db.Users
                                where u.EmployeeID == txtUsername.Text
                                select u).FirstOrDefault();

                    if (user != null)
                    {
                        user.PasswordRecoveryQuestion1 = cboPasswordQuestion.SelectedItem.Text;
                        user.PasswordRecoveryAnswer1 = txtAnswer.Text;

                        db.SaveChanges();

                        btnSubmit.Enabled = false;
                        txtAnswer.Enabled = false;
                        btnContinue.Visible = true;
                        cboPasswordQuestion.Enabled = false;

                        lblStatus.Text = "Password Question/Answer has been successfully changed! Click Continue to Log-out and to take effect the changes.";
                    }
                }
            }
            catch (Exception ex)
            {
                lblStatus.Text = "Error: " + ex.Message;
            }
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("Login.aspx");
        }
    }
}
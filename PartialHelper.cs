﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

/// <summary>
/// Helper class for partial caching
/// </summary>
public static class PartialHelper
{
    #region "Variables"

    /// <summary>
    /// Number of the variations in level 1 of the loading process
    /// </summary>
    private const int LEVEL1_VARIATIONS = 100;


    /// <summary>
    /// Number of the variations in level 2 of the loading process
    /// </summary>
    private const int LEVEL2_VARIATIONS = 100;


    /// <summary>
    /// Maximum number of the cached controls
    /// </summary>
    public const int MAX_CACHED_CONTROLS = LEVEL1_VARIATIONS * LEVEL2_VARIATIONS + 2 * LEVEL2_VARIATIONS;


    /// <summary>
    /// Current maximum iteration
    /// </summary>
    private static int mMaxIteration = 0;


    /// <summary>
    /// Table assigning the specific control guid the loading iteration
    /// </summary>
    private static Hashtable mControlIterations = new Hashtable();

    #endregion


    #region "Methods"

    /// <summary>
    /// Loads the cached control
    /// </summary>
    /// <param name="page">Page</param>
    /// <param name="type">Control type</param>
    /// <param name="parameters">Control parameters</param>
    /// <param name="guid">Guid to use for caching</param>
    public static Control LoadCachedControl(Page page, Type type, object[] parameters, string guid)
    {
        // Get the existing registered iteration
        object iterObj = mControlIterations[guid];
        int iteration = -1;
        if (iterObj != null)
        {
            iteration = (int)iterObj;
        }
        if (iteration < 0)
        {
            // Get the new iteration
            iteration = mMaxIteration++;
            if (iteration >= MAX_CACHED_CONTROLS)
            {
                throw new Exception("[PartialHelper.LoadCachedControl]: The maximal number of the partially cached controls currenly supported by the application is " + MAX_CACHED_CONTROLS + ".");
            }
            mControlIterations[guid] = iteration;
        }

        return LoadCachedControl1(page, type, parameters, iteration);
    }


    /// <summary>
    /// Loads the cached control
    /// </summary>
    /// <param name="page">Page</param>
    /// <param name="type">Control type</param>
    /// <param name="parameters">Control parameters</param>
    /// <param name="i">Iteration ID</param>
    private static Control LoadCachedControl1(Page page, Type type, object[] parameters, int i)
    {
        int step = LEVEL2_VARIATIONS;
        int largeStep = step * 10;

        // Following code must be kept in order to avoid the .NET bug where the dynamically loaded cached control has ID based on the stack trace
        if (i < largeStep)
        {
            if (i < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
        }
        else
        {
            i -= largeStep;
        }

        if (i < largeStep)
        {
            if (i < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
        }
        else
        {
            i -= largeStep;
        }

        if (i < largeStep)
        {
            if (i < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
        }
        else
        {
            i -= largeStep;
        }

        if (i < largeStep)
        {
            if (i < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
        }
        else
        {
            i -= largeStep;
        }

        if (i < largeStep)
        {
            if (i < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
        }
        else
        {
            i -= largeStep;
        }

        if (i < largeStep)
        {
            if (i < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
        }
        else
        {
            i -= largeStep;
        }

        if (i < largeStep)
        {
            if (i < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
        }
        else
        {
            i -= largeStep;
        }

        if (i < largeStep)
        {
            if (i < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
        }
        else
        {
            i -= largeStep;
        }

        if (i < largeStep)
        {
            if (i < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
        }
        else
        {
            i -= largeStep;
        }

        if (i < largeStep)
        {
            if (i < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
            if ((i -= step) < step) { return LoadCachedControl2(page, type, parameters, i); }
        }
        else
        {
            i -= largeStep;
        }

        // Currently supported: 100 x 100 unique cached controls by combination of level 1 -> level 2

        return LoadCachedControl2(page, type, parameters, i);
    }


    /// <summary>
    /// Loads the cached control
    /// </summary>
    /// <param name="page">Page</param>
    /// <param name="type">Control type</param>
    /// <param name="parameters">Control parameters</param>
    /// <param name="i">Iteration ID</param>
    private static Control LoadCachedControl2(Page page, Type type, object[] parameters, int i)
    {
        // Following code must be kept in order to avoid the .NET bug where the dynamically loaded cached control has ID based on the stack trace
        if (i < 10)
        {
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
        }
        else
        {
            i -= 10;
        }

        if (i < 10)
        {
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
        }
        else
        {
            i -= 10;
        }

        if (i < 10)
        {
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
        }
        else
        {
            i -= 10;
        }

        if (i < 10)
        {
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
        }
        else
        {
            i -= 10;
        }

        if (i < 10)
        {
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
        }
        else
        {
            i -= 10;
        }

        if (i < 10)
        {
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
        }
        else
        {
            i -= 10;
        }

        if (i < 10)
        {
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
        }
        else
        {
            i -= 10;
        }

        if (i < 10)
        {
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
        }
        else
        {
            i -= 10;
        }

        if (i < 10)
        {
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
        }
        else
        {
            i -= 10;
        }

        if (i < 10)
        {
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
            if (i-- <= 0) { return page.LoadControl(type, parameters); }
        }
        else
        {
            i -= 10;
        }

        // Currently supported: 100 unique cached controls by level 2

        return LoadCachedControl2(page, type, parameters, i);
    }

    #endregion
}

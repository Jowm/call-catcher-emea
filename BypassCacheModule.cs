﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class BypassCacheModule : IHttpModule
{
    public void Dispose()
    {
    }

    public void Init(HttpApplication application)
    {
        application.PostMapRequestHandler += (object sender, EventArgs e) =>
        {
            if (!String.Equals(application.Context.Request.HttpMethod, "GET",
               StringComparison.InvariantCultureIgnoreCase))
            {
                application.Context.Response.Cache.SetNoServerCaching();
            }
        };
    }
}

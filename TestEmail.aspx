﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" 
    CodeBehind="TestEmail.aspx.cs" Inherits="CallDispositionTool.TestEmail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMain" runat="server">
    <div style="padding: 10px 10px 10px 10px; width: 100%">
        <asp:Panel ID="pnlMain" runat="server" DefaultButton="btnSubmit" Width="100%">
            <h2>
                <asp:Label ID="lblFormName" runat="server"></asp:Label>
            </h2>
            <br />
            <br />
            <br />
            <asp:PlaceHolder ID="formPlaceHolder" runat="server"></asp:PlaceHolder>
            <br />
            <hr />
            <div style="float: left">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Clicked" OnClientClick="showNotification();" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="return CloseWithoutRefresh();" />
            </div>
        </asp:Panel>
</asp:Content>

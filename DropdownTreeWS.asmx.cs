﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Telerik.Web.UI;
using CallDispositionTool.Model;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace CallDispositionTool
{
    /// <summary>
    /// Summary description for DropdownTreeWS
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DropdownTreeWS : System.Web.Services.WebService
    {
        [WebMethod]
        public List<DropDownNodeData> LoadNodes(DropDownNodeData node)
        {
            //Create Instance of CacheManager 
            CacheManager _objCacheManager = CacheFactory.GetCacheManager();
            List<DropDownNodeData> nodes = new List<DropDownNodeData>();

            if (_objCacheManager.Contains("LoadNodes~" + node.Value))
            {
                nodes = (List<DropDownNodeData>)_objCacheManager.GetData("LoadNodes~" + node.Value);
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    int? dispoID = Convert.ToInt32(node.Value);

                    var result = (from link in db.Dispositions
                                  let lookup = (from d in db.Dispositions
                                                where d.DispositionID == dispoID
                                                select new { d.FilterDispositionID, d.AttributeID }).FirstOrDefault()
                                  where link.HideFromList == false && link.AttributeID == lookup.AttributeID && link.ParentDispositionID == dispoID && (link.FilterDispositionID.HasValue ? link.FilterDispositionID == lookup.FilterDispositionID : link.FilterDispositionID == null)
                                  let childCount = (from child in db.Dispositions
                                                    where child.HideFromList == false && child.ParentDispositionID == link.DispositionID && child.AttributeID == lookup.AttributeID && (child.FilterDispositionID.HasValue ? child.FilterDispositionID == lookup.FilterDispositionID : child.FilterDispositionID == null)
                                                    select child).Count()
                                  select new
                                  {
                                      Description = link.Description,
                                      ChildCount = childCount,
                                      link.DispositionID
                                  }).ToList();


                    foreach (var item in result)
                    {
                        DropDownNodeData n = new DropDownNodeData();
                        n.Text = item.Description;
                        n.Value = item.DispositionID.ToString();
                        n.ExpandMode = item.ChildCount > 0 ? DropDownTreeNodeExpandMode.WebService : DropDownTreeNodeExpandMode.ClientSide;
                        nodes.Add(n);
                    }

                    _objCacheManager.Add("LoadNodes~" + node.Value, nodes);
                }
            }

            return nodes;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallDispositionTool
{
    public partial class UserAccess : FormPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Helper.IsUserAllowFormAccessGrantor(this.UserID) || Helper.IsUserAdmin(this.UserID))
                {
                    TabStripUser.Tabs[0].Visible = true;
                    PageViewFormAccess.Visible = true;
                }

                if (Helper.IsUserAllowAssignUserRole(this.UserID) || Helper.IsUserAdmin(this.UserID))
                {
                    TabStripUser.Tabs[1].Visible = true;
                    PageViewUserRole.Visible = true;
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            HttpCookie ck = Request.Cookies["UserInfo"];
            string skinID = string.Empty;

            if (ck != null)
            {
                skinID = ck.Values["Skin"];
                this.skinManager1.Skin = skinID;
            }
            else
            {
                ck = new HttpCookie("UserInfo");
                ck.Values["Skin"] = this.skinManager1.Skin;
                Response.Cookies.Remove("UserInfo");
                Response.Cookies.Add(ck);
            }
        }
    }
}
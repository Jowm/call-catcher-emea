﻿using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.UI.WebControls;
using System;
using System.Web;

namespace CallDispositionTool
{
    public partial class Site : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            HttpCookie ck = Request.Cookies["UserInfo"];
            string skinID = string.Empty;

            if (ck != null)
            {
                ck.Expires = DateTime.Now.AddYears(1);
                skinID = ck.Values["Skin"];
                this.skinManager1.Skin = skinID;
                Response.Cookies.Add(ck);
            }
            else
            {
                ck = new HttpCookie("UserInfo");
                ck.Values["Skin"] = this.skinManager1.Skin;
                ck.Expires = DateTime.Now.AddYears(1);
                Response.Cookies.Remove("UserInfo");
                Response.Cookies.Add(ck);
            }            
        }
    }
}

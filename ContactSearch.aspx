﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContactSearch.aspx.cs"
    Inherits="CallDispositionTool.ContactSearch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="Scripts/GAnalytics.js" type="text/javascript"></script>
    <link href="CSS/FormStyle.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <rad:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow)
                    oWindow = window.RadWindow; //Will work in Moz in all cases, including clasic dialog     
                else if (window.frameElement.radWindow)
                    oWindow = window.frameElement.radWindow; //IE (and Moz as well)     
                return oWindow;
            }

            function CloseDialog() {
                GetRadWindow().Close();
            }

            function SelectContact(contactID) {
                var oArg = new Object();
                oArg.ContactID = contactID;
                GetRadWindow().Close(oArg);
            }

            function GetSelectedRowClicked() {
                var firstDataItem = $find("<%=grdContactSearch.ClientID %>").get_masterTableView().get_dataItems()[0];

                if (firstDataItem) {
                    var keyValues = firstDataItem.getDataKeyValue("ContactID");
                    SelectContact(keyValues);
                }
                else
                    alert('No item selected.');
            }

            function RowDblClick(sender, eventArgs) {
                var index = eventArgs.get_itemIndexHierarchical();
                var tableView = eventArgs.get_tableView();
                var dataItems = tableView.get_dataItems();
                var dataItem = dataItems[index];
                var contactID = dataItem.getDataKeyValue("ContactID");
                SelectContact(contactID);
            }
        </script>
    </rad:RadScriptBlock>
    <rad:RadCodeBlock ID="CodeBlock1" runat="server">
        <script type="text/javascript">
            $(function () {
                var grid = $get("<%= grdContactSearch.ClientID %>");
                grid.focus();
            });
        </script>
    </rad:RadCodeBlock>
    <rad:RadSkinManager ID="skinManager1" runat="server" PersistenceMode="Cookie">
    </rad:RadSkinManager>
    <rad:RadFormDecorator ID="formDecorator1" runat="server" EnableRoundedCorners="true"
        RenderMode="Lightweight" ControlsToSkip="H4H5H6" />
    <rad:RadScriptManager ID="ScriptManager1" runat="server">
    </rad:RadScriptManager>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManager ID="ajaxManager1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="grdContactSearch">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdContactSearch" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="radFilterOption">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdContactSearch" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        ShowContentDuringLoad="false" Width="680px" Height="500px" VisibleOnPageLoad="false"
        VisibleStatusbar="false" DestroyOnClose="true" ReloadOnShow="true" Modal="true"
        AutoSize="true" InitialBehaviors="Maximize" RenderMode="Lightweight">
    </telerik:RadWindowManager>
    <div>
        <rad:RadGrid ID="grdContactSearch" runat="server" OnNeedDataSource="grdContactSearch_NeedDataSource"
            AutoGenerateColumns="false" Width="100%">
            <ItemStyle Wrap="true"></ItemStyle>
            <ClientSettings AllowKeyboardNavigation="true">
                <ClientEvents OnRowDblClick="RowDblClick" />
                <Selecting AllowRowSelect="true" />
                <KeyboardNavigationSettings EnableKeyboardShortcuts="false" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" ScrollHeight="220px" />
            </ClientSettings>
            <MasterTableView AutoGenerateColumns="false" TableLayout="Auto" DataKeyNames="ContactID"
                EnableHeaderContextMenu="true" AllowSorting="true" ClientDataKeyNames="ContactID"
                AllowPaging="true" PageSize="50">
                <PagerStyle AlwaysVisible="true" Mode="Advanced" Visible="true" />
                <Columns>
                    <rad:GridBoundColumn UniqueName="Phone" HeaderText="Phone" DataField="Phone" SortExpression="Phone">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="FirstName" HeaderText="First Name" DataField="FirstName"
                        SortExpression="FirstName">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="LastName" HeaderText="Last Name" DataField="LastName"
                        SortExpression="LastName">
                    </rad:GridBoundColumn>
                </Columns>
                <SortExpressions>
                    <rad:GridSortExpression FieldName="Phone" SortOrder="Ascending" />
                </SortExpressions>
            </MasterTableView>
        </rad:RadGrid>
        <p>
        </p>
        <div style="float: left">
            <table>
                <tr>
                    <td>
                        <strong>Search Option:</strong>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="radFilterOption" runat="server" AppendDataBoundItems="true"
                            AutoPostBack="true" RepeatLayout="Table" RepeatDirection="Horizontal" OnSelectedIndexChanged="radFilterOption_SelectedIndexChanged">
                            <asp:ListItem Text="Starts With" Value="0" Selected="true"></asp:ListItem>
                            <asp:ListItem Text="Contains" Value="1"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
        </div>
        <div style="float: right">
            <asp:Button ID="btnSelect" runat="server" Text="Select" OnClientClick="GetSelectedRowClicked();" />
            <asp:Button ID="btnClose" runat="server" Text="Close" OnClientClick="CloseDialog();" />
        </div>
    </div>
    </form>
</body>
</html>

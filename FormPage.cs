﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.Security;
using CallDispositionTool.Model;
using System.Web.Configuration;
namespace CallDispositionTool
{
    public class FormPage : Page
    {
        public int UserID
        {
            get
            {
                return (int)Membership.GetUser().ProviderUserKey;
            }
        }

        public DateTime ClientTime
        {
            get
            {
                int minutesOffSet = 0;

                HttpCookie cookieLT = HttpContext.Current.Request.Cookies["localtime"];

                if (cookieLT != null)
                    minutesOffSet = Convert.ToInt32(cookieLT.Value);

                TimeZone localZone = TimeZone.CurrentTimeZone;
                DateTime ClientTime = localZone.ToUniversalTime(DateTime.Now).AddMinutes(minutesOffSet);

                return ClientTime;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

    }
}
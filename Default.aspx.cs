﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.UI.WebControls;
using System.Linq;
using CallDispositionTool.Model;
using System.Web.Security;
using System.Web.UI;
using System.Collections.Generic;
using Telerik.Web.UI;
using System.Web.Configuration;
using CallDispositionTool.CTI;

namespace CallDispositionTool
{
    public partial class Default : NotificationPage
    {
        private List<DockState> CurrentDockStates
        {
            get
            {
                //Store the info about the added docks in the session. For real life
                // applications we recommend using database or other storage medium
                // for persisting this information.
                List<DockState> _currentDockStates = (List<DockState>)Session["CurrentDockStatesDynamicDocks"];
                if (Object.Equals(_currentDockStates, null))
                {
                    _currentDockStates = new List<DockState>();
                    Session["CurrentDockStatesDynamicDocks"] = _currentDockStates;
                }
                return _currentDockStates;
            }
            set
            {
                Session["CurrentDockStatesDynamicDocks"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private void CreateSaveStateTrigger(RadDock dock)
        {
            //Ensure that the RadDock control will initiate postback
            // when its position changes on the client or any of the commands is clicked.
            //Using the trigger we will "ajaxify" that postback.
            dock.AutoPostBack = true;
            dock.CommandsAutoPostBack = true;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Control widgetCTI = LoadControl("~/UserControl/CTIDockUserControl.ascx");
            Control widgetTracker = LoadControl("~/UserControl/TrackerListUserControl.ascx");
            Control widgetCallBack = LoadControl("~/UserControl/CallBackDockUserControl.ascx");

            radDock2.ContentContainer.Controls.Add(widgetCTI);
            radDock3.ContentContainer.Controls.Add(widgetCallBack);
            radDock1.ContentContainer.Controls.Add(widgetTracker);
        }

        private RadDock CreateRadDockFromState(DockState state)
        {
            RadDock dock = new RadDock();
            dock.DockMode = DockMode.Docked;
            dock.ID = string.Format("RadDock{0}", state.UniqueName);
            dock.ApplyState(state);
            dock.Commands.Add(new DockCloseCommand());
            dock.Commands.Add(new DockExpandCollapseCommand());

            return dock;
        }
    }
}
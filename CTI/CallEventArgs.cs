﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace CallDispositionTool.CTI
{
    [DataContract]
    public class CallEventArgs : EventArgs
    {
        [DataMember]
        public string Caller { get; set; }
        [DataMember]
        public string CallID { get; set; }
        [DataMember]
        public string Dn { get; set; }
        [DataMember]
        public string Dnis { get; set; }
        [DataMember]
        public string Event { get; set; }
        [DataMember]
        public string Split { get; set; }
        [DataMember]
        public string Ucid { get; set; }

        public CallEventArgs(string dn, string evt, string callID, string caller, string dnis)
        {
            this.Dn = dn;
            this.Event = evt;
            this.CallID = callID;
            this.Caller = caller;
            this.Dnis = dnis;
            this.Split = "";
            this.Ucid = "";
        }

        public CallEventArgs(string dn, string evt, string callID, string caller, string dnis, string split)
        {
            this.Dn = dn;
            this.Event = evt;
            this.CallID = callID;
            this.Caller = caller;
            this.Dnis = dnis;
            this.Split = split;
            this.Ucid = "";
        }

        public CallEventArgs(string dn, string evt, string callID, string caller, string dnis, string split, string ucid)
        {
            this.Dn = dn;
            this.Event = evt;
            this.CallID = callID;
            this.Caller = caller;
            this.Dnis = dnis;
            this.Split = split;
            this.Ucid = ucid;
        }
    }
}
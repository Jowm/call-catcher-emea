﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace CallDispositionTool.CTI
{
    [DataContract]
    public class ExtensionStatusEventArgs : EventArgs
    {
        [DataMember]
        public string Dn { get; set; }
        [DataMember]
        public string Event { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string State { get; set; }

        public ExtensionStatusEventArgs(string dn, string state, string evt, string message)
        {
            this.Dn = dn;
            this.State = state;
            this.Event = evt;
            this.Message = message;
        }
    }
}
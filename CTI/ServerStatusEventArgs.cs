﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace CallDispositionTool.CTI
{
    [DataContract]
    public class ServerStatusEventArgs : EventArgs
    {
        [DataMember]
        public string Event { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string State { get; set; }

        public ServerStatusEventArgs(string state, string evt, string message)
        {
            this.State = state;
            this.Event = evt;
            this.Message = message;
        }
    }
}
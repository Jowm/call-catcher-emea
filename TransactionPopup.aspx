﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransactionPopup.aspx.cs"
    Inherits="CallDispositionTool.TransactionPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Google Analytics -->
    <script src="Scripts/GAnalytics.js" type="text/javascript"></script>
    <link href="TransactionPoup.css" rel="stylesheet" type="text/css" />
    <title>Transaction</title>
</head>
<body>
    <form id="form1" runat="server">
    <rad:RadSkinManager ID="skinManager1" runat="server" PersistenceMode="Cookie">
    </rad:RadSkinManager>
    <rad:RadFormDecorator ID="formDecorator1" runat="server" EnableRoundedCorners="true"
        RenderMode="Lightweight" ControlsToSkip="H4H5H6" />
    <div style="margin: 10px 10px 10px 10px; width: 100%;">
        <asp:Table ID="CustomUITable" runat="server">
        </asp:Table>
    </div>
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace CallDispositionTool.UserControl
{
    public partial class CallBackUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dtpCallBackDate.MinDate = DateTime.Now.Subtract(new TimeSpan(24, 0, 0));
        }

        void BindAssignedTo(int FormID)
        {
            //Create Instance of CacheManager 
            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            dynamic empList;


            if (_objCacheManager.Contains("BindAssignedTo~" + FormID.ToString()))
            {
                empList = _objCacheManager.GetData("BindAssignedTo~" + FormID.ToString());
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    empList = (from uf in db.UserForms
                               join u in db.Users on uf.UserID equals u.UserID
                               where uf.FormID == FormID && !u.Inactive.Value
                               orderby u.FirstName ascending
                               let EmployeeName = u.FirstName + " " + u.LastName
                               select new { EmployeeName, u.UserID }).Distinct().ToList();

                    cboAssignedTo.Items.Clear();
                    cboAssignedTo.Items.Add(new Telerik.Web.UI.RadComboBoxItem("", ""));
                    cboAssignedTo.DataTextField = "EmployeeName";
                    cboAssignedTo.DataValueField = "UserID";
                    cboAssignedTo.DataSource = empList;
                    cboAssignedTo.DataBind();
                }

                _objCacheManager.Add("BindAssignedTo~" + FormID.ToString(), empList);
            }
        }

        public DateTime? CallBackDateTime
        {
            get
            {
                return dtpCallBackDate.SelectedDate;
            }
        }

        //public int FormID { get; set; }

        public int FormID
        {
            get {
                int _FormID;
                int.TryParse(Convert.ToString(ViewState["FormID"]), out _FormID);
                return _FormID;
            }
            set { ViewState["FormID"] = value; }
        }

        public int TimeZoneID
        {
            get
            {
                int _TimeZoneID;

                int.TryParse(cboAssignedTo.SelectedItem.Value, out _TimeZoneID);

                return _TimeZoneID;
            }
        }

        public int CallBackReasonID
        {
            get
            {
                int _CallBackReasonID;
                int.TryParse(cboAssignedTo.SelectedItem.Value, out _CallBackReasonID);
                return _CallBackReasonID;
            }
        }

        public int? AssignedTo
        {
            get
            {
                int _AssignedTo;
                int.TryParse(cboAssignedTo.SelectedItem.Value, out _AssignedTo);
                return _AssignedTo;
            }
        }

        public int AssignedReasonID
        {
            get
            {
                int _AssignedReasonID;
                int.TryParse(cboAssignedTo.SelectedItem.Value, out _AssignedReasonID);
                return _AssignedReasonID;
            }
        }

        public string ContactNumber
        {
            get
            {
                return txtContactNumber.Text.Trim();
            }
        }

        public string Notes
        {
            get
            {
                return txtNotes.Text.Trim();
            }
        }

        protected void radAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radAssignedTo.SelectedItem.Text == "Others")
            {
                BindAssignedTo(FormID);
                pnlAssignment.Visible = true;
            }
            else
            {
                pnlAssignment.Visible = false;
            }
        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AudienceManagerUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.AudienceManagerUserControl" %>
<div>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true"
        Width="680px" Height="500px" VisibleOnPageLoad="false" VisibleStatusbar="false"
        Modal="true" RenderMode="Lightweight" OnClientClose="OnClientClose" DestroyOnClose="true" Animation="Fade">
    </telerik:RadWindowManager> 
    <rad:RadAjaxManagerProxy ID="ajaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="btnPause">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnResume">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnStop">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMain" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
             </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <asp:Panel ID="pnlMain" runat="server">
        <table>
            <tr>
                <td>
                    <asp:ImageButton ID="btnPause" runat="server" ImageUrl="~/images/Pause32x32.png"
                        AlternateText="Pause Recording" ToolTip="Pause Recording" OnClick="btnPause_OnClick" CausesValidation="false" />
                    <asp:ImageButton ID="btnResume" runat="server" ImageUrl="~/images/Play32x32.png"
                        Visible="false" AlternateText="Resume Recording" ToolTip="Resume Recording" OnClick="btnResume_OnClick" CausesValidation="false" />
                </td>
                <td>
                    <asp:ImageButton ID="btnStop" runat="server" ImageUrl="~/images/Stop32x32.png" AlternateText="Stop Recording"
                        ToolTip="Stop Recording" OnClick="btnStop_OnClick" CausesValidation="false" />
                </td>
            </tr>
        </table>
        <asp:Label ID="lblStatus" runat="server" ForeColor="Red"></asp:Label>
    </asp:Panel>
</div>

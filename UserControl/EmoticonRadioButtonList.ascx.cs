﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallDispositionTool.UserControl
{
    public partial class EmoticonRadioButtonList : System.Web.UI.UserControl
    {
        public delegate void OnCheckedChangedEventHandler(object sender, CheckedChangedEventArgs e);
        public event OnCheckedChangedEventHandler checkHandler;

        public int AttributeID { get; set; }
        public string AttributeName { get; set; }
        public string rdoValue
        {
            get
            {
                if (ViewState["rdoValue"] != null)
                    return ViewState["rdoValue"].ToString();
                return string.Empty;
            }
            set { ViewState["rdoValue"] = value; }
        }

        public enum MessageType { Success, Error, Notice }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //protected void rdoID_CheckedChanged(object sender, EventArgs e)
        //{
        //    var rdoName = ((RadioButton)sender).ID;
        //    rdoValue = rdoName.Remove(0, 3);// rdoName.Substring(rdoName.Length - 1);

        //    hiddenEmoticonValue.Value = rdoValue;
        //}


        public class CheckedChangedEventArgs : EventArgs
        {
            public string rdoValue { get; set; }
        }

        protected void rdoID_CheckedChanged(object sender, EventArgs e)
        {
            var rdoName = ((RadioButton)sender).ID;
            rdoValue = rdoName.Remove(0, 3);// rdoName.Substring(rdoName.Length - 1);

            //hiddenEmoticonValue.Value = rdoValue;
            labelMessage.Text = GetFormattedMessage("<strong>Tips on handling irate customer</strong><br/> Tips on handling irate customer goes here.", MessageType.Error);
            AssignArguments(sender);
        }

        private void AssignArguments(object sender)
        {
            CheckedChangedEventArgs args = new CheckedChangedEventArgs();
            args.rdoValue = rdoValue;

            if (this.checkHandler != null)
            {
                this.checkHandler(sender, args);
            }
        }

        private string GetFormattedMessage(string message, MessageType messageType = MessageType.Notice)
        {
            switch (messageType)
            {
                case MessageType.Success: return "<div class='success'>" + message + "</div>";
                case MessageType.Error: return "<div class='error'>" + message + "</div>";
                default: return "<div class='notice'>" + message + "</div>";
            }
        }
    }
}
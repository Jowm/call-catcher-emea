﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using Telerik.Web.UI;
using System.Web.Security;

namespace CallDispositionTool.UserControl
{
    public partial class PickListOptionAdminUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindAttributeDropdown();
            }
        }

        void BindAttributeDropdown()
        {
            //5 Dropdown, 6 = Listbox
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var items = (from a in db.Attributes
                        where (a.DataTypeID == 5 || a.DataTypeID == 6) && a.HideFromList == false
                        orderby a.Form.FormName, a.AttributeName ascending
                        select new { a.AttributeID, a.AttributeName, a.Form.FormName }).ToList();

                foreach (var item in items)
                {
                    cboAttribute.Items.Add(new RadComboBoxItem(String.Format("{0} - {1}", item.FormName, item.AttributeName), item.AttributeID.ToString()));                    
                }
            }
        }

        protected void dsPickListOption_Inserting(object sender, EntityDataSourceChangingEventArgs e)
        {
            PickListOption pickList = e.Entity as PickListOption;
            pickList.CreatedOn = DateTime.Now;
            pickList.CreatedBy = Membership.GetUser().ProviderUserKey as Int32?;
            pickList.AttributeID = Convert.ToInt32(cboAttribute.SelectedItem.Value);
            Helper.ClearCache();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.WebService.AudienceManager;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Web.Configuration;
using System.Web.Security;
using Telerik.Web.UI;

namespace CallDispositionTool.UserControl
{
    public partial class AudienceManagerUserControl : System.Web.UI.UserControl
    {
        int LogID;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnPause_OnClick(object sender, EventArgs e)
        {
            try
            {
                AudienceManagerServiceSoapClient client = new AudienceManagerServiceSoapClient();
                int pauseReturn = client.PauseRecording(Request["CallID"]);

                if (pauseReturn == 0)
                {
                    LogID = LogPauseRecordingToAdonis(Request["CallID"], 0, Convert.ToInt32(Membership.GetUser().UserName), "");
                    Session["LogID"] = LogID;
                    RadAjaxManager.GetCurrent(this.Page).Alert("Recording Paused Successfully. LogID: " + LogID.ToString());
                    btnPause.Visible = false;
                    btnResume.Visible = true;
                }
                else
                {
                    RadAjaxManager.GetCurrent(this.Page).Alert(string.Format("Pause Recording Failed. Please try again. Error Code: {0} UCID: {1}", pauseReturn.ToString(), Request["CallID"].ToString()));
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                RadAjaxManager.GetCurrent(this.Page).Alert(ex.Message.Replace("'", "").Replace("\\", ""));
            }
        }

        protected void btnResume_OnClick(object sender, EventArgs e)
        {
            try
            {
                AudienceManagerServiceSoapClient client = new AudienceManagerServiceSoapClient();
                int resumeReturn = client.ResumeRecording(Request["CallID"]);

                if (resumeReturn == 0)
                {
                    LogID = Convert.ToInt32(Session["LogID"]);
                    LogResumeRecordingToAdonis(Request["CallID"], LogID);
                    RadAjaxManager.GetCurrent(this.Page).Alert(string.Format("alert('Recording Resumed Successfully. Log ID: {0}');", LogID.ToString()));
                    btnPause.Visible = true;
                    btnResume.Visible = false;
                }
                else
                {
                    RadAjaxManager.GetCurrent(this.Page).Alert(string.Format("Pause Recording Failed. Please try again. Error Code: {0} UCID: {1}", resumeReturn.ToString(), Request["CallID"].ToString()));
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                RadAjaxManager.GetCurrent(this.Page).Alert(ex.Message.Replace("'", "").Replace("\\", ""));
            }
        }

        protected void btnStop_OnClick(object sender, EventArgs e)
        {
            try
            {
                AudienceManagerServiceSoapClient client = new AudienceManagerServiceSoapClient();
                int stopReturn = client.StopRecording(Request["CallID"]);

                if (stopReturn == 0)
                {
                    RadAjaxManager.GetCurrent(this.Page).Alert("Recording Stoped Successfully. UCID: " + Request["CallID"]);
                    btnPause.Enabled = false;
                    btnResume.Enabled = false;
                }
                else
                {
                    RadAjaxManager.GetCurrent(this.Page).Alert(string.Format("Pause Recording Failed. Please try again. Error Code: {0} UCID: {1}", stopReturn.ToString(), Request["CallID"].ToString()));
                }
            }
            catch (Exception ex)
            {
                Helper.LogError(ex);
                RadAjaxManager.GetCurrent(this.Page).Alert(ex.Message.Replace("'", "").Replace("\\", ""));
            }
        }

        int LogPauseRecordingToAdonis(string UCID, int CompanySiteID, int CimNumber, string Campaign)
        {
            int LogID = 0;

            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_Adonis"].ConnectionString))
            {
                cn.Open();
                string sql = "pr_Artery_Log_PauseRecording";

                SqlCommand cmd = new SqlCommand(sql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UCID", UCID);
                cmd.Parameters.AddWithValue("@CimNumber", CimNumber);
                cmd.Parameters.AddWithValue("@CompanySiteID", CompanySiteID);
                cmd.Parameters.AddWithValue("@Campaign", Campaign);

                LogID = Convert.ToInt32(cmd.ExecuteScalar());
            }

            return LogID;
        }

        void LogResumeRecordingToAdonis(string UCID, int LogID)
        {
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_Adonis"].ConnectionString))
            {
                cn.Open();
                string sql = "pr_Artery_Log_ResumeRecording";

                SqlCommand cmd = new SqlCommand(sql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UCID", UCID);
                cmd.Parameters.AddWithValue("@AMLogID", LogID);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
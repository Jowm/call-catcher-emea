﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Linq;
using CallDispositionTool.Model;
using CallDispositionTool.Security;
using System.Collections.Generic;
using Telerik.Web.UI;
using Telerik.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace CallDispositionTool.UserControl
{
    public partial class UserAdminUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void grdUsers_ItemCommand(object sender, GridCommandEventArgs e)
        {
            GridDataItem parentItem = e.Item as GridDataItem;

            if (e.CommandName == "ResetPassword")
            {
                GridDataItem item = e.Item as GridDataItem;

                string userID = item.GetDataKeyValue("UserID").ToString();

                try
                {
                    Helper.ResetPassword(Convert.ToInt32(userID), "Changeme" + item["EmployeeID"].Text);
                    grdUsers.Controls.Add(new LiteralControl(item["EmployeeID"].Text + " password has been reset"));
                }
                catch (Exception ex)
                {
                    grdUsers.Controls.Add(new LiteralControl(string.Format("Error resetting password: {0}", ex.Message)));
                }

            }
            else if (e.CommandName == RadGrid.ExpandCollapseCommandName && !e.Item.Expanded)
            {
                //foreach (GridItem item in e.Item.OwnerTableView.Items)
                //{
                //    if (item.Expanded && item != e.Item)
                //    {
                //        item.Expanded = false;
                //    }
                //}

                //RadGrid grdFormAccess = parentItem.ChildItem.FindControl("LoginViewFormAccess").FindControl("grdFormAccess") as RadGrid;

                //if (parentItem.ItemType == GridItemType.AlternatingItem || parentItem.ItemType == GridItemType.Item)
                //{
                //    RadGrid grdUserRole = parentItem.ChildItem.FindControl("LoginViewUserRole").FindControl("grdUserRole") as RadGrid;

                //    if (grdUserRole != null)
                //        grdUserRole.Rebind();

                //    UserFormAdminUserControl userFormAdminUserControl1 = parentItem.ChildItem.FindControl("LoginViewFormAccess").FindControl("userFormAdminUserControl1") as UserFormAdminUserControl;

                //    int UserID = Convert.ToInt32(parentItem.GetDataKeyValue("UserID"));

                //    if (userFormAdminUserControl1 != null)
                //        userFormAdminUserControl1.UserID = UserID;
                //}

                //if (grdFormAccess != null)
                //    grdFormAccess.Rebind();
            }
            else if (e.CommandName == "FilterRadGrid")
            {
                RadFilter1.FireApplyCommand();
            }
        }

        protected string GetFilterIcon()
        {
            return SkinRegistrar.GetWebResourceUrl(Page, typeof(RadGrid), "Telerik.Web.UI.Skins.Metro.Grid.Filter.gif");
        }

        protected string GetAddRecordIcon()
        {
            return SkinRegistrar.GetWebResourceUrl(Page, typeof(RadGrid), "Telerik.Web.UI.Skins.Metro.Grid.AddRecord.gif");
        }

        protected void grdUsers_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                int iUsersCount = (from u in db.Users
                                   select u).Count();

                int startRowIndex = (ShouldApplySortFilterOrGroup()) ? 0 : grdUsers.CurrentPageIndex * grdUsers.PageSize;
                int maximumRows = (ShouldApplySortFilterOrGroup()) ? iUsersCount : grdUsers.PageSize;

                grdUsers.AllowCustomPaging = !ShouldApplySortFilterOrGroup();
                grdUsers.DataSource = (from u in db.Users
                                       select u).OrderBy(user => user.UserID).Skip(startRowIndex).Take(maximumRows).ToList();
            }
        }

        //protected void grdFormAccess_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        //{
        //    using (CallDispositionEntities db = new CallDispositionEntities())
        //    {
        //        GridDataItem parentItem = (((sender as RadGrid).NamingContainer as LoginView).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;
        //        int UserID = Convert.ToInt32(parentItem.GetDataKeyValue("UserID"));

        //        var formAccess = (from o in db.UserForms
        //                          where o.UserID == UserID && o.Form.HideFromList == false
        //                          select o).ToList();

        //        (sender as RadGrid).DataSource = formAccess;
        //    }
        //}

        protected void grdUserRole_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                GridDataItem parentItem = (((sender as RadGrid).NamingContainer as LoginView).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;
                int UserID = Convert.ToInt32(parentItem.GetDataKeyValue("UserID"));

                var userRoles = (from o in db.UserRoles
                                  where o.UserID == UserID && o.Role.HideFromList == false
                                  select o).ToList();

                (sender as RadGrid).DataSource = userRoles;
            }
        }

        bool isGrouping = false;

        public bool ShouldApplySortFilterOrGroup()
        {
            return grdUsers.MasterTableView.FilterExpression != "" ||
                (grdUsers.MasterTableView.GroupByExpressions.Count > 0 || isGrouping) ||
                grdUsers.MasterTableView.SortExpressions.Count > 0;
        }

        protected void grdUsers_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);


            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                string sEmployeeID = values["EmployeeID"] as string;

                var existingUser = (from u in db.Users
                                    where u.EmployeeID == sEmployeeID
                                    select u).FirstOrDefault();

                if (existingUser == null)
                {
                    User usr = new User();

                    usr.EmployeeID = values["EmployeeID"] as string;
                    usr.FirstName = values["FirstName"] as string;
                    usr.LastName = values["LastName"] as string;
                    usr.EmailAddress = values["EmailAddress"] as string;
                    usr.AvayaLogin = values["AvayaLogin"] as string;
                    usr.Inactive = values["Inactive"] as bool?;
                    usr.PasswordHash = PasswordHash.CreateHash("Changeme" + usr.EmployeeID);
                    usr.CreatedOn = DateTime.Now;
                    usr.CreatedBy = Membership.GetUser().ProviderUserKey as Int32?;

                    try
                    {
                        db.Users.AddObject(usr);
                        db.SaveChanges();
                        grdUsers.Controls.Add(new LiteralControl(string.Format("<strong style='color: green'>Record for {0} {1} successfully updated: {0}</strong>", usr.FirstName, usr.LastName)));
                    }
                    catch (Exception ex)
                    {
                        grdUsers.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error inserting User: {0}</strong>", ex.Message)));
                        e.Canceled = true;
                    }
                }
                else
                {
                    grdUsers.Controls.Add(new LiteralControl(String.Format("<strong style='color: red'>Employee ID {0} already exist. Please input a unique Employee ID.</strong>", values["EmployeeID"])));
                    e.Canceled = true;
                }
            }
        }

        //protected void grdFormAccess_InsertCommand(object sender, GridCommandEventArgs e)
        //{
        //    var editableItem = ((GridEditableItem)e.Item);
        //    Hashtable values = new Hashtable();
        //    editableItem.ExtractValues(values);
        //    GridDataItem parentItem = (((sender as RadGrid).NamingContainer as LoginView).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;

        //    using (CallDispositionEntities db = new CallDispositionEntities())
        //    {
        //        UserForm item = new UserForm();
        //        item.UserID = Convert.ToInt32(parentItem.GetDataKeyValue("UserID"));
        //        item.FormID = Convert.ToInt32(values["FormID"]);
        //        item.CreatedOn = DateTime.Now;
        //        item.CreatedBy = Membership.GetUser().ProviderUserKey as Int32?;

        //        try
        //        {
        //            db.UserForms.AddObject(item);
        //            db.SaveChanges();
        //            Helper.ClearCache();
        //            grdUsers.Controls.Add(new LiteralControl("<strong style='color: green'>Record has been inserted</strong>"));
        //        }
        //        catch (Exception ex)
        //        {
        //            grdUsers.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error inserting record: {0}</strong>", ex.Message)));
        //            e.Canceled = true;
        //        }
        //    }
        //}

        protected void grdUserRole_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);
            GridDataItem parentItem = (((sender as RadGrid).NamingContainer as LoginView).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                UserRole item = new UserRole();
                item.UserID = Convert.ToInt32(parentItem.GetDataKeyValue("UserID"));
                item.RoleID = Convert.ToInt32(values["RoleID"]);
                item.CreatedOn = DateTime.Now;
                item.CreatedBy = Membership.GetUser().ProviderUserKey as Int32?;

                try
                {
                    db.UserRoles.AddObject(item);
                    db.SaveChanges();
                    grdUsers.Controls.Add(new LiteralControl("<strong style='color: green'>Record has been inserted</strong>"));
                }
                catch (Exception ex)
                {
                    grdUsers.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error inserting record: {0}</strong>", ex.Message)));
                    e.Canceled = true;
                }
            }
        }

        protected void grdUsers_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            var userID = (int)editableItem.GetDataKeyValue("UserID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var user = (from u in db.Users
                            where u.UserID == userID
                            select u).FirstOrDefault();

                if (user != null)
                {
                    //update entity's state
                    editableItem.UpdateValues(user);

                    try
                    {
                        db.SaveChanges();
                        CacheFactory.GetCacheManager().Flush();

                        grdUsers.Controls.Add(new LiteralControl(string.Format("<strong style='color: green'>Record for {0} {1} successfully updated: {0}</strong>", user.FirstName, user.LastName)));
                    }
                    catch (Exception ex)
                    {
                        grdUsers.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error Updating User: {0}</strong>", ex.Message)));
                        e.Canceled = true;
                    }
                }
            }
        }

        //protected void grdFormAccess_UpdateCommand(object sender, GridCommandEventArgs e)
        //{
        //    var editableItem = ((GridEditableItem)e.Item);
        //    decimal userFormID = (decimal)editableItem.GetDataKeyValue("UserFormID");

        //    //retrive entity form the Db
        //    using (CallDispositionEntities db = new CallDispositionEntities())
        //    {
        //        var userForm = (from u in db.UserForms
        //                    where u.UserFormID == userFormID
        //                    select u).FirstOrDefault();

        //        if (userForm != null)
        //        {
        //            //update entity's state
        //            editableItem.UpdateValues(userForm);

        //            try
        //            {
        //                db.SaveChanges();
        //                Helper.ClearCache();
        //                grdUsers.Controls.Add(new LiteralControl("<strong style='color: green'>Record has been updated</strong>"));
        //            }
        //            catch (Exception ex)
        //            {
        //                grdUsers.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error updating record: {0}</strong>", ex.Message)));
        //                e.Canceled = true;
        //            }
        //        }
        //    }
        //}

        protected void grdUserRole_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            decimal userFormID = (decimal)editableItem.GetDataKeyValue("UserRoleID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var userRole = (from u in db.UserRoles
                                where u.UserRoleID == userFormID
                                select u).FirstOrDefault();

                if (userRole != null)
                {
                    //update entity's state
                    editableItem.UpdateValues(userRole);

                    try
                    {
                        db.SaveChanges();
                        grdUsers.Controls.Add(new LiteralControl("<strong style='color: green'>Record has been updated</strong>"));
                    }
                    catch (Exception ex)
                    {
                        grdUsers.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error updating record: {0}</strong>", ex.Message)));
                        e.Canceled = true;
                    }
                }
            }
        }

        protected void grdUsers_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            int userID = (int)((GridDataItem)e.Item).GetDataKeyValue("UserID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var user = (from u in db.Users
                            where u.UserID == userID
                            select u).FirstOrDefault();

                if (user != null)
                {
                    try
                    {
                        db.Users.DeleteObject(user);
                        db.SaveChanges();
                        grdUsers.Controls.Add(new LiteralControl(string.Format("<strong style='color: green'>User ID {0} has been deleted</strong>", user.UserID)));
                    }
                    catch (Exception ex)
                    {
                        grdUsers.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error deleting User: {0}</strong>", ex.Message)));
                        e.Canceled = true;
                    }
                }
            }
        }

        //protected void grdFormAccess_DeleteCommand(object sender, GridCommandEventArgs e)
        //{
        //    decimal userFormID = (decimal)((GridDataItem)e.Item).GetDataKeyValue("UserFormID");

        //    //retrive entity form the Db
        //    using (CallDispositionEntities db = new CallDispositionEntities())
        //    {
        //        var userForm = (from u in db.UserForms
        //                        where u.UserFormID == userFormID
        //                    select u).FirstOrDefault();

        //        if (userForm != null)
        //        {
        //            try
        //            {
        //                db.UserForms.DeleteObject(userForm);
        //                db.SaveChanges();
        //                Helper.ClearCache();
        //                grdUsers.Controls.Add(new LiteralControl("<strong style='color: green'>Record has been updated</strong>"));
        //            }
        //            catch (Exception ex)
        //            {
        //                grdUsers.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error deleting User: {0}</strong>", ex.Message)));
        //                e.Canceled = true;
        //            }
        //        }
        //    }
        //}

        protected void grdUserRole_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            decimal userRoleID = (decimal)((GridDataItem)e.Item).GetDataKeyValue("UserRoleID");

            //retrieve entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var userRole = (from u in db.UserRoles
                                where u.UserRoleID == userRoleID
                                select u).FirstOrDefault();

                if (userRole != null)
                {
                    try
                    {
                        db.UserRoles.DeleteObject(userRole);
                        db.SaveChanges();
                        grdUsers.Controls.Add(new LiteralControl("<strong style='color: green'>Record has been updated</strong>"));
                    }
                    catch (Exception ex)
                    {
                        grdUsers.Controls.Add(new LiteralControl(string.Format("<strong style='color: green'>Error deleting User: {0}</strong>", ex.Message)));
                        e.Canceled = true;
                    }
                }
            }
        }

        protected void grdUsers_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item.ItemType == GridItemType.AlternatingItem || e.Item.ItemType == GridItemType.Item)
            {
                ImageButton EditUserAccess = e.Item.FindControl("EditUserAccess") as ImageButton;
                var itemData = ((GridDataItem)e.Item);
                var userID = (int)itemData.GetDataKeyValue("UserID");

                if (EditUserAccess != null)
                {
                    EditUserAccess.OnClientClick = string.Format("return ShowUserAccessPage('{0}');", userID);
                }
            }
        }
    }
}
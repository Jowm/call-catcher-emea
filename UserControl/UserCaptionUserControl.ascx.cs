﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallDispositionTool.UserControl
{
    public partial class UserCaptionUserControl : System.Web.UI.UserControl
    {
        public int AttributeID { get; set; }
        public string AttributeName { get; set; }

        public string Username
        {
            set { tbUserCaptionUserControl.Text = value; }
            get { return tbUserCaptionUserControl.Text; }
        }

        public void Reset()
        {
            tbUserCaptionUserControl.Text = "";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            tbUserCaptionUserControl.Text = CallDispositionTool.Helper.GetEmployeeName(HttpContext.Current.User.Identity.Name);
        }
    }
}
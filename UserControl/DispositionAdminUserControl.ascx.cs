﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using Telerik.Web.UI;
using System.Web.Security;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace CallDispositionTool.UserControl
{
    public partial class DispositionAdminUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    //7 = Dispostion Control
                    var items = (from a in db.Attributes
                                 where (a.DataTypeID == 7 || a.DataTypeID == 8) && a.HideFromList == false
                                 orderby a.Form.FormName, a.AttributeName ascending
                                 select new { a.AttributeID, a.AttributeName, a.Form.FormName }).ToList();

                    foreach (var item in items)
                    {
                        cboAttribute.Items.Add(new RadComboBoxItem(String.Format("{0} - {1}", item.FormName, item.AttributeName), item.AttributeID.ToString()));
                    }
                }
            }
        }

        protected void dsDisposition_Inserting(object sender, EntityDataSourceChangingEventArgs e)
        {
            Disposition dispo = e.Entity as Disposition;
            dispo.CreateDate = DateTime.Now;
            dispo.CreatedBy = Membership.GetUser().ProviderUserKey as Int32?;

        }

        protected void trvDisposition_NeedDataSource(object sender, TreeListNeedDataSourceEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                int a = Convert.ToInt32(cboAttribute.SelectedValue);

                trvDisposition.DataSource = (from d in db.Dispositions
                                             where d.ParentDispositionID == null && d.AttributeID == a
                                             select d).ToList();
            }
        }

        protected void trvDisposition_ChildItemsDataBind(object sender, TreeListChildItemsDataBindEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                int? id = Convert.ToInt32(e.ParentDataKeyValues["DispositionID"].ToString());
                int a = Convert.ToInt32(cboAttribute.SelectedValue);

                e.ChildItemsDataSource = (from s in db.Dispositions
                                          where s.ParentDispositionID == id && s.AttributeID == a
                                          select s).ToList();
            }
        }

        protected void cboAttribute_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            trvDisposition.Rebind();
        }

        protected void trvDisposition_InsertCommand(object sender, TreeListCommandEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                Hashtable table = new Hashtable();
                Disposition dispo = new Disposition();
                TreeListEditableItem item = e.Item as TreeListEditableItem;
                item.ExtractValues(table);

                dispo.AttributeID = Convert.ToInt32(cboAttribute.SelectedItem.Value);
                dispo.Description = table["Description"].ToString();
                dispo.HideFromList = Convert.ToBoolean(table["HideFromList"]);

                if (table["FilterDispositionID"] == null)
                {
                    dispo.FilterDispositionID = null;
                }
                else
                {
                    dispo.FilterDispositionID = Convert.ToInt32(table["FilterDispositionID"]);
                }

                if (table["ParentDispositionID"] == null)
                {
                    dispo.ParentDispositionID = null;
                }
                else
                {
                    dispo.ParentDispositionID = Convert.ToInt32(table["ParentDispositionID"]);
                }

                dispo.CreatedBy = Membership.GetUser().ProviderUserKey as int?;
                dispo.CreateDate = DateTime.Now;

                db.Dispositions.AddObject(dispo);

                try
                {
                    db.SaveChanges();
                    Helper.ClearCache();
                }
                catch (Exception ex)
                {
                    ((RadWindowManager)this.Page.Master.FindControl("RadWindowManager1")).RadAlert(ex.Message, 330, 180, "Error Message", "");
                }

            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            trvDisposition.ExportToExcel();
        }

        protected void trvDisposition_UpdateCommand(object sender, TreeListCommandEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                Hashtable table = new Hashtable();
                TreeListEditFormItem editedItem = (e.Item as TreeListEditFormItem);
                editedItem.ExtractValues(table);
                int dispoID = Convert.ToInt32(editedItem.ParentItem.GetDataKeyValue("DispositionID"));

                Disposition dispo = (from d in db.Dispositions
                                     where d.DispositionID == dispoID
                                     select d).FirstOrDefault();

                if (dispo != null)
                {
                    dispo.Description = table["Description"].ToString();
                    dispo.HideFromList = Convert.ToBoolean(table["HideFromList"]);

                    if (table["FilterDispositionID"] == null)
                    {
                        dispo.FilterDispositionID = null;
                    }
                    else
                    {
                        dispo.FilterDispositionID = Convert.ToInt32(table["FilterDispositionID"]);
                    }

                    try
                    {
                        db.SaveChanges();
                        Helper.ClearCache();
                    }
                    catch (Exception ex)
                    {
                        ((RadWindowManager)this.Page.Master.FindControl("RadWindowManager1")).RadAlert(ex.Message, 330, 180, "Error Message", "");

                    }
                }
            }
        }

        protected void trvDisposition_DeleteCommand(object sender, TreeListCommandEventArgs e)
        {
            TreeListDataItem item = e.Item as TreeListDataItem;

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                int dispoID = Convert.ToInt32(item.ParentItem.GetDataKeyValue("DispositionID"));


                Disposition dispo = (from d in db.Dispositions
                                     where d.DispositionID == dispoID
                                     select d).FirstOrDefault();

                if (dispo != null)
                {
                    db.Dispositions.DeleteObject(dispo);

                    try
                    {
                        db.SaveChanges();
                        Helper.ClearCache();
                    }
                    catch (Exception ex)
                    {
                        ((RadWindowManager)this.Page.Master.FindControl("RadWindowManager1")).RadAlert(ex.Message, 330, 180, "Error Message", "");
                    }
                }
            }
        }
    }
}
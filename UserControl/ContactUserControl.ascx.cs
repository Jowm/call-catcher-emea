﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using CallDispositionTool.Model;
using CallDispositionTool.UserControl;

namespace CallDispositionTool.UserControl
{
    public partial class ContactUserControl : System.Web.UI.UserControl
    {
        public int AttributeID { get; set; }
        public string AttributeName { get; set; }
        public decimal ContactID
        {
            get
            {
                decimal contactID;
                decimal.TryParse(txtContactID.Text, out contactID);
                return contactID;
            }
        }
        public string ContactInfo {
            get {
                decimal contactID = Convert.ToDecimal(txtContactID.Text);
                string returnValue = string.Empty;

                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    var contact = (from c in db.Contacts
                                   where c.ContactID == contactID
                                   select c).FirstOrDefault();

                    if (contact != null)
                    {
                        returnValue = contact.FirstName;
                        returnValue += "\\" + contact.LastName;
                        returnValue += "\\" + contact.Phone;
                        returnValue += "\\" + contact.MobilePhone;
                        returnValue += "\\" + contact.Address1;
                        returnValue += "\\" + contact.Address2;
                        returnValue += "\\" + contact.Email;
                    }
                }

                return returnValue;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                reqContact.ErrorMessage = "Please Enter value for " + AttributeName;
            }
        }

        public void ResetControl()
        {
            searchBox1.Text = string.Empty;
            pnlContactInfo.Visible = false;
            txtContactID.Text = string.Empty;
            lblContactName.Text = string.Empty;
            lblPhone.Text = string.Empty;
            lblMobile.Text = string.Empty;
            lblAddress.Text = string.Empty;
        }

        protected void searchBox1_Search(object sender, Telerik.Web.UI.SearchBoxEventArgs e)
        {
            if (e.DataItem != null)
            {
                pnlContactInfo.Visible = true;
                lblContactName.Text = ((Dictionary<string, object>)e.DataItem)["ContactName"].ToString();
                txtContactID.Text = ((Dictionary<string, object>)e.DataItem)["ContactID"].ToString();
                lblPhone.Text = ((Dictionary<string, object>)e.DataItem)["Phone"].ToString();
                lblMobile.Text = ((Dictionary<string, object>)e.DataItem)["MobilePhone"].ToString();
                lblAddress.Text = ((Dictionary<string, object>)e.DataItem)["Address"].ToString();
                lblEmail.Text = ((Dictionary<string, object>)e.DataItem)["Email"].ToString();
            }
        }

        protected void searchBox1_DataBound(object sender, EventArgs e)
        {

        }

        protected void searchBox1_ButtonCommand(object sender, SearchBoxButtonEventArgs e)
        {
            if (e.CommandName == "Reset")
            {
                ResetControl();
            }
            else if (e.CommandName == "NewContact")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "NewContact", "return ShowAddContact();");
            }
        }
    }
}
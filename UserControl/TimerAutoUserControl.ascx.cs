﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallDispositionTool.UserControl
{
    public partial class TimerAutoUserControl : System.Web.UI.UserControl
    {
        public int AttributeID { get; set; }
        public string AttributeName { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Reset();
            }
        }

        public string ElapsedTime
        {
            get
            {
                return Convert.ToString(hiddenElapsed.Value);
            }
        }

        public void Reset()
        {
            hiddenElapsed.Value = "00:00:00";
        }
    }
}
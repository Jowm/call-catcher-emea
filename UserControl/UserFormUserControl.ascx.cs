﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Linq;
using CallDispositionTool.Model;
using CallDispositionTool.Security;
using System.Collections.Generic;
using Telerik.Web.UI;
using Telerik.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace CallDispositionTool.UserControl
{
    public partial class UserFormUserControl : System.Web.UI.UserControl
    {
        public int UserID
        {
            get
            {
                int iUserID;

                int.TryParse(ViewState["CurrentValue"] as string, out iUserID);

                return iUserID;
            }
            set
            {
                ViewState["UserID"] = value;
                this.LoadTabs();
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            writer.Write(this.UserID);
        }

        internal class FormDataItem
        {
            public int FormID { get; set; }
            public string FormName { get; set; }

            public FormDataItem(int FormID, string FormName)
            {
                this.FormID = FormID;
                this.FormName = FormName;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //LoadTabs(UserID);
        }

        void LoadTabs()
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                List<FormDataItem> forms = (from f in db.UserForms
                             where f.UserID == UserID
                             select new FormDataItem(f.FormID, f.Form.FormName )).ToList();

                tabStrip1.DataTextField = "FormName";
                tabStrip1.DataValueField = "FormID";
                tabStrip1.DataSource = forms;
                tabStrip1.DataBind();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using Telerik.Web.UI;
using System.Web.Security;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace CallDispositionTool.UserControl
{
    public partial class DispositionUserControl : System.Web.UI.UserControl
    {
        public delegate void OnDispositionChangedEventHandler(object sender, DispositionChangedEventArgs e);
        public event OnDispositionChangedEventHandler dispoHandler;
        public bool Mandatory { get; set; }
        public int AttributeID { get; set; }

        public int? FilterDispositionID
        {
            get
            {
                try
                {
                    return Convert.ToInt32(hdnFilterDispositionID.Value);
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                hdnFilterDispositionID.Value = Convert.ToString(value);
            }
        }

        public string AttributeName { get; set; }

        public class DispositionChangedEventArgs : EventArgs
        {
            public string DispositionValue { get; set; }
            public int? Dispo1 { get; set; }
            public int? Dispo2 { get; set; }
            public int? Dispo3 { get; set; }
            public int? Dispo4 { get; set; }
            public int? Dispo5 { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Rebind();

                string errorMessage = "Please enter value for " + AttributeName;

                reqDisposition1.ErrorMessage = errorMessage;
                reqDisposition2.ErrorMessage = errorMessage;
                reqDisposition3.ErrorMessage = errorMessage;
                reqDisposition4.ErrorMessage = errorMessage;
                reqDisposition5.ErrorMessage = errorMessage;
            }
        }

        public int? Dispo1ID
        {
            get
            {
                try
                {
                    return Convert.ToInt32(cboDispo1.SelectedItem.Value);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public int? Dispo2ID
        {
            get
            {
                try
                {
                    return Convert.ToInt32(cboDispo2.SelectedItem.Value);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public int? Dispo3ID
        {
            get
            {
                try
                {
                    return Convert.ToInt32(cboDispo3.SelectedItem.Value);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public int? Dispo4ID
        {
            get
            {
                try
                {
                    return Convert.ToInt32(cboDispo4.SelectedItem.Value);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public int? Dispo5ID
        {
            get
            {
                try
                {
                    return Convert.ToInt32(cboDispo5.SelectedItem.Value);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public string Dispo1
        {
            get
            {
                return cboDispo1.Text;
            }
        }

        public string Dispo2
        {
            get
            {
                return cboDispo2.Text;
            }
        }

        public string Dispo3
        {
            get
            {
                return cboDispo3.Text;
            }
        }

        public string Dispo4
        {
            get
            {
                return cboDispo4.Text;
            }
        }

        public string Dispo5
        {
            get
            {
                return cboDispo4.Text;
            }
        }

        dynamic DispoFilterDataSource(int i, int? p, int? f)
        {
            dynamic dispo;

            //Create Instance of CacheManager 
            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                #region tocomment
                if ((!p.HasValue) && f.HasValue)
                {
                    if (_objCacheManager.Contains("DispoFilterDataSource~" + i.ToString() + "0" + "~" + f.ToString()))
                    {
                        dispo = _objCacheManager.GetData("DispoFilterDataSource~" + i.ToString() + "0" + "~" + f.ToString());
                    }
                    else
                    {
                        dispo = (from d in db.Dispositions
                                 where (d.AttributeID == i && d.ParentDispositionID == null && d.FilterDispositionID == f && d.HideFromList == false)
                                 orderby d.Description ascending
                                 select new { d.DispositionID, d.Description }).ToList();

                        _objCacheManager.Add("DispoFilterDataSource~" + i.ToString() + "0" + "~" + f.ToString(), dispo);
                    }
                }
                else if ((!p.HasValue || p.Value == 0) && !f.HasValue)
                {
                    if (_objCacheManager.Contains("DispoFilterDataSource~" + i.ToString() + "0" + "~" + "0"))
                    {
                        dispo = _objCacheManager.GetData("DispoFilterDataSource~" + i.ToString() + "0" + "~" + "0");
                    }
                    else
                    {
                        dispo = (from d in db.Dispositions
                                 where (d.AttributeID == i && d.ParentDispositionID == null && d.FilterDispositionID == null && d.HideFromList == false)
                                 orderby d.Description ascending
                                 select new { d.DispositionID, d.Description }).ToList();

                        _objCacheManager.Add("DispoFilterDataSource~" + i.ToString() + "0" + "~" + "0", dispo);
                    }
                }
                else if (p.HasValue && !f.HasValue)
                {
                    if (_objCacheManager.Contains("DispoFilterDataSource~" + i.ToString() + p.ToString() + "~" + "0"))
                    {
                        dispo = _objCacheManager.GetData("DispoFilterDataSource~" + i.ToString() + p.ToString() + "~" + "0");
                    }
                    else
                    {
                        dispo = (from d in db.Dispositions
                                 where (d.AttributeID == i && d.ParentDispositionID == p && d.FilterDispositionID == null && d.HideFromList == false)
                                 orderby d.Description ascending
                                 select new { d.DispositionID, d.Description }).ToList();

                        _objCacheManager.Add("DispoFilterDataSource~" + i.ToString() + p.ToString() + "~" + "0", dispo);
                    }
                }
                else
                {
                    if (_objCacheManager.Contains("DispoFilterDataSource~" + i.ToString() + p.ToString() + "~" + f.ToString()))
                    {
                        dispo = _objCacheManager.GetData("DispoFilterDataSource~" + i.ToString() + p.ToString() + "~" + f.ToString());
                    }
                    else
                    {
                        dispo = (from d in db.Dispositions
                                 where (d.AttributeID == i && d.ParentDispositionID == p && d.FilterDispositionID == f && d.HideFromList == false)
                                 orderby d.Description ascending
                                 select new { d.DispositionID, d.Description }).ToList();

                        _objCacheManager.Add("DispoFilterDataSource~" + i.ToString() + p.ToString() + "~" + f.ToString(), dispo);
                    }
                }

                #endregion

                return dispo;
            }
        }

        protected void cboDispo1_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            cboDispo2.Items.Clear();
            cboDispo2.Items.Add(new Telerik.Web.UI.RadComboBoxItem("", null));
            cboDispo2.DataSource = DispoFilterDataSource(AttributeID, Dispo1ID, FilterDispositionID);

            cboDispo2.DataBind();

            if (cboDispo2.Items.Count > 1)
            {
                cboDispo2.Visible = true;
                reqDisposition2.Visible = true;
                cboDispo2.SelectedIndex = -1;
            }
            else
            {
                cboDispo2.Visible = false;
                reqDisposition2.Visible = false;
            }

            cboDispo2.Text = string.Empty;

            cboDispo3.Text = string.Empty;
            cboDispo3.SelectedIndex = -1;
            cboDispo3.Visible = false;
            reqDisposition3.Visible = false;

            cboDispo4.Text = string.Empty;
            cboDispo4.Visible = false;
            cboDispo4.SelectedIndex = -1;
            reqDisposition4.Visible = false;

            AssignArguments(sender);
        }

        public void ResetControl()
        {
            cboDispo1.SelectedIndex = -1;
            cboDispo2.SelectedIndex = -1;
            cboDispo2.Visible = false;
            reqDisposition2.Visible = false;
            cboDispo3.SelectedIndex = -1;
            cboDispo3.Visible = false;
            reqDisposition3.Visible = false;
            cboDispo4.Visible = false;
            reqDisposition4.Visible = false;
            cboDispo5.Visible = false;
            reqDisposition5.Visible = false;

        }

        protected void cboDispo2_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            cboDispo3.Items.Clear();
            cboDispo3.Items.Add(new Telerik.Web.UI.RadComboBoxItem("", null));
            cboDispo3.DataSource = DispoFilterDataSource(AttributeID, Dispo2ID, FilterDispositionID);

            cboDispo3.DataBind();

            if (cboDispo3.Items.Count > 1)
            {
                cboDispo3.Visible = true;
                reqDisposition3.Visible = true;
                cboDispo3.SelectedIndex = -1;
            }
            else
            {
                cboDispo3.Visible = false;
                reqDisposition3.Visible = false;
            }

            cboDispo3.Text = string.Empty;

            cboDispo4.Text = string.Empty;
            cboDispo4.SelectedIndex = -1;
            cboDispo4.Visible = false;
            reqDisposition4.Visible = false;
            cboDispo5.Visible = false;
            reqDisposition5.Visible = false;

            AssignArguments(sender);
        }
        protected void cboDispo3_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            cboDispo4.Items.Clear();
            cboDispo4.Items.Add(new Telerik.Web.UI.RadComboBoxItem("", null));
            cboDispo4.DataSource = DispoFilterDataSource(AttributeID, Dispo3ID, FilterDispositionID);

            cboDispo4.DataBind();

            if (cboDispo4.Items.Count > 1)
            {
                cboDispo4.Visible = true;
                reqDisposition4.Visible = true;
                cboDispo4.SelectedIndex = -1;
            }
            else
            {
                cboDispo4.Text = string.Empty;
                cboDispo4.Visible = false;
                reqDisposition4.Visible = false;
            }

            cboDispo4.Text = string.Empty;
            cboDispo5.Visible = false;
            reqDisposition5.Visible = false;

            AssignArguments(sender);
        }

        protected void cboDispo4_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            cboDispo5.Items.Clear();
            cboDispo5.Items.Add(new Telerik.Web.UI.RadComboBoxItem("", null));
            cboDispo5.DataSource = DispoFilterDataSource(AttributeID, Dispo4ID, FilterDispositionID);
            cboDispo5.DataBind();

            if (cboDispo5.Items.Count > 1)
            {
                cboDispo5.Visible = true;
                reqDisposition5.Visible = true;
                cboDispo5.SelectedIndex = -1;
            }
            else
            {
                cboDispo5.Text = string.Empty;
                cboDispo5.Visible = false;
                reqDisposition4.Visible = false;
            }

            cboDispo5.Text = string.Empty;
            AssignArguments(sender);
        }

        protected void cboDispo5_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            AssignArguments(sender);
        }

        private void AssignArguments(object sender)
        {
            DispositionChangedEventArgs args = new DispositionChangedEventArgs();
            args.DispositionValue = DispositionValue;
            args.Dispo1 = Dispo1ID;
            args.Dispo2 = Dispo2ID;
            args.Dispo3 = Dispo3ID;
            args.Dispo4 = Dispo4ID;

            if (this.dispoHandler != null)
            {
                this.dispoHandler(sender, args);
            }
        }

        public void Rebind()
        {
            cboDispo1.Items.Clear();
            cboDispo1.Items.Add(new Telerik.Web.UI.RadComboBoxItem("", ""));
            
            cboDispo1.DataSource = DispoFilterDataSource(AttributeID, null, FilterDispositionID);

            cboDispo1.DataBind();
            cboDispo1.Text = string.Empty;
            cboDispo1.SelectedIndex = -1;

            cboDispo2.Text = string.Empty;
            cboDispo2.SelectedIndex = -1;
            cboDispo2.Visible = false;
            reqDisposition2.Visible = false;

            cboDispo3.Text = string.Empty;
            cboDispo3.SelectedIndex = -1;
            cboDispo3.Visible = false;
            reqDisposition3.Visible = false;

            cboDispo4.Text = string.Empty;
            cboDispo4.SelectedIndex = -1;
            cboDispo4.Visible = false;
            reqDisposition4.Visible = false;

            cboDispo5.Text = string.Empty;
            cboDispo5.SelectedIndex = -1;
            cboDispo5.Visible = false;
            reqDisposition5.Visible = false;
        }

        public string DispositionValue
        {
            get
            {
                string tmp = string.Empty;

                if (Dispo1.Length > 0) {
                    tmp = Dispo1;
                }

                if (Dispo2.Length > 0)
                {
                    tmp += "\\" + Dispo2;
                }

                if (Dispo3.Length > 0)
                {
                    tmp += "\\" + Dispo3;
                }

                if (Dispo4.Length > 0)
                {
                    tmp += "\\" + Dispo4;
                }

                if (Dispo5.Length > 0)
                {
                    tmp += "\\" + Dispo5;
                }
                
                return tmp;
            }
        }
    }
}
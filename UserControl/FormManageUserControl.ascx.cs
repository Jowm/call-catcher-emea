﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Linq;
using CallDispositionTool.Model;
using CallDispositionTool.Security;
using System.Collections.Generic;
using Telerik.Web.UI;
using Telerik.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace CallDispositionTool.UserControl
{
    public partial class FormManageUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void grdForm_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.ExpandCollapseCommandName && !e.Item.Expanded)
            {
                foreach (GridItem item in e.Item.OwnerTableView.Items)
                {
                    if (item.Expanded && item != e.Item)
                    {
                        item.Expanded = false;
                    }
                }

                GridDataItem parentItem = e.Item as GridDataItem;
                RadGrid grdAttribute = parentItem.ChildItem.FindControl("grdAttribute") as RadGrid;

                if (grdAttribute != null)
                    grdAttribute.Rebind();

            }
            else if (e.CommandName == "FilterRadGrid")
            {
                RadFilter1.FireApplyCommand();
            }
        }

        protected void grdForm_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            int formID = (int)editableItem.GetDataKeyValue("FormID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var form = (from u in db.Forms
                            where u.FormID == formID
                            select u).FirstOrDefault();

                if (form != null)
                {
                    //update entity's state
                    editableItem.UpdateValues(form);

                    try
                    {
                        db.SaveChanges();
                        CacheFactory.GetCacheManager().Flush();
                        grdForm.Controls.Add(new LiteralControl(string.Format("Record for form {0} has been updated: {0}", form.FormName)));
                    }
                    catch (Exception ex)
                    {
                        grdForm.Controls.Add(new LiteralControl(string.Format("Error Updating Form: {0}", ex.Message)));
                    }
                }
            }
        }

        protected void grdAttributes_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.ExpandCollapseCommandName && !e.Item.Expanded)
            {
                foreach (GridItem item in e.Item.OwnerTableView.Items)
                {
                    if (item.Expanded && item != e.Item)
                    {
                        item.Expanded = false;
                    }
                }

                GridDataItem parentItem = e.Item as GridDataItem;
                RadGrid grdAttributeTextboxProperties = parentItem.ChildItem.FindControl("grdTextboxProperties") as RadGrid;

                if (grdAttributeTextboxProperties != null)
                    grdAttributeTextboxProperties.Rebind();

            }
        }

        protected void grdAttributes_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            int attributeID = (int)editableItem.GetDataKeyValue("AttributeID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var attribute = (from u in db.Attributes
                                 where u.AttributeID == attributeID
                                 select u).FirstOrDefault();

                if (attribute != null)
                {
                    //update entity's state
                    editableItem.UpdateValues(attribute);

                    try
                    {
                        db.SaveChanges();
                        CacheFactory.GetCacheManager().Flush();

                        grdForm.Controls.Add(new LiteralControl(string.Format("Record for Attribute {0} has been updated: {0}", attribute.AttributeName)));
                    }
                    catch (Exception ex)
                    {
                        grdForm.Controls.Add(new LiteralControl(string.Format("Error Updating Attribute: {0}", ex.Message)));
                    }
                }
            }
        }


        protected string GetFilterIcon()
        {
            return SkinRegistrar.GetWebResourceUrl(Page, typeof(RadGrid), "Telerik.Web.UI.Skins.Vista.Grid.Filter.gif");
        }

        protected string GetAddRecordIcon()
        {
            return SkinRegistrar.GetWebResourceUrl(Page, typeof(RadGrid), "Telerik.Web.UI.Skins.Vista.Grid.AddRecord.gif");
        }

        protected void grdForm_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                int count = (from u in db.Forms
                             select u).Count();

                int startRowIndex = (ShouldApplySortFilterOrGroup()) ? 0 : grdForm.CurrentPageIndex * grdForm.PageSize;
                int maximumRows = (ShouldApplySortFilterOrGroup()) ? count : grdForm.PageSize;

                grdForm.AllowCustomPaging = !ShouldApplySortFilterOrGroup();

                grdForm.DataSource = (from u in db.Forms
                                      select u).OrderBy(form => form.FormID).Skip(startRowIndex).Take(maximumRows).ToList();
            }
        }

        protected void grdAttribute_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                GridDataItem parentItem = ((sender as RadGrid).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;
                int FormID = Convert.ToInt32(parentItem.GetDataKeyValue("FormID"));

                var attributes = (from o in db.Attributes
                                  where o.FormID == FormID
                                  select o).ToList();

                (sender as RadGrid).DataSource = attributes;
            }
        }

        protected void grdTextboxProperties_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                GridDataItem parentItem = ((sender as RadGrid).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;
                int attributeID = Convert.ToInt32(parentItem.GetDataKeyValue("AttributeID"));

                var attributesProperties = (from o in db.AttributeTextboxProperties
                                            where o.AttributeID == attributeID
                                            select o).ToList();

                (sender as RadGrid).DataSource = attributesProperties;
            }
        }

        protected void grdTextboxProperties_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            int attributeID = (int)editableItem.GetDataKeyValue("AttributeID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var prop = (from u in db.AttributeTextboxProperties
                            where u.AttributeID == attributeID
                            select u).FirstOrDefault();

                if (prop != null)
                {
                    //update entity's state
                    editableItem.UpdateValues(prop);

                    try
                    {
                        db.SaveChanges();
                        CacheFactory.GetCacheManager().Flush();
                        grdForm.Controls.Add(new LiteralControl(string.Format("Record for Textbox Property {0} has been updated: {0}", prop.AttributeID)));
                    }
                    catch (Exception ex)
                    {
                        grdForm.Controls.Add(new LiteralControl(string.Format("Error Updating Textbox Property: {0}", ex.Message)));
                    }
                }
            }
        }

        bool isGrouping = false;

        public bool ShouldApplySortFilterOrGroup()
        {
            return grdForm.MasterTableView.FilterExpression != "" ||
                (grdForm.MasterTableView.GroupByExpressions.Count > 0 || isGrouping) ||
                grdForm.MasterTableView.SortExpressions.Count > 0;
        }

        protected void grdForm_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                CallDispositionTool.Model.Form form = new CallDispositionTool.Model.Form();

                form.FormName = values["FormName"] as string;
                form.FormDescription = values["FormDescription"] as string;
                //form.AllowCallBackFeature = values["AllowCallBackFeature"] as bool?;
                //form.AllowContactFeature = values["AllowContactFeature"] as bool?;
                //form.AllowAudienceManager = values["AllowAudienceManager"] as bool?;
                form.HideFromList = values["HideFromList"] as bool?;

                try
                {
                    db.Forms.AddObject(form);
                    db.SaveChanges();
                    grdForm.Controls.Add(new LiteralControl(string.Format("New form for {0} has been inserted", form.FormName)));
                }
                catch (Exception ex)
                {
                    grdForm.Controls.Add(new LiteralControl(string.Format("Error inserting Form: {0}", ex.Message)));
                }
            }
        }

        protected void grdAttributes_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);
            GridDataItem parentItem = ((sender as RadGrid).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;
            int formID = Convert.ToInt32(parentItem.GetDataKeyValue("FormID"));

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                CallDispositionTool.Model.Attribute attribute = new CallDispositionTool.Model.Attribute();

                attribute.FormID = formID;
                attribute.AttributeName = values["AttributeName"] as string;
                attribute.DataTypeID = Convert.ToInt32(values["DataTypeID"]);
                attribute.HideFromList = values["HideFromList"] as bool?;
                attribute.Mandatory = values["Mandatory"] as bool?;
                attribute.SortOrder = Convert.ToInt32(values["SortOrder"]);
                attribute.CreatedOn = DateTime.Now;
                attribute.CreatedBy = Membership.GetUser().ProviderUserKey as int?;

                try
                {
                    db.Attributes.AddObject(attribute);
                    db.SaveChanges();
                    grdForm.Controls.Add(new LiteralControl(string.Format("New Attribute for {0} has been inserted", attribute.AttributeName)));
                }
                catch (Exception ex)
                {
                    grdForm.Controls.Add(new LiteralControl(string.Format("Error inserting Attribute: {0}\nInner Exception: {1}", ex.Message, ex.InnerException.Message)));
                }
            }
        }

        protected void grdTextboxProperties_InsertCommand(object sender, GridCommandEventArgs e)
        {
            var editableItem = ((GridEditableItem)e.Item);
            Hashtable values = new Hashtable();
            editableItem.ExtractValues(values);
            GridDataItem parentItem = ((sender as RadGrid).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;
            int attributeID = Convert.ToInt32(parentItem.GetDataKeyValue("AttributeID"));

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                AttributeTextboxProperty prop = new AttributeTextboxProperty();
                prop.AttributeID = attributeID;
                prop.MaxLength = Convert.ToInt32(values["MaxLength"]);
                prop.TextMode = Convert.ToInt32(values["TextMode"]);
                prop.RegularExpression = values["RegularExpression"] as string;
                prop.ErrorMessage = values["ErrorMessage"] as string;
                prop.CreatedOn = DateTime.Now;
                prop.CreatedBy = Membership.GetUser().ProviderUserKey as int?;

                try
                {
                    db.AttributeTextboxProperties.AddObject(prop);
                    db.SaveChanges();
                    grdForm.Controls.Add(new LiteralControl(string.Format("New Textbox Property for {0} has been inserted", prop.AttributeID)));
                }
                catch (Exception ex)
                {
                    grdForm.Controls.Add(new LiteralControl(string.Format("Error inserting Textbox Property: {0}\nInner Exception: {1}", ex.Message, ex.InnerException.Message)));
                }
            }
        }

        protected void grdForm_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            int formID = (int)((GridDataItem)e.Item).GetDataKeyValue("FormID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var form = (from u in db.Forms
                            where u.FormID == formID
                            select u).FirstOrDefault();

                if (form != null)
                {
                    try
                    {
                        db.Forms.DeleteObject(form);
                        db.SaveChanges();
                        grdForm.Controls.Add(new LiteralControl(string.Format("Form ID {0} has been deleted", form.FormID)));
                    }
                    catch (Exception ex)
                    {
                        grdForm.Controls.Add(new LiteralControl(string.Format("Error deleting User: {0}\nInner Exception: {1}", ex.Message, ex.InnerException.Message)));
                    }
                }
            }
        }

        protected void grdAttribute_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            int attributeID = (int)((GridDataItem)e.Item).GetDataKeyValue("AttributeID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var attribute = (from u in db.Attributes
                                 where u.AttributeID == attributeID
                                 select u).FirstOrDefault();

                if (attribute != null)
                {
                    try
                    {
                        db.Attributes.DeleteObject(attribute);
                        db.SaveChanges();
                        grdForm.Controls.Add(new LiteralControl(string.Format("Attribute ID {0} has been deleted", attribute.AttributeID)));
                    }
                    catch (Exception ex)
                    {
                        grdForm.Controls.Add(new LiteralControl(string.Format("Error deleting Attribute: {0}\nInner Exception: {1}", ex.Message, ex.InnerException.Message)));
                    }
                }
            }
        }

        protected void grdTextBoxProperties_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            int attributeID = (int)((GridDataItem)e.Item).GetDataKeyValue("AttributeID");

            //retrive entity form the Db
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var attribute = (from u in db.AttributeTextboxProperties
                                 where u.AttributeID == attributeID
                                 select u).FirstOrDefault();

                if (attribute != null)
                {
                    try
                    {
                        db.AttributeTextboxProperties.DeleteObject(attribute);
                        db.SaveChanges();
                        grdForm.Controls.Add(new LiteralControl(string.Format("Attribute Textbox Property ID {0} has been deleted", attribute.AttributeID)));
                    }
                    catch (Exception ex)
                    {
                        grdForm.Controls.Add(new LiteralControl(string.Format("Error deleting Attribute: {0}\nInner Exception: {1}", ex.Message, ex.InnerException.Message)));
                    }
                }
            }
        }

        protected void grdForm_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item.ItemType == GridItemType.AlternatingItem || e.Item.ItemType == GridItemType.Item)
            {
                ImageButton EditFormButton = e.Item.FindControl("EditFormButton") as ImageButton;
                var itemData = ((GridDataItem)e.Item);
                var formID = (int)itemData.GetDataKeyValue("FormID");

                if (EditFormButton != null)
                {
                    EditFormButton.OnClientClick = string.Format("return ShowFormProperties('{0}');", formID);
                }
            }
        }
    }
}
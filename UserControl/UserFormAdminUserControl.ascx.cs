﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Linq;
using CallDispositionTool.Model;
using CallDispositionTool.Security;
using System.Collections.Generic;
using Telerik.Web.UI;
using Telerik.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace CallDispositionTool.UserControl
{
    public partial class UserFormAdminUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindListBox();
            }
        }

        internal class FormDataItem
        {
            public int FormID { get; set; }
            public string FormName { get; set; }
        }

        public void BindListBox()
        {
            int UserID;
            int TLUserID;

            if (int.TryParse(Request["UserID"], out UserID) && int.TryParse(Membership.GetUser().ProviderUserKey.ToString(), out TLUserID))
            {
                if (UserID > 0 && TLUserID > 0)
                {
                    using (CallDispositionEntities db = new CallDispositionEntities())
                    {
                        lstSource.DataTextField = "FormName";
                        lstSource.DataValueField = "FormID";
                        lstDestination.DataTextField = "FormName";
                        lstDestination.DataValueField = "FormID";

                        List<FormDataItem> userForms = (from f in db.Forms
                                                        where (from uf in db.UserForms
                                                               where uf.UserID == UserID && uf.Form.HideFromList == false
                                                               select uf.FormID).Contains(f.FormID) && f.HideFromList == false
                                                        orderby f.FormName
                                                        select new FormDataItem() { FormID = f.FormID, FormName = f.FormName }).ToList<FormDataItem>();

                        if (Helper.IsUserAdmin(TLUserID))
                        {
                            lstSource.DataSource = (from f in db.Forms
                                                    where !(from uf in db.UserForms
                                                            where uf.UserID == UserID && uf.Form.HideFromList == false
                                                            select uf.FormID).Any<Int32>(uf => uf == f.FormID) && f.HideFromList == false
                                                    orderby f.FormName
                                                    select new FormDataItem() { FormID = f.FormID, FormName = f.FormName }).ToList();
                        }
                        else
                        {
                            lstSource.DataSource = (from f in db.Forms
                                                    where (from f1 in db.UserForms
                                                           where f1.UserID == TLUserID && f1.Form.HideFromList == false && !(from uf in db.UserForms
                                                                                                                             where uf.UserID == UserID && uf.Form.HideFromList == false
                                                                                                                             select uf.FormID).Any<Int32>(uf => uf == f.FormID)
                                                           select f1.FormID).Contains(f.FormID)
                                                    orderby f.FormName
                                                    select new FormDataItem() { FormID = f.FormID, FormName = f.FormName }).ToList();
                        }

                        lstSource.DataBind();

                        lstDestination.DataSource = userForms;
                        lstDestination.DataBind();
                    }
                }
            }
        }

        protected void lstDestination_OnInserted(object sender, RadListBoxEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                int CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                int UserID;
                int FormID;

                if (int.TryParse(Request["UserID"], out UserID))
                {
                    if (UserID > 0)
                    {
                        foreach (RadListBoxItem item in e.Items)
                        {
                            UserForm uf = new UserForm();
                            FormID = Convert.ToInt32(item.Value);

                            uf.FormID = FormID;
                            uf.UserID = UserID;
                            uf.CreatedBy = CreatedBy;
                            uf.CreatedOn = DateTime.Now;
                            db.UserForms.AddObject(uf);
                        }
                    }
                }

                CacheFactory.GetCacheManager().Remove("GetFormsByUser~" + UserID.ToString());
                db.SaveChanges();
            }
        }

        protected void lstSource_OnInserted(object sender, RadListBoxEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                int CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                int UserID;
                int FormID;

                if (int.TryParse(Request["UserID"], out UserID))
                {
                    if (UserID > 0)
                    {
                        foreach (RadListBoxItem item in e.Items)
                        {
                            FormID = Convert.ToInt32(item.Value);

                            UserForm uf = (from f in db.UserForms
                                           where f.FormID == FormID && f.UserID == UserID
                                           select f).FirstOrDefault();

                            if (uf != null)
                            {
                                db.UserForms.DeleteObject(uf);
                            }
                        }
                    }
                }

                CacheFactory.GetCacheManager().Remove("GetFormsByUser~" + UserID.ToString());
                db.SaveChanges();
            }
        }
    }
}
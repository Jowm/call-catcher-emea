﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallDispositionTool.UserControl
{
    public partial class CustomerSatisfactionUserControl : System.Web.UI.UserControl
    {
        public int AttributeID { get; set; }
        public string AttributeName { get; set; }

        public string CustomerSatisfaction
        {
            get { return radCustomerSatisfaction.SelectedValue; }
        }

        public void Reset()
        {
            radCustomerSatisfaction.SelectedIndex = -1;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}
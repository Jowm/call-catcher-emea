﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallDispositionTool.UserControl
{
    public partial class AssignToUserControl : System.Web.UI.UserControl
    {
        public int AttributeID { get; set; }
        public string AttributeName { get; set; }

        public string AssignTo
        {
            set { tbAssignToUserControl.Text = value; }
            get { return tbAssignToUserControl.Text; }
        }

        public void Reset()
        {
            tbAssignToUserControl.Text = "";
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.UI.WebControls;
using System.Linq;
using CallDispositionTool.Model;
using System.Web.Security;
using System.Web.UI;
using System.Collections.Generic;
using Telerik.Web.UI;
using System.Web.Configuration;

namespace CallDispositionTool.UserControl
{
    public partial class CallBackDockUserControl : System.Web.UI.UserControl
    {
        public int UserID { get { return Convert.ToInt32(Membership.GetUser().ProviderUserKey); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindCallBackCountRepeater();
            }
        }

        void BindCallBackCountRepeater()
        {
            CallDispositionEntities db = new CallDispositionEntities();
            var query = (from p in db.CallBacks
                         where (p.CreatedBy == this.UserID || p.AssignedTo == this.UserID) && (p.CallBackStatu.CallBackStatus == "Outstanding" || !p.CallBackStatusID.HasValue)
                         orderby p.CallBackID descending
                         select new { p.CallBackID, p.CallBackReason.Description, p.ContactNumber }).ToList().Take(10);

            repCallback.DataSource = query;
            repCallback.DataBind();
        }

        protected void repCallBack_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            Repeater rptDemo = sender as Repeater; // Get the Repeater control object.

            // If the Repeater contains no data.
            if (repCallback != null && repCallback.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    // Show the Error Label (if no data is present).
                    Label lblErrorMsg = e.Item.FindControl("lblErrorMsg") as Label;

                    if (lblErrorMsg != null)
                    {
                        lblErrorMsg.Visible = true;
                    }
                }
            }
        }
    }
}
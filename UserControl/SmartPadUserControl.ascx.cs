﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using CallDispositionTool.Model;
using System.Globalization;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.ComponentModel;
using System.Collections;
using System.Web.Security;
using System.Data;

namespace CallDispositionTool.UserControl
{
    public partial class SmartPadUserControl : System.Web.UI.UserControl
    {
        public string rdoValueDuring
        {
            get
            {
                if (ViewState["rdoValue"] != null)
                    return ViewState["rdoValue"].ToString();
                return string.Empty;
            }
            set { ViewState["rdoValue"] = value; }
        }

        public string rdoValue2
        {
            get
            {
                if (ViewState["rdoValue2"] != null)
                    return ViewState["rdoValue2"].ToString();
                return string.Empty;
            }
            set { ViewState["rdoValue2"] = value; }
        }


        private string durationInMinutes
        {
            get
            {
                if (ViewState["durationInMinutes"] != null)
                    return ViewState["durationInMinutes"].ToString();
                return string.Empty;
            }
            set
            {
                ViewState["durationInMinutes"] = value;
            }
        }

        public string savingValue
        {
            get
            {
                if (ViewState["_savingValue"] != null)
                    return ViewState["_savingValue"].ToString();
                return string.Empty;
            }
            set
            {
                ViewState["_savingValue"] = value;
            }
        }
        private int activeDropDown
        {
            get
            {
                if (ViewState["activeDropDown"] != null)
                    return Convert.ToInt32(ViewState["activeDropDown"]);
                return 0;
            }
            set
            {
                ViewState["activeDropDown"] = value;
            }
        }

        public int AttributeID { get; set; }
        public string AttributeName { get; set; }

        //List<CallDispositionTool.Model.Attribute> _Attributes;


        public DataTable dtCallTypes
        {
            get
            {
                return ViewState["dtCallTypes"] as DataTable;
            }
            set
            {
                ViewState["dtCallTypes"] = value;
            }
        }

        int UPS_SmartPad = Convert.ToInt32(ConfigurationManager.AppSettings["SmartPadFormID"]);
        int UK_SmartPad = Convert.ToInt32(ConfigurationManager.AppSettings["UKSmartPadFormID"]);

        private int formID
        {
            get
            {
                if (ViewState["formID"] != null)
                    return Convert.ToInt32(ViewState["formID"]);
                return 0;
            }
            set
            {
                ViewState["formID"] = value;
            }
        }

        #region .Events


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                formID = Convert.ToInt32(Request.QueryString["FormID"]);
                if (UPS_SmartPad == formID)
                {
                    var attribID = Convert.ToInt32(ConfigurationManager.AppSettings["CallTypeAttributeID"]);
                    var ShippingID = Convert.ToInt32(ConfigurationManager.AppSettings["ShippingID"]);
                    var TrackingID = Convert.ToInt32(ConfigurationManager.AppSettings["TrackingID"]);
                    var UniversalID = Convert.ToInt32(ConfigurationManager.AppSettings["UniversalID"]);
                    var TransferID = Convert.ToInt32(ConfigurationManager.AppSettings["TransferID"]);
                    GetDropdownPicklistOption(attribID, cboShipping, ShippingID, 1);
                    GetDropdownPicklistOption(attribID, cboTracking, TrackingID, 1);
                    GetDropdownPicklistOption(attribID, cboUniversal, UniversalID, 1);
                    GetDropdownPicklistOption(attribID, cboTransfer, TransferID, 1);
                    pnlUK.Visible = false;
                    rowShipping.Visible = false;
                }
                else
                {
                    var attribID = Convert.ToInt32(ConfigurationManager.AppSettings["UKCallTypeAttributeID"]);
                    var ShippingID = Convert.ToInt32(ConfigurationManager.AppSettings["UKShippingID"]);
                    var TrackingID = Convert.ToInt32(ConfigurationManager.AppSettings["UKTrackingID"]);
                    var TransferID = Convert.ToInt32(ConfigurationManager.AppSettings["UKTransferID"]);
                    GetDropdownPicklistOption(attribID, cboShipping, ShippingID, 1);
                    GetDropdownPicklistOption(attribID, cboTracking, TrackingID, 1);
                    GetDropdownPicklistOption(attribID, cboTransfer, TransferID, 1);
                    pnlUK.Visible = true;
                    rowUniversal.Visible = false;
                }



                InitCallTypesDT();
            }
        }


        protected void rdoID_CheckedChanged(object sender, EventArgs e)
        {
            var rdoName = ((RadioButton)sender).ID;
            rdoValueDuring = rdoName.Remove(0, 3);
            SetMessage(rdoValueDuring);
        }

        protected void cbo_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var cboName = ((RadComboBox)sender).ID;
            var cboSelectedText = ((RadComboBox)sender).SelectedItem.Text;


            if (cboSelectedText != string.Empty)
            {
                clearDropDownSelectionLinkText(cboName);
            }

            var dispositionID = 0;
            var strLink = "";
            switch (activeDropDown)
            {
                case 1:
                    dispositionID = Convert.ToInt32(cboShipping.SelectedValue);
                    strLink = getDecisionLink(dispositionID);
                    lnkShipping.Text = strLink;
                    lnkShipping.OnClientClick = "window.open('" + strLink + "');";
                    break;
                case 2:
                    dispositionID = Convert.ToInt32(cboTracking.SelectedValue);
                    strLink = getDecisionLink(dispositionID);
                    lnkTracking.Text = strLink;
                    lnkTracking.OnClientClick = "window.open('" + strLink + "');";
                    break;
                case 3:
                    dispositionID = Convert.ToInt32(cboUniversal.SelectedValue);
                    strLink = getDecisionLink(dispositionID);
                    lnkUniversal.Text = strLink;
                    lnkUniversal.OnClientClick = "window.open('" + strLink + "');";
                    break;
                case 4:
                    dispositionID = Convert.ToInt32(cboTransfer.SelectedValue);
                    strLink = getDecisionLink(dispositionID);
                    lnkUniversal.Text = strLink;
                    lnkUniversal.OnClientClick = "window.open('" + strLink + "');";
                    break;
            }


            //2nd Layer
            var selectedcbo = new RadComboBox();
            switch (cboName)
            {
                case "cboShipping":
                    selectedcbo = cboShipping2;
                    break;
                case "cboTracking":
                    selectedcbo = cboTracking2;
                    break;
                case "cboUniversal":
                    selectedcbo = cboUniversal2;
                    break;
                case "cboTransfer":
                    selectedcbo = cboTransfer2;
                    break;
            }

            selectedcbo.Items.Clear();

            int attribID;

            if (UPS_SmartPad == formID)
            {
                attribID = Convert.ToInt32(ConfigurationManager.AppSettings["CallTypeAttributeID"]);

            }
            else
            {
                attribID = Convert.ToInt32(ConfigurationManager.AppSettings["UKCallTypeAttributeID"]);

            }
            GetDropdownPicklistOption(attribID, selectedcbo, dispositionID, 2);
            selectedcbo.ClearSelection();
            selectedcbo.Text = string.Empty;

            if (selectedcbo.Items.Count > 0)
            {
                selectedcbo.Visible = true;
            }
            else
            {
                if (cboSelectedText != string.Empty)
                {
                    AppendToTextBox(cboName, cboSelectedText);
                }
                selectedcbo.Visible = false;

                if (rdoAngry.Checked || rdoSad.Checked || rdoNormal.Checked || rdoHappy.Checked || rdoEcstatic.Checked)
                {
                    labelMessage.Text = GetFormattedMessage(getTip(dispositionID, rdoValueDuring), rdoValueDuring);
                    var btnSubmit = (Button)this.Parent.Parent.Parent.FindControl("btnSubmitSmartPad");// FindControl(this.Parent.Parent.Parent.Parent.Controls,
                    if (dtCallTypes.Rows.Count > 0)
                    {

                        btnSubmit.Enabled = true;
                    }

                }


            }



        }

        protected void cbo_SelectedIndexChanged2(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var cboName = ((RadComboBox)sender).ID;
            var cboSelectedText = ((RadComboBox)sender).SelectedItem.Text;

            if (cboSelectedText != string.Empty)
            {
                clearDropDownSelectionLinkText(cboName);
                AppendToTextBox(cboName, cboSelectedText);
            }

            var dispositionID = 0;

            switch (activeDropDown)
            {
                case 1:
                    dispositionID = Convert.ToInt32(cboShipping.SelectedValue);
                    break;
                case 2:
                    dispositionID = Convert.ToInt32(cboTracking.SelectedValue);
                    break;
                case 3:
                    dispositionID = Convert.ToInt32(cboUniversal.SelectedValue);
                    break;
                case 4:
                    dispositionID = Convert.ToInt32(cboTransfer.SelectedValue);
                    break;
            }

            if (rdoAngry.Checked || rdoSad.Checked || rdoNormal.Checked || rdoHappy.Checked || rdoEcstatic.Checked)
            {
                labelMessage.Text = GetFormattedMessage(getTip(dispositionID, rdoValueDuring), rdoValueDuring);

                var btnSubmit = (Button)this.Parent.Parent.Parent.FindControl("btnSubmitSmartPad");// FindControl(this.Parent.Parent.Parent.Parent.Controls,
                if (dtCallTypes.Rows.Count > 0)
                {

                    btnSubmit.Enabled = true;
                }

            }
        }

        protected void img_Click(object sender, ImageClickEventArgs e)
        {
            var imgCtrl = ((ImageButton)sender);
            var isEmoticonTip = imgCtrl.ID.Substring(imgCtrl.ID.Length - 1) == "2" ? false : true;
            var rdoValueTemp = string.Empty;

            rdoValueTemp = imgCtrl.ID.Remove(0, 3);

            if (isEmoticonTip)
            {
                rdoValueDuring = rdoValueTemp;
                SetMessage(rdoValueDuring);


                var rdo = (RadioButton)imgCtrl.Parent.FindControl("rdo" + rdoValueTemp);
                clearRadioSelection(isEmoticonTip);
                rdo.Checked = true;

            }
        }


        protected void btnDropCall_Click(object sender, EventArgs e)
        {
            setSavingValueDuring();

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                Transaction tran = new Transaction();
                CallDispositionTool.Model.CallBack cb = new CallDispositionTool.Model.CallBack();
                CallBackHistoryLog log = new CallBackHistoryLog();
                TransactionCallData callData = new TransactionCallData();


                tran.FormID = formID;
                tran.CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                tran.CreatedOn = DateTime.Now;
                tran.AttributeValues.Add(GetValueForAttribute());
                db.Transactions.AddObject(tran);

                try
                {

                    db.SaveChanges();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "RefreshPage", "window.location.reload();", true);

                }
                catch (Exception ex)
                {
                    Helper.LogError(ex);
                }
            }
        }


        protected virtual void chkDiscussed_OnCheckedChanged(Object sender, EventArgs e)
        {
            CheckBox chkDiscussed = sender as CheckBox;
            pnlSmart2.Visible = chkDiscussed.Checked;
            rfvSentiment.Enabled = chkDiscussed.Checked;
            rfvClassification.Enabled = chkDiscussed.Checked;
            rfvAccountHolder.Enabled = chkDiscussed.Checked;
            rfvFeeProcess.Enabled = chkDiscussed.Checked;
            rfvApplicable.Enabled = chkDiscussed.Checked;
            rfvNotApplicable.Enabled = chkDiscussed.Checked;
            rfvApplicableNonAcct.Enabled = chkDiscussed.Checked;

            if (pnlSmart2.Visible)
            {
                var attribID = Convert.ToInt32(ConfigurationManager.AppSettings["UKCallTypeAttributeID"]);
                var classificationID = Convert.ToInt32(ConfigurationManager.AppSettings["UKClassificationID"]);
                var sentimentID = Convert.ToInt32(ConfigurationManager.AppSettings["UKSentimentID"]);
                var accountHolderID = Convert.ToInt32(ConfigurationManager.AppSettings["UKAccountHolderID"]);
                var FeeProcessID = Convert.ToInt32(ConfigurationManager.AppSettings["UKFeeProcessID"]);
                var FeeApplicableID = Convert.ToInt32(ConfigurationManager.AppSettings["UKFeeApplicableID"]);
                var FeeNotApplicableID = Convert.ToInt32(ConfigurationManager.AppSettings["UKFeeNotApplicableID"]);
                var FeeApplicableNonAcct = Convert.ToInt32(ConfigurationManager.AppSettings["UKFeeApplicableNonAcctID"]);

                GetDropdownPicklistOption(attribID, cboClassification, classificationID, 3);
                GetDropdownPicklistOption(attribID, cboSentiment, sentimentID, 3);
                GetDropdownPicklistOption(attribID, cboAccountHolder, accountHolderID, 3);
                GetDropdownPicklistOption(attribID, cboFeeProcess, FeeProcessID, 3);
                GetDropdownPicklistOption(attribID, cboFeeApplicable, FeeApplicableID, 3);
                GetDropdownPicklistOption(attribID, cboFeeNotApplicable, FeeNotApplicableID, 3);
                GetDropdownPicklistOption(attribID, cboFeeApplicableNonAcct, FeeApplicableNonAcct, 3);
            }
        }


        protected void gridCallTypes_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            gridCallTypes.DataSource = dtCallTypes;
        }

        protected void gridCallTypes_ItemCommand(object sender, GridCommandEventArgs e)
        {

            if (e.CommandName == RadGrid.DeleteCommandName)
            {
                var deletedItem = e.Item as GridEditableItem;

                if (deletedItem != null)
                {
                    var ID = Convert.ToInt32(deletedItem.GetDataKeyValue("ID"));
                    var rows = dtCallTypes.Select("ID= '" + ID + "'");
                    foreach (var row in rows)
                        row.Delete();

                }

                if (dtCallTypes.Rows.Count == 0)
                {
                    var btnSubmit = (Button)this.Parent.Parent.Parent.FindControl("btnSubmitSmartPad");// FindControl(this.Parent.Parent.Parent.Parent.Controls,
                    btnSubmit.Enabled = false;
                }
            }
        }

        #endregion

        #region .Methods

        private void setSavingValueDuring()
        {

            var defaultTime = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd 00:00:00.000"));
            DateTime TimeStart = defaultTime,
                TimeEnd = defaultTime;

            if (hdnStart.Value != string.Empty || hdnStartDrop.Value != string.Empty)
            {
                TimeStart = Convert.ToDateTime(hdnStart.Value == "" ? hdnStartDrop.Value : hdnStart.Value);
                TimeEnd = Convert.ToDateTime(hdnEnd.Value);
            }
            TimeSpan span = new TimeSpan();
            span = TimeEnd.Subtract(TimeStart);

            durationInMinutes = string.Format("{0:D2}:{1:D2}:{2:D2}", span.Minutes, span.Seconds, span.Milliseconds);

            var savingValueTemp = durationInMinutes;
            var strCallType = string.Empty;
            var strIssue = string.Empty;
            var country = chkDiscussed.Checked == true ? cboCountry.SelectedItem.Text : "N/A";
            var site = chkDiscussed.Checked == true ? cboSite.SelectedItem.Text : "N/A";
            var sentiment = chkDiscussed.Checked == true ? cboSentiment.SelectedItem.Text : "N/A";
            var accountholder = chkDiscussed.Checked == true ? cboAccountHolder.SelectedItem.Text : "N/A";
            var classification = chkDiscussed.Checked == true ? cboClassification.SelectedItem.Text : "N/A";
            var ECMNumber = chkDiscussed.Checked == true ? txtECMCaseNumber.Text : "N/A";

            var FeeProcess = chkDiscussed.Checked == true ? cboFeeProcess.SelectedItem.Text : "N/A";
            var FeeApplicable = chkDiscussed.Checked == true ? cboFeeApplicable.SelectedItem.Text : "N/A";
            var FeeNotApplicable = chkDiscussed.Checked == true ? cboFeeNotApplicable.SelectedItem.Text : "N/A";
            var FeeApplicableNonAcct = chkDiscussed.Checked == true ? cboFeeApplicableNonAcct.SelectedItem.Text : "N/A";

            var isPickupFeeDiscussed = chkDiscussed.Checked == true ? "Yes" : "No";

            ECMNumber = ECMNumber.Trim() == "" ? "None" : ECMNumber;
            var strPickupFee = "PickupFeeDiscussed: " + isPickupFeeDiscussed + "\\Country: " + country + "\\Site: " + site + "\\Sentiment: " +
                sentiment + "\\AccountHolder: " + accountholder + "\\Classification: " + classification + "\\ECMNumber: " + ECMNumber + "\\FeeProcess: " +
                FeeProcess + "\\FeeApplicable: " + FeeApplicable + "\\FeeNotApplicable: " + FeeNotApplicable + "\\FeeApplicableNonAcct: " + FeeApplicableNonAcct + "\\";

            if (dtCallTypes.Rows.Count > 0)
            {
                foreach (DataRow row in dtCallTypes.Rows)
                {
                    strCallType = row["CallType"].ToString();
                    strIssue = row["Issue"].ToString();
                    savingValueTemp += "\\Call Type: " + strCallType + "\\Issue: " + strIssue + "\\Case Notes:\n";
                }
            }
            else
            {
                savingValueTemp += "\\Call Type: \\Issue: \\Case Notes:\n";
            }

            rdoValueDuring = rdoValueDuring == string.Empty ? "None" : rdoValueDuring;
            rdoValue2 = rdoValue2 == string.Empty ? "None" : rdoValue2;

            savingValueTemp += txtCaseNotes.Text + "\\" + rdoValueDuring + "\\" + rdoValue2;
            savingValue = "Dropped" + "\\" + savingValueTemp;


            if (formID == UK_SmartPad)
            {
                savingValue = strPickupFee + savingValue;
            }
        }

        private void AppendToTextBox(string cboName, string cboSelectedText)
        {
            AddCallType(cboName.Replace("2", "").Replace("cbo", ""), cboSelectedText);
        }

        private void GetDropdownPicklistOption(int AttributeID, RadComboBox ddl, int dispositionID, int layer)
        {
            ddl.ClearSelection();
            ddl.DataSource = null;
            ddl.Items.Clear();
            ddl.DataBind();

            dynamic items;
            if (layer == 1)
            {
                CacheManager _objCacheManager = CacheFactory.GetCacheManager();
                if (_objCacheManager.Contains("DispoFilterDataSource~0~" + AttributeID.ToString()))
                {
                    items = _objCacheManager.GetData("DispoFilterDataSource~0~" + AttributeID.ToString());
                }
                else
                {
                    using (CallDispositionEntities db = new CallDispositionEntities())
                    {
                        items = (from i in db.Dispositions
                                 where i.AttributeID == AttributeID && i.ParentDispositionID == dispositionID && (i.HideFromList == false || i.HideFromList == null)
                                 orderby i.FilterDispositionID ascending
                                 select new { i.Description, i.DispositionID }).ToList();

                        _objCacheManager.Add("DispoFilterDataSource~~" + AttributeID.ToString(), items);
                    }
                }

                foreach (var item in items)
                {
                    RadComboBoxItem radBoxItem = new RadComboBoxItem();
                    string name = Convert.ToString(item.Description);
                    if (name.Contains("(DT)"))
                    {
                        radBoxItem.Text = name.Replace(" (DT)", "");
                        radBoxItem.ImageUrl = @"~/images/Call Driver Icons/Desicion Tree.png";
                    }
                    else
                    {
                        radBoxItem.Text = name;
                    }
                    radBoxItem.Value = Convert.ToString(item.DispositionID);
                    ddl.Items.Add(radBoxItem);
                    radBoxItem.DataBind();
                }
            }
            else if (layer == 2)
            {

                CacheManager _objCacheManager = CacheFactory.GetCacheManager();
                if (_objCacheManager.Contains("DispoFilterDataSource~1~" + AttributeID.ToString()))
                {
                    items = _objCacheManager.GetData("DispoFilterDataSource~1~" + AttributeID.ToString());
                }
                else
                {
                    using (CallDispositionEntities db = new CallDispositionEntities())
                    {
                        items = (from i in db.Dispositions
                                 where i.AttributeID == AttributeID && i.ParentDispositionID == dispositionID && (i.HideFromList == false || i.HideFromList == null) && i.Description.Contains("(2)")
                                 orderby i.FilterDispositionID ascending
                                 select new { i.Description, i.DispositionID }).ToList();

                    }
                }

                foreach (var item in items)
                {
                    RadComboBoxItem radBoxItem = new RadComboBoxItem();
                    string name = Convert.ToString(item.Description);

                    radBoxItem.Text = name.Replace(" (2)", "");
                    radBoxItem.Value = Convert.ToString(item.DispositionID);
                    ddl.Items.Add(radBoxItem);
                    radBoxItem.DataBind();
                }

            }
            else
            {

                CacheManager _objCacheManager = CacheFactory.GetCacheManager();
                if (_objCacheManager.Contains("DispoFilterDataSource~2~" + AttributeID.ToString()))
                {
                    items = _objCacheManager.GetData("DispoFilterDataSource~2~" + AttributeID.ToString());
                }
                else
                {
                    using (CallDispositionEntities db = new CallDispositionEntities())
                    {
                        items = (from i in db.Dispositions
                                 where i.AttributeID == AttributeID && i.ParentDispositionID == dispositionID && (i.HideFromList == false || i.HideFromList == null)
                                 orderby i.FilterDispositionID ascending
                                 select new { i.Description, i.DispositionID }).ToList();

                    }
                }

                foreach (var item in items)
                {
                    RadComboBoxItem radBoxItem = new RadComboBoxItem();
                    string name = Convert.ToString(item.Description);

                    radBoxItem.Text = name;
                    radBoxItem.Value = name;
                    ddl.Items.Add(radBoxItem);
                    radBoxItem.DataBind();
                }
            }

        }

        public string GetFormattedMessage(string message, string messageType = "")
        {
            var str = string.Empty;

            if (message != string.Empty && message != null)
            {
                str = "<div class='" + messageType + "'>" + message + "</div>";
            }
            return str;
        }

        private void clearRadioSelection(bool isEmoticonTip)
        {
            if (isEmoticonTip)
            {
                rdoAngry.Checked = false;
                rdoSad.Checked = false;
                rdoNormal.Checked = false;
                rdoHappy.Checked = false;
                rdoEcstatic.Checked = false;
            }
        }

        private void SetMessage(string rdoValue)
        {
            var dispositionID = 0;
            switch (activeDropDown)
            {
                case 1:
                    dispositionID = Convert.ToInt32(cboShipping.SelectedValue);
                    break;
                case 2:
                    dispositionID = Convert.ToInt32(cboTracking.SelectedValue);
                    break;
                case 3:
                    dispositionID = Convert.ToInt32(cboUniversal.SelectedValue);
                    break;
                case 4:
                    dispositionID = Convert.ToInt32(cboTransfer.SelectedValue);
                    break;

            }
            if (activeDropDown != 0)
            {
                labelMessage.Text = GetFormattedMessage(getTip(dispositionID, rdoValue), rdoValue);
                var btnSubmit = (Button)this.Parent.Parent.Parent.FindControl("btnSubmitSmartPad");// FindControl(this.Parent.Parent.Parent.Parent.Controls,
                if (dtCallTypes.Rows.Count > 0)
                {

                    btnSubmit.Enabled = true;
                }
            }
        }

        private void clearDropDownSelectionLinkText(string selectedDropDown)
        {
            switch (selectedDropDown)
            {
                case "cboShipping":
                case "cboShipping2":
                    cboTracking.ClearSelection();
                    cboTracking2.ClearSelection();
                    cboTracking2.Visible = false;
                    cboUniversal.ClearSelection();
                    cboUniversal2.ClearSelection();
                    cboUniversal2.Visible = false;
                    cboTransfer.ClearSelection();
                    cboTransfer2.ClearSelection();
                    cboTransfer2.Visible = false;
                    activeDropDown = 1;
                    break;
                case "cboTracking":
                case "cboTracking2":
                    cboShipping.ClearSelection();
                    cboShipping2.ClearSelection();
                    cboShipping2.Visible = false;
                    cboUniversal.ClearSelection();
                    cboUniversal2.ClearSelection();
                    cboUniversal2.Visible = false;
                    cboTransfer.ClearSelection();
                    cboTransfer2.ClearSelection();
                    cboTransfer2.Visible = false;
                    activeDropDown = 2;
                    break;
                case "cboUniversal":
                case "cboUniversal2":
                    cboShipping.ClearSelection();
                    cboShipping2.ClearSelection();
                    cboShipping2.Visible = false;
                    cboTracking.ClearSelection();
                    cboTracking2.ClearSelection();
                    cboTracking2.Visible = false;
                    cboTransfer.ClearSelection();
                    cboTransfer2.ClearSelection();
                    cboTransfer2.Visible = false;
                    activeDropDown = 3;
                    break;
                case "cboTransfer":
                case "cboTransfer2":
                    cboUniversal.ClearSelection();
                    cboUniversal2.ClearSelection();
                    cboUniversal2.Visible = false;
                    cboTracking.ClearSelection();
                    cboTracking2.ClearSelection();
                    cboTracking2.Visible = false;
                    cboShipping.ClearSelection();
                    cboShipping2.ClearSelection();
                    cboShipping2.Visible = false;
                    activeDropDown = 4;
                    break;
            }
            if (!selectedDropDown.Contains("2"))
            {
                lnkTracking.Text = "";
                lnkTracking.OnClientClick = "";
                lnkShipping.Text = "";
                lnkShipping.OnClientClick = "";
                lnkUniversal.Text = "";
                lnkUniversal.OnClientClick = "";
                lnkTransfer.Text = "";
                lnkTransfer.OnClientClick = "";
            }
        }

        protected string getTip(int parentID, string emotion)
        {
            var number = string.Empty;
            var retVal = string.Empty;
            switch (emotion)
            {
                case "Angry":
                    emotion = "1";
                    break;
                case "Sad":
                    emotion = "2";
                    break;
                case "Normal":
                    emotion = "3";
                    break;
                case "Happy":
                    emotion = "4";
                    break;
                case "Ecstatic":
                    emotion = "5";
                    break;
            }

            using (CallDispositionEntities db = new CallDispositionEntities())
            {

                retVal = (from i in db.Dispositions
                          where i.Description.Substring(0, 1) == emotion && i.ParentDispositionID == parentID && (i.HideFromList == false || i.HideFromList == null)
                          select i.Description.Substring(3)
                         ).FirstOrDefault();
            }
            return retVal;

        }

        private static T FindControl<T>(ControlCollection controls, string controlId)
        {
            T ctrl = default(T);
            foreach (Control ctl in controls)
            {
                if (ctl.ClientID.Length <= controlId.Length)
                {
                    if (ctl.GetType() == typeof(T) && ctl.ClientID == controlId)
                    {
                        ctrl = (T)Convert.ChangeType(ctl, typeof(T), CultureInfo.InvariantCulture);
                        return ctrl;

                    }
                }
                else
                {
                    if (ctl.GetType() == typeof(T) && ctl.ClientID.Substring(ctl.ClientID.Length - controlId.Length) == controlId)
                    {
                        ctrl = (T)Convert.ChangeType(ctl, typeof(T), CultureInfo.InvariantCulture);
                        return ctrl;

                    }
                }

                if (ctl.Controls.Count > 0 && ctrl == null)
                    ctrl = FindControl<T>(ctl.Controls, controlId);
            }
            return ctrl;
        }

        protected string getDecisionLink(int parentID)
        {
            var retVal = string.Empty;
            using (CallDispositionEntities db = new CallDispositionEntities())
            {

                retVal = (from i in db.Dispositions
                          where i.Description.StartsWith("http") && i.ParentDispositionID == parentID && (i.HideFromList == false || i.HideFromList == null)
                          select i.Description
                         ).FirstOrDefault();
            }
            return retVal;

        }

        private void InitCallTypesDT()
        {
            dtCallTypes = new DataTable();

            var column = new DataColumn
            {
                DataType = Type.GetType("System.Int32"),
                ColumnName = "ID",
                AutoIncrement = true,
                AutoIncrementSeed = 1,
                AutoIncrementStep = 1
            };

            dtCallTypes.Columns.Add(column);
            dtCallTypes.Columns.Add("CallType", typeof(String));
            dtCallTypes.Columns.Add("Issue", typeof(String));
        }

        private void AddCallType(string callType, string issue)
        {
            DataRow dr = dtCallTypes.NewRow();
            dr["CallType"] = callType;
            dr["Issue"] = issue;
            dtCallTypes.Rows.Add(dr);
            gridCallTypes.Rebind();
        }

        AttributeValue GetValueForAttribute()
        {

            var attribID = formID == UPS_SmartPad ? Convert.ToInt32(ConfigurationManager.AppSettings["SmartPadAttributeID"]) : Convert.ToInt32(ConfigurationManager.AppSettings["UKSmartPadAttributeID"]);
            AttributeValue value = new AttributeValue();
            value.AttributeID = attribID;
            value.Value = savingValue;
            return value;
        }

        #endregion
    }
}

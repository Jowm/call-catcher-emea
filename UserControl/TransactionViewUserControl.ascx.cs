﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using CallDispositionTool.UserControl;
using Telerik.Web.UI;
using System.Web.Security;
using System.Web.Configuration;

namespace CallDispositionTool.UserControl
{
    public partial class TransactionViewUserControl : System.Web.UI.UserControl
    {
        List<CallDispositionTool.Model.Attribute> _Attributes;

        public decimal TransactionID { get; set; }
        public decimal CallBackID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (CallBackID > 0)
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    var query = (from p in db.CallBacks
                                 where p.CallBackID == CallBackID
                                 select new { p.TransactionID, p.Transaction.FormID }).FirstOrDefault();

                    int formID = query.FormID;
                    TransactionID = query.TransactionID;

                    _Attributes = (from p in db.Attributes
                                   where p.FormID == formID
                                   select p).ToList<CallDispositionTool.Model.Attribute>();

                    foreach (CallDispositionTool.Model.Attribute item in _Attributes)
                    {
                        AddCustomAttribute(item);
                    }
                }
            }
        }

        void AddCustomAttribute(CallDispositionTool.Model.Attribute attribute)
        {
            //Add a row to CustomUITable
            TableRow tr = new TableRow();
            TableCell tdName = new TableCell();

            //Add the name as the left cell
            tdName.Text = "<h4>" + attribute.AttributeName + "</h4>";
            tdName.VerticalAlign = VerticalAlign.Middle;
            tdName.Width = new Unit("100px");
            tr.Cells.Add(tdName);

            //Add the UI as the right cell
            //List<Control> UIControls = CreateCustomAttributeUI(AttributeID, AttributeName, DataTypeID, Mandatory, AttributeValue);
            //TableCell tdUI = new TableCell();

            //tdUI.VerticalAlign = VerticalAlign.Middle;

            //foreach (Control ctrl in UIControls)
            //{
            //    tdUI.Controls.Add(ctrl);
            //}

            //tr.Cells.Add(tdUI);

            CustomUITable.Rows.Add(tr);
        }

    }
}
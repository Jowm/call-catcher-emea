﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Linq;
using CallDispositionTool.Model;
using CallDispositionTool.Security;
using System.Collections.Generic;
using Telerik.Web.UI;
using Telerik.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;

namespace CallDispositionTool.UserControl
{
    public partial class UserRoleAdminUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindListBox();
            }
        }

        internal class RoleDataItem
        {
            public int RoleID { get; set; }
            public string Role { get; set; }
        }

        public void BindListBox()
        {
            int UserID;
            int TLUserID;

            if (int.TryParse(Request["UserID"], out UserID) && int.TryParse(Membership.GetUser().ProviderUserKey.ToString(), out TLUserID))
            {
                if (UserID > 0 && TLUserID > 0)
                {
                    using (CallDispositionEntities db = new CallDispositionEntities())
                    {
                        List<RoleDataItem> userRoles;

                        if (Helper.IsUserAdmin(TLUserID))
                        {
                            userRoles = (from f in db.Roles
                                         where (from uf in db.UserRoles
                                                where uf.UserID == UserID
                                                select uf.RoleID).Contains(f.RoleID)
                                         orderby f.RoleName
                                         select new RoleDataItem() { RoleID = f.RoleID, Role = f.RoleName }).ToList<RoleDataItem>();
                        }
                        else
                        {
                            userRoles = (from f in db.Roles
                                         where (from uf in db.UserRoles
                                                where uf.UserID == UserID
                                                select uf.RoleID).Contains(f.RoleID) && f.HideFromList == false
                                         orderby f.RoleName
                                         select new RoleDataItem() { RoleID = f.RoleID, Role = f.RoleName }).ToList<RoleDataItem>();
                        }


                        lstSource.DataTextField = "Role";
                        lstSource.DataValueField = "RoleID";
                        lstDestination.DataTextField = "Role";
                        lstDestination.DataValueField = "RoleID";

                        if (Helper.IsUserAdmin(TLUserID))
                        {
                            lstSource.DataSource = (from r in db.Roles
                                                    where !(from uf in db.UserRoles
                                                            where uf.UserID == UserID
                                                            select uf.RoleID).Contains(r.RoleID)
                                                    orderby r.RoleName
                                                    select new RoleDataItem() { RoleID = r.RoleID, Role = r.RoleName }).ToList();
                        }
                        else
                        {
                            lstSource.DataSource = (from r in db.Roles
                                                    where !(from uf in db.UserRoles
                                                            where uf.UserID == UserID
                                                            select uf.RoleID).Contains(r.RoleID) && (r.RoleName != "AdminRole" && r.RoleName != "FormManager")
                                                    orderby r.RoleName
                                                    select new RoleDataItem() { RoleID = r.RoleID, Role = r.RoleName }).ToList();
                        }


                        lstSource.DataBind();

                        lstDestination.DataSource = userRoles;
                        lstDestination.DataBind();
                    }
                }
            }
        }

        protected void lstDestination_OnInserted(object sender, RadListBoxEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                int CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                int UserID;
                int RoleID;

                if (int.TryParse(Request["UserID"], out UserID))
                {
                    if (UserID > 0)
                    {
                        foreach (RadListBoxItem item in e.Items)
                        {
                            UserRole ur = new UserRole();
                            RoleID = Convert.ToInt32(item.Value);

                            ur.RoleID = RoleID;
                            ur.UserID = UserID;
                            ur.CreatedBy = CreatedBy;
                            ur.CreatedOn = DateTime.Now;
                            db.UserRoles.AddObject(ur);
                        }
                    }
                }

                db.SaveChanges();
            }
        }

        protected void lstSource_OnInserted(object sender, RadListBoxEventArgs e)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                int CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                int UserID;
                int RoleID;

                if (int.TryParse(Request["UserID"], out UserID))
                {
                    if (UserID > 0)
                    {
                        foreach (RadListBoxItem item in e.Items)
                        {
                            RoleID = Convert.ToInt32(item.Value);

                            UserRole ur = (from r in db.UserRoles
                                           where r.RoleID == RoleID && r.UserID == UserID
                                           select r).FirstOrDefault();

                            if (ur != null)
                            {
                                db.UserRoles.DeleteObject(ur);
                            }
                        }
                    }
                }

                db.SaveChanges();
            }
        }


    }
}
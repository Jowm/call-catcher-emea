﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.UI.WebControls;
using System.Linq;
using CallDispositionTool.Model;
using System.Web.Security;
using System.Web.UI;
using System.Collections.Generic;
using Telerik.Web.UI;
using System.Web.Configuration;
using CallDispositionTool.CTI;

namespace CallDispositionTool.UserControl
{
    public partial class CTIDockUserControl : System.Web.UI.UserControl
    {
        public int UserID { get { return Convert.ToInt32(Membership.GetUser().ProviderUserKey); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
            }
        }

    }
}
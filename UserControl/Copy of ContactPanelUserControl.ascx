﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactPanelUserControl.ascx.cs"
    Inherits="CallDispositionTool.UserControl.ContactPanelUserControl" %>
<style type="text/css">
    .clsHidden
    {
        display: none;
        z-index: 0;
        width: 0px;
        height: 0px;
    }
</style>
<script type="text/javascript">
    function ShowAddContact() {
        var windowURL = 'ContactDetails.aspx?Mode=Add';
        window.open(windowURL, "AddContact", 'width=430,height=500,resizable=no,scrollbars=yes').focus();
    }

    function ShowContactDetails(contactID) {
        var windowURL = 'ContactDetails.aspx?ContactID=' + contactID;
        window.open(windowURL, "AddContact", 'width=430,height=450,resizable=no,scrollbars=yes').focus();
    }

    function ShowTransactionHistory(contactID, formID, contactName) {
        var windowURL = 'TransactionHistory.aspx?ContactID=' + contactID + '&FormID=' + formID + '&ContactName=' + contactName;
        window.open(windowURL, "AddContact", 'width=800,height=600,resizable=yes,scrollbars=yes').focus();
    }

    function OnClientButtonCommand(sender, args) {
        if (args.get_commandName() == "NewContact") {
            ShowAddContact();
        }
    }
</script>
<div>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server">
    </rad:RadAjaxLoadingPanel>
    <rad:RadAjaxManagerProxy ID="ajaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="searchBox1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlContactInfo" LoadingPanelID="LoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <table style='width: 100%'>
        <tr>
            <td style="vertical-align: middle; width: 20%; text-align: left;">
                <strong>
                    <label for="chkCallBackRequired">
                        Contact Info</label></strong>
            </td>
            <td style="vertical-align: middle; width: 80%; text-align: left;">
                <rad:RadSearchBox ID="searchBox1" runat="server" DataTextField="Phone" DataKeyNames="Phone,LastName,FirstName,CorpSysPrin,AccountNumber,Address,ContactID"
                    DataValueField="ContactID" Width="300px" EmptyMessage="Type to search" EnableAutoComplete="true"
                    MinFilterLength="4" MaxResultCount="20" OnSearch="searchBox1_Search" ShowSearchButton="true"
                    OnButtonCommand="searchBox1_ButtonCommand" OnClientButtonCommand="OnClientButtonCommand" >
                    <SearchContext ShowDefaultItem="false" Width="65px">
                        <Items>
                            <rad:SearchContextItem Text="Phone" Key="Phone" Selected="true" />
                            <rad:SearchContextItem Text="Name" Key="Name" />
                            <rad:SearchContextItem Text="Phone (Contains)" Key="Phone2" />
                            <rad:SearchContextItem Text="Name (Contains)" Key="Name2" />
                        </Items>
                    </SearchContext>
                    <WebServiceSettings Path="TransactionForm.aspx" Method="GetResults" />
                    <Localization LoadingItemsMessage="Searching..." />
                    <Buttons>
                        <rad:SearchBoxButton ID="btnReset" runat="server" CommandName="Reset" CommandArgument="Reset"
                            Position="Right" ToolTip="Reset" AlternateText="Reset" ImageUrl="~/images/close.png" />
                        <rad:SearchBoxButton ID="btnAddNewContact" runat="server" CommandName="NewContact"
                            CommandArgument="NewContact" Position="Right" ToolTip="New Contact" AlternateText="New Contact"
                            ImageUrl="~/images/AddContact16.png" />
                    </Buttons>
                    <DropDownSettings Height="250px" Width="420px">
                    </DropDownSettings>
                </rad:RadSearchBox>
                <asp:TextBox ID="txtContactID" runat="server" CssClass="clsHidden" Width="0px" Height="0px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqContact" runat="server" ErrorMessage="Please enter valid value for Contact Info"
                    Text="*" ControlToValidate="txtContactID"></asp:RequiredFieldValidator>
                <asp:Panel ID="pnlContactInfo" runat="server" Visible="false" Width="300px">
                    <fieldset style="display: block;">
                        <legend>Contact Info</legend>
                        <div>
                            <table>
                                <tr>
                                    <td>
                                        <h2>
                                            <asp:Label ID="lblFirstName" runat="server"></asp:Label></h2>
                                    </td>
                                    <td>
                                        <h2>
                                            <asp:Label ID="lblLastName" runat="server"></asp:Label></h2>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table>
                            <tr>
                                <td>
                                    <strong>Phone:</strong>
                                </td>
                                <td>
                                    <asp:Label ID="lblPhone" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Address:</strong>
                                </td>
                                <td>
                                    <asp:Label ID="lblAddress" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Corp/SysPrin:</strong>
                                </td>
                                <td>
                                    <asp:Label ID="lblCorpSysPrin" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Account Number:</strong>
                                </td>
                                <td>
                                    <asp:Label ID="lblAccountNumber" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <hr />
                        <div style="float: right">
                            <asp:ImageButton ID="btnDetails" runat="server" ImageUrl="~/Images/contactsItems.gif"
                                AlternateText="Click here to view contact details" CausesValidation="false" />
                            <asp:ImageButton ID="btnHistory" runat="server" ImageUrl="~/Images/15printPreview.gif"
                                AlternateText="Click here to view transactions" CausesValidation="false" />
                        </div>
                    </fieldset>
                </asp:Panel>
            </td>
        </tr>
    </table>
</div>

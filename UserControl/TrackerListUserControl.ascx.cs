﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.UI.WebControls;
using System.Linq;
using CallDispositionTool.Model;
using System.Web.Security;
using System.Web.UI;
using System.Collections.Generic;
using Telerik.Web.UI;
using System.Web.Configuration;

namespace CallDispositionTool.UserControl
{
    public partial class TrackerListUserControl : System.Web.UI.UserControl
    {
        public int UserID { get { return Convert.ToInt32(Membership.GetUser().ProviderUserKey); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindTrackerRepeater();
            }
        }

        void BindTrackerRepeater()
        {
            CallDispositionEntities db = new CallDispositionEntities();

            var query = (from p in db.UserForms
                         where p.UserID == UserID
                         select new { p.Form.FormID, p.Form.FormName }).ToList();

            repTracker.DataSource = query;
            repTracker.DataBind();
        }

        protected void repTracker_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            Repeater rptDemo = sender as Repeater; // Get the Repeater control object.

            // If the Repeater contains no data.
            if (repTracker != null && repTracker.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    // Show the Error Label (if no data is present).
                    Label lblErrorMsg = e.Item.FindControl("lblErrorMsg") as Label;

                    if (lblErrorMsg != null)
                    {
                        lblErrorMsg.Visible = true;
                    }
                }
            }
        }
    }
}
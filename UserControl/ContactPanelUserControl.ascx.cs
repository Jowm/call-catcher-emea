﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Linq;
using CallDispositionTool.Model;
using CallDispositionTool.Security;
using System.Collections.Generic;
using Telerik.Web.UI;
using Telerik.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Web.Services;

namespace CallDispositionTool.UserControl
{
    public partial class ContactPanelUserControl : System.Web.UI.UserControl
    {
        public int FormID { get; set; }

        public decimal ContactID
        {
            get
            {
                decimal contactID;
                decimal.TryParse(txtContactID.Text, out contactID);
                return contactID;
            }
            set
            {
                txtContactID.Text = value.ToString();
            }
        }

        public string FirstName
        {
            get
            {
                return lblFirstName.Text;
            }
        }

        public string LastName
        {
            get
            {
                return lblLastName.Text;
            }
        }

        public string ContactInfo
        {
            get
            {
                decimal contactID = Convert.ToDecimal(txtContactID.Text);
                string returnValue = string.Empty;

                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    var contact = (from c in db.Contacts
                                   where c.ContactID == contactID
                                   select c).FirstOrDefault();

                    if (contact != null)
                    {
                        returnValue = contact.FirstName;
                        returnValue += "\\" + contact.LastName;
                        returnValue += "\\" + contact.Phone;
                        returnValue += "\\" + contact.MobilePhone;
                        returnValue += "\\" + contact.Address1;
                        returnValue += "\\" + contact.Address2;
                        returnValue += "\\" + contact.Email;
                    }
                }

                return returnValue;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                FormID = Convert.ToInt32(Request["FormID"]);
            }
            catch (Exception)
            {
            }
        }

        public void ResetControl()
        {
            searchBox1.Text = string.Empty;
            pnlContactInfo.Visible = false;
            txtContactID.Text = string.Empty;
            lblFirstName.Text = string.Empty;
            lblLastName.Text = string.Empty;
            lblPhone.Text = string.Empty;
            lblStoreNumber.Text = string.Empty;
            lblNTLoginID.Text = string.Empty;
            lblTransactionID.Text = string.Empty;
            lblAddress.Text = string.Empty;
        }

        public void RefreshContact(int ContactID)
        {
            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                var contact = (from c in db.Contacts
                               where c.ContactID == ContactID
                               select c).FirstOrDefault();

                if (contact != null)
                {
                    searchBox1.Text = contact.Phone;
                    pnlContactInfo.Visible = true;
                    txtContactID.Text = contact.ContactID.ToString();
                    lblFirstName.Text = contact.FirstName;
                    lblLastName.Text = contact.LastName;
                    lblPhone.Text = contact.Phone;
                    lblStoreNumber.Text = contact.StoreNumber;
                    lblNTLoginID.Text = contact.NTLoginID;
                    lblTransactionID.Text = contact.TransactionID;
                    lblAddress.Text = string.Format("{0} {1} {2} {3} {4} {5}", contact.Address1, contact.Address2, contact.City, contact.StateRegion, contact.PostalCode, contact.Country);

                    //TODO: GLA 07232014
                    btnDetails.OnClientClick = String.Format("return ShowContactDetails('{0}');", txtContactID.Text);
                    btnHistory.OnClientClick = String.Format("return ShowTransactionHistory('{0}', '{1}', '{2}');", txtContactID.Text, FormID, lblFirstName.Text + " " + lblLastName.Text);
                }
            }
        }

        protected void searchBox1_Search(object sender, Telerik.Web.UI.SearchBoxEventArgs e)
        {
            if (e.DataItem != null)
            {
                pnlContactInfo.Visible = true;
                lblFirstName.Text = ((Dictionary<string, object>)e.DataItem)["FirstName"] != null ? ((Dictionary<string, object>)e.DataItem)["FirstName"].ToString() : string.Empty;
                lblLastName.Text = ((Dictionary<string, object>)e.DataItem)["LastName"] != null ? ((Dictionary<string, object>)e.DataItem)["LastName"].ToString() : string.Empty;
                txtContactID.Text = ((Dictionary<string, object>)e.DataItem)["ContactID"] != null ? ((Dictionary<string, object>)e.DataItem)["ContactID"].ToString() : string.Empty;
                lblPhone.Text = ((Dictionary<string, object>)e.DataItem)["Phone"] != null ? ((Dictionary<string, object>)e.DataItem)["Phone"].ToString() : string.Empty;
                lblAddress.Text = ((Dictionary<string, object>)e.DataItem)["Address"] != null ? ((Dictionary<string, object>)e.DataItem)["Address"].ToString() : string.Empty;
                lblStoreNumber.Text = ((Dictionary<string, object>)e.DataItem)["StoreNumber"] != null ? ((Dictionary<string, object>)e.DataItem)["StoreNumber"].ToString() : string.Empty;
                lblNTLoginID.Text = ((Dictionary<string, object>)e.DataItem)["NTLoginID"] != null ? ((Dictionary<string, object>)e.DataItem)["NTLoginID"].ToString() : string.Empty;
                lblTransactionID.Text = ((Dictionary<string, object>)e.DataItem)["TransactionID"] != null ? ((Dictionary<string, object>)e.DataItem)["TransactionID"].ToString() : string.Empty;


                btnDetails.OnClientClick = String.Format("return ShowContactDetails('{0}');", txtContactID.Text);
                btnHistory.OnClientClick = String.Format("return ShowTransactionHistory('{0}', '{1}', '{2}');", txtContactID.Text, FormID, lblFirstName.Text + " " + lblLastName.Text);
            }
        }

        protected void searchBox1_ButtonCommand(object sender, SearchBoxButtonEventArgs e)
        {
            if (e.CommandName == "Reset")
            {
                ResetControl();
            }
            else if (e.CommandName == "NewContact")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "NewContact", "return ShowAddContact();");
            }
        }
    }
}
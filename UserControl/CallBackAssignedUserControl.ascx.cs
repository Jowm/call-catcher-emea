﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using CallDispositionTool.Model;
using System.Data.Objects;

namespace CallDispositionTool.UserControl
{
    public partial class CallBackAssignedUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                cboCallBackStatus.DataBind();

                grdCallBack.Rebind();

                if (Request.QueryString["CallBackID"] != null && grdCallBack.MasterTableView.Items.Count > 0)
                    grdCallBack.MasterTableView.Items[0].Expanded = true;
            }
        }
    
        protected void cboCallBackStatus_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox cbo = sender as RadComboBox;
            GridDataItem dataItem = cbo.NamingContainer as GridDataItem;

            decimal callbackID = (decimal)dataItem.GetDataKeyValue("CallBackID");

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                int? callbackStatusID = Convert.ToInt32(e.Value);
                CallDispositionTool.Model.CallBack cb = (from p in db.CallBacks
                                                         where p.CallBackID == callbackID
                                                         select p).FirstOrDefault<CallDispositionTool.Model.CallBack>();

                CallBackHistoryLog log = new CallBackHistoryLog();
                log.Action = "Status Change";
                log.ActionLog = "From " + e.OldText + " To " + e.Text;
                log.CreatedBy = (int)Membership.GetUser().ProviderUserKey;
                log.CreatedOn = DateTime.Now;

                cb.CallBackStatusID = callbackStatusID;
                cb.CallBackHistoryLogs.Add(log);
                db.SaveChanges();
            }

            grdCallBack.Controls.Add(new LiteralControl(String.Format("<h4>Call Back ID {0} status was successfully changed</h4>", callbackID.ToString())));
            grdCallBack.Rebind();
        }

        public void Rebind()
        {
            grdCallBack.Rebind();
        }

        protected void grdCallBack_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
            {
                GridDataItem dataItem = e.Item as GridDataItem;

                RadComboBox cbo = dataItem.FindControl("cboCallBackStatus") as RadComboBox;

                if (cbo.SelectedItem.Text == "Completed")
                {
                    cbo.Enabled = false;
                }

                LiteralControl litCtrl = new LiteralControl();
                litCtrl.Text = Convert.ToDateTime(dataItem["CallBackDate"].Text).AddHours(Convert.ToDouble(dataItem["UtcOffset"].Text)).ToString();
                dataItem["AdjustedCallBackDateTime"].Controls.Add(litCtrl);
            }
            if (e.Item.ItemType == GridItemType.NestedView)
            {
                GridNestedViewItem nestedItem = e.Item as GridNestedViewItem;
                RadMultiPage mp = nestedItem.Controls[1].Controls[0].Controls[3] as RadMultiPage;
                HyperLink link = mp.FindControl("lnkTrans") as HyperLink;
                link.Attributes["href"] = "#";
                link.Attributes["onclick"] = String.Format("return OpenTransaction('{0}');", nestedItem.ParentItem.GetDataKeyValue("CallBackID"));
            }
        }

        protected void grdCallBack_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (e.RebindReason == GridRebindReason.ExplicitRebind || e.RebindReason == GridRebindReason.PostBackEvent)
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    int? assignedTo = (int)Membership.GetUser().ProviderUserKey;
                    int callBackStatusID = Convert.ToInt32(cboCallBackStatus.SelectedItem.Value);

                    var items = (from p in db.CallBacks.Include("Timezone")
                                 where p.AssignedTo == assignedTo && p.CallBackStatusID == callBackStatusID
                                 let AdjustedCallBackDate = EntityFunctions.AddHours(p.CallBackDate, p.Timezone.UtcOffset)
                                 select new
                                 {
                                     p.TransactionID,
                                     p.CallBackStatusID,
                                     p.CallBackID,
                                     ActualCallBackDate = p.CallBackDate,
                                     Timezone = p.Timezone.Abbreviation,
                                     AdjustedCallBackDate,
                                     p.ContactNumber,
                                     CallBackReason = p.CallBackReason.Description,
                                     CreatedBy = p.User.EmployeeID,
                                     CreatedOn = p.CreatedOn
                                 }).ToList();

                    grdCallBack.DataSource = items;

                    if (!Page.IsPostBack && Request.QueryString["CallBackID"] != null)
                    {
                        string callBackID = Convert.ToString(Request.QueryString["CallBackID"]);
                        grdCallBack.MasterTableView.FilterExpression = String.Format("([CallBackID] = '{0}')", callBackID);

                        GridColumn column = grdCallBack.MasterTableView.GetColumnSafe("CallBackID");
                        column.CurrentFilterFunction = GridKnownFunction.EqualTo;
                        column.CurrentFilterValue = callBackID;
                    }
                }
            }
        }

        protected void cboCallBackStatus_SelectedIndexChanged1(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            grdCallBack.Rebind();
        }
    }
}
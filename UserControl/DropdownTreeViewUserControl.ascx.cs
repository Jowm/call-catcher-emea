﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using CallDispositionTool.Model;
using Telerik.Web.UI;

namespace CallDispositionTool.UserControl
{
    public partial class DropdownTreeViewUserControl : System.Web.UI.UserControl
    {
        const int BHN_RETENTION = 7;
        public delegate void OnDispositionChangedEventHandler(object sender, DispositionChangedEventArgs e);
        public event OnDispositionChangedEventHandler dispoHandler;

        public int? Dispo1ID { get; set; }
        public int? Dispo2ID { get; set; }
        public int? Dispo3ID { get; set; }
        public int? Dispo4ID { get; set; }
        public bool AutoPostBack { get { return ddtDisposition.AutoPostBack; } set { ddtDisposition.AutoPostBack = value; } }
        public bool Enabled { get { return ddtDisposition.Enabled; } set { ddtDisposition.Enabled = value; } }
        public string AttributeName { get; set; }
        public int? AttributeID { get; set; }
        public int? FilterDispositionID { get; set; }
        public string DispositionValue { get { return ddtDisposition.SelectedText; }
            set {
                if (value != null)
                {
                    ddtDisposition.SelectedText = value;
                    ddtDisposition.SelectedValue = value;
                    ddtDisposition.DefaultMessage = value;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            reqDisposition.ErrorMessage = "Please enter value for " + String.Format("{0}", AttributeName);

            if (!Page.IsPostBack)
            {
                Rebind();               
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(System.Web.HttpCacheability.Private);
            Response.Cache.SetMaxAge(new TimeSpan(24, 0, 0));
            Response.Cache.SetExpires(DateTime.Now.AddHours(24));

            string formid = Request["FormID"];
            int iFormID;

            if (int.TryParse(formid, out iFormID))
            {
                //Use Filter Dispo
                if (iFormID == BHN_RETENTION)
                {
                    ddtDisposition.WebServiceSettings.Path = "DropdownTreeWS.asmx";
                    ddtDisposition.WebServiceSettings.Method = "LoadNodes";
                }
            }
        }

        public class DispositionChangedEventArgs : EventArgs
        {
            public string DispositionValue { get; set; }
            public int? Dispo1 { get; set; }
            public int? Dispo2 { get; set; }
            public int? Dispo3 { get; set; }
            public int? Dispo4 { get; set; }
        }

        class DropdownTreeData {
            public int DispositionID { get; set; }
            public string Description { get; set; }
            public int? ParentDispositionID { get; set; }
            public bool HasChildren { get; set; }
        }

        public void Rebind()
        {
            List<DropdownTreeData> treeDS = null;

            //Create Instance of CacheManager 
            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            if (_objCacheManager.Contains("LoadDropdownTreeView~" + AttributeID.ToString() + "~" + (FilterDispositionID.HasValue ? FilterDispositionID.ToString() : "0")))
            {
                treeDS = _objCacheManager.GetData("LoadDropdownTreeView~" + AttributeID.ToString() + "~" + (FilterDispositionID.HasValue ? FilterDispositionID.ToString() : "0")) as List<DropdownTreeData>;
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    string formid = Request["FormID"];
                    int iFormID;

                    if (int.TryParse(formid, out iFormID))
                    {
                        //Use Filter Dispo
                        if (iFormID == BHN_RETENTION)
                        {
                            DropDownNodeBinding ddnb1 = new DropDownNodeBinding();
                            ddnb1.ExpandMode = DropDownTreeNodeExpandMode.WebService;
                            ddnb1.Depth = 0;

                            DropDownNodeBinding ddnb2 = new DropDownNodeBinding();
                            ddnb2.ExpandMode = DropDownTreeNodeExpandMode.WebService;
                            ddnb2.Depth = 1;

                            ddtDisposition.DataBindings.Clear();
                            ddtDisposition.DataBindings.Add(ddnb1);
                            ddtDisposition.DataBindings.Add(ddnb2);

                            treeDS = (from t in db.Dispositions
                                      where t.AttributeID == AttributeID && t.HideFromList == false && t.ParentDispositionID == null && (FilterDispositionID.HasValue ? t.FilterDispositionID == FilterDispositionID : t.FilterDispositionID == null)
                                      orderby t.Description ascending
                                      let DispositionID = t.DispositionID
                                      let Description = t.Description
                                      let ParentDispositionID = t.ParentDispositionID
                                      let HasChildren = (from t2 in db.Dispositions
                                                         where t2.AttributeID == AttributeID && t2.HideFromList == false && t2.ParentDispositionID == t.DispositionID
                                                         select t2).Any()
                                      select new DropdownTreeData() { DispositionID = DispositionID, Description = Description, ParentDispositionID = ParentDispositionID, HasChildren = HasChildren }).ToList();
                        }
                        else
                        {
                            ddtDisposition.FilterSettings.Highlight = DropDownTreeHighlight.Matches;
                            ddtDisposition.FilterSettings.EmptyMessage = "Type here to filter";
                            ddtDisposition.FilterSettings.Filter = DropDownTreeFilter.Contains;
                            ddtDisposition.CheckBoxes = DropDownTreeCheckBoxes.SingleCheck;
                            ddtDisposition.EnableFiltering = true;

                            treeDS = (from t in db.Dispositions
                                      where t.AttributeID == AttributeID && t.HideFromList == false
                                      orderby t.Description ascending
                                      let DispositionID = t.DispositionID
                                      let Description = t.Description
                                      let ParentDispositionID = t.ParentDispositionID
                                      let HasChildren = (from t2 in db.Dispositions
                                                         where t2.AttributeID == AttributeID && t2.HideFromList == false && t2.ParentDispositionID == t.DispositionID
                                                         select t2).Any()
                                      select new DropdownTreeData() { DispositionID = DispositionID, Description = Description, ParentDispositionID = ParentDispositionID, HasChildren = HasChildren }).ToList();
                        }
                    }

                    _objCacheManager.Add("LoadDropdownTreeView~" + AttributeID.ToString() + "~" + (FilterDispositionID.HasValue ? FilterDispositionID.ToString() : "0"), treeDS);
                }
            }

            if (treeDS != null)
            {
                ddtDisposition.DataFieldParentID = "ParentDispositionID";
                ddtDisposition.DataFieldID = "DispositionID";
                ddtDisposition.DataTextField = "Description";
                ddtDisposition.DataSource = treeDS;
                ddtDisposition.DataBind();
                ddtDisposition.Entries.Clear();
            }
        }

        protected void ddtDisposition_EntryAdded(object sender, Telerik.Web.UI.DropDownTreeEntryEventArgs e)
        {
            Dispo2ID = Convert.ToInt32(GetFilterDispositionID(Convert.ToInt32(e.Entry.Value), ddtDisposition.SelectedText));
            AssignArguments(sender);
        }

        int? GetFilterDispositionID(int DispositionID, string FullPath)
        {
            string[] nodes = FullPath.Split(new char[] { '\\' });
            int counter = 0;
            int? iReturnValue = null;
            string searchText = string.Empty;

            Disposition previousNode;

            //Get Level 2 Node Text
            foreach (string node in nodes)
            {
                if (counter == 1)
                {
                    searchText = node;
                    break;
                }
                counter++;
            }

            //Create Instance of CacheManager 
            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            if (_objCacheManager.Contains("GetFilterDispositionID~" + DispositionID.ToString()))
            {
                return (int?)_objCacheManager.GetData("GetFilterDispositionID~" + DispositionID.ToString());
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    previousNode = (from p in db.Dispositions
                                    where p.DispositionID == DispositionID && p.HideFromList == false
                                    select p).FirstOrDefault();

                    if (previousNode.Description == searchText)
                    {
                        _objCacheManager.Add("GetFilterDispositionID~" + DispositionID.ToString(), previousNode.DispositionID);
                        iReturnValue = previousNode.DispositionID;
                    }
                    else
                    {
                        while (previousNode.ParentDispositionID.HasValue)
                        {
                            previousNode = (from d in db.Dispositions
                                            where d.DispositionID == previousNode.ParentDispositionID && d.HideFromList == false
                                            select d).FirstOrDefault();

                            if (previousNode.Description == searchText)
                            {
                                _objCacheManager.Add("GetFilterDispositionID~" + DispositionID.ToString(), previousNode.DispositionID);
                                iReturnValue = previousNode.DispositionID;
                            }
                        }
                    }
                }
            }

            return iReturnValue;
        }

        public void ClearEntries()
        {
            ddtDisposition.Entries.Clear();
            ddtDisposition.DefaultMessage = "Please select";
        }

        private void AssignArguments(object sender)
        {
            DispositionChangedEventArgs args = new DispositionChangedEventArgs();
            args.DispositionValue = DispositionValue;
            args.Dispo1 = Dispo1ID;
            args.Dispo2 = Dispo2ID;
            args.Dispo3 = Dispo3ID;
            args.Dispo4 = Dispo4ID;

            if (this.dispoHandler != null)
            {
                this.dispoHandler(sender, args);
            }
        }

        protected void ddtDisposition_NodeDataBound(object sender, DropDownTreeNodeDataBoundEventArguments e)
        {
            DropdownTreeData node = e.DropDownTreeNode.DataItem as DropdownTreeData;

            if (node.HasChildren)
            {
                e.DropDownTreeNode.Checkable = false;
            }
        }
    }
}
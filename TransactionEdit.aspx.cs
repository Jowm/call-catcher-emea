﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using CallDispositionTool.UserControl;
using Telerik.Web.UI;
using System.Web.Security;
using System.Web.Configuration;
using Microsoft.AspNet.SignalR;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Text;

namespace CallDispositionTool
{
    public partial class TransactionEdit : System.Web.UI.Page
    {
        #region _FormEditNotificationBody
        const string _FormEditNotificationBody = @"
                <html>
                    <body>
                    <h3>Senukai Form</h3>
                    <p>
                    This email confirms that the form has been edited.
                    </p>
                    <table>
                    <tr>
                        <td>
                            <b>Uzregistravo :</b> {0}
                        </td>
                    </tr>
                    <tr>   
                        <td>
                            <b>kliento Vardas :</b> {1}
                        </td>  
                    </tr>
                    <tr>   
                        <td>
                            <b>Problema :</b> {2}
                        </td>  
                    </tr>
                    <tr>   
                        <td>
                            <b>Pastabos :</b> {3}
                        </td>  
                    </tr>
                    <tr>   
                        <td>
                            <b>Telefono numeris :</b> {4}
                        </td>  
                    </tr>
                    <tr>   
                        <td>
                            <b>Email :</b> {5}
                        </td>  
                    </tr>
                    <tr>   
                        <td>
                            <b>Assign To :</b> {6}
                        </td>  
                    </tr>
                    <tr>   
                        <td>
                            <b>Kvito nr. :</b> {7}
                        </td>  
                    </tr>
                    <tr>   
                        <td>
                            <b>Prekybos centras :</b> {8}
                        </td>  
                    </tr>
                    <tr>   
                        <td>
                            <b>Pirkimo data :</b> {9}
                        </td>  
                    </tr>
                    </table>
                    <p>
                    If you have any questions or encounter any problems,
                    please contact a site administrator.
                    </p>
                    </body>
                </html>
            ";
        #endregion

        List<CallDispositionTool.Model.Attribute> _Attributes;
        List<AttributeValue> _AttributeValues;

        const int BHN_RETENTION = 7;

        int SendEmailID = Convert.ToInt32(ConfigurationManager.AppSettings["SendEmailID"]);
        int ProblemaAttributeID = Convert.ToInt32(ConfigurationManager.AppSettings["ProblemaAttributeID"]);
        int HiddenDropdown = Convert.ToInt32(ConfigurationManager.AppSettings["HiddenDropdown"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Page.Title = "Transaction Edit - " + Request["TransactionID"];
                RadWindowManager1.RadAlert(_FormEditNotificationBody,
          330,
          180,
          "Save Success",
          "CloseWindow");
                if (Request["Mode"] == "View")
                {
                    btnSubmit.Visible = false;
                }
            }
        }

        void LoadAttributeValues()
        {
            foreach (var item in _Attributes)
            {
                Control control = formPlaceHolder.FindControl("Ctr_" + item.AttributeID);
                string attributeValue = string.Empty;
                
                try
                {
                    attributeValue = _AttributeValues.Where(a => a.AttributeID == item.AttributeID).FirstOrDefault().Value;
                }
                catch (Exception ex)
                {
                    Helper.LogError(ex);
                }

                if (control is RadTextBox)
                {
                    RadTextBox tb = control as RadTextBox;
                    tb.Text = attributeValue;
                }
                else if (control is RadNumericTextBox)
                {
                    RadNumericTextBox num = control as RadNumericTextBox;
                    num.Value = Convert.ToDouble(attributeValue);
                }
                else if (control is RadDatePicker)
                {
                    RadDatePicker dt = control as RadDatePicker;
                    dt.SelectedDate = Convert.ToDateTime(attributeValue);
                }
                else if (control is RadComboBox)
                {
                    RadComboBox cbo = control as RadComboBox;
                    RadComboBoxItem cboItem = cbo.FindItemByText(attributeValue);

                    if (cboItem != null)
                        cboItem.Selected = true;
                    cbo.Visible = true;

                    if (item.DataTypeID == HiddenDropdown)
                    {
                        cbo.Visible = true;
                    }
                }
                else if (control is RadListBox)
                {
                    RadListBox lst = control as RadListBox;
                    string[] tmp = attributeValue.Split('|');

                    foreach (string b in tmp)
                    {
                        RadListBoxItem itm = lst.FindItemByText(b);

                        if (itm != null)
                        {
                            itm.Selected = true;
                        }
                    }

                    if (item.DataTypeID == HiddenDropdown)
                    {
                        lst.Visible = true;
                    }

                }
                else if (control is DispositionUserControl)
                {
                    if (item.AttributeID != ProblemaAttributeID)
                    {
                        //DispositionUserControl disp = control as DispositionUserControl;
                        //_AttributeValues.Where(a => a.AttributeID == item.AttributeID).FirstOrDefault().Value = disp.DispositionValue;
                        DropdownTreeViewUserControl ttv = control as DropdownTreeViewUserControl;
                        ttv.DispositionValue = attributeValue;
                    }
                    else
                    {
                        RadTextBox tb = control as RadTextBox;
                        tb.Text = attributeValue;
                    }
                }
                else if (control is DropdownTreeViewUserControl)
                {
                    DropdownTreeViewUserControl ttv = control as DropdownTreeViewUserControl;
                    ttv.DispositionValue = attributeValue;
                }
                else if (control is UserCaptionUserControl)
                {
                    UserCaptionUserControl ucuc = control as UserCaptionUserControl;
                    ucuc.Username = attributeValue;
                }
                else if (control is AssignToUserControl)
                {
                    AssignToUserControl atctrl = control as AssignToUserControl;
                    atctrl.AssignTo = attributeValue;
                }
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            HttpCookie ck = Request.Cookies["UserInfo"];
            string skinID = string.Empty;

            if (ck != null)
            {
                skinID = ck.Values["Skin"];
                this.skinManager1.Skin = skinID;
            }
            else
            {
                ck = new HttpCookie("UserInfo");
                ck.Values["Skin"] = this.skinManager1.Skin;
                Response.Cookies.Remove("UserInfo");
                Response.Cookies.Add(ck);
            }
        }

        public void Dispo24_SelectedIndexChange(object sender, DropdownTreeViewUserControl.DispositionChangedEventArgs e)
        {
            DropdownTreeViewUserControl ctr24 = formPlaceHolder.FindControl("Ctr_24") as DropdownTreeViewUserControl;
            DropdownTreeViewUserControl ctr31 = formPlaceHolder.FindControl("Ctr_31") as DropdownTreeViewUserControl;

            RadAjaxManager manager = RadAjaxManager.GetCurrent(this.Page);
            manager.AjaxSettings.AddAjaxSetting(ctr24, pnlMain, LoadingPanel1);

            ctr31.FilterDispositionID = e.Dispo2;

            if (e.Dispo2.HasValue)
            {
                ctr31.AutoPostBack = false;
                ctr31.Enabled = true;
                ctr31.Rebind();
                //ctr31.ClearEntries();
            }
            else
            {
                ctr31.Enabled = false;
            }
        }

        public void Dispo24_SelectedIndexChange(object sender, DispositionUserControl.DispositionChangedEventArgs e)
        {
            DispositionUserControl ctr24 = formPlaceHolder.FindControl("Ctr_24") as DispositionUserControl;
            DispositionUserControl ctr31 = formPlaceHolder.FindControl("Ctr_31") as DispositionUserControl;

            RadAjaxManager manager = RadAjaxManager.GetCurrent(this.Page);
            manager.AjaxSettings.AddAjaxSetting(ctr24, pnlMain, LoadingPanel1);

            if (e.Dispo2.HasValue)
            {
                ctr31.FilterDispositionID = e.Dispo2;
                ctr31.Rebind();
            }
        }

        RequiredFieldValidator CreateRequiredFieldValidator(int AttributeID, string ErrorMessage, string InitialValue)
        {
            RequiredFieldValidator rfv = new RequiredFieldValidator();
            rfv.ID = "ReqVal_Ctr_" + AttributeID.ToString();
            rfv.ControlToValidate = "Ctr_" + AttributeID.ToString();
            rfv.Display = ValidatorDisplay.Static;
            rfv.Text = "*";
            rfv.ErrorMessage = ErrorMessage;
            rfv.InitialValue = InitialValue;
            rfv.SetFocusOnError = true;

            return rfv;
        }

        List<Control> CreateCustomAttributeUI(int AttributeID, string AttributeName, int DataTypeID, bool? Mandatory, object AttributeValue)
        {
            List<Control> ctrls = new List<Control>();
            DataTypeIDEnum myEnum = (DataTypeIDEnum)DataTypeID;
            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            switch (myEnum)
            {
                case DataTypeIDEnum.String:
                    //Use Textbox
                    RadTextBox tb = new RadTextBox();
                    tb.ID = "Ctr_" + AttributeID.ToString();
                    tb.Width = new Unit("300px");
                    ctrls.Add(tb);

                    AttributeTextboxProperty atp;

                    if (_objCacheManager.Contains("AttributeTextboxProperty~" + AttributeID.ToString()))
                    {
                        atp = (AttributeTextboxProperty)_objCacheManager.GetData("AttributeTextboxProperty~" + AttributeID.ToString());
                    }
                    else
                    {
                        using (CallDispositionEntities db = new CallDispositionEntities())
                        {
                            atp = (from a in db.AttributeTextboxProperties
                                   where a.AttributeID == AttributeID
                                   select a).FirstOrDefault();

                            _objCacheManager.Add("AttributeTextboxProperty~" + AttributeID.ToString(), atp);
                        }
                    }

                    if (atp != null)
                    {
                        tb.MaxLength = atp.MaxLength;
                        tb.Columns = atp.MaxLength;

                        if (atp.TextMode == 1)
                        {
                            tb.TextMode = InputMode.MultiLine;
                            tb.Rows = 4;
                        }
                        else if (atp.TextMode == 2)
                        {
                            tb.TextMode = InputMode.Password;
                        }
                        else if (atp.TextMode == 3)
                        {
                            tb.TextMode = InputMode.SingleLine;
                        }

                        RegularExpressionValidator regexValidator = new RegularExpressionValidator();
                        regexValidator.ID = "ctrl_Regex_" + AttributeID.ToString();
                        regexValidator.ControlToValidate = tb.ID;
                        regexValidator.ErrorMessage = atp.ErrorMessage;

                        try
                        {
                            regexValidator.Display = ValidatorDisplay.Dynamic;
                            regexValidator.ValidationExpression = atp.RegularExpression;
                        }
                        catch (Exception ex)
                        {
                            Helper.LogError(ex);
                        }
                        regexValidator.SetFocusOnError = true;
                        ctrls.Add(new LiteralControl("<br />"));
                        ctrls.Add(regexValidator);
                    }

                    if (Mandatory.Value)
                    {
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }

                    break;
                case DataTypeIDEnum.Boolean:
                    //Use Checkbox
                    CheckBox cb = new CheckBox();
                    cb.ID = "Ctr_" + AttributeID.ToString();
                    ctrls.Add(cb);

                    break;
                case DataTypeIDEnum.Numeric:
                    //Use Numeric Textbox
                    RadNumericTextBox numericTB = new RadNumericTextBox();

                    numericTB.ID = "Ctr_" + AttributeID.ToString();
                    numericTB.NumberFormat.DecimalDigits = 0;
                    numericTB.ShowSpinButtons = true;
                    numericTB.NumberFormat.GroupSeparator = "";
                    numericTB.Width = new Unit("300px");
                    ctrls.Add(numericTB);

                    if (Mandatory.Value)
                    {
                        //ctrls.Add(new LiteralControl("<br />"));
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }

                    break;
                case DataTypeIDEnum.Date:
                    //Use Date Picker
                    RadDatePicker dp = new RadDatePicker();
                    dp.ID = "Ctr_" + AttributeID.ToString();

                    ctrls.Add(dp);

                    if (Mandatory.Value)
                    {
                        //ctrls.Add(new LiteralControl("<br />"));
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }

                    break;
                case DataTypeIDEnum.DropdownList:
                    //Use dropdown list
                    RadComboBox ddl = new RadComboBox();
                    RadComboBoxItemCollection ddlItems = GetDropdownPicklistOption(AttributeID, ddl);

                    ddl.ID = "Ctr_" + AttributeID.ToString();
                    ddl.DropDownAutoWidth = RadComboBoxDropDownAutoWidth.Enabled;
                    ddl.MaxHeight = new Unit("250px");

                    foreach (RadComboBoxItem item in ddlItems)
                    {
                        ddl.Items.Add(item);
                    }

                    ctrls.Add(ddl);

                    if (Mandatory.Value)
                    {
                        //ctrls.Add(new LiteralControl("<br />"));
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }
                    break;
                case DataTypeIDEnum.Listbox:
                    RadListBox lst = new RadListBox();
                    RadListBoxItemCollection lstItems = GetListboxPickListOption(AttributeID, lst);
                    lst.ID = "Ctr_" + AttributeID.ToString();
                    lst.SelectionMode = ListBoxSelectionMode.Multiple;

                    foreach (RadListBoxItem item in lstItems)
                    {
                        lst.Items.Add(item);
                    }

                    ctrls.Add(lst);

                    if (Mandatory.Value)
                    {
                        //ctrls.Add(new LiteralControl("<br />"));
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }

                    break;
                case DataTypeIDEnum.Disposition:
                    //    //Use Disposition Control
                    //    DispositionUserControl dispoCtrl = LoadControl("~/UserControl/DispositionUserControl.ascx") as DispositionUserControl;
                    //    dispoCtrl.ID = "Ctr_" + AttributeID.ToString();
                    //    dispoCtrl.AttributeID = AttributeID;

                    //    ctrls.Add(dispoCtrl);

                    //    break;
                    //Use Disposition Control
                    if (AttributeID != ProblemaAttributeID)
                    {
                        DropdownTreeViewUserControl dropdownTree1 = LoadControl("~/UserControl/DropdownTreeViewUserControl.ascx") as DropdownTreeViewUserControl;

                        dropdownTree1.ID = "Ctr_" + AttributeID.ToString();
                        dropdownTree1.AttributeID = AttributeID;
                        dropdownTree1.AttributeName = AttributeName;
                        ctrls.Add(dropdownTree1);
                    }
                    else
                    {
                        //Use Textbox
                        RadTextBox tbd = new RadTextBox();
                        tbd.ID = "Ctr_" + AttributeID.ToString();
                        tbd.Width = new Unit("300px");
                        ctrls.Add(tbd);

                        if (Mandatory.Value)
                        {
                            ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                        }
                    }

                    break;
                case DataTypeIDEnum.DropdownTreeView:
                    //xxxxx
                    //Use Disposition Control


                    DropdownTreeViewUserControl dropdownTree = LoadControl("~/UserControl/DropdownTreeViewUserControl.ascx") as DropdownTreeViewUserControl;

                    dropdownTree.ID = "Ctr_" + AttributeID.ToString();
                    dropdownTree.AttributeID = AttributeID;
                    dropdownTree.AttributeName = AttributeName;

                    ctrls.Add(dropdownTree);

                    break;
                case DataTypeIDEnum.Contact:
                    //Use Contact User Control
                    ContactUserControl contact = LoadControl("~/UserControl/ContactUserControl.ascx") as ContactUserControl;

                    contact.ID = "Ctr_" + AttributeID.ToString();
                    contact.AttributeName = AttributeName;
                    contact.AttributeID = AttributeID;

                    ctrls.Add(contact);
                    break;
                case DataTypeIDEnum.CustomerSatisfactionRadioButton:
                    //Use Contact User Control
                    CustomerSatisfactionUserControl csUC = LoadControl("~/UserControl/CustomerSatisfactionUserControl.ascx") as CustomerSatisfactionUserControl;

                    csUC.ID = "Ctr_" + AttributeID.ToString();
                    csUC.AttributeName = AttributeName;
                    csUC.AttributeID = AttributeID;

                    ctrls.Add(csUC);
                    break;
                case DataTypeIDEnum.AssignToUserControl:
                    //Use Contact User Control
                    AssignToUserControl atctrl = LoadControl("~/UserControl/AssignToUserControl.ascx") as AssignToUserControl;

                    atctrl.ID = "Ctr_" + AttributeID.ToString();
                    atctrl.AttributeName = AttributeName;
                    atctrl.AttributeID = AttributeID;

                    ctrls.Add(atctrl);
                    break;
                case DataTypeIDEnum.UserCaptionUserControl:
                    //Use Contact User Control
                    UserCaptionUserControl ucuc = LoadControl("~/UserControl/UserCaptionUserControl.ascx") as UserCaptionUserControl;

                    ucuc.ID = "Ctr_" + AttributeID.ToString();
                    ucuc.AttributeName = AttributeName;
                    ucuc.AttributeID = AttributeID;

                    ctrls.Add(ucuc);
                    break;
                default:
                    break;
            }

            return ctrls;
        }

        void AddCustomAttribute(CallDispositionTool.Model.Attribute attribute)
        {
            formPlaceHolder.Controls.Add(new LiteralControl(String.Format("<td style='vertical-align: text-top;'><strong><label for='{0}'>", "Ctr_" + attribute.AttributeID.ToString()) + attribute.AttributeName + "</label></strong></td>"));

            //Add the UI as the right cell
            List<Control> UIControls = CreateCustomAttributeUI(attribute.AttributeID, attribute.AttributeName, attribute.DataTypeID, attribute.Mandatory, null);

            formPlaceHolder.Controls.Add(new LiteralControl("<td>"));

            foreach (Control ctrl in UIControls)
            {
                formPlaceHolder.Controls.Add(ctrl);
            }

            formPlaceHolder.Controls.Add(new LiteralControl("</td>"));
        }

        RadComboBoxItemCollection GetDropdownPicklistOption(int AttributeID, RadComboBox ddl)
        {
            RadComboBoxItemCollection ddlItems = new RadComboBoxItemCollection(ddl);
            dynamic items;

            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            if (_objCacheManager.Contains("GetDropdownPicklistOption~" + AttributeID.ToString()))
            {
                items = _objCacheManager.GetData("GetDropdownPicklistOption~" + AttributeID.ToString());
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    items = (from i in db.PickListOptions
                             where i.AttributeID == AttributeID && (i.HideFromList == false || i.HideFromList == null)
                             orderby i.SortOrder, i.DisplayText ascending
                             select new { i.DisplayText, i.OptionValue }).ToList();

                    _objCacheManager.Add("GetDropdownPicklistOption~" + AttributeID.ToString(), items);
                }
            }

            ddlItems.Add(new RadComboBoxItem("", ""));

            foreach (var item in items)
            {
                ddlItems.Add(new RadComboBoxItem(item.DisplayText, item.OptionValue));
            }

            return ddlItems;
        }

        RadListBoxItemCollection GetListboxPickListOption(int AttributeID, RadListBox lst)
        {
            RadListBoxItemCollection lstItems = new RadListBoxItemCollection(lst);
            CacheManager _objCacheManager = CacheFactory.GetCacheManager();
            dynamic items;

            if (_objCacheManager.Contains("GetDropdownPicklistOption~" + AttributeID.ToString()))
            {
                items = _objCacheManager.GetData("GetListboxPickListOption~" + AttributeID.ToString());
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    items = (from i in db.PickListOptions
                             where i.AttributeID == AttributeID && (i.HideFromList == false || i.HideFromList == null)
                             orderby i.SortOrder, i.DisplayText ascending
                             select new { i.DisplayText, i.OptionValue }).ToList();

                    _objCacheManager.Add("GetListboxPickListOption~" + AttributeID.ToString(), items);
                }
            }

            foreach (var item in items)
            {
                lstItems.Add(new RadListBoxItem(item.DisplayText, item.OptionValue));
            }

            return lstItems;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            string transactionID = Request["TransactionID"];
            int iTransactionID;
            int iFormID;

            if (int.TryParse(transactionID, out iTransactionID))
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    var labels = (from a in db.AttributeValues
                                  where a.TransactionID == iTransactionID
                                  select new { a.Attribute.Form.FormName, a.Attribute.Form.FormID }).FirstOrDefault();

                    lblFormName.Text = labels.FormName;
                    iFormID = labels.FormID;

                    _AttributeValues = (from values in db.AttributeValues
                                        where values.TransactionID == iTransactionID
                                        select values).ToList();

                    _Attributes = (from p in db.Attributes
                                   where p.FormID == iFormID && p.HideFromList == false
                                   orderby p.SortOrder
                                   select p).ToList();
                }

                formPlaceHolder.Controls.Add(new LiteralControl("<table>"));

                foreach (CallDispositionTool.Model.Attribute item in _Attributes)
                {
                    formPlaceHolder.Controls.Add(new LiteralControl("<tr>"));

                    AddCustomAttribute(item);

                    formPlaceHolder.Controls.Add(new LiteralControl("</tr>"));
                }

                formPlaceHolder.Controls.Add(new LiteralControl("</table>"));

                //Assign an on change handler for custom BHN disposition control
                if (iFormID == BHN_RETENTION)
                {
                    DropdownTreeViewUserControl ctr24_Ddtv = formPlaceHolder.FindControl("Ctr_24") as DropdownTreeViewUserControl;
                    DropdownTreeViewUserControl ctr31_Ddtv = formPlaceHolder.FindControl("Ctr_31") as DropdownTreeViewUserControl;

                    DispositionUserControl ctr24_Cdd = formPlaceHolder.FindControl("Ctr_24") as DispositionUserControl;
                    DispositionUserControl ctr31_Cdd = formPlaceHolder.FindControl("Ctr_31") as DispositionUserControl;

                    if (ctr24_Ddtv != null && ctr31_Ddtv != null)
                    {
                        ctr31_Ddtv.Enabled = false;
                        ctr24_Ddtv.AutoPostBack = true;
                        ctr24_Ddtv.dispoHandler += new DropdownTreeViewUserControl.OnDispositionChangedEventHandler(Dispo24_SelectedIndexChange);
                    }
                    else if (ctr24_Cdd != null && ctr31_Cdd != null)
                    {
                        ctr24_Cdd.dispoHandler += new DispositionUserControl.OnDispositionChangedEventHandler(Dispo24_SelectedIndexChange);
                    }
                }

                LoadAttributeValues();
            }
            else
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        protected void btnSubmit_Clicked(object sender, EventArgs e)
        {
            foreach (var item in _Attributes)
            {
                Control control = formPlaceHolder.FindControl("Ctr_" + item.AttributeID);

                if (control is RadTextBox)
                {
                    RadTextBox tb = control as RadTextBox;
                    _AttributeValues.Where(a => a.AttributeID == item.AttributeID).FirstOrDefault().Value = tb.Text;
                }
                else if (control is RadNumericTextBox)
                {
                    RadNumericTextBox num = control as RadNumericTextBox;
                    _AttributeValues.Where(a => a.AttributeID == item.AttributeID).FirstOrDefault().Value = num.Value.ToString();
                }
                else if (control is RadDatePicker)
                {
                    RadDatePicker dt = control as RadDatePicker;
                    _AttributeValues.Where(a => a.AttributeID == item.AttributeID).FirstOrDefault().Value = dt.SelectedDate.ToString();
                }
                else if (control is RadComboBox)
                {
                    RadComboBox cbo = control as RadComboBox;
                    _AttributeValues.Where(a => a.AttributeID == item.AttributeID).FirstOrDefault().Value = cbo.Text;
                }
                else if (control is RadListBox)
                {
                    RadListBox lst = control as RadListBox;
                    string tmp = string.Empty;

                    foreach (RadListBoxItem b in lst.SelectedItems)
                    {
                        tmp += b.Text + "|";
                    }

                    if (tmp.Length > 0)
                        tmp.Remove(tmp.Length - 1, 1);

                    _AttributeValues.Where(a => a.AttributeID == item.AttributeID).FirstOrDefault().Value = tmp;
                }
                else if (control is DispositionUserControl)
                {
                    DispositionUserControl disp = control as DispositionUserControl;
                    _AttributeValues.Where(a => a.AttributeID == item.AttributeID).FirstOrDefault().Value = disp.DispositionValue;
                }
                else if (control is DropdownTreeViewUserControl)
                {
                    DropdownTreeViewUserControl ttv = control as DropdownTreeViewUserControl;
                    _AttributeValues.Where(a => a.AttributeID == item.AttributeID).FirstOrDefault().Value = ttv.DispositionValue;
                    String ProblemaValue = ttv.DispositionValue;
                }
                else if (control is AssignToUserControl)
                {
                    AssignToUserControl atc = control as AssignToUserControl;
                    _AttributeValues.Where(a => a.AttributeID == item.AttributeID).FirstOrDefault().Value = atc.AssignTo;
                }

            }


            try
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    MembershipUser user = Membership.GetUser(_AttributeValues[0].Value);

                    foreach (var att in _AttributeValues)
                    {
                        db.AttributeValues.Attach(att);
                        db.ObjectStateManager.ChangeObjectState(att, System.Data.EntityState.Modified);
                    }
         
                    try
                    {
                        SendEmail();
                    }
                    catch (Exception ex)
                    {


                    }

                    db.SaveChanges();

                    RadWindowManager1.RadAlert("Transaction ID # <strong>" + _AttributeValues.FirstOrDefault().TransactionID.ToString() + " </strong> has been saved.",
                                     330,
                                     180,
                                     "Save Success",
                                     "CloseWindow");
                    //RadWindowManager1.RadAlert("Transaction ID # <strong>" + _AttributeValues.FirstOrDefault().TransactionID.ToString() + " </strong> has been saved.",
                    //    330,
                    //    180,
                    //    "Save Success",
                    //    "CloseWindow");
                }
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;

                if (ex.InnerException.Message != null)
                {
                    errorMsg += Environment.NewLine + "Inner exception: " + ex.InnerException.Message;
                }

                RadWindowManager1.RadAlert(
                    errorMsg,
                    330,
                    180,
                    "Error",
                    "");

                Helper.LogError(ex);
            }
        }


        void SendEmail()
        {
            SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CallDisposition"].ConnectionString);
            cn.Open();
            string sql = @"dbo.GetDispositionEmail";
            SqlCommand cmd = new SqlCommand(sql, cn);
            cmd.CommandTimeout = 9000;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AttributeID", ProblemaAttributeID);
            cmd.Parameters.AddWithValue("@ProblemaValue", _AttributeValues[2].Value);

            string emails = Convert.ToString(cmd.ExecuteScalar());
            MailAddressCollection TO_addressList = new MailAddressCollection();
            MailAddress mFrom = new MailAddress("noreply@transcom.com");
            foreach (var curr_address in emails.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
            {
                MailAddress mytoAddress = new MailAddress(curr_address);
                TO_addressList.Add(mytoAddress);
            }

            MailMessage m = new MailMessage();
            m.From = mFrom;
            m.To.Add(TO_addressList.ToString());
            m.Subject = string.Format("Transcom Call Disposition Tool - Problema - {0} Edit Notification", _AttributeValues[2].Value);
            m.Body = String.Format(_FormEditNotificationBody, _AttributeValues[0].Value, _AttributeValues[1].Value, _AttributeValues[2].Value, _AttributeValues[4].Value
                , _AttributeValues[5].Value, _AttributeValues[6].Value, _AttributeValues[7].Value, _AttributeValues[8].Value, _AttributeValues[9].Value, _AttributeValues[10].Value);

            m.IsBodyHtml = true;
            SmtpClient client = new SmtpClient();
            //client.EnableSsl = true;

       
            client.Send(m);
            cn.Close();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CallDispositionTool.Model;
using CallDispositionTool.UserControl;
using Telerik.Web.UI;
using System.Web.Security;
using System.Web.Configuration;
using Microsoft.AspNet.SignalR;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using CallDispositionTool.WebService.Comcast;
using System.Xml;

namespace CallDispositionTool
{
    public partial class Form : FormPage
    {
        const int BHN_RETENTION = 7;

        List<CallDispositionTool.Model.Attribute> _Attributes;

        void SetCallerID(string CallerID)
        {
            foreach (var attribute in _Attributes)
            {
                DataTypeIDEnum dt = (DataTypeIDEnum)attribute.DataTypeID;
                Control ctrl = formPlaceHolder.FindControl("Ctr_" + attribute.AttributeID);

                switch (dt)
                {
                    case DataTypeIDEnum.String:
                        RadTextBox txt = ctrl as RadTextBox;

                        if (txt != null)
                        {
                            if (attribute.AttributeName.Trim() == "ANI")
                            {
                                txt.Text = CallerID;
                            }
                        }                                                      

                        break;
                    default:
                        break;
                }           
            }
        }

        //void GetComcastGatewayInfo(string CallerID)
        //{
        //    int formID;
        //    string formSessionID = Session["FormID"].ToString();

        //    if (int.TryParse(formSessionID, out formID))
        //    {
        //        if (formID == 55 || formID == 54 || formID == 53)
        //        {
        //            ComcastGatewayServiceSoapClient client = new ComcastGatewayServiceSoapClient();
        //            string xmlReturn = Helper.GetComcastXMLRequestHeader(CallerID);
        //            string xmlReturn2;
        //            XmlDocument xmlDoc = new XmlDocument();

        //            try
        //            {
        //                xmlReturn2 = client.getTrasfer(xmlReturn);
        //                xmlDoc.LoadXml(xmlReturn2);

        //                foreach (var attribute in _Attributes)
        //                {
        //                    DataTypeIDEnum dt = (DataTypeIDEnum)attribute.DataTypeID;
        //                    Control ctrl = formPlaceHolder.FindControl("Ctr_" + attribute.AttributeID);

        //                    switch (dt)
        //                    {
        //                        case DataTypeIDEnum.String:
        //                            RadTextBox txt = ctrl as RadTextBox;

        //                            if (txt != null)
        //                            {
        //                                if (attribute.AttributeName.Trim() == "ANI")
        //                                {
        //                                    if (xmlDoc.DocumentElement.SelectSingleNode("BODY").SelectSingleNode("getTrasfer").SelectSingleNode("getTransferRequest").SelectSingleNode("ANI").InnerText.Length > 0)
        //                                    {
        //                                        txt.Text = xmlDoc.DocumentElement.SelectSingleNode("BODY").SelectSingleNode("getTrasfer").SelectSingleNode("getTransferRequest").SelectSingleNode("ANI").InnerText;
        //                                    }
        //                                }
        //                                else if (attribute.AttributeName.Trim() == "Phone Number")
        //                                {
        //                                    if (xmlDoc.DocumentElement.SelectSingleNode("BODY").SelectSingleNode("getTrasfer").SelectSingleNode("getTransferRequest").SelectSingleNode("PHONENUMBER").InnerText.Length > 0)
        //                                    {
        //                                        txt.Text = xmlDoc.DocumentElement.SelectSingleNode("BODY").SelectSingleNode("getTrasfer").SelectSingleNode("getTransferRequest").SelectSingleNode("PHONENUMBER").InnerText;
        //                                    }
        //                                }
        //                                else if (attribute.AttributeName.Trim() == "UCID")
        //                                {
        //                                    if (xmlDoc.DocumentElement.SelectSingleNode("BODY").SelectSingleNode("getTrasfer").SelectSingleNode("getTransferRequest").SelectSingleNode("UOID").InnerText.Length > 0)
        //                                    {
        //                                        txt.Text = xmlDoc.DocumentElement.SelectSingleNode("BODY").SelectSingleNode("getTrasfer").SelectSingleNode("getTransferRequest").SelectSingleNode("UOID").InnerText;
        //                                    }
        //                                }
        //                                else if (attribute.AttributeName.Trim() == "Caller Intent")
        //                                {
        //                                    if (xmlDoc.DocumentElement.SelectSingleNode("BODY").SelectSingleNode("getTrasfer").SelectSingleNode("getTransferRequest").SelectSingleNode("CALLERINTENT").InnerText.Length > 0)
        //                                    {
        //                                        txt.Text = xmlDoc.DocumentElement.SelectSingleNode("BODY").SelectSingleNode("getTrasfer").SelectSingleNode("getTransferRequest").SelectSingleNode("CALLERINTENT").InnerText;
        //                                    }
        //                                }
        //                                else if (attribute.AttributeName.Trim() == "Market")
        //                                {
        //                                    if (xmlDoc.DocumentElement.SelectSingleNode("BODY").SelectSingleNode("getTrasfer").SelectSingleNode("getTransferRequest").SelectSingleNode("MARKET").InnerText.Length > 0)
        //                                    {
        //                                        txt.Text = xmlDoc.DocumentElement.SelectSingleNode("BODY").SelectSingleNode("getTrasfer").SelectSingleNode("getTransferRequest").SelectSingleNode("MARKET").InnerText;
        //                                    }
        //                                }
        //                                else if (attribute.AttributeName.Trim() == "Language Preference")
        //                                {
        //                                    if (xmlDoc.DocumentElement.SelectSingleNode("BODY").SelectSingleNode("getTrasfer").SelectSingleNode("getTransferRequest").SelectSingleNode("LANGUAGEPREFERENCE").InnerText.Length > 0)
        //                                    {
        //                                        txt.Text = xmlDoc.DocumentElement.SelectSingleNode("BODY").SelectSingleNode("getTrasfer").SelectSingleNode("getTransferRequest").SelectSingleNode("LANGUAGEPREFERENCE").InnerText;
        //                                    }
        //                                }
        //                                else if (attribute.AttributeName.Trim() == "Description")
        //                                {
        //                                    if (xmlDoc.DocumentElement.SelectSingleNode("BODY").SelectSingleNode("getTrasfer").SelectSingleNode("getTransferRequest").SelectSingleNode("DESCRIPTION").InnerText.Length > 0)
        //                                    {
        //                                        txt.Text = xmlDoc.DocumentElement.SelectSingleNode("BODY").SelectSingleNode("getTrasfer").SelectSingleNode("getTransferRequest").SelectSingleNode("DESCRIPTION").InnerText;
        //                                    }
        //                                }
        //                            }

        //                            break;
        //                        default:
        //                            break;
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                RadWindowManager1.RadAlert("Error: " + ex.Message.Replace("'", string.Empty) + "\nInner Exception: " + ex.InnerException.Message.Replace("'", string.Empty), 330, 180, "Comcast Gateway Error", "");
        //                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "pop", "alert('Comcast Gateway Error: " + ex.Message.Replace("'", string.Empty) + "\nInner Exception: {1}" + ex.InnerException.Message.Replace("'", string.Empty) + "');", true);
        //                Helper.LogError(ex);
        //            }
        //        }
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Response.Cache.SetCacheability(System.Web.HttpCacheability.Private);
                Response.Cache.SetMaxAge(new TimeSpan(24, 0, 0));
                Response.Cache.SetExpires(DateTime.Now.AddHours(24));
                Response.Cache.SetAllowResponseInBrowserHistory(true);

                int iFormID;
                string formid = Session["FormID"].ToString();
                //string formid = string.Empty;

                //HttpCookie ck = Request.Cookies["FormChooser"];

                //if (ck != null)
                //{
                //    formid = ck.Values["FormID"];
                //}

                if (int.TryParse(formid, out iFormID))
                {
                    if (!Helper.CheckFormAccess(Convert.ToInt32(formid), Convert.ToInt32(Membership.GetUser().ProviderUserKey)))
                    {
                        Response.Redirect("~/Unauthorized.aspx");
                    }

                    if (Helper.HasCallBackFeature(iFormID))
                    {
                        CallBackUserControl1.FormID = iFormID;
                    }

                    if (Helper.HasContactFeature(iFormID))
                    {
                        pnlContact.Visible = true;
                        ContactUserControl1.FormID = iFormID;
                    }

                    if (Helper.HasTransferCallFeature(iFormID))
                    {
                        pnlTransferCall.Visible = true;
                    }

                    if (Helper.HasLinkToLiberator(iFormID))
                    {
                        pnlLiberator.Visible = true;
                        hypLiberator.NavigateUrl = String.Format(WebConfigurationManager.AppSettings["LiberatorURL"], Helper.GetEmployeeName(User.Identity.Name), User.Identity.Name, DateTime.Now.Ticks.ToString());
                    }

                    if (Helper.HasAudienceManagerFeature(iFormID))
                    {
                        pnlAudienceManager.Visible = true;
                    }

                    if (Request["FormName"] != null)
                    {
                        Page.Title = Request["FormName"];
                    }

                    if (!Page.IsPostBack && Request["CallerID"] != null)
                    {
                        SetCallerID(Request["CallerID"]);
                    }
                }
                else
                {
                    Response.Redirect("~/Unauthorized.aspx");
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            HttpCookie ck = Request.Cookies["UserInfo"];
            string skinID = string.Empty;

            if (ck != null)
            {
                skinID = ck.Values["Skin"];
                this.skinManager1.Skin = skinID;
            }
            else
            {
                ck = new HttpCookie("UserInfo");
                ck.Values["Skin"] = this.skinManager1.Skin;
                Response.Cookies.Remove("UserInfo");
                Response.Cookies.Add(ck);
            }
        }

        void ResetControls()
        {
            foreach (var item in _Attributes)
            {
                DataTypeIDEnum dt = (DataTypeIDEnum)item.DataTypeID;

                Control ctrl = formPlaceHolder.FindControl("Ctr_" + item.AttributeID);

                switch (dt)
                {
                    case DataTypeIDEnum.String:
                        RadTextBox txt = ctrl as RadTextBox;
                        txt.Text = string.Empty;
                        break;
                    case DataTypeIDEnum.Boolean:
                        CheckBox cb = ctrl as CheckBox;
                        cb.Checked = false;
                        break;
                    case DataTypeIDEnum.Numeric:
                        RadNumericTextBox num = ctrl as RadNumericTextBox;
                        num.Value = null;
                        break;
                    case DataTypeIDEnum.Date:
                        RadDatePicker dtp = ctrl as RadDatePicker;
                        dtp.SelectedDate = null;
                        break;
                    case DataTypeIDEnum.DropdownList:
                        RadComboBox cbo = ctrl as RadComboBox;
                        cbo.Text = string.Empty;
                        cbo.ClearSelection();
                        break;
                    case DataTypeIDEnum.Listbox:
                        RadListBox lst = ctrl as RadListBox;
                        lst.ClearSelection();

                        foreach (RadListBoxItem itm in lst.Items)
                        {
                            itm.Checked = false;
                        }
                        break;
                    case DataTypeIDEnum.Disposition:
                        DispositionUserControl dispo = ctrl as DispositionUserControl;
                        dispo.Rebind();
                        break;
                    case DataTypeIDEnum.DropdownTreeView:
                        DropdownTreeViewUserControl tree = ctrl as DropdownTreeViewUserControl;
                        tree.Rebind();
                        break;
                    case DataTypeIDEnum.Contact:
                        ContactUserControl contactUC = ctrl as ContactUserControl;
                        contactUC.ResetControl();
                        break;
                    case DataTypeIDEnum.TimerAuto:
                        TimerAutoUserControl timerAuto = ctrl as TimerAutoUserControl;
                        timerAuto.Reset();
                        break;
                    case DataTypeIDEnum.TimerManual:
                        TimerManualUserControl timerManual = ctrl as TimerManualUserControl;
                        timerManual.Reset();
                        break;
                    case DataTypeIDEnum.CustomerSatisfactionRadioButton:
                        CustomerSatisfactionUserControl csUC = ctrl as CustomerSatisfactionUserControl;
                        csUC.Reset();
                        break;
                    default:
                        break;
                }
            }

            chkCallBackRequired.ClearSelection();
        }

        public void Dispo24_SelectedIndexChange(object sender, DropdownTreeViewUserControl.DispositionChangedEventArgs e)
        {
            DropdownTreeViewUserControl ctr24 = formPlaceHolder.FindControl("Ctr_24") as DropdownTreeViewUserControl;
            DropdownTreeViewUserControl ctr31 = formPlaceHolder.FindControl("Ctr_31") as DropdownTreeViewUserControl;

            RadAjaxManager manager = RadAjaxManager.GetCurrent(this.Page);
            manager.AjaxSettings.AddAjaxSetting(ctr24, pnlMain, LoadingPanel1);

            ctr31.FilterDispositionID = e.Dispo2;

            if (e.Dispo2.HasValue)
            {
                ctr31.AutoPostBack = false;
                ctr31.Enabled = true;
                ctr31.Rebind();
                //ctr31.ClearEntries();
            }
            else
            {
                ctr31.Enabled = false;
            }
        }

        public void Dispo24_SelectedIndexChange(object sender, DispositionUserControl.DispositionChangedEventArgs e)
        {
            DispositionUserControl ctr24 = formPlaceHolder.FindControl("Ctr_24") as DispositionUserControl;
            DispositionUserControl ctr31 = formPlaceHolder.FindControl("Ctr_31") as DispositionUserControl;

            RadAjaxManager manager = RadAjaxManager.GetCurrent(this.Page);
            manager.AjaxSettings.AddAjaxSetting(ctr24, pnlMain, LoadingPanel1);

            if (e.Dispo2.HasValue)
            {
                ctr31.FilterDispositionID = e.Dispo2;
                ctr31.Rebind();
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            string sFormID = null;
            int iFormID = 0;

            sFormID = Convert.ToString(CacheFactory.GetCacheManager().GetData("SelectedFormIDbyEmployeeID~" + Membership.GetUser().UserName));

            if (Session["FormID"] == null)
            {
                if (sFormID == null)
                {
                    Response.Redirect(String.Format("~/FormSessionRedirect.aspx?iframe={0}&CallerID={1}&DNIS={2}&CallID={3}&Queue={4}&BackURL=http{5}",
                        Request["iframe"],
                        Request["CallerID"],
                        Request["DNIS"],
                        Request["CallID"],
                        Request["Queue"],
                        Request["BackURL"]));                        
                }
            }
            else if (!Helper.CheckFormAccess(Convert.ToInt32(sFormID), this.UserID))
            {
                if (!Helper.CheckFormAccess(Convert.ToInt32(Session["FormID"]), this.UserID))
                {
                    Response.Redirect("~/Unauthorized.aspx");
                }
                else
                {
                    iFormID = Convert.ToInt32(sFormID);
                }
            }
            else
            {
                iFormID = Convert.ToInt32(Session["FormID"]);
                //int iFormID = BHN_RETENTION;
            }

            //Create Instance of CacheManager 
            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            if (_objCacheManager.Contains("GetAttributes~" + iFormID.ToString()))
            {
                _Attributes = (List<CallDispositionTool.Model.Attribute>)_objCacheManager.GetData("GetAttributes~" + iFormID.ToString());
                lblFormName.Text = (string)_objCacheManager.GetData("GetFormNameByFormID~" + iFormID.ToString());
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    _Attributes = (from p in db.Attributes
                                   where p.FormID == iFormID && p.HideFromList == false
                                   orderby p.SortOrder
                                   select p).ToList();

                    lblFormName.Text = _Attributes.First().Form.FormName;

                    _objCacheManager.Add("GetAttributes~" + iFormID.ToString(), _Attributes);
                    _objCacheManager.Add("GetFormNameByFormID~" + iFormID.ToString(), lblFormName.Text);
                }
            }

            formPlaceHolder.Controls.Add(new LiteralControl("<table>"));

            foreach (CallDispositionTool.Model.Attribute item in _Attributes)
            {
                formPlaceHolder.Controls.Add(new LiteralControl("<tr>"));

                AddCustomAttribute(item);

                formPlaceHolder.Controls.Add(new LiteralControl("</tr>"));
            }

            formPlaceHolder.Controls.Add(new LiteralControl("</table>"));

            //Assign an on change handler for custom BHN disposition control
            if (iFormID == BHN_RETENTION)
            {
                DropdownTreeViewUserControl ctr24_Ddtv = formPlaceHolder.FindControl("Ctr_24") as DropdownTreeViewUserControl;
                DropdownTreeViewUserControl ctr31_Ddtv = formPlaceHolder.FindControl("Ctr_31") as DropdownTreeViewUserControl;

                DispositionUserControl ctr24_Cdd = formPlaceHolder.FindControl("Ctr_24") as DispositionUserControl;
                DispositionUserControl ctr31_Cdd = formPlaceHolder.FindControl("Ctr_31") as DispositionUserControl;

                if (ctr24_Ddtv != null && ctr31_Ddtv != null)
                {
                    ctr31_Ddtv.Enabled = false;
                    ctr24_Ddtv.AutoPostBack = true;
                    ctr24_Ddtv.dispoHandler += new DropdownTreeViewUserControl.OnDispositionChangedEventHandler(Dispo24_SelectedIndexChange);
                }
                else if (ctr24_Cdd != null && ctr31_Cdd != null)
                {
                    ctr24_Cdd.dispoHandler += new DispositionUserControl.OnDispositionChangedEventHandler(Dispo24_SelectedIndexChange);
                }
            }
        }


        bool isGrouping = false;
        public bool ShouldApplySortFilterOrGroup()
        {
            return grdTransaction.MasterTableView.FilterExpression != "" ||
                (grdTransaction.MasterTableView.GroupByExpressions.Count > 0 || isGrouping) ||
                grdTransaction.MasterTableView.SortExpressions.Count > 0;
        }

        protected void grdTransaction_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
            {
                //low level sql datatable 
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["cn_CallDisposition"].ConnectionString))
                {
                    cn.Open();

                    string sql = @"GetTransactionsByUser";
                    int transCount = 0;
                    int userID = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                    int formID = Convert.ToInt32(Session["FormID"]);

                    using (CallDispositionEntities db = new CallDispositionEntities())
                    {
                        transCount = (from t in db.Transactions
                                      where t.CreatedBy == userID && t.FormID == formID
                                      select t).Count();
                    }

                    grdTransaction.VirtualItemCount = transCount;

                    int startRowIndex = (ShouldApplySortFilterOrGroup()) ? 0 : grdTransaction.CurrentPageIndex * grdTransaction.PageSize;
                    //int maximumRows = (ShouldApplySortFilterOrGroup()) ? transCount : grdTransaction.PageSize;
                    int maximumRows = grdTransaction.PageSize;

                    SqlCommand cmd = new SqlCommand(sql, cn);
                    cmd.CommandTimeout = 9000;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserID", Convert.ToInt32(Membership.GetUser().ProviderUserKey));
                    cmd.Parameters.AddWithValue("@FormID", formID);
                    cmd.Parameters.AddWithValue("@StartRowIndex", startRowIndex);
                    cmd.Parameters.AddWithValue("@MaximumRows", maximumRows);
                    grdTransaction.AllowCustomPaging = !ShouldApplySortFilterOrGroup();

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();

                    da.Fill(dt);

                    try
                    {
                        if (dt.Columns.Count > 0)
                        {
                            grdTransaction.DataSource = dt;
                        }
                        else
                        {
                            grdTransaction.DataSource = null;
                        }
                    }
                    catch (Exception ex)
                    {
                        RadWindowManager1.RadAlert(ex.Message, 330, 180, "Error Message", "");
                    }
                }
            }
        }

        void AddCustomAttribute(CallDispositionTool.Model.Attribute attribute)
        {
            formPlaceHolder.Controls.Add(new LiteralControl(String.Format("<td><strong><label for='{0}'>", "Ctr_" + attribute.AttributeID.ToString()) + attribute.AttributeName + "</label></strong></td>"));

            //Add the UI as the right cell
            List<Control> UIControls = CreateCustomAttributeUI(attribute.AttributeID, attribute.AttributeName, attribute.DataTypeID, attribute.Mandatory, null);

            formPlaceHolder.Controls.Add(new LiteralControl("<td>"));

            foreach (Control ctrl in UIControls)
            {
                formPlaceHolder.Controls.Add(ctrl);
            }

            formPlaceHolder.Controls.Add(new LiteralControl("</td>"));
        }

        List<Control> CreateCustomAttributeUI(int AttributeID, string AttributeName, int DataTypeID, bool? Mandatory, object AttributeValue)
        {
            List<Control> ctrls = new List<Control>();
            DataTypeIDEnum myEnum = (DataTypeIDEnum)DataTypeID;
            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            switch (myEnum)
            {
                case DataTypeIDEnum.String:
                    //Use Textbox
                    RadTextBox tb = new RadTextBox();
                    tb.ID = "Ctr_" + AttributeID.ToString();
                    tb.Width = new Unit("300px");
                    tb.ToolTip = AttributeName;

                    ctrls.Add(tb);

                    AttributeTextboxProperty atp;

                    if (_objCacheManager.Contains("AttributeTextboxProperty~" + AttributeID.ToString()))
                    {
                        atp = (AttributeTextboxProperty)_objCacheManager.GetData("AttributeTextboxProperty~" + AttributeID.ToString());
                    }
                    else
                    {
                        using (CallDispositionEntities db = new CallDispositionEntities())
                        {
                            atp = (from a in db.AttributeTextboxProperties
                                   where a.AttributeID == AttributeID
                                   select a).FirstOrDefault();

                            _objCacheManager.Add("AttributeTextboxProperty~" + AttributeID.ToString(), atp);
                        }
                    }

                    if (atp != null)
                    {
                        tb.MaxLength = atp.MaxLength;
                        tb.Columns = atp.MaxLength;

                        if (atp.TextMode == 1)
                        {
                            tb.TextMode = InputMode.MultiLine;
                            tb.Rows = 4;
                        }
                        else if (atp.TextMode == 2)
                        {
                            tb.TextMode = InputMode.Password;
                        }
                        else if (atp.TextMode == 3)
                        {
                            tb.TextMode = InputMode.SingleLine;
                        }

                        RegularExpressionValidator regexValidator = new RegularExpressionValidator();
                        regexValidator.ID = "ctrl_Regex_" + AttributeID.ToString();
                        regexValidator.ControlToValidate = tb.ID;
                        regexValidator.ErrorMessage = atp.ErrorMessage;

                        try
                        {
                            regexValidator.Display = ValidatorDisplay.Dynamic;
                            regexValidator.ValidationExpression = atp.RegularExpression;
                        }
                        catch (Exception ex)
                        {
                            Helper.LogError(ex);
                        }

                        regexValidator.SetFocusOnError = true;
                        ctrls.Add(new LiteralControl("<br />"));
                        ctrls.Add(regexValidator);
                    }

                    if (Mandatory.Value)
                    {
                        //ctrls.Add(new LiteralControl("<br />"));
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }

                    break;
                case DataTypeIDEnum.Boolean:
                    //Use Checkbox

                    CheckBox cb = new CheckBox();
                    cb.ID = "Ctr_" + AttributeID.ToString();

                    ctrls.Add(cb);

                    break;
                case DataTypeIDEnum.Numeric:
                    //Use Numeric Textbox
                    RadNumericTextBox numericTB = new RadNumericTextBox();

                    numericTB.ID = "Ctr_" + AttributeID.ToString();
                    numericTB.NumberFormat.DecimalDigits = 0;
                    numericTB.ShowSpinButtons = true;
                    numericTB.NumberFormat.GroupSeparator = "";
                    numericTB.Width = new Unit("300px");

                    ctrls.Add(numericTB);

                    if (Mandatory.Value)
                    {
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }

                    break;
                case DataTypeIDEnum.Date:
                    //Use Date Picker
                    RadDatePicker dp = new RadDatePicker();

                    dp.ID = "Ctr_" + AttributeID.ToString();

                    ctrls.Add(dp);

                    if (Mandatory.Value)
                    {
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }

                    break;
                case DataTypeIDEnum.DropdownList:
                    //Use dropdown list
                    RadComboBox ddl = new RadComboBox();
                    RadComboBoxItemCollection ddlItems = GetDropdownPicklistOption(AttributeID, ddl);

                    ddl.ID = "Ctr_" + AttributeID.ToString();
                    ddl.DropDownAutoWidth = RadComboBoxDropDownAutoWidth.Enabled;
                    ddl.MaxHeight = new Unit("250px");
                    ddl.MarkFirstMatch = true;

                    foreach (RadComboBoxItem item in ddlItems)
                    {
                        ddl.Items.Add(item);
                    }

                    ctrls.Add(ddl);

                    if (Mandatory.Value)
                    {
                        //ctrls.Add(new LiteralControl("<br />"));
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }
                    break;
                case DataTypeIDEnum.Listbox:
                    RadListBox lst = new RadListBox();
                    RadListBoxItemCollection lstItems = GetListboxPickListOption(AttributeID, lst);
                    lst.ID = "Ctr_" + AttributeID.ToString();
                    lst.SelectionMode = ListBoxSelectionMode.Multiple;
                    lst.CheckBoxes = true;
                    lst.Width = new Unit("300px");
                    lst.OnClientSelectedIndexChanged = "OnClientSelectedIndexChanged";

                    foreach (RadListBoxItem item in lstItems)
                    {
                        lst.Items.Add(item);
                    }

                    ctrls.Add(lst);

                    if (Mandatory.Value)
                    {
                        ctrls.Add(CreateRequiredFieldValidator(AttributeID, "Please enter value for " + AttributeName, string.Empty));
                    }

                    break;
                case DataTypeIDEnum.Disposition:
                    //Use Disposition Control
                    DispositionUserControl dispoCtrl = LoadControl("~/UserControl/DispositionUserControl.ascx") as DispositionUserControl;

                    dispoCtrl.ID = "Ctr_" + AttributeID.ToString();
                    dispoCtrl.AttributeID = AttributeID;

                    ctrls.Add(dispoCtrl);

                    break;
                case DataTypeIDEnum.DropdownTreeView:
                    //Use Disposition Control
                    DropdownTreeViewUserControl dropdownTree = LoadControl("~/UserControl/DropdownTreeViewUserControl.ascx") as DropdownTreeViewUserControl;

                    dropdownTree.ID = "Ctr_" + AttributeID.ToString();
                    dropdownTree.AttributeID = AttributeID;
                    dropdownTree.AttributeName = AttributeName;

                    ctrls.Add(dropdownTree);

                    break;
                case DataTypeIDEnum.Contact:
                    //Use Contact User Control
                    ContactUserControl contact = LoadControl("~/UserControl/ContactUserControl.ascx") as ContactUserControl;

                    contact.ID = "Ctr_" + AttributeID.ToString();
                    contact.AttributeName = AttributeName;
                    contact.AttributeID = AttributeID;

                    ctrls.Add(contact);
                    break;
                case DataTypeIDEnum.TimerAuto:
                    //User Auto Timer control
                    TimerAutoUserControl timerAuto = LoadControl("~/UserControl/TimerAutoUserControl.ascx") as TimerAutoUserControl;

                    timerAuto.ID = "Ctr_" + AttributeID.ToString();
                    timerAuto.AttributeName = AttributeName;
                    timerAuto.AttributeID = AttributeID;

                    ctrls.Add(timerAuto);
                    break;
                case DataTypeIDEnum.TimerManual:
                    //User Auto Timer control
                    TimerAutoUserControl timerManual = LoadControl("~/UserControl/TimerManualUserControl.ascx") as TimerAutoUserControl;

                    timerManual.ID = "Ctr_" + AttributeID.ToString();
                    timerManual.AttributeName = AttributeName;
                    timerManual.AttributeID = AttributeID;

                    ctrls.Add(timerManual);
                    break;
                case DataTypeIDEnum.CustomerSatisfactionRadioButton:
                    //Use Contact User Control
                    CustomerSatisfactionUserControl csUC = LoadControl("~/UserControl/CustomerSatisfactionUserControl.ascx") as CustomerSatisfactionUserControl;

                    csUC.ID = "Ctr_" + AttributeID.ToString();
                    csUC.AttributeName = AttributeName;
                    csUC.AttributeID = AttributeID;

                    ctrls.Add(csUC);
                    break;
                default:
                    break;
            }

            return ctrls;
        }

        RequiredFieldValidator CreateRequiredFieldValidator(int AttributeID, string ErrorMessage, string InitialValue)
        {
            RequiredFieldValidator rfv = new RequiredFieldValidator();
            rfv.ID = "ReqVal_Ctr_" + AttributeID.ToString();
            rfv.ControlToValidate = "Ctr_" + AttributeID.ToString();
            rfv.Display = ValidatorDisplay.Static;
            rfv.Text = "*";
            rfv.ErrorMessage = ErrorMessage;
            rfv.InitialValue = InitialValue;
            rfv.SetFocusOnError = true;

            return rfv;
        }

        RadComboBoxItemCollection GetDropdownPicklistOption(int AttributeID, RadComboBox ddl)
        {
            RadComboBoxItemCollection ddlItems = new RadComboBoxItemCollection(ddl);
            dynamic items;

            CacheManager _objCacheManager = CacheFactory.GetCacheManager();

            if (_objCacheManager.Contains("GetDropdownPicklistOption~" + AttributeID.ToString()))
            {
                items = _objCacheManager.GetData("GetDropdownPicklistOption~" + AttributeID.ToString());
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    items = (from i in db.PickListOptions
                             where i.AttributeID == AttributeID && (i.HideFromList == false || i.HideFromList == null)
                             orderby i.SortOrder, i.DisplayText ascending
                             select new { i.DisplayText, i.OptionValue }).ToList();

                    _objCacheManager.Add("GetDropdownPicklistOption~" + AttributeID.ToString(), items);
                }
            }

            ddlItems.Add(new RadComboBoxItem("", ""));

            foreach (var item in items)
            {
                ddlItems.Add(new RadComboBoxItem(item.DisplayText, item.OptionValue));
            }

            return ddlItems;
        }

        RadListBoxItemCollection GetListboxPickListOption(int AttributeID, RadListBox lst)
        {
            RadListBoxItemCollection lstItems = new RadListBoxItemCollection(lst);
            CacheManager _objCacheManager = CacheFactory.GetCacheManager();
            dynamic items;

            if (_objCacheManager.Contains("GetDropdownPicklistOption~" + AttributeID.ToString()))
            {
                items = _objCacheManager.GetData("GetListboxPickListOption~" + AttributeID.ToString());
            }
            else
            {
                using (CallDispositionEntities db = new CallDispositionEntities())
                {
                    items = (from i in db.PickListOptions
                             where i.AttributeID == AttributeID && (i.HideFromList == false || i.HideFromList == null)
                             orderby i.SortOrder, i.DisplayText ascending
                             select new { i.DisplayText, i.OptionValue }).ToList();

                    _objCacheManager.Add("GetListboxPickListOption~" + AttributeID.ToString(), items);
                }
            }

            foreach (var item in items)
            {
                lstItems.Add(new RadListBoxItem(item.DisplayText, item.OptionValue));
            }

            return lstItems;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            ResetControls();
        }

        bool IsRecordContainsOnHold()
        {
            bool returnValue = false;

            foreach (var item in _Attributes)
            {
                DataTypeIDEnum dt = (DataTypeIDEnum)item.DataTypeID;

                Control ctrl = formPlaceHolder.FindControl("Ctr_" + item.AttributeID);

                switch (dt)
                {
                    case DataTypeIDEnum.Disposition:
                        DispositionUserControl d = ctrl as DispositionUserControl;
                        returnValue = d.DispositionValue.ToUpper().Contains("\\ON HOLD");
                        break;
                    case DataTypeIDEnum.DropdownTreeView:
                        DropdownTreeViewUserControl tree = ctrl as DropdownTreeViewUserControl;
                        returnValue = tree.DispositionValue.ToUpper().Contains("\\ON HOLD");
                        break;
                    default:
                        break;
                }
            }

            return returnValue;
        }

        void SaveRecord()
        {
            pnlMain.Enabled = false;
            btnSubmit.Enabled = false;

            using (CallDispositionEntities db = new CallDispositionEntities())
            {
                Transaction tran = new Transaction();
                CallDispositionTool.Model.CallBack cb = new CallDispositionTool.Model.CallBack();
                CallBackHistoryLog log = new CallBackHistoryLog();
                TransactionCallData callData = new TransactionCallData();

                tran.FormID = Convert.ToInt32(Session["FormID"]);
                tran.CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                tran.CreatedOn = DateTime.Now;

                foreach (CallDispositionTool.Model.Attribute item in _Attributes)
                {
                    tran.AttributeValues.Add(GetValueForAttribute(item.AttributeID, item.DataTypeID));
                }

                if (pnlContact.Visible)
                {
                    Model.TransactionContact tc = new TransactionContact();

                    tc.ContactID = ContactUserControl1.ContactID;
                    tc.CreateDate = DateTime.Now;
                    tran.TransactionContact = tc;
                }

                if (chkCallBackRequired.SelectedItem != null)
                {
                    if (chkCallBackRequired.SelectedItem.Text == "Yes")
                    {
                        cb.CallBackDate = CallBackUserControl1.CallBackDateTime.Value;
                        cb.TimezoneID = CallBackUserControl1.TimeZoneID;
                        cb.ContactNumber = CallBackUserControl1.ContactNumber;
                        cb.CallBackReasonID = CallBackUserControl1.CallBackReasonID;

                        if (CallBackUserControl1.AssignedTo > 0)
                        {
                            cb.AssignedTo = CallBackUserControl1.AssignedTo;
                            cb.AssignedReasonID = CallBackUserControl1.AssignedReasonID;
                        }

                        cb.Notes = CallBackUserControl1.Notes;
                        cb.CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                        cb.CreatedOn = DateTime.Now;
                        cb.CallBackStatusID = 1;

                        log.Action = "Create";
                        log.ActionLog = "Call Back Created";
                        log.CreatedBy = Convert.ToInt32(Membership.GetUser().ProviderUserKey);
                        log.CreatedOn = DateTime.Now;

                        cb.CallBackHistoryLogs.Add(log);
                        tran.CallBacks.Add(cb);
                    }
                }

                //Validate and save call data info
                if (Request["CallerID"] != null && Request["DNIS"] != null && Request["CallID"] != null && Request["Queue"] != null && Request["DN"] != null)
                {
                    try
                    {
                        callData.CallerID = Request["CallerID"];
                        callData.DNIS = Request["DNIS"];
                        callData.CallID = Request["CallID"];
                        callData.Queue = Request["Queue"];
                        callData.DN = Request["DN"];
                        tran.TransactionCallData = callData;
                    }
                    catch (Exception ex)
                    {
                        Helper.LogError(ex);
                        RadWindowManager1.RadAlert(
                            ex.Message,
                            330,
                            180,
                            "Error",
                            "");
                    }
                }

                db.Transactions.AddObject(tran);

                try
                {
                    //commit database transaction
                    db.SaveChanges();
                    RadWindowManager1.RadAlert(
                        "Transaction ID # <strong>" + tran.TransactionID.ToString() + " </strong> has been saved.",
                        330,
                        180,
                        "Save Success",
                        "PageReset()");
                }
                catch (Exception ex)
                {
                    string errorMsg = ex.Message;
                    if (ex.InnerException.Message != null)
                    {
                        errorMsg += Environment.NewLine + "Inner exception: " + ex.InnerException.Message;
                    }

                    RadWindowManager1.RadAlert(
                        errorMsg,
                        330,
                        180,
                        "Error",
                        "");

                    Helper.LogError(ex);
                }

                if (cb.AssignedTo.HasValue)
                {
                    try
                    {
                        //Send real time notification to assigned user
                        var context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
                        string msg = String.Format("You have pending call back.<br><strong>Contact Number:</strong>{3}. <br><strong>Call Back Date/Time:</strong> {1}<br>Click <a href='{2}/CallBack.aspx?CallBack={0}&Assigned=1'>here</a> to view details", cb.CallBackID, cb.CallBackDate.ToString(), WebConfigurationManager.AppSettings["ApplicationURL"], cb.ContactNumber);
                        context.Clients.All.showAssignedNotification(Helper.GetEmployeeIDByUserID(cb.AssignedTo.Value), msg);
                    }
                    catch (Exception ex)
                    {
                        RadWindowManager1.RadAlert(
                            ex.Message,
                            330,
                            180,
                            "Error",
                            "");

                        Helper.LogError(ex);
                    }
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (IsRecordContainsOnHold())
            {
                int formID = Convert.ToInt32(Session["FormID"]);
                Session["SignOffFormID"] = formID;

                RadAjaxManager1.ResponseScripts.Add(String.Format("showSignOffForm({0})", formID));

            }
            else
            {
                SaveRecord();
            }
        }

        AttributeValue GetValueForAttribute(int AttributeID, int DataTypeID)
        {
            AttributeValue value = new AttributeValue();
            Control ctrl = formPlaceHolder.FindControl("Ctr_" + AttributeID);
            DataTypeIDEnum myEnum = (DataTypeIDEnum)DataTypeID;

            value.AttributeID = AttributeID;

            switch (myEnum)
            {
                case DataTypeIDEnum.String:
                    RadTextBox tb = ctrl as RadTextBox;
                    value.Value = tb.Text.Trim();
                    break;
                case DataTypeIDEnum.Boolean:
                    CheckBox cb = ctrl as CheckBox;
                    value.Value = cb.Checked.ToString();
                    break;
                case DataTypeIDEnum.Numeric:
                    RadNumericTextBox num = ctrl as RadNumericTextBox;
                    value.Value = num.Text;
                    break;
                case DataTypeIDEnum.Date:
                    RadDatePicker dtp = ctrl as RadDatePicker;
                    value.Value = dtp.SelectedDate.Value.ToString();
                    break;
                case DataTypeIDEnum.DropdownList:
                    RadComboBox cbo = ctrl as RadComboBox;
                    value.Value = cbo.SelectedItem.Text;
                    break;
                case DataTypeIDEnum.Listbox:
                    RadListBox lst = ctrl as RadListBox;

                    foreach (RadListBoxItem item in lst.SelectedItems)
                    {
                        value.Value += item.Text + "|";
                    }

                    if (value.Value.Length > 0)
                        value.Value.Remove(value.Value.Length - 1, 1);

                    break;
                case DataTypeIDEnum.Disposition:
                    DispositionUserControl disp = ctrl as DispositionUserControl;
                    value.Value = disp.DispositionValue;
                    break;
                case DataTypeIDEnum.DropdownTreeView:
                    DropdownTreeViewUserControl tree = ctrl as DropdownTreeViewUserControl;
                    value.Value = tree.DispositionValue;
                    break;
                case DataTypeIDEnum.Contact:
                    ContactUserControl contactUC = ctrl as ContactUserControl;
                    value.Value = contactUC.ContactInfo;
                    break;
                case DataTypeIDEnum.CustomerSatisfactionRadioButton:
                    CustomerSatisfactionUserControl csUC = ctrl as CustomerSatisfactionUserControl;
                    value.Value = csUC.CustomerSatisfaction;
                    break;
                default:
                    break;
            }

            return value;
        }

        protected void chkCallBackRequired_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chkCallBackRequired.SelectedIndex == 0)
            {
                CallBackUserControl1.Visible = true;
            }
            else if (chkCallBackRequired.SelectedIndex == 1)
            {
                CallBackUserControl1.Visible = false;
            }
        }

        protected void TabStrip1_TabClick(object sender, RadTabStripEventArgs e)
        {
            if (TabStrip1.SelectedIndex == 1)
            {
                grdTransaction.Rebind();
            }
        }

        protected void grdTransaction_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                ImageButton editLink = (ImageButton)e.Item.FindControl("EditLink");
                editLink.Attributes["href"] = "javascript:void(0);";
                editLink.Attributes["target"] = "_blank";
                editLink.OnClientClick = String.Format("return ShowEditForm('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["TransactionID"], e.Item.ItemIndex);
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdTransaction.MasterTableView.SortExpressions.Clear();
                grdTransaction.MasterTableView.GroupByExpressions.Clear();
                grdTransaction.MasterTableView.CurrentPageIndex = grdTransaction.MasterTableView.PageCount - 1;
                grdTransaction.Rebind();
            }
            else if (e.Argument == "SaveAndSignOffRecord")
            {
                SaveRecord();
            }
        }
    }
}